<script src="<?=base_url('assets/js/jquery-min.js');?>"></script>

<link rel="stylesheet" href="<?=base_url('assets/css/datatable/dataTables.bootstrap4.min.css');?>" type="text/css"/>
<script src="<?=base_url('assets/js/datatable/jquery.dataTables.min.js');?>"></script>
<script src="<?=base_url('assets/js/datatable/dataTables.bootstrap4.min.js');?>"></script>
<script src="<?=base_url('assets/js/datatable/dataTables.select.min.js');?>"></script>

<!-- <script src="<?=base_url('asset/js/datatable/jquery.dataTables.min.js');?>"></script> -->
<script src="<?=base_url('assets/js/datatable/dataTables.bootstrap4.min.js');?>"></script>
<script src="<?=base_url('assets/js/datatable/dataTables.select.min.js');?>"></script>  
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>  -->

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> 




<div class="">
    <table border="1" class="table table-hover" id="table_iu">
    	<thead>
    		<tr>
			  
              <th>Id Dokumen</th>
              <th width="500px">Nama Dokumen</th>
              <th>Nomor Dokumen</th>
			  <th>Jenis Dokumen</th>
			  <th>File Upload</th>
			  <th>Option</th>
    		</tr>
    	</thead>
    </table>
</div>





<script type="text/javascript">


	$(function(){
        // alert('1');
        $('#table_iu').DataTable({ 
			"pageLength" : 10,
            "processing": true, 
            "serverSide": true, 
            "order": [[0, "asc" ]], 
             
            "ajax": {
                "url": "<?php echo site_url('');?>/Home/ajaxTable/list_sop",
                "type": "POST"
            },
            "columns": [
                { "data": "ID_PERATURAN"},
                { "data": "NAMA_FILE"},
				{ "data": "NOMOR_FILE"},
                { "data": "JENIS_DOKUMEN"},
			    { "data": "view"},
                { "data": "option"},
               
            ],
 
             
            "columnDefs": [
            { 
                "targets": [ 0], 
                "orderable": false, 
            },
            ],
 
        });
           
	});
</script>