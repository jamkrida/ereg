<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Aplikasi e-Registrasi Pembayaran :: Jamkrida-Jakarta</title>
  <link rel="icon" type="image/png" href="<?= base_url();?>assets/img/favicon.ico"/>
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?= base_url();?>plugins/fontawesome-free/css/all.min.css?v=<?=date('YmdHis')?>">
  <link rel="stylesheet" href="<?= base_url();?>assets/css/newtable.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= base_url();?>plugins/daterangepicker/daterangepicker.css">
  
    <!-- Toastr -->
	<link rel="stylesheet" href="<?= base_url();?>plugins/toastr/toastr.min.css">

  <link rel="stylesheet" href="<?= base_url();?>plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <link rel="stylesheet" href="<?= base_url();?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url();?>dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	
	<!--Summernote CSS-->
	 <link rel="stylesheet" href="<?= base_url();?>plugins/fontawesome-free/css/all.min.css">
     <!-- Ionicons -->
     <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	  <!-- Theme style -->
	 <link rel="stylesheet" href="<?= base_url();?>dist/css/adminlte.min.css">
	  <!-- summernote -->
	 <link rel="stylesheet" href="<?= base_url();?>plugins/summernote/summernote-bs4.css">
	  <!-- Google Font: Source Sans Pro -->
	 <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	 <link rel="stylesheet" href="<?= base_url();?>plugins/summernote/summernote-bs4.css">
	 <!--Summernote CSS-->
	 
	 
	 <link rel="stylesheet" href="<?= base_url();?>plugins/daterangepicker/daterangepicker.css">
	  <!-- iCheck for checkboxes and radio inputs -->
	  <link rel="stylesheet" href="<?= base_url();?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	  <!-- Bootstrap Color Picker -->
	  <link rel="stylesheet" href="<?= base_url();?>plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
	  <!-- Tempusdominus Bbootstrap 4 -->
	  <link rel="stylesheet" href="<?= base_url();?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
	  <!-- Select2 -->
	  <link rel="stylesheet" href="<?= base_url();?>plugins/select2/css/select2.min.css">
	  <link rel="stylesheet" href="<?= base_url();?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
	  <!-- Bootstrap4 Duallistbox -->
	  <link rel="stylesheet" href="<?= base_url();?>plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
	  <!-- Theme style -->
	  <link rel="stylesheet" href="<?= base_url();?>dist/css/adminlte.min.css">
	  <!-- Google Font: Source Sans Pro -->
	  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  
  
	<script src="<?= base_url();?>plugins/jquery/jquery.min.js?v=<?=date('YmdHis');?>"></script>
	<script src="<?= base_url();?>assets/js/newtable.js?v=<?=date('YmdHis');?>"></script>
	<script src="<?= base_url();?>assets/js/newtablehash.js?v=<?=date('YmdHis');?>"></script>
	<!-- Bootstrap -->
	<script src="<?= base_url();?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- overlayScrollbars -->
	<script src="<?= base_url();?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
	<!-- AdminLTE App -->
	<script src="<?= base_url();?>dist/js/adminlte.js"></script>
	<!-- OPTIONAL SCRIPTS -->
	<script src="<?= base_url();?>dist/js/demo.js"></script>

	<!-- Toastr -->
	<script src="<?= base_url();?>plugins/toastr/toastr.min.js"></script>	

	<!-- PAGE PLUGINS -->
	<!-- jQuery Mapael -->
	<script src="<?= base_url();?>plugins/daterangepicker/daterangepicker.js"></script>
	
	<script src="<?= base_url();?>plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
	<script src="<?= base_url();?>plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
	<script src="<?= base_url();?>plugins/raphael/raphael.min.js"></script>
	<script src="<?= base_url();?>plugins/jquery-mapael/jquery.mapael.min.js"></script>
	<script src="<?= base_url();?>plugins/jquery-mapael/maps/usa_states.min.js"></script>
	<!-- ChartJS -->
	<script src="<?= base_url();?>plugins/chart.js/Chart.min.js"></script>
	<!-- PAGE SCRIPTS -->
	<script src="<?= base_url();?>dist/js/pages/dashboard2.js"></script>
	<script src="<?= base_url();?>assets/js/home.js?v=<?=date('YmdHis');?>"></script>
	<script type="text/javascript">
	 var site = "<?php echo site_url(); ?>";
	 var isUrl = "<?php echo base_url(); ?>";
	</script>
	
	<!-- datepicker-->
	<script src="<?= base_url();?>plugins/summernote/summernote-bs4.min.js"></script>
	<!--<script src="<?= base_url();?>plugins/jquery/jquery.min.js"></script>-->
	<!-- Bootstrap 4 -->
	<script src="<?= base_url();?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- Select2 -->
	<script src="<?= base_url();?>plugins/select2/js/select2.full.min.js"></script>
	<!-- Bootstrap4 Duallistbox -->
	<script src="<?= base_url();?>plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
	<!-- InputMask -->
	<script src="<?= base_url();?>plugins/moment/moment.min.js"></script>
	<script src="<?= base_url();?>plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
	<!-- date-range-picker -->
	<script src="<?= base_url();?>plugins/daterangepicker/daterangepicker.js"></script>
	<!-- bootstrap color picker -->
	<script src="<?= base_url();?>plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
	<!-- Tempusdominus Bootstrap 4 -->
	<script src="<?= base_url();?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
	<!-- Bootstrap Switch -->
	<script src="<?= base_url();?>plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
	
	<!-- AdminLTE App -->
	<!--<script src="<?= base_url();?>dist/js/adminlte.min.js"></script>-->
	
	<!-- AdminLTE for demo purposes -->
	<!--<script src="<?= base_url();?>dist/js/demo.js"></script>-->
	<!-- datepicker-->
	<script src="<?= base_url();?>assets/js/numeric/autoNumeric.js?v=<?=date('YmdHis');?>"></script>
	
	<!-- Summernote -->
	<script src="<?= base_url();?>plugins/summernote/summernote-bs4.min.js"></script>
	<!--Summernote JS-->
	<script src="<?= base_url();?>assets/js/jquery.autocomplete.js?v=<?=date('YmdHis');?>"></script>
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?= base_url();?>" class="nav-link"><i class="fas fa-home"></i> Home</a>
      </li>
      
    </ul>
	 <ul class="navbar-nav ml-auto">
      
      <li class="nav-item d-none d-sm-inline-block">
        
		<a href="<?= site_url();?>/home/logout" class="nav-link"> <i class="fas fa-sign-out-alt"></i> Logout</a>
      </li>
    </ul>


    <!-- Right navbar links -->
  
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="<?= base_url();?>assets/img/white-logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8; ">
      <span class="brand-text font-weight-light">Aplikasi e-Registrasi</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
     

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
		  
          <li class="nav-item">
            <a href="<?= base_url();?>" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                
              </p>
            </a>
          </li>
		  <li class="nav-header">PEMBAYARAN</li>
		  <li class="nav-item" >
			<a href="<?= site_url();?>/home/dokumen/new" class="nav-link <?=(in_array($_SESSION['menu'], array('newact')) ? 'active' : '')?>" >
			  <i class="nav-icon fas fa-edit"></i>
			  <p>Form Pembayaran</p>
			</a>
		  </li>
		  <li class="nav-item" >
			<a href="<?= site_url();?>/home/dokumen/new_upload" class="nav-link <?=(in_array($_SESSION['menu'], array('newact_upload')) ? 'active' : '')?>" >
			  <i class="nav-icon fas fa-edit"></i>
			  <p>Upload Pembayaran Klaim</p>
			</a>
		  </li>
          <li class="nav-item has-treeview <?=(in_array($_SESSION['menu'], array('pembayaran','kasbon','NewPayment','pelunasan_kasbon','canceled','uploaded')) ? 'menu-open' : '')?>">
            <a href="#" class="nav-link <?=(in_array($_SESSION['menu'], array('pembayaran','kasbon','NewPayment','pelunasan_kasbon','canceled','uploaded')) ? 'active' : '')?>">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                List Dokumen
                <i class="fas fa-angle-left right"></i>
               
              </p>
            </a>
            <ul class="nav nav-treeview">
			  <li class="nav-item">
                <a href="<?= site_url();?>/home/dokumen/listdata/newtable/NewPayment" class="nav-link <?=(in_array($_SESSION['menu'], array('NewPayment')) ? 'active' : '')?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pembayaran > 10jt</p>
				   <span class="badge badge-info right"><?= $counter['PEMBAYARANNEW']; ?></span>
                </a>
              </li>	
              <li class="nav-item">
                <a href="<?= site_url();?>/home/dokumen/listdata/newtable/pembayaran" class="nav-link <?=(in_array($_SESSION['menu'], array('pembayaran')) ? 'active' : '')?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pembayaran</p>
				   <span class="badge badge-info right"><?= $counter['PEMBAYARAN']; ?></span>
                </a>
              </li>
			   <li class="nav-item">
                <a href="<?= site_url();?>/home/dokumen/listdata/newtable/kasbon" class="nav-link <?=(in_array($_SESSION['menu'], array('kasbon')) ? 'active' : '')?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Kasbon</p>
				   <span class="badge badge-info right"><?= $counter['KASBON'];?></span>
                </a>
              </li>
			   <li class="nav-item">
                <a href="<?= site_url();?>/home/dokumen/listdata/newtable/pelunasan_kasbon" class="nav-link <?=(in_array($_SESSION['menu'], array('pelunasan_kasbon')) ? 'active' : '')?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pelunasan Kasbon</p>
				   <span class="badge badge-info right"><?= $counter['PELUNASAN_KASBON'];?></span>
                </a>
              </li>
               <li class="nav-item" >
				<a href="<?= site_url();?>/home/dokumen/listdata/newtable/canceled" class="nav-link <?=(in_array($_SESSION['menu'], array('canceled')) ? 'active' : '')?>">
				 <i class="far fa-circle nav-icon"></i>
				  <p>canceled</p>
				   <span class="badge badge-info right"><?= $counter['CANCELED'];?></span>
				</a>
			  </li>
			   <li class="nav-item" >
				<a href="<?= site_url();?>/home/dokumen/listdata/newtable/uploaded" class="nav-link <?=(in_array($_SESSION['menu'], array('uploaded')) ? 'active' : '')?>">
				 <i class="far fa-circle nav-icon"></i>
				  <p>Uploaded</p>
				   <span class="badge badge-info right"><?= $counter['UPLOADED'];?></span>
				</a>
			   </li>
            </ul>
          </li>
		  
		  
		  <?php 
			if($this->session->userdata('USER_DEPARTEMEN')=="4" || $this->session->userdata('USER_ROLE')=="01"){
		  ?>
		   <li class="nav-item" >
			<a href="<?= site_url();?>/home/dokumen/report" class="nav-link <?=(in_array($_SESSION['menu'], array('reportAct')) ? 'active' : '')?>" >
			  <i class="nav-icon fas fa-file-alt"></i>
			  <p>Laporan</p>
			</a>
		  </li>
		  
		  <?php 
			}
			
			if($this->session->userdata('USER_DEPARTEMEN')=="4" || $this->session->userdata('USER_DEPARTEMEN')=="11" || $this->session->userdata('USER_ROLE')=="01"){
		  ?>
		
		 <?php 
		 	if($this->session->userdata('USER_JABATAN')=="3.2.2" || $this->session->userdata('USER_JABATAN')=="2.3.1" || $this->session->userdata('USER_ROLE')=="01"){
		 ?>
		 <!--<li class="nav-item" >
			<a href="<?= site_url();?>/home/dokumen/regCMS" class="nav-link <?=(in_array($_SESSION['menu'], array('regCMS')) ? 'active' : '')?>" >
			  <i class="nav-icon fas fa-edit"></i>
			  <p>Form Registrasi CMS</p>
			</a>
		  </li>
		 <li class="nav-item" >
			<a href="<?= site_url();?>/home/dokumen/inputCMS" class="nav-link <?=(in_array($_SESSION['menu'], array('inputCMS')) ? 'active' : '')?>" >
			  <i class="nav-icon fas fa-edit"></i>
			  <p>Upload excel CMS</p>
			</a>
		  </li>-->

		  <?php 
			 } 
		  ?>
		  <!--<li class="nav-item" >
			<a href="<?= site_url();?>/home/dokumen/CashFLow" class="nav-link <?=(in_array($_SESSION['menu'], array('CashFLow')) ? 'active' : '')?>" >
			 <i class="nav-icon fas fa-money-bill"></i>
			  <p>Cash Flow CMS</p>
			</a>
		  </li>
		  <li class="nav-item has-treeview <?=(in_array($_SESSION['menu'], array('CMSnewSyr','CMS_fix','CMS_fix_approve')) ? 'menu-open' : '')?>">
            <a href="#" class="nav-link <?=(in_array($_SESSION['menu'], array('CMSnewSyr','CMS_fix','CMS_fix_approve')) ? 'active' : '')?>">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                List CMS Syariah
                <i class="fas fa-angle-left right"></i>
               
              </p>
            </a>
            <ul class="nav nav-treeview">
			
				 <li class="nav-item">
					<a href="<?= site_url();?>/home/dokumen/listCMS/CMSnewSyr" class="nav-link <?=(in_array($_SESSION['menu'], array('CMSnewSyr')) ? 'active' : '')?>">
					 <i class="far fa-circle nav-icon"></i>
					  <p>List CMS</p>
					  <span class="badge badge-info right"><?= $counter['NEWCMSSYR'];?></span>
					</a>
				</li>
				
				<li class="nav-item">
					<a href="<?= site_url();?>/home/dokumen/listCMS/CMS_fix" class="nav-link <?=(in_array($_SESSION['menu'], array('CMS_fix')) ? 'active' : '')?>">
					 <i class="far fa-circle nav-icon"></i>
					  <p>List CMS Fix</p>
					  <span class="badge badge-info right"><?= $counter['FIXCMSSYR'];?></span>
					</a>
				</li>
				
				<li class="nav-item">
					<a href="<?= site_url();?>/home/dokumen/listCMS/CMS_fix_approve" class="nav-link <?=(in_array($_SESSION['menu'], array('CMS_fix_approve')) ? 'active' : '')?>">
					 <i class="far fa-circle nav-icon"></i>
					  <p>List CMS Terbit</p>
					  <span class="badge badge-info right"><?= $counter['FIXCMSSYRTERBIT'];?></span>
					</a>
				</li>	

			</ul>
			</li>
			
			<li class="nav-item has-treeview <?=(in_array($_SESSION['menu'], array('NominatifNEW')) ? 'menu-open' : '')?>">
            <a href="#" class="nav-link <?=(in_array($_SESSION['menu'], array('NominatifNEW')) ? 'active' : '')?>">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                List Nominatif
                <i class="fas fa-angle-left right"></i>
               
              </p>
            </a>
            
			<ul class="nav nav-treeview">
			
				 <li class="nav-item">
					<a href="<?= site_url();?>/home/dokumen/listNominatif/NominatifNEW" class="nav-link <?=(in_array($_SESSION['menu'], array('NominatifNEW')) ? 'active' : '')?>">
					 <i class="far fa-circle nav-icon"></i>
					  <p>List Nominatif</p>
					  <span class="badge badge-info right"><?= "123";?></span>
					</a>
				</li>
				


			</ul>
			</li>-->
		<?php 
		 	
			}
		?>
		 <li class="nav-header">REGISTRASI SURAT</li>	
		<?php 
			if( $this->session->userdata('USER_ROLE')=="01" || $this->session->userdata('USER_JABATAN')=="1.2.1"){
		?>
    	<li class="nav-item" >
			<a href="<?= site_url();?>/home/dokumen/regSurat" class="nav-link <?=(in_array($_SESSION['menu'], array('regSurat')) ? 'active' : '')?>" >
			  <i class="nav-icon fas fa-edit"></i>
			  <p>Form Registrasi</p>
			</a>
		  </li>	
		<?php 
			}
		?>
		  <li class="nav-item has-treeview <?=(in_array($_SESSION['menu'], array('listS2M','listPembayaran','listOverTime')) ? 'menu-open' : '')?>">
            <a href="#" class="nav-link <?=(in_array($_SESSION['menu'], array('listS2M','listPembayaran','listOverTime')) ? 'active' : '')?>">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                List Registrasi Surat
                <i class="fas fa-angle-left right"></i>
               
              </p>
            </a>
            <ul class="nav nav-treeview">
			
				<li class="nav-item">
					<a href="<?= site_url();?>/home/dokumen/listRegSurat/listS2M" class="nav-link <?=(in_array($_SESSION['menu'], array('listS2M')) ? 'active' : '')?>">
					 <i class="far fa-circle nav-icon"></i>
					  <p>Surat Masuk/Keluar</p>
					  <span class="badge badge-info right"><?= '1';?></span>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?= site_url();?>/home/dokumen/listRegSurat/listPembayaran" class="nav-link <?=(in_array($_SESSION['menu'], array('listPembayaran')) ? 'active' : '')?>">
					 <i class="far fa-circle nav-icon"></i>
					  <p>Pembayaran</p>
					  <span class="badge badge-info right"><?= '1';?></span>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?= site_url();?>/home/dokumen/listRegSurat/listOverTime" class="nav-link <?=(in_array($_SESSION['menu'], array('listOverTime')) ? 'active' : '')?>">
					 <i class="far fa-circle nav-icon"></i>
					  <p>SPKL & Laporan lembur</p>
					  <span class="badge badge-info right"><?= '1';?></span>
					</a>
				</li>
			</ul>
		</li>		
		<li class="nav-header">REGISTRASI KENDARAAN V.1.1</li>	
		<li class="nav-item" >
			<a href="<?= site_url();?>/home/dokumen/kendaraan" class="nav-link <?=(in_array($_SESSION['menu'], array('newreg')) ? 'active' : '')?>" >
			  <i class="nav-icon fas fa-edit"></i>
			  <p>Form Registrasi Kendaraan</p>
			</a>
		  </li>
		  <?php 
			if($this->session->userdata('USER_ROLE')=="01" ||  $this->session->userdata('USER_ID')=="37" || $this->session->userdata('USER_ID')=="29"  || $this->session->userdata('USER_ID')=="33" || $this->session->userdata('USER_ID')=="13"){
		  ?>
		   <li class="nav-item">
				<a href="<?= site_url();?>/home/user/list_driver" class="nav-link <?=(in_array($_SESSION['menu'], array('driver')) ? 'active' : '')?>">
				  <i class="fas fa-users nav-icon"></i>
				  <p>List Driver</p>
				</a>
		    </li>
		  <?php 
			}
		  ?>
		 <li class="nav-item has-treeview <?=(in_array($_SESSION['menu'], array('new','approve','denied','prosesReg','accreg')) ? 'menu-open' : '')?>">
            <a href="#" class="nav-link <?=(in_array($_SESSION['menu'], array('new','approve','denied','prosesReg','accreg')) ? 'active' : '')?>">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                List Registrasi
                <i class="fas fa-angle-left right"></i>
               
              </p>
            </a>
            <ul class="nav nav-treeview">
			<?php
			if($this->session->userdata('USER_ROLE')=="03"){
			?>
				<li class="nav-item">
					<a href="<?= site_url();?>/home/dokumen/listregis/accreg" class="nav-link <?=(in_array($_SESSION['menu'], array('accreg')) ? 'active' : '')?>">
					  <i class="fas fa-car-side nav-icon"></i>
					  <p>Persetujuan</p>
					  <span class="badge badge-info right"><?= $counter['ALLACC'];?></span>
					</a>
				</li>
				
				<li class="nav-item">
					<a href="<?= site_url();?>/home/dokumen/listregis/new" class="nav-link <?=(in_array($_SESSION['menu'], array('new')) ? 'active' : '')?>">
					  <i class="fas fa-car-side nav-icon"></i>
					  <p>Proses</p>
					  <span class="badge badge-info right"><?= $counter['ALLPROSES'];?></span>
					</a>
				</li>
					
				<li class="nav-item">
					<a href="<?= site_url();?>/home/dokumen/listregis/approve" class="nav-link <?=(in_array($_SESSION['menu'], array('approve')) ? 'active' : '')?>">
					  <i class="fas fa-car-side nav-icon"></i>
					  <p>Pengajuan Selesai</p>
					  <span class="badge badge-info right"><?= $counter['ALLAPPROVE'];?></span>
					</a>
				</li>
			
			<?php 
			}else{
			?>
				<li class="nav-item">
					<a href="<?= site_url();?>/home/dokumen/listregis/new" class="nav-link <?=(in_array($_SESSION['menu'], array('new')) ? 'active' : '')?>">
					  <i class="fas fa-car-side nav-icon"></i>
					  <p>Pengajuan Baru</p>
					  <span class="badge badge-info right"><?= $counter['ALLPROSES'];?></span>
					</a>
				</li>
				<li class="nav-item">
					<a href="<?= site_url();?>/home/dokumen/listregis/accreg" class="nav-link <?=(in_array($_SESSION['menu'], array('accreg')) ? 'active' : '')?>">
					  <i class="fas fa-car-side nav-icon"></i>
					  <p>Persetujuan Dokumen</p>
					  <span class="badge badge-info right"><?= $counter['ALLACC'];?></span>
					</a>
				</li>	
				<li class="nav-item">
					<a href="<?= site_url();?>/home/dokumen/listregis/approve" class="nav-link <?=(in_array($_SESSION['menu'], array('approve')) ? 'active' : '')?>">
					  <i class="fas fa-car-side nav-icon"></i>
					  <p>Pengajuan Selesai</p>
					  <span class="badge badge-info right"><?= $counter['ALLAPPROVE'];?></span>
					</a>
				</li>
				
				<li class="nav-item">
					<a href="<?= site_url();?>/home/dokumen/listregis/denied" class="nav-link <?=(in_array($_SESSION['menu'], array('denied')) ? 'active' : '')?>">
					  <i class="fas fa-car-side nav-icon"></i>
					  <p>Batal/Ditolak</p>
					  <span class="badge badge-info right"><?= $counter['ALLDANIED'];?></span>
					</a>
				</li>
			
			<?php 
			}
			
			?>
				<!-- 
				<li class="nav-item">
					<a href="<?= site_url();?>/home/dokumen/listregis/prosesReg" class="nav-link <?=(in_array($_SESSION['menu'], array('prosesReg')) ? 'active' : '')?>">
					  <i class="fas fa-car-side nav-icon"></i>
					  <p>Registrasi Draft</p>
					  <span class="badge badge-info right"><?= $counter['ALLDRAFT'];?></span>
					</a>
				</li>
				-->
				
			</ul>
			</li>
			
			<?php 
				if($this->session->userdata('USER_ROLE')=="01" || $this->session->userdata('USER_ID')=="13" || $this->session->userdata('USER_ID')=="33"|| $this->session->userdata('USER_ID')=="37"|| $this->session->userdata('USER_ID')=="29"){
			?>
			
				<li class="nav-item" >
					<a href="<?= site_url();?>/home/dokumen/report_kendaraan" class="nav-link <?=(in_array($_SESSION['menu'], array('reporkendaraan')) ? 'active' : '')?>" >
					  <i class="nav-icon fas fa-file-alt"></i>
					  <p>Laporan Kendaraan</p>
					</a>
				</li>
			
			<?php 
				}
				if($this->session->userdata('USER_ROLE')=="01"){
			 ?>
			 <!--
			 <li class="nav-header">Modul Stok</li>
			 <li class="nav-item" >
				<a href="<?= site_url();?>/home/dokumen/inputStok" class="nav-link <?=(in_array($_SESSION['menu'], array('inputStok')) ? 'active' : '')?>" >
				  <i class="nav-icon fas fa-edit"></i>
				  <p>Input Stok</p>
				</a>
			 </li>

			 <li class="nav-header">Modul Konsumsi Rapat</li>
			 <li class="nav-item" >
				<a href="<?= site_url();?>/home/dokumen/inputRapat" class="nav-link <?=(in_array($_SESSION['menu'], array('inputRapat')) ? 'active' : '')?>" >
				  <i class="nav-icon fas fa-edit"></i>
				  <p>Input Jadwal Rapat</p>
				</a>
			 </li> 	
			 -->
			 <?php 
			 
				}
			 ?>
		  
		  <li class="nav-header">RAPAT</li>
		  <li class="nav-item" >
				<a href="<?= site_url();?>/home/dokumen/inputRapat" class="nav-link <?=(in_array($_SESSION['menu'], array('inputRapat')) ? 'active' : '')?>" >
				  <i class="nav-icon fas fa-edit"></i>
				  <p>Input Jadwal Rapat</p>
				</a>
		  </li>
		  <li class="nav-item" >
			<a href="<?= site_url();?>/home/dokumen/listdata/newtable/rapat" class="nav-link <?=(in_array($_SESSION['menu'], array('rapat')) ? 'active' : '')?>">
			  <i class="nav-icon fas fa-file-alt"></i>
			  <p>List Rapat</p>
			  
			</a>
		  </li>
		  
		  <li class="nav-header">USER MANAJEMEN</li>	
		  <li class="nav-item">
			<a href="<?= site_url();?>/home/password/edit" class="nav-link <?=(in_array($_SESSION['menu'], array('pass')) ? 'active' : '')?>">
			  <i class="fas fa-user-cog nav-icon"></i>
			  <p>Setting</p>
			</a>
		  </li>
		  <?php 
			if($this->session->userdata('USER_ROLE')=="01"){
		  ?>
		    <li class="nav-item">
				<a href="<?= site_url();?>/home/user/list" class="nav-link <?=(in_array($_SESSION['menu'], array('listuser')) ? 'active' : '')?>">
				  <i class="fas fa-users nav-icon"></i>
				  <p>List User</p>
				</a>
		    </li>
		  
		  <?php 
			}
		  ?>
		  <li class="nav-item">
			<a href="<?= site_url();?>/home/logout" class="nav-link">
			  <i class="fas fa-sign-out-alt nav-icon"></i>
			  <p>Logout</p>
			</a>
		  </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  <div class="content-header">
	  <div class="container-fluid">
		<div class="row mb-2">
		  <div class="col-sm-6">
			<span id="liveclock"> 
				<script>
				 set_datetimeclock('liveclock');
			   </script>
			 </span> 
			<!--<h5 class="m-0 text-dark"> <?= date("d-m-Y");?> <span id="span"></span></h5>-->
		  </div><!-- /.col -->
		  <div class="col-sm-6">
			
		  </div><!-- /.col -->
		</div><!-- /.row -->
	  </div><!-- /.container-fluid -->
	</div>
	
	<!-- /.content-header --> 
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        {_content_}
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020 Jamkrida-Jakarta
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0
    </div>
  </footer>
</div>
<!-- ./wrapper -->




<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->



</body>
</html>
<?php 
if($this->session->userdata('USER_ROLE')=="01"|| $this->session->userdata('USER_ROLE')=="03" || $this->session->userdata('USER_ID')=="53" || $this->session->userdata('USER_ID')=="13"){
?>
<script type="text/javascript">
          $(document).ready(function(){
            
          setInterval(function(){
              $.ajax({
                type: 'POST',
                url: '<?php echo site_url(); ?>/proses/cek_reg_kendaraan/'+Math.random() ,
                data: {session:'<?php echo $this->session->userdata('LOGGED'); ?>'},
                success: function(data){
                  if(data > 0){

					toastr.info('ada ' + data + ' pengajuan peminjaman kendaraan');	

                  }
                }
              })
            }, 30000);



          });
</script>
<?php
}
?>