<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Daftar User</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="fdokupload_" action="<?= $act;?>" method="post">
                <div class="card-body">
                 
				
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama</label>
                    <input type="text" class="form-control" id="proses" name="USER[USER_FULLNAME]" style="width:520px;" placeholder="Nama Pemohon" wajib="yes"/>
                  </div>
				  
				  <div class="form-group">
                    <label for="exampleInputEmail1">User Id</label>
                    <input type="text" class="form-control" id="proses" name="USER[USER_NAME]" style="width:520px;" placeholder="User_id" wajib="yes"/>
                  </div>
				  
                  <div class="form-group">
                    <label for="exampleInputFile">Unit Kerja</label>
                    <?= form_dropdown('USER[USER_DEPARTEMEN]', $departemen, '', 'id="proses" class="form-control" style="width:520px;" wajib="yes"'); ?>
                  </div>
				  
				 
				  <div class="form-group">
				    <label>Jabatan</label>
				    <div class="select2-purple">
					  <?= form_dropdown('USER[USER_JABATAN]', $jabatan, '', 'class="form-control select2"  style="width:520px;" wajib="yes"'); ?>
				    </div>
				  </div>
				  	
				  <div class="form-group">
                    <label for="exampleInputEmail1">NIK</label>
                    <input type="text" class="form-control" id="proses" name="USER[USER_NIK]" style="width:520px;"  wajib="yes"/>
                  </div>
					
				  <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="text" class="form-control" id="proses" name="USER[USER_EMAIL]" style="width:520px;"  wajib="yes"/>
                  </div>

				 <div class="form-group">
                    <label for="exampleInputEmail1">No.Telp</label>
                    <input type="text" class="form-control" id="proses" name="USER[USER_PHONE]" style="width:520px;"  />
                  </div>	
				 
                 <div class="form-group">
				    <label>Role ID</label>
				    <div class="select2-purple">
					  <?= form_dropdown('USER[USER_ROLE]', $role, '', 'class="form-control select2"  style="width:520px;" wajib="yes"'); ?>
				    </div>
				  </div>
				 
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" onclick="save_post('#fdokupload_'); return false;">Submit</button> &nbsp;<br/><p> <div class="msgtitle_">&nbsp;</div>
                </div>
              </form>
            </div>
            <!-- /.card -->
		</div>
	</div>	
	<script type="text/javascript" src="<?= base_url();?>assets/js/jquery.ajaxfileupload.js?v=<?=date('YmdHis');?>"></script>
	<script>
	
	var rupiah = document.getElementById('rupiah');
		rupiah.addEventListener('keyup', function(e){
			// tambahkan 'Rp.' pada saat form di ketik
			// gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
			rupiah.value = formatRupiah(this.value, 'Rp. ');
		});
		
		var pemakaian = document.getElementById('pemakaian');
		    pemakaian.addEventListener('keyup', function(e){
			// tambahkan 'Rp.' pada saat form di ketik
			// gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
			pemakaian.value = formatPemakaian(this.value, 'Rp. ');
		});
 
		/* Fungsi formatRupiah */
		function formatRupiah(angka, prefix){
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
		}
		
		function formatPemakaian(angka, prefix){
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			pemakaian     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				pemakaian += separator + ribuan.join('.');
			}
 
			pemakaian = split[1] != undefined ? pemakaian + ',' + split[1] : pemakaian;
			return prefix == undefined ? pemakaian : (pemakaian ? '' + pemakaian : '');
		}
	
	$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

	  $('.textarea').summernote()
    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
	
	

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  });
	
	$(document).ready(function() {
			
			
			$("#selectkasbon").change(function(){
				var forms = $(this).val().split('||'); 
				$('#tglkasbon').val(forms[0].toString());
				//alert(forms[1].toString()); return false;
				$('#perihal123').val(forms[1].toString());
				$('#nomorkasbon').val(forms[2].toString());
			});
			
			$("#jnsproduk").change(function(){
				if($(this).val()=='101'){
					$("#penerima").show();
					$(".kasbon").hide();
					$(".norek").show();
					$("#approval").show();
				}else if($(this).val()=='102'){
					$("#penerima").hide();
					$(".kasbon").hide();
					$(".norek").show();
					$("#approval").show();
				}else{
					$("#penerima").hide();
					$("#approval").hide();
					$(".kasbon").show();
					$(".norek").hide();
				}		
			});	
			
			 $('#reservationdate').datetimepicker({
				format: 'L'
			});
			
			$('.btnBrowse').click(function(){
				var id = $(this).attr('uploadid').toString();
				$('#upload'+id).click();
			});
			var interval;
			function applyAjaxFileUpload(element, nama){
				$(element).AjaxFileUpload({
					action: site + '/proses/upload_dok/<?= date("Ymd");?>/' + nama,
					onChange: function(filename) {
						// Create a span element to notify the user of an upload in progress
						var $span = $("<span />")
							.attr("class", $(this).attr("id"))
							.text("Uploading")
							.insertAfter($(this));
						
						$('#btn'+nama).hide();
						//$(this).remove();

						interval = window.setInterval(function() {
							var text = $span.text();
							if (text.length < 13) {
								$span.text(text + ".");
							} else {
								$span.text("Uploading");
							}
						}, 200);
					},
					onSubmit: function(filename) {
						return true;
					},
					onComplete: function(filename, response) {
						//alert('sii'); return false;
						window.clearInterval(interval);
						var $span = $("span." + $(this).attr("id")).text(""),
							$fileInput = $("<input />")
								.attr({
									type: "file",
									name: $(this).attr("name"),
									id: $(this).attr("id"),
									style: "display:none"
								});

						if (typeof(response.error) === "string") {
							$span.replaceWith($fileInput);
							applyAjaxFileUpload($fileInput);
							alert(response.error);
							return;
						}
						
						$("<a />")
							.attr("href", isUrl + 'dat/<?= date("Ymd");?>/' + '<?= date("Ymd")."_";?>' + filename.split(' ').join('_'))
							.attr("target", "blank_")
							.attr("class", "btn btn-mini btn-primary")
							.html('<i class="icon-search"></i> Preview')
							.appendTo($span);
						$("<span />").html('&nbsp; &nbsp;').appendTo($span);
						$("<a />")
							.attr("href", "#")
							.css('color', 'red')
							.attr("class", "btn btn-mini btn-danger")
							.html('<i class="icon-trash"></i> <span  style="color:white;">Hapus</span>')
							.bind("click", function(e) {
								//$span.replaceWith($fileInput);
								$span.remove();
								$('#btn'+nama).show();
								//applyAjaxFileUpload($fileInput);
							})
							.appendTo($span);
						$("#uppath"+nama).val(isUrl + 'dat/<?= date("Ymd")?>/' + '<?= date("Ymd")."_"; ?>' + filename.split(' ').join('_'));
					}
				});
			}
			
			applyAjaxFileUpload("#upload01", "01");
		});
</script>