<!DOCTYPE html>
<html>
<head>
    <title>PT. Jamkrida Jakarta :: Form Daftar Hadir</title>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.css">
  
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
    <link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/south-street/jquery-ui.css" rel="stylesheet"> 
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="<?= base_url();?>assets/js/home.js?v=<?=date('YmdHis');?>"></script>
    <script type="text/javascript" src="<?= base_url();?>assets/signature/js/jquery.signature.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/signature/css/jquery.signature.css">
  
    <style>
        .kbw-signature { width: 400px; height: 200px;}
        #sig canvas{
            width: 100% !important;
            height: auto;
        }
		.wrapper {
		  position: relative;
		  width: 400px;
		  height: 200px;
		  -moz-user-select: none;
		  -webkit-user-select: none;
		  -ms-user-select: none;
		  user-select: none;
		}
    </style>
  
</head>
<body>
  
  
  <?php 
	$tgl = $this->uri->segment(3);
	$id = $this->uri->segment(4);
	$query = "SELECT * FROM t_rapat WHERE ID_RAPAT = '".$id."'";
	$judul = $this->db->query($query)->row_array();
	#print_r($judul); die($query);
  ?>
<div class="container">
  
    <form id="fdokupload_" action="<?= site_url();?>/home/register_act/save_rapat" method="post">
  
        <h1><?= $judul['AGENDA']; ?></h1><br>
		<?php 
			if($judul['SUB_AGENDA']!=""){
		?>
		<h2><?= $judul['SUB_AGENDA']; ?></h2><br>
		<?php 
			}
		?>
		<h2>Tanggal : <?= $tgl; ?></h2>
		<hr>
		<input type="hidden" name="rapatid"  value="<?= $id;?>">
		<div class="col-md-12">
            <label >Nama:</label>
            <br/>
            <input type="text" name="reg[NAMA]" class="form-control col-md-5" wajib="yes">
        </div>
		
		<div class="col-md-12">
            <label >Email:</label>
            <br/>
            <input type="text" name="reg[EMAIL]" class="form-control col-md-5" wajib="yes">
        </div>
		
		<div class="col-md-12">
            <label >No. Hp</label>
            <br/>
            <input type="text" name="reg[NO_TELP]" class="form-control col-md-5" wajib="yes">
        </div>
		
		<div class="col-md-12">
            <label >Instansi:</label>
            <br/>
            <input type="text" name="reg[INSTANSI]" class="form-control col-md-5" wajib="yes">
        </div>
		
		<div class="col-md-12">
            <label >Jabatan:</label>
            <br/>
            <input type="text" name="reg[JABATAN]" class="form-control col-md-5" wajib="yes">
        </div>
		
        <div class="col-md-12">
            <label class="" for="">Signature:</label>
            <br/>
            <div id="sig" ></div>
            <br/>
            <button id="clear"  class="">Clear Signature</button>
            <textarea id="signature64" name="signed" style="display: none"></textarea>
        </div>
		 <br>
        <div class="col-md-12">
			<button type="submit" class="btn btn-primary" onclick="save_post('#fdokupload_'); return false;">Submit</button>
        </div>
		 <br>
        <div class="col-md-12">
			<div class="msgtitle_">&nbsp;</div>
        </div>
	   <br>
	   <br>
	   
    </form>
  
</div>
  
<script type="text/javascript">
    var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
    $('#clear').click(function(e) {
        e.preventDefault();
        sig.signature('clear');
        $("#signature64").val('');
    });
</script>
  
</body>
</html>