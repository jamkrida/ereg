<style>
table.xcel{text-align: left;border:1px solid #000; border-collapse:collapse; color:#000; font-family: "lucida grande", Tahoma, verdana, arial, sans-serif; font-size:10px; direction:ltr;}
table.xcelheader{text-align: left; border-collapse:collapse; color:#000; font-family: "lucida grande", Tahoma, verdana, arial, sans-serif; font-size:10px; direction:ltr;}
</style>
<table>
      <tr>
          <td colspan="9"><center><b>LAPORAN PENGGUNAAN KENDARAAN</b></center></td>
      </tr>
	   <tr>
          <td colspan="9"><center><b>PT JAMKRIDA JAKARTA</b></center></td>
      </tr>
     <tr>
          <td colspan="9"><center><b><?= $awal.' s/d '.$akhir; ?></b></center></td>
      </tr>
     
     
</table>
<table>


<table>
			
			<tr style="border:1px; border-collapse:collapse; background:#CCC;">
				<th class="center" style="border:1px solid #000; border-collapse:collapse; background:#CCC; text-align:center;">
					NO
				</th>
				<th style="border:1px solid #000; border-collapse:collapse; background:#CCC; text-align:center;">TANGGAL PEMAKAIAN</th>	
				<th style="border:1px solid #000; border-collapse:collapse; background:#CCC; text-align:center;">DRIVER</th>
				<th style="border:1px solid #000; border-collapse:collapse; background:#CCC; text-align:center;">PENILAIAN USER</th>
				<th style="border:1px solid #000; border-collapse:collapse; background:#CCC; text-align:center;">CATATAN USER</th>
				<th style="border:1px solid #000; border-collapse:collapse; background:#CCC; text-align:center;">KENDARAAN /NOPOL</th> <
				<th style="border:1px solid #000; border-collapse:collapse; background:#CCC; text-align:center;">USER</th>	
				<th style="border:1px solid #000; border-collapse:collapse; background:#CCC; text-align:center;">TUJUAN</th>
				<th style="border:1px solid #000; border-collapse:collapse; background:#CCC; text-align:center;">KEPERLUAN</th>
				
			</tr>
			
			
	
      <?php
     if(count($datas) > 0){
		$no = 1;
		foreach($datas as $rows){
			
		$bulanarr = array('','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November', 'Desember');
		$tanggal = explode('#', $rows['TGL_PEMAKAIAN']);
		
		$tglind1 = date_create($tanggal[0]);
		$tglind1 = date_format($tglind1,"Y-n-d");
		$tglind1 = explode('-', $tglind1);
		$tglind1 = $tglind1[2].'-'.$bulanarr[$tglind1[1]].'-'.$tglind1[0];

	
				
      ?>
				<tr style="border:1px solid #000; border-collapse:collapse;">
					<td style="border:1px solid #000; border-collapse:collapse;"><?= $no;?></td>
					<td style="border:1px solid #000; border-collapse:collapse;"><?= $tglind1.' '.$tanggal[1]; ?></td>
					<td style="border:1px solid #000; border-collapse:collapse;"><?= $rows['DRIVER']; ?></td><!--JUMLAH KEHADIRAN-->
					<td style="border:1px solid #000; border-collapse:collapse;"><?= $rows['PENILAIAN']; ?></td><!--JUMLAH KEHADIRAN-->
					<td style="border:1px solid #000; border-collapse:collapse;"><?= $rows['CTTN_DRIVER']; ?></td><!--JUMLAH KEHADIRAN-->
					<td style="border:1px solid #000; border-collapse:collapse;"><?= $rows['NO_KENDARAAN']; ?></td><!--JUMLAH ABSEN/TIDAK MASUK-->
					<td style="border:1px solid #000; border-collapse:collapse;"><?= $rows['USER']; ?></td><!--JUMLAH TERLAMBAT-->
					<td style="border:1px solid #000; border-collapse:collapse;"><?= $rows['TUJUAN']; ?></td><!--JUMLAH TERLAMBAT-->
					<td style="border:1px solid #000; border-collapse:collapse;"><?= $rows['KEPERLUAN']; ?></td><!--JUMLAH TERLAMBAT-->					
				</tr>
	

	<?php
			$no++;
			}
	 }else{ #TUTUP COUNT SESS
	?>
       <tr>
          <td colspan="9" valign="top" style="border:1px solid #000; border-collapse:collapse;"><center>Data Tidak Ditemukan</center></td>
      </tr>
	 <?php 
		 }
	 ?>
	
</table>
