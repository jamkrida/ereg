<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>            
#password-strength-status {
	padding: 5px 10px;
	color: #FFFFFF;
	border-radius: 4px;
	margin-top: 5px;
}

.medium-password {
	background-color: #b7d60a;
	border: #BBB418 1px solid;
}

.weak-password {
	background-color: #ce1d14;
	border: #AA4502 1px solid;
}

.strong-password {
	background-color: #12CC1A;
	border: #0FA015 1px solid;
}
</style> 
<div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Setting</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="ubahpassword" action="<?= site_url();?>/home/password/setpassword" method="post">
                <div class="card-body">
				   
				  
				   <div class="form-group">
                    <label for="exampleInputEmail1">Old Password</label>
                    <input type="password" class="form-control"  id="old_pass" name="old_pass"  placeholder="Password" wajib="yes"/>
					
                  </div>
				  
				  <div class="form-group">
                    <label for="exampleInputEmail1">New Password</label>
                    <input type="password" class="form-control"  id="new_pass" name="new_pass"  placeholder="Password Baru" wajib="yes"/>
					<div id="password-strength-status"></div>
				  </div>
				  
				  <div class="form-group">
                    <label for="exampleInputEmail1">Confirm Password</label>
                    <input type="password" class="form-control"  id="confirm_pass" name="confirm_pass"  placeholder="Confirm Password" wajib="yes"/>
				  </div>
                 
				 
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" onclick="save_post('#ubahpassword'); return false;">Simpan</button> &nbsp;<br/><p> <div class="msgtitle_">&nbsp;</div>
                </div>
              </form>
            </div>
            <!-- /.card -->
		</div>
	</div>	
	
	<script>
	$(document).ready(function () {
		  $("#new_pass").on('keyup', function(){
			var number = /([0-9])/;
			var alphabets = /([a-zA-Z])/;
			var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
			if ($('#new_pass').val().length < 6) {
				$('#password-strength-status').removeClass();
				$('#password-strength-status').addClass('weak-password');
				$('#password-strength-status').html("Weak (minimal harus 6 karakter.)");
			} else {
				if ($('#new_pass').val().match(number) && $('#new_pass').val().match(alphabets) && $('#new_pass').val().match(special_characters)) {
					$('#password-strength-status').removeClass();
					$('#password-strength-status').addClass('strong-password');
					$('#password-strength-status').html("Strong");
				} else {
					$('#password-strength-status').removeClass();
					$('#password-strength-status').addClass('medium-password');
					$('#password-strength-status').html("Medium (harus menyertakan huruf, angka, dan karakter khusus atau kombinasi apa pun.)");
				}
			}
		  });
		}); 
	</script>