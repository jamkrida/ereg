
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
error_reporting(E_ERROR);

?>

<table width="100%"  border="1px" align="center">
		<tr>
			<td >
				<span style="font-size:16px;">PT JAMKRIDA JAKARTA</span>
			</td>
		</tr>
		<tr>
			<td >
				<span style="font-size:16px;">FORMULIR PERMINTAAN PEMBAYARAN</span>
			</td>
		</tr>
</table>		

<table><tr><td>&nbsp;</td></tr></table>
<table  width="100%" >
		<tr>
			<td width="30%" height="25px"> Nomor</td>
			<td width="3%">:</td>
			<td width="67%" style="border-bottom:1px solid black;"> <?= $sess['REG_NOMOR'];?></td>
		</tr>
		<tr>
			<td width="30%" height="25px"> Tanggal</td>
			<td width="3%">:</td>
			<td width="67%" style="border-bottom:1px solid black;"> <?= $tglind;?></td>
		</tr>
		<tr>
			<td width="30%" height="25px"> Divisi</td>
			<td width="3%">:</td>
			<td width="67%" style="border-bottom:1px solid black;"> <?= $sess['UNIT_KERJA'];?></td>
		</tr>
		<tr>
			<td width="30%" height="25px"> Nama Pemohon</td>
			<td width="3%">:</td>
			<td width="67%" style="border-bottom:1px solid black;"> <?= $sess['TTD_STAFF_PEMOHON']; ?></td>
		</tr>
		<tr>
			<td width="30%" height="25px"> Nama Penerima</td>
			<td width="3%">:</td>
			<td width="67%" style="border-bottom:1px solid black;"> <?= $sess['REG_PENERIMA'];?></td>
		</tr>
		<tr>
			<td width="30%" height="25px"> Nomor Rekening</td>
			<td width="3%">:</td>
			<td width="67%" style="border-bottom:1px solid black;"> <?= $sess['REG_NOMOR_REKENING'];?></td>
		</tr>
		
</table>
<p>
<table>
<tr >
			<td width="75%">&nbsp;</td>
			<td align="center" width="25%">Jumlah</td>
		</tr>
</table>
<table  width="100%" border="1px" cellpadding="4" cellspacing="1">
		
		<tr >
			<td width="75%"  height="30px"><?= str_replace('style="font-size: 1rem;"','',$sess['REG_PERIHAL']);?></td>
			<td width="25%" ><?php 
				if(strlen($sess['FILE_UPLOAD']) > 20 ){
					#$jmlklaim = str_replace(',','',$sess['REG_JUMLAH']);
					#echo 'Rp '.number_format($jmlklaim,2,",",".");
					$sum = 0;
					foreach($detil as $datas2){
						$counts = explode('.',$datas2['REG_DETIL_NOMINAL']);
						$dtnm = str_replace(',','',$counts[0]);
						if($counts[1] < 50){
							$jmls = $dtnm;	
						}else if($counts[1]>= 50){
							$jmls = $dtnm + 1;
						} 
						$sum += $dtnm;
					}
					echo number_format($sum,2,',','.');
				}else{
					echo 'Rp '.$sess['REG_JUMLAH'];
			
				}?>
				</td>
		</tr>

		<?php 
			if(count($detil) > 0){
		?>
		<tr>
				<td  width="75%" >Rincian Pembayaran :</td>
				<td >&nbsp;</td>
		</tr>	
		<?php 
			$no = 1;
				foreach($detil as $datas){
					
				
			?>		
		<tr nobr="true">
			<td width="75%" ><?= $no.'. '.$datas['REG_DETIL_INPUT']; ?></td>
			<td width="25%" >

			<?php 
				if(strlen($sess['FILE_UPLOAD']) > 20 ){
					
						$jmlklaimdtl = str_replace(',','',$datas['REG_DETIL_NOMINAL']);
						
						$nominal = number_format($jmlklaimdtl,2,",",".");
					    $pembualan = explode(',', $nominal); 
						if($pembulatan[1] < 50){
							$nom = $pembualan[0];
						}else if($pembualan[1]>= 50){
							$nom = $pembualan[0] +1;
						}
						echo 'Rp '.$nom.',00';
					
				}else{
					
						echo 'Rp '.$datas['REG_DETIL_NOMINAL'];
					
				}
					
			?>
			
			</td>
		</tr>
		<?php
				$no++;
				}
			}
		?>
		<tr>
			<td height="30px" >TOTAL</td>
			<td width="25%">Rp 
			<?php 
				if(strlen($sess['FILE_UPLOAD']) > 20 ){
					#$jmlklaim = str_replace(',','', $jmlklaim);
					#echo number_format($jmlklaim,2,",",".");
					$sum = 0;
					foreach($detil as $datas2){
						$counts = explode('.',$datas2['REG_DETIL_NOMINAL']);
						$dtnm = str_replace(',','',$counts[0]);
						if($counts[1] < 50){
							$jmls = $dtnm;	
						}else if($counts[1]>= 50){
							$jmls = $dtnm + 1;
						} 
						$sum += $dtnm;
					}
					echo number_format($sum,2,',','.');
				}else{

				$sum = 0;
				foreach($detil as $datas2){
					$sum += str_replace(',','.',$datas2['NM']);
				}	
				if($sum==0) echo $sess['REG_JUMLAH'];
				else echo number_format($sum,2,',','.');
			}
				?>
				</td>
		</tr>
</table>

<br/>
<p>
<p>	
<table width="100%" style="font-size:12px;">
		<tr nobr="true">
			<td width="25%">
			
			<table style="border:1px  solid black;">
					<tr>
						<td colspan="2" style="border:1px  solid black;" align="center">Pemohon</td>
					</tr>
					<?php 
						if(strlen($sess['STATUS_TTD_MANAJER_PEMOHON'])== 3) $jabatan = $sess['STATUS_TTD_MANAJER_PEMOHON'].'. ';
						else $jabatan = '';
						$t = strlen($sess['JABATAN_MANAJER_PEMOHON']);
						if($t > 20) $ta = '<br>';
						if(strlen($sess['MANAJER_PEMOHON']) < 12) $sx = '<br>';
						
						if($sess['MANAJER_PEMOHON'] =="Chusnul Maarif"){
							$namaatasan ="Chusnul Ma'arif";
						}else{
							$namaatasan = $sess['MANAJER_PEMOHON'];
						}
						if($sess['STATUS_TTD_MANAJER_PEMOHON']!="") $jbtn = $sess['STATUS_TTD_MANAJER_PEMOHON'].'.';
						else $jbtn = '';
						
					?>
					<tr>
						<td align="center"  style="border:1px  solid black;"><?= $sess['JABATAN_STAF'];?></td>
						<td align="center"  style="border:1px  solid black;"><?= $jbtn.' Atasan<br>';?></td>
					</tr>
					<tr>
						<td  height="50px"  style="border:1px  solid black;">&nbsp;</td>
						<td  height="50px"  style="border:1px  solid black;">&nbsp;</td>						
					</tr>
					<tr>
						<td align="center"  style="border:1px  solid black;"><?= $sess['TTD_STAFF_PEMOHON'];?></td>
						<td align="center"  style="border:1px  solid black;"><?= $namaatasan;?></td>
					</tr>
			</table>	
			
			</td>
			<td width="1%">
			&nbsp;
			</td>
			<td width="48%">
			<table style="border:1px  solid black;" >
					<tr>
						<td colspan="4" style="border:1px  solid black;" align="center">Menyetujui</td>
					</tr>
					<?php 
						$tgl = '2024-08-01';
						if( $sess['TANGGAL_PENGAJUAN'] < $tgl) $kbg = 'Staf';
						else $kbg = 'Kabag';
					?>
					<tr>
						<td align="center" style="border:1px  solid black;"><?= $kbg; ?> Anggaran</td>
						<td align="center" style="border:1px  solid black;"><?= $sess['PLT'];?>Kadiv Keuangan</td>
						<td align="center" style="border:1px  solid black;">Direktur</td>
						<td align="center" style="border:1px  solid black;">Direktur Utama</td>
					</tr>
					<tr>
						<td  height="50px" style="border:1px  solid black;">&nbsp;</td>
						<td  height="50px" style="border:1px  solid black;">&nbsp;</td>
						<td  height="50px" style="border:1px  solid black;">&nbsp;</td>
						<td  height="50px" style="border:1px  solid black;">&nbsp;</td>	
					</tr>
					<tr>
						<td align="center" style="border:1px  solid black;"><?= 'Mazidun Niam';?></td>
						<td align="center" style="border:1px  solid black;"><?= $sess['MANAJER_APPROVE']?></td>
						<td align="center" style="border:1px  solid black;">Jimmi Setya Budi</td>
						<td align="center" style="border:1px  solid black;">Agus Supriadi</td>
					</tr>
			</table>	
			
			</td>
			<td width="1%">&nbsp;</td>
			<td width="25%">
			<table style="border:1px  solid black;">
					<tr>
						<td colspan="2" style="border:1px  solid black;" align="center">Pembayaran</td>
					</tr>
					<tr>
						<td align="center" style="border:1px  solid black;">Staf Pembayaran</td>
						<td align="center" style="border:1px  solid black;">Staf Keuangan</td>
					</tr>
					<tr>
						<td  height="50px" style="border:1px  solid black;">&nbsp;</td>
						<td  height="50px" style="border:1px  solid black;">&nbsp;</td>						
					</tr>
					<tr>
						<td align="center" style="border:1px  solid black;"><?= 'Muthia Fajri';?></td>
						<td rowspan="2" align="center" style="border:1px  solid black;"><?= $sess['STAFF_KEU'];?></td>
					</tr>
					<tr>
						<td align="center" style="border:1px  solid black;"><?= 'M. Ariz';?></td>
						
					</tr>
			</table>	
			
			</td>
		</tr>
		
</table>

<table><tr><td>&nbsp;</td></tr></table>
<table  width="100%" style="font-size:12px;">
		<tr>
			<td style="border:1px  solid black;">Catatan :<br><?= str_replace('style="font-size: 1rem;"','',$sess['REG_CATATAN']);?></td>
		</tr>
		
</table>


 
