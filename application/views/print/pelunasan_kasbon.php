
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
error_reporting(E_ERROR);

?>

<table width="100%"  border="1px" align="center">
		<tr>
			<td >
				<span style="font-size:16px;">PT JAMKRIDA JAKARTA</span>
			</td>
		</tr>
		<tr>
			<td >
				<span style="font-size:16px;">FORMULIR PENYELESAIAN KASBON</span>
			</td>
		</tr>
</table>		

<table><tr><td>&nbsp;</td></tr></table>
<table><tr><td>&nbsp;</td></tr></table>
<table  width="100%" >
		<tr>
			<td width="30%" height="25px"> Nomor</td>
			<td width="3%">:</td>
			<td width="67%" style="border-bottom:1px solid black;"> <?= $sess['REG_NOMOR'];?></td>
		</tr>
		<tr>
			<td width="30%" height="25px"> Tanggal</td>
			<td width="3%">:</td>
			<td width="67%" style="border-bottom:1px solid black;"> <?= $tglind;?></td>
		</tr>
		<tr>
			<td width="30%" height="25px"> Divisi</td>
			<td width="3%">:</td>
			<td width="67%" style="border-bottom:1px solid black;"> <?= $sess['UNIT_KERJA'];?></td>
		</tr>
		<tr>
			<td width="30%" height="25px"> Nama</td>
			<td width="3%">:</td>
			<td width="67%" style="border-bottom:1px solid black;"> <?= $sess['TTD_STAFF_PEMOHON'];?></td>
		</tr>
	
		<tr>
			<td width="30%" height="25px"> No Form Kasbon</td>
			<td width="3%">:</td>
			<td width="67%" style="border-bottom:1px solid black;"> <?= $sess['REG_NOMOR_KASBON'];?></td>
		</tr>
		<tr>
			<td width="30%" height="25px"> Tanggal Kasbon</td>
			<td width="3%">:</td>
			<td width="67%" style="border-bottom:1px solid black;"> <?= $tglkasbon;?></td>
		</tr>
		
</table>
<p>
<table  width="100%">
		<tr>
			<td width="70%">Uraian Penyelesaian Kasbon:</td>
			<td align="center" width="30%">Jumlah</td>
		</tr>
		<tr>
			<td style="border:1px solid black;">Jumlah Kasbon</td>
			<td style="border:1px solid black;"><?=  'Rp. '.$sess['REG_JUMLAH']?></td>	
		</tr>
				<tr>
			<td style="border:1px solid black;" colspan="2">Perihal<?=  $sess['REG_PERIHAL']?></td>
				
		</tr> 
		
		<tr>
			<td style="border-left:1px solid black;">Rincian Kasbon :</td>
			<td style="border-right:1px solid black; border-left:1px solid black;">&nbsp;</td>	
		</tr>
		<?php 
			$no = 1;
																					
			foreach($detil as $datas){
		?>

		<tr>
			<td style="border-left:1px solid black;border-right:1px solid black; "><?php echo $no.'. '.$datas['REG_DETIL_INPUT']; ?></td>
			<td style="border-right:1px solid black;"><?php echo $datas['REG_DETIL_NOMINAL']; ?></td>	
		</tr>

		<?php 
		
			$no++;
			}
		?>
		
		<!--
		<tr >
			<td width="70%" style="border:1px solid black;" height="20px" ><table >
																			<tr><td>Jumlah Kasbon</td></tr>
																			<?php 
																				if(count($detil) > 0){
																			?>
																			<tr>
																				<td>Rincian Kasbon :<br>
																					<?php 
																					$no = 1;
																					
																						foreach($detil as $datas){
																							echo $no.'. '.$datas['REG_DETIL_INPUT'].'<br>';
																						$no++;
																						}	
																					?>
																				</td>
																			</tr>
																			<?php 
																				}else{
																			?>
																			<tr><td>&nbsp;</td></tr>
																			<?php 
																			}
																			?>
																			<tr><td><?= $sess['REG_PERIHAL'];?></td></tr>
																         </table>
			</td>
			<td width="30%" style="border:1px solid black;"> <table >
																<tr><td><br><?=  'Rp. '.$sess['REG_JUMLAH']?></td></tr>
																<?php 
																	if(count($detil)> 0){
																?>
																
																<tr>
																	<td> &nbsp;<br>
																		<?php 

																			foreach($detil as $datas2){
																				echo 'Rp '.$datas2['REG_DETIL_NOMINAL'].'<br>';
																			}	
																		?>
																	</td>
																</tr>
																<tr><td>&nbsp;</td></tr>
																<?php 
																	}else{
																?>
																<tr><td>&nbsp;</td></tr>
																
																<?php 
																}
																?>
																<tr><td><?=  'Rp. '.$sess['REG_JUMLAH']?></td></tr>
															 </table>
			</td>
		</tr> -->
		<tr>
			<td height="30px" style="border:1px solid black;">Total Penggunaan</td>
			<td width="30%" style="border:1px solid black;"> <?=  'Rp. '.$sess['REG_PEMAKAIAN']; ?></td>
		</tr>
		 <?php
			$total = $sess['REG_JUMLAH_TERBILANG']-$sess['REG_PEMAKAIAN_TERBILANG'];
			if($total > 0) $hasil = 'Lebih/<del>Kurang</del> &nbsp;';
			else $hasil = '<del>Lebih</del>/Kurang &nbsp;';
		 ?>
		<tr>
			<td height="30px" style="border:1px solid black;" align="right"><?= $hasil;?> </td>
			<td width="30%" style="border:1px solid black;"> <?php 
															
															echo 'Rp. '.number_format($total,2,",",".");
														?></td>
		</tr>
</table>

<br/>
<p>
<p>	
<table width="100%" style="font-size:12px;">
		<tr>
			<td width="49%">
			
			<table style="border:1px  solid black;">
					<tr>
						<td  colspan="2"  style="border:1px  solid black;" align="center">Pemohon</td>
					</tr>
					<?php 
						if(strlen($sess['STATUS_TTD_MANAJER_PEMOHON'])== 3) $jabatan = $sess['STATUS_TTD_MANAJER_PEMOHON'].'. ';
						else $jabatan = '';
						
						$t = strlen($sess['JABATAN_MANAJER_PEMOHON']);
						if($t > 20) $ta = '<br>';
						//$jabatan.''.$sess['JABATAN_MANAJER_PEMOHON']
						if($sess['MANAJER_PEMOHON'] =="Achmad Ivan S. Soeparno"){
							$namaatasan ="&nbsp;";
							$jbtn = 'Direktur Utama';
						}else{
							$jbtn = $jabatan.'Atasan';
							$namaatasan = $sess['MANAJER_PEMOHON'];
						}   
					?>
					<tr>
						<td align="center"  style="border:1px  solid black;"><?= $sess['JABATAN_STAF']; ?> </td>
						<td align="center"  style="border:1px  solid black;"><?= $jbtn.'<br/>'; ?> </td>
						
					</tr>
					<tr>
						<td  height="50px" style="border:1px  solid black;">&nbsp;</td>	
						<td  height="50px" style="border:1px  solid black;">&nbsp;</td>						
					</tr>
					<tr>
						<td align="center"  style="border:1px  solid black;"><?= $sess['TTD_STAFF_PEMOHON'];?></td>
						<td align="center"  style="border:1px  solid black;"><?= $namaatasan;?></td>
						
					</tr>
			</table>	
			
			</td>
			<td width="1%">
			&nbsp;
			</td>
			
			<td width="1%">&nbsp;</td>
			<td width="49%">
			<table style="border:1px  solid black;">
					<tr>
						<td colspan="2" style="border:1px  solid black;" align="center">Pembayaran</td>
					</tr>
					<tr>
						<td align="center" style="border:1px  solid black;">Staf Pembayaran<br/></td>
						<td align="center" style="border:1px  solid black;">Staff Keuangan<br/></td>
					</tr>
					<tr>
						<td  height="50px" style="border:1px  solid black;">&nbsp;</td>	
						<td  height="50px" style="border:1px  solid black;">&nbsp;</td>						
					</tr>
					<tr>
						<td align="center" style="border:1px  solid black;"><?= 'Muthia Fajri';?></td>
						<td rowspan="2" align="center" style="border:1px  solid black;"><?= $sess['STAFF_KEU'];?></td>
					</tr>
					<tr>
						<td align="center" style="border:1px  solid black;"><?= 'M. Ariz';?></td>
						
					</tr>
			</table>	
			
			</td>
		</tr>
		
</table>

<table><tr><td>&nbsp;</td></tr></table>
<table  width="100%" style="font-size:12px;">
		<tr>
			<td style="border:1px  solid black;">Catatan :<br><?= $sess['REG_CATATAN'];?></td>
		</tr>
		
</table>


 
