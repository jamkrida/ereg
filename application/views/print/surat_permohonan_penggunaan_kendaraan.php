
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
error_reporting(E_ERROR);
$jarak1 = 30;
$jarak2 = 40;
$judul = '<b>FORM PERMOHONAN PENGGUNAAN KENDARAAN OPERASIONAL</b>';
?>
<table>
		<tr><td align="left"><img src="<?= base_url().'/assets/img/01_LOGO_UTAMA.jpg';?>" style="width:90;height:90"></td></tr>
</table>
<p>
<table width="100%" align="center">
		<tr>
			<td >
				<?= $judul; ?>
			</td>
		</tr>
</table>		
<p>
<table  width="100%" border="1px" cellpadding="2">
		<tr>
			<td width="47%" height="30px">NAMA PEMAKAI KENDARAAN</td>
			<td width="3%" align="center">:</td>
			<td width="50%"><?= $sess['NAMA_PEMOHON'];?></td>
		</tr>
		<tr>
			<td width="47%" height="30px">UNIT KERJA</td>
			<td width="3%" align="center">:</td>
			<td width="50%"><?= $sess['NAMA_DIV'];?></td>
		</tr>
		<tr>
			<td width="47%" height="30px">HARI/ TANGGAL PENGGUNAAN</td>
			<td width="3%" align="center">:</td>
			<td width="50%"><?= $tglpermohonan;?></td>
		</tr>
		<tr>
			<td width="47%" height="30px">WAKTU PENGGUNAAN</td>
			<td width="3%" align="center">:</td>
			<td width="50%"><?= $sess['WAKTU_PERMOHONAN'];?> WIB</td>
		</tr>	
		<tr>
			<td width="47%" height="30px">KEPERLUAN</td>
			<td width="3%" align="center">:</td>
			<td width="50%"><?= $sess['KEPERLUAN'];?></td>
		</tr>
		<tr>
			<td width="47%" height="30px">TUJUAN</td>
			<td width="3%" align="center">:</td>
			<td width="50%"><?= $sess['TUJUAN'];?></td>
		</tr>
		<?php 
			if($sess['JENIS_KENDARAAN']=="1"){
				$js = 'Kendaraan Operasional';
			}else if($sess['JENIS_KENDARAAN']=="2"){
				$js = 'Driver Online';
			}else{
				$js = '-';
			}
		?>
		<tr>
			<td width="47%" height="30px">JENIS KENDARAAN</td>
			<td width="3%" align="center">:</td>
			<td width="50%"><?= $js; ?></td>
		</tr>
		<?php 
			if($sess['JENIS_KENDARAAN']=="1"){
		?>
		<tr>
			<td width="47%" height="30px">NAMA PENGEMUDI</td>
			<td width="3%" align="center">:</td>
			<td width="50%"><?php  echo $sess['NAMA_DRIVER'];?></td>
		</tr>
		<tr>
			<td width="47%" height="30px">NOMOR POLISI</td>
			<td width="3%" align="center">:</td>
			<td width="50%"><?php  echo $sess['NO_KENDARAAN'];?></td>
		</tr>
		<?php
			}else if($sess['JENIS_KENDARAAN']=="2"){
		?>
		<tr>
			<td width="47%" height="30px">KETERANGAN</td>
			<td width="3%" align="center">:</td>
			<td width="50%"><?php  echo $sess['KETERANGAN'];?></td>
		</tr>	
		<?php 
			}
		?>
</table>

<br/>
<p>
<table>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align="right">Jakarta, <?= $tglind; ?></td>
		</tr>	
</table>
<p>	
<table border ="1" width ="100%" cellpadding="2">
		
		<tr>
			
			<th align="center">Atasan Langsung </th>
			<th align="center">Pemohon</th>	
		</tr>
		<?php 
		if($sess['TTD']!=""){
		?>
		<tr>
			<td align="center"><img src="<?= base_url().'img/'.$sess['TTD']; ?>" style="width:120px;height:120px"><br><?= $sess['ATASAN']?></td>
			<?php 
			if($sess['USER_TTD']!=""){
			?>
			<td align="center"><img src="<?= base_url().$sess['USER_TTD']; ?>" style="width:120px;height:120px"><br>(<?= $sess['NAMA_PEMOHON'];?>)</td>
			<?php 
			}else{
			?>
			<td>&nbsp;</td>
			<?php 
			}
			?>
		</tr>
		<?php 
		}else{
		?>
		<tr>
			<td height="80px">&nbsp;</td>
			<?php 
			if($sess['USER_TTD']!=""){
			?>
			<td align="center"><img src="<?= base_url().$sess['USER_TTD']; ?>" style="width:120px;height:120px"><br>(<?= $sess['NAMA_PEMOHON'];?>)</td>
			<?php 
			}else{
			?>
			<td>&nbsp;</td>
			<?php 
			}
			?>
			
		</tr>	
		<?php 
		}
		?>	
</table>	

<p>
<table>
		<tr>
			<td>&nbsp;</td>
		</tr>	
		<tr>
			<td>&nbsp;</td>
		</tr>	
</table>
<p>
Mengetahui : <br/>
- Kepala Divisi Keuangan, IT & SDM

 
