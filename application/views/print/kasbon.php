
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
error_reporting(E_ERROR);

?>

<table width="100%"  border="1px" align="center">
		<tr>
			<td >
				<span style="font-size:16px;">PT JAMKRIDA JAKARTA</span>
			</td>
		</tr>
		<tr>
			<td >
				<span style="font-size:16px;">FORMULIR PERMINTAAN KASBON</span>
			</td>
		</tr>
</table>		

<table><tr><td>&nbsp;</td></tr></table>
<table><tr><td>&nbsp;</td></tr></table>
<table  width="100%" >
		<tr>
			<td width="30%" height="25px"> Nomor</td>
			<td width="3%">:</td>
			<td width="67%" style="border-bottom:1px solid black;"> <?= $sess['REG_NOMOR'];?></td>
		</tr>
		<tr>
			<td width="30%" height="25px"> Tanggal</td>
			<td width="3%">:</td>
			<td width="67%" style="border-bottom:1px solid black;"> <?= $tglind;?></td>
		</tr>
		<tr>
			<td width="30%" height="25px"> Divisi</td>
			<td width="3%">:</td>
			<td width="67%" style="border-bottom:1px solid black;"> <?= $sess['UNIT_KERJA'];?></td>
		</tr>
		<tr>
			<td width="30%" height="25px"> Nama Pemohon</td>
			<td width="3%">:</td>
			<td width="67%" style="border-bottom:1px solid black;"> <?= $sess['TTD_STAFF_PEMOHON']; ?></td>
		</tr>
	
		<tr>
			<td width="30%" height="25px"> Nomor Rekening</td>
			<td width="3%">:</td>
			<td width="67%" style="border-bottom:1px solid black;"> <?= $sess['REG_NOMOR_REKENING'];?></td>
		</tr>
		
</table>
<p>
<table  width="100%">
		<tr>
			<td width="70%">Uraian Tujuan Kasbon:</td>
			<td align="center" width="30%">Jumlah</td>
		</tr>
		<tr >
			<td width="70%" style="border:1px solid black;" height="30px"><?= $sess['REG_PERIHAL'];?></td>
			<td width="30%" style="border:1px solid black;"> <?=  'Rp '.$sess['REG_JUMLAH']?></td>
		</tr>
		<?php 
			if(count($detil) > 0){
		?>
		<tr>
			<td width="70%" style="border:1px solid black;" height="30px">Rincian Pembayaran :<br>
			<?php 
			$no = 1;
				foreach($detil as $datas){
					echo $no.'. '.$datas['REG_DETIL_INPUT'].'<br>';
				$no++;
				}	
			?>
			</td>
			<td width="30%" style="border:1px solid black;"> &nbsp;<br/>
			<?php 
			
				foreach($detil as $datas2){
					echo 'Rp '.$datas2['REG_DETIL_NOMINAL'].'<br>';
				}	
			?>
			
			</td>
		</tr>
		<?php
			}
		?>
		<tr>
			<td height="30px" style="border:1px solid black;">TOTAL</td>
			<td width="30%" style="border:1px solid black;"> <?=  'Rp '.$sess['REG_JUMLAH']?></td>
		</tr>
</table>

<br/>
<p>
<p>	
<table width="100%" style="font-size:12px;">
		<tr>
			<td width="25%">
			
			<table style="border:1px  solid black;">
					<tr>
						<td style="border:1px  solid black;" colspan="2" align="center">Pemohon</td>
					</tr>
					<?php 
						if(strlen($sess['STATUS_TTD_MANAJER_PEMOHON'])== 3) $jabatan = $sess['STATUS_TTD_MANAJER_PEMOHON'].'. ';
						else $jabatan = '';
						
						$t = strlen($sess['JABATAN_MANAJER_PEMOHON']);
						if($t > 20) $ta = '';
						
						if($sess['MANAJER_PEMOHON'] =="Chusnul Maarif"){
							$namaatasan="Chusnul Ma'arif";
						}else{
							$namaatasan = $sess['MANAJER_PEMOHON'];
						}
					
					?>
					<tr>
						<td align="center"  style="border:1px  solid black;"><?= $sess['JABATAN_STAF']; ?> </td>
						<td align="center"  style="border:1px  solid black;"><?= $sess['STATUS_TTD_MANAJER_PEMOHON'].' Kadiv<br>';?></td>
					</tr>
					<tr>
						
						<td  height="50px"  style="border:1px  solid black;">&nbsp;</td>						
					</tr>
					<tr>
						<td align="center"  style="border:1px  solid black;"><?= $sess['TTD_STAFF_PEMOHON'];?></td>
						<td align="center"  style="border:1px  solid black;"><?= $namaatasan;?></td>
					</tr>
			</table>	
			
			</td>
			<td width="1%">
			&nbsp;
			</td>
			<td width="48%">
			<table style="border:1px  solid black;" >
					<tr>
						<td colspan="4" style="border:1px  solid black;" align="center">Menyetujui</td>
					</tr>
					<?php 
						$tgl = '2024-8-01';
						if( $sess['TANGGAL_PENGAJUAN'] < $tgl) $kbg = 'Staf';
						else $kbg = 'Kabag';
					?>
					<tr>
						<td align="center" style="border:1px  solid black;"><?= $kbg; ?> Anggaran</td>
						<td align="center" style="border:1px  solid black;"><?= $sess['PLT'];?>Kadiv Keuangan<?= $ta;?></td>
						<td align="center" style="border:1px  solid black;">Direktur</td>
						<td align="center" style="border:1px  solid black;">Direktur Utama</td>
					</tr>
					<tr>
						<td  height="50px" style="border:1px  solid black;">&nbsp;</td>
						<td  height="50px" style="border:1px  solid black;">&nbsp;</td>
						<td  height="50px" style="border:1px  solid black;">&nbsp;</td>
						<td  height="50px" style="border:1px  solid black;">&nbsp;</td>	
					</tr>
					<tr>
						<td align="center" style="border:1px  solid black;"><?= 'Mazidun Niam';?></td>
						<td align="center" style="border:1px  solid black;"><?= $sess['MANAJER_APPROVE']?></td>
						<td align="center" style="border:1px  solid black;">Jimmy Setya Budi</td>
						<td align="center" style="border:1px  solid black;">Agus Supriadi</td>
					</tr>
			</table>	
			
			</td>
			<td width="1%">&nbsp;</td>
			<td width="25%">
			<table style="border:1px  solid black;">
					<tr>
						<td colspan="2" style="border:1px  solid black;" align="center">Pembayaran</td>
					</tr>
					<tr>
						<td align="center" style="border:1px  solid black;">Staf Pembayaran</td>
						<td align="center" style="border:1px  solid black;">Staff Keuangan</td>
					</tr>
					<tr>
						<td  height="50px" style="border:1px  solid black;">&nbsp;</td>
						<td  height="50px" style="border:1px  solid black;">&nbsp;</td>						
					</tr>
					<tr>
						<td align="center" style="border:1px  solid black;"><?= 'Muthia Fajri';?></td>
						<td rowspan="2" align="center" style="border:1px  solid black;"><?= $sess['STAFF_KEU'];?></td>
					</tr>
					<tr>
						<td align="center" style="border:1px  solid black;"><?= 'M. Ariz';?></td>
						
					</tr>
			</table>	
			
			</td>
		</tr>
		
</table>

<table><tr><td>&nbsp;</td></tr></table>
<table  width="100%" style="font-size:12px;">
		<tr>
			<td style="border:1px  solid black;">Catatan :<br><?= $sess['REG_CATATAN'];?></td>
		</tr>
		
</table>


 
