<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PT. Jamkrida Jakarta :: Registrasi Daftar Rekanan</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="icon" type="image/png" href="<?= base_url();?>assets/img/jamkrida.png"/>
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?= base_url();?>plugins/fontawesome-free/css/all.min.css?v=<?=date('YmdHis')?>">
  <link rel="stylesheet" href="<?= base_url();?>assets/css/newtable.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= base_url();?>plugins/daterangepicker/daterangepicker.css">
  
  <link rel="stylesheet" href="<?= base_url();?>plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <link rel="stylesheet" href="<?= base_url();?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url();?>dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	
	<!--Summernote CSS-->
	 <link rel="stylesheet" href="<?= base_url();?>plugins/fontawesome-free/css/all.min.css">
     <!-- Ionicons -->
     <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	  <!-- Theme style -->
	 <link rel="stylesheet" href="<?= base_url();?>dist/css/adminlte.min.css">
	  <!-- summernote -->
	 <link rel="stylesheet" href="<?= base_url();?>plugins/summernote/summernote-bs4.css">
	  <!-- Google Font: Source Sans Pro -->
	 <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	 <link rel="stylesheet" href="<?= base_url();?>plugins/summernote/summernote-bs4.css">
	 <!--Summernote CSS-->
	 
	 
	 <link rel="stylesheet" href="<?= base_url();?>plugins/daterangepicker/daterangepicker.css">
	  <!-- iCheck for checkboxes and radio inputs -->
	  <link rel="stylesheet" href="<?= base_url();?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	  <!-- Bootstrap Color Picker -->
	  <link rel="stylesheet" href="<?= base_url();?>plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
	  <!-- Tempusdominus Bbootstrap 4 -->
	  <link rel="stylesheet" href="<?= base_url();?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
	  <!-- Select2 -->
	  <link rel="stylesheet" href="<?= base_url();?>plugins/select2/css/select2.min.css">
	  <link rel="stylesheet" href="<?= base_url();?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
	  <!-- Bootstrap4 Duallistbox -->
	  <link rel="stylesheet" href="<?= base_url();?>plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
	  <!-- Theme style -->
	  <link rel="stylesheet" href="<?= base_url();?>dist/css/adminlte.min.css">
	  <!-- Google Font: Source Sans Pro -->
	  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  
  
	<script src="<?= base_url();?>plugins/jquery/jquery.min.js?v=<?=date('YmdHis');?>"></script>
	<script src="<?= base_url();?>assets/js/newtable.js?v=<?=date('YmdHis');?>"></script>
	<script src="<?= base_url();?>assets/js/newtablehash.js?v=<?=date('YmdHis');?>"></script>
	<!-- Bootstrap -->
	<script src="<?= base_url();?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- overlayScrollbars -->
	<script src="<?= base_url();?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
	<!-- AdminLTE App -->
	<script src="<?= base_url();?>dist/js/adminlte.js"></script>
	<!-- OPTIONAL SCRIPTS -->
	<script src="<?= base_url();?>dist/js/demo.js"></script>

	<!-- PAGE PLUGINS -->
	<!-- jQuery Mapael -->
	<script src="<?= base_url();?>plugins/daterangepicker/daterangepicker.js"></script>
	
	<script src="<?= base_url();?>plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
	<script src="<?= base_url();?>plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
	<script src="<?= base_url();?>plugins/raphael/raphael.min.js"></script>
	<script src="<?= base_url();?>plugins/jquery-mapael/jquery.mapael.min.js"></script>
	<script src="<?= base_url();?>plugins/jquery-mapael/maps/usa_states.min.js"></script>
	<!-- ChartJS -->
	<script src="<?= base_url();?>plugins/chart.js/Chart.min.js"></script>
	<!-- PAGE SCRIPTS -->
	<script src="<?= base_url();?>dist/js/pages/dashboard2.js"></script>
	<script src="<?= base_url();?>assets/js/home.js?v=<?=date('YmdHis');?>"></script>
	<script type="text/javascript">
	 var site = "<?php echo site_url(); ?>";
	 var isUrl = "<?php echo base_url(); ?>";
	</script>
	
	<!-- datepicker-->
	<script src="<?= base_url();?>plugins/summernote/summernote-bs4.min.js"></script>
	<!--<script src="<?= base_url();?>plugins/jquery/jquery.min.js"></script>-->
	<!-- Bootstrap 4 -->
	<script src="<?= base_url();?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- Select2 -->
	<script src="<?= base_url();?>plugins/select2/js/select2.full.min.js"></script>
	<!-- Bootstrap4 Duallistbox -->
	<script src="<?= base_url();?>plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
	<!-- InputMask -->
	<script src="<?= base_url();?>plugins/moment/moment.min.js"></script>
	<script src="<?= base_url();?>plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
	<!-- date-range-picker -->
	<script src="<?= base_url();?>plugins/daterangepicker/daterangepicker.js"></script>
	<!-- bootstrap color picker -->
	<script src="<?= base_url();?>plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
	<!-- Tempusdominus Bootstrap 4 -->
	<script src="<?= base_url();?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
	<!-- Bootstrap Switch -->
	<script src="<?= base_url();?>plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
	
	<!-- AdminLTE App -->
	<!--<script src="<?= base_url();?>dist/js/adminlte.min.js"></script>-->
	
	<!-- AdminLTE for demo purposes -->
	<!--<script src="<?= base_url();?>dist/js/demo.js"></script>-->
	<!-- datepicker-->
	<script src="<?= base_url();?>assets/js/numeric/autoNumeric.js?v=<?=date('YmdHis');?>"></script>
	
	<!-- Summernote -->
	<script src="<?= base_url();?>plugins/summernote/summernote-bs4.min.js"></script>
	<!--Summernote JS-->
</head>
<body class="hold-transition register-page">
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>PT. Jamkrida Jakarta</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Proses Berhasil, Silahkan tunggu email balasan dari administrator kami.</p>

     

     
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
</body>
</html>
