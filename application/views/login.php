<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$chr = str_shuffle("0123456789");
$chr = substr($chr, 3, 6);
$newdata = array('uid_'  => md5($chr."uid_"),
				 'pass_'     => md5($chr."pass_"),
				 'session_id' => md5($chr."session_id"));
$this->session->set_userdata($newdata);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Jamkrida Jakarta :: Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?= base_url();?>assets/img/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/login/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/login/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/login/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/login/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/login/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/login/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/login/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/login/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/login/css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form" method="POST" id="flogin_" action ="<?= site_url();?>/login/verify/<?= $this->session->userdata('session_id'); ?>" onsubmit="login('#flogin_'); return false;">
					
					<center>
							<img src="<?= base_url();?>assets/img/favicon.ico"><br/>
							<h4>
								e-Registrasi
							</h4>
					</center>
					<br>

					<div class="wrap-input100 " >
						<input class="input100" type="text" name="uid_">
						<span class="focus-input100" data-placeholder="Username"></span>
					</div>

					<div class="wrap-input100 " data-validate="Enter password">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" type="password" name="pass_">
						<span class="focus-input100" data-placeholder="Password"></span>
					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn" id="ok_">
								Login
							</button>
						</div>
					</div>
					<div class="text-center p-t-90">
						<div class="msgtitle_" style="margin-top:10px;">&nbsp;</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="<?= base_url();?>assets/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?= base_url();?>assets/login/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?= base_url();?>assets/login/vendor/bootstrap/js/popper.js"></script>
	<script src="<?= base_url();?>assets/login/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?= base_url();?>assets/login/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?= base_url();?>assets/login/vendor/daterangepicker/moment.min.js"></script>
	<script src="<?= base_url();?>assets/login/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?= base_url();?>assets/login/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?= base_url();?>assets/login/js/main.js"></script>
	<script src="<?= base_url();?>assets/login/login.js?v=<?= date('YmdHis');?>"></script>

</body>
</html>