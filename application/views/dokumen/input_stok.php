<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Input Stok</h3>
              </div>

              <form role="form" id="fdokupload_" action="<?= $act; ?>" method="post">
                <div class="card-body">
                 
                  <div class="form-group">
                    <label for="exampleInputEmail1">Jenis Stok</label>
                    <?= form_dropdown('REG[JENIS_STOK]', $jnsstok, '', 'class="form-control select2 col-md-6" wajib="yes"'); ?>
                  </div>
				   <div class="form-group">
                    <label for="exampleInputEmail1">Nama Barang</label>
                    <input type="text" class="form-control"  name="REG[PERIHAL]" style="width:520px;"  wajib="yes"/>
                  </div>
				   <div class="form-group">
                    <label for="exampleInputEmail1">Merk/Jenis</label>
                    <input type="text" class="form-control"  name="REG[PERIHAL]" style="width:520px;"  wajib="yes"/>
                  </div>
				 
				 <div class="row">
					  <div class="col-12 col-sm-3">
							<div class="form-group">
							  <label>Jumlah</label>
							   <input type="text" class="form-control"  name="REG[PERIHAL]"   wajib="yes"/>
							  
							</div>
							
						</div>
					  
						<div class="col-12 col-sm-3">
							<div class="form-group">
							  <label>Satuan</label>
							  <div class="select2-purple">
								 <?= form_dropdown('REG[TTD_JABATAN_MANAJER_PEMOHON]', $satuan, '', 'class="form-control select2" wajib="yes"'); ?>
							  </div>
							</div>
							<!-- /.form-group -->
						</div>
				  </div>
				  
				    <div class="form-group">
					  <label>Tanggal Pembelian</label>
						<div class="input-group date" id="reservationdate" data-target-input="nearest" >
							<input type="text" class="form-control col-md-6" name="REG[TANGGAL_PEMBELIAN]" data-target="#reservationdate" />
							<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
								<div class="input-group-text"><i class="fa fa-calendar"></i></div>
							</div>
						</div>
					</div>
				  
				  <div class="form-group">
                    <label for="exampleInputEmail1">Harga</label>
                    <input type="text" class="form-control" id="rupiah" name="REG[PERIHAL]" style="width:520px;"  wajib="yes"/>
                  </div>
				  
				   <div class="form-group">
                    <label for="exampleInputEmail1">Catatan</label>
                    <textarea type="text" class="form-control col-md-6"  name="REG[PERIHAL]"  wajib="yes"></textarea>
                  </div>
				 
				  
				
			
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" onclick="save_post('#fdokupload_'); return false;">Submit</button> &nbsp;<br/><p> <div class="msgtitle_">&nbsp;</div>
                </div>
              </form>
            </div>
            <!-- /.card -->
		</div>
	</div>	
	<script>
	
	$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    //Date range picker
    $('#reservation').daterangepicker({
		  format: 'YYYY-MM-DD'
	})
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

	  $('#reservationdate').datetimepicker({
        format: 'L'
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });
	
	$("#selectdriver").change(function(){
				var forms = $(this).val().split('||'); 
				$('#nopol').val(forms[1].toString());
				
			});

  })
	
	var rupiah = document.getElementById('rupiah');
		rupiah.addEventListener('keyup', function(e){
			// tambahkan 'Rp.' pada saat form di ketik
			// gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
			rupiah.value = formatRupiah(this.value, 'Rp. ');
		});
		
 
	/* Fungsi formatRupiah */
	function formatRupiah(angka, prefix){
		var number_string = angka.replace(/[^,\d]/g, '').toString(),
		split   		= number_string.split(','),
		sisa     		= split[0].length % 3,
		rupiah     		= split[0].substr(0, sisa),
		ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

		// tambahkan titik jika yang di input sudah menjadi angka ribuan
		if(ribuan){
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}

		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
	}
	//Date range picker
	
	
</script>