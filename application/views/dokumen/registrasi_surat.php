<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Registrasi Surat</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="fdokupload_" action="<?= $act; ?>" method="post">
                <div class="card-body">

			  <div class="form-group" >
				  <label for="exampleInputEmail1">Jenis Registrasi Surat</label>
				  <?= form_dropdown('REG[JENIS_REG_SURAT]', $jnsregsurat, '', 'class="form-control col-md-5" id="jnsReg"  wajib="yes"'); ?>
				</div>
					
			 <div class="form-group ap1" >
			  <label for="exampleInputEmail1">Jenis Surat</label>
			  <?= form_dropdown('REG[JENIS_SURAT]', $jns_surat, '', 'id="jnsSurat" class="form-control col-md-5"  wajib="yes"'); ?>
			</div>

			<div class="form-group">
			  <label>Tanggal</label>
				<div class="input-group date" id="reservationdate" data-target-input="nearest" >
					<input type="text" class="form-control datetimepicker-input col-md-4" name="REG[TANGGAL_SURAT]"  data-target="#reservationdate"/>
					<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
						<div class="input-group-text"><i class="fa fa-calendar"></i></div>
					</div>
				</div>
			</div>
			
			<div class="form-group" id="namapemohon" style="display:none;">
			   <label for="exampleInputEmail1">Nama Pemohon</label>
			   <input type="text" class="form-control col-md-12"  name="REG[NAMA_PEMOHON]" />
			</div>
			
			<div class="form-group pembayaran" style="display:none;">
			   <label for="exampleInputEmail1">Tujuan Pembayaran</label>
			   <input type="text" class="form-control col-md-12"  name="REG[TUJUAN_PEMBAYARAN]" />
			</div>
			
			<div class="form-group pembayaran" style="display:none;">
			   <label for="exampleInputEmail1">Total Pembayaran</label>
			   <input type="text" class="form-control col-md-12"  name="REG[TOTAL_PEMBAYARAN]" />
			</div>
			
			<div class="form-group" id="nosurat">
			   <label for="exampleInputEmail1">Nomor Surat</label>
			   <input type="text" class="form-control col-md-12"  name="REG[NOMOR_SURAT]" wajib="yes"/>
			</div>
				
			<div class="form-group ap1" >
			  <label for="exampleInputEmail1">Perihal</label>
			  <textarea class="textarea" name="REG[PERIHAL_SURAT]" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
			</div>
				
			<div class="form-group SuratMasuk" style="display:none;">
			   <label for="exampleInputEmail1">Dari</label>
			   <input type="text" class="form-control col-md-12"  name="REG[ASAL_SURAT]" />
			</div> 
				
			 <div class="form-group SuratKeluar" style="display:none;">
				  <label for="exampleInputEmail1">Untuk</label>
				  <input type="text" class="form-control col-md-12"  name="REG[TUJUAN_SURAT]" />
			  </div> 

			  <div class="form-group SuratMasuk" style="display:none;">
				  <label for="exampleInputEmail1">Copy/Asli/Tembusan</label>
				  <?= form_dropdown('REG[TIPE_SURAT]', $type, '', 'class="form-control col-md-5"'); ?>
			  </div> 	
			  
			  <div class="form-group lembur" style="display:none;">
			  <label>Tanggal Lembur</label>
				<div class="input-group date" id="reservationdate2" data-target-input="nearest" >
					<input type="text" class="form-control datetimepicker-input col-md-4" name="REG[TANGGAL_LEMBUR]"  data-target="#reservationdate2"/>
					<div class="input-group-append" data-target="#reservationdate2" data-toggle="datetimepicker">
						<div class="input-group-text"><i class="fa fa-calendar"></i></div>
					</div>
				</div>
			</div>	
			  
			  <div class="form-group" id="uker" style="display:none;">
				  <label for="exampleInputEmail1">Unit Kerja</label>
				  <?= form_dropdown('REG[UNIT_KERJA]', $dispo, '', 'class="form-control col-md-5"  wajib="yes"'); ?>
			  </div>	
				
			   <div class="form-group" >
				  <label for="exampleInputEmail1">Disposisi</label>
				  <?= form_dropdown('REG[DISPOSISI_SURAT]', $dispo, '', 'class="form-control col-md-5"  wajib="yes"'); ?>
			  </div>
					
			  <div class="form-group">
				<label>Out</label>
				<div class="input-group date" id="timepicker" data-target-input="nearest">
				  <input type="text" name="REG[JAM_OUT]" class="form-control datetimepicker-input col-md-4" data-target="#timepicker"/>
				  <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
					  <div class="input-group-text"><i class="far fa-clock"></i></div>
				  </div>
				  </div>
				<!-- /.input group -->
			  </div>	
					
			  <div class="form-group" >
				  <label for="exampleInputEmail1">Note</label>
				  <input type="text" class="form-control col-md-12"  name="REG[NOTE_SURAT]"/>
			  </div>


                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" onclick="save_post('#fdokupload_'); return false;">Submit</button> &nbsp;<br/><p> <div class="msgtitle_">&nbsp;</div>
                </div>
              </form>
            </div>
            <!-- /.card -->
		</div>
	</div>	
	
	<script>
	
	
	$("#jnsSurat").change(function(){
		if($(this).val()=='1'){
			$(".SuratMasuk").show();
			$(".SuratKeluar").hide();
			
		}else if($(this).val()=='2'){			
			$(".SuratMasuk").hide();
			$(".SuratKeluar").show();
		}else{
			$(".SuratMasuk").hide();
			$(".SuratKeluar").hide();
		}		
	});	
	
	$("#jnsReg").change(function(){
		if($(this).val()=='P'){
			$(".ap1").hide();
			$(".lembur").hide();
			$(".pembayaran").show();
			$("#nosurat").hide();
			$("#namapemohon").show();
			$("#uker").hide();
		}else if($(this).val()=='L'){
			$(".pembayaran").hide();
			$(".lembur").show();
			$("#namapemohon").show();
			$("#nosurat").hide();	
			$(".ap1").show();
			$("#uker").show();
			$(".spkl").show();	
		}else{
			$("#namapemohon").hide();
			$(".lembur").hide();
			$("#uker").hide();
			$("#nosurat").show();
			$(".pembayaran").hide();	
			$(".ap1").show();
		}		
	});	
	
	$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'YYYY-MM-DD'
    });
	
	$('#reservationdate2').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    //Date range picker
    $('#reservation').daterangepicker({
		  format: 'YYYY-MM-DD'
	})
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

	  $('#reservationdate').datetimepicker({
        format: 'L'
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });
	
	$("#selectdriver").change(function(){
				var forms = $(this).val().split('||'); 
				$('#nopol').val(forms[1].toString());
				
			});

  })
</script>