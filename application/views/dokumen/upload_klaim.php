<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Upload Klaim</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="fdokupload_" action="<?= site_url();?>/home/registrasi_act/saveuploadklaim" method="post">
                <div class="card-body">

				<div class="form-group">
                    <label for="exampleInputEmail1">Jenis Registrasi</label>
                   <?= form_dropdown('REG_JENIS_DOKUMEN', array('101' => 'Pembayaran'), '', 'id="jnsproduk" class="form-control" style="width:520px;" wajib="yes"'); ?>
                </div>

				<div class="form-group">
					<label>Tanggal</label>
					<div class="input-group date" data-target-input="nearest" style="width:520px;">
						<input type="text" class="form-control" name="TANGGAL_PENGAJUAN" value="<?= date('Y-m-d');?>" style="width:300px;" data-target="#reservationdate" readonly/>
						<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
							<div class="input-group-text"><i class="fa fa-calendar"></i></div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label" for="form-field-1">File Upload excel Klaim<span style="color:red">*</span></label>
					<div class="controls">
						<input style="display:none;" class="inputFiles" type="file" name="01" id="upload01" />
						<input type="hidden"name="PATH" id="uppath01">
						<button type="button" id="btn01" uploadid="01" class="btnBrowse btn btn-small btn-success">Browse <i class="icon-search icon-on-right bigger-110"></i></button>
					</div>
				</div>
                  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" onclick="save_post('#fdokupload_'); return false;">Submit</button> &nbsp;<br/><p> <div class="msgtitle_">&nbsp;</div>
                </div>
              </form>
            </div>
            <!-- /.card -->
		</div>
	</div>	
	<script type="text/javascript" src="<?= base_url();?>assets/js/jquery.ajaxfileupload.js?v=<?=date('YmdHis');?>"></script>
	<script>
	$(document).ready(function() {
			
			$('.btnBrowse').click(function(){
				var id = $(this).attr('uploadid').toString();
				$('#upload'+id).click();
			});
			var interval;
			function applyAjaxFileUpload(element, nama){
				$(element).AjaxFileUpload({
					action: site + '/proses/upload_dok/<?= date("Ymd");?>/' + nama,
					onChange: function(filename) {
						// Create a span element to notify the user of an upload in progress
						var $span = $("<span />")
							.attr("class", $(this).attr("id"))
							.text("Uploading")
							.insertAfter($(this));
						
						$('#btn'+nama).hide();
						//$(this).remove();

						interval = window.setInterval(function() {
							var text = $span.text();
							if (text.length < 13) {
								$span.text(text + ".");
							} else {
								$span.text("Uploading");
							}
						}, 200);
					},
					onSubmit: function(filename) {
						return true;
					},
					onComplete: function(filename, response) {
						
						var chkname = filename.substr(0, 5);
						
						if(chkname=='PP.JJ'){
							filename = filename.replace('PP.JJ.','PP_JJ_');
						}else if(chkname=='PK.JJ'){
							filename = filename.replace('PK.JJ.','PK_JJ_');
						}else if(chkname=='PKB.J'){
							filename = filename.replace('PKB.JJ.','PKB_JJ_');
						}
						
						window.clearInterval(interval);
						var $span = $("span." + $(this).attr("id")).text(""),
							$fileInput = $("<input />")
								.attr({
									type: "file",
									name: $(this).attr("name"),
									id: $(this).attr("id"),
									style: "display:none"
								});

						if (typeof(response.error) === "string") {
							$span.replaceWith($fileInput);
							applyAjaxFileUpload($fileInput);
							alert(response.error);
							return;
						}
						
						$("<a />")
							.attr("href", isUrl + 'dat/<?= date("Ymd");?>/' + '<?= date("Ymd")."_";?>' + filename.split(' ').join('_'))
							.attr("target", "blank_")
							.attr("class", "btn btn-mini btn-primary")
							.html('<i class="icon-search"></i> Preview')
							.appendTo($span);
						$("<span />").html('&nbsp; &nbsp;').appendTo($span);
						$("<a />")
							.attr("href", "#")
							.css('color', 'red')
							.attr("class", "btn btn-mini btn-danger")
							.html('<i class="icon-trash"></i> <span  style="color:white;">Hapus</span>')
							.bind("click", function(e) {
								//$span.replaceWith($fileInput);
								$span.remove();
								$('#btn'+nama).show();
								//applyAjaxFileUpload($fileInput);
							})
							.appendTo($span);
						$("#uppath"+nama).val(isUrl + 'dat/<?= date("Ymd")?>/' + '<?= date("Ymd")."_"; ?>' + filename.split(' ').join('_'));
					}
				});
			}
			
			applyAjaxFileUpload("#upload01", "01");
		});
</script>