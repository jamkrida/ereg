<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Registrasi CMS</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="fdokupload_" action="<?= site_url();?>/home/registrasi_act/saveregcms" method="post">
                <div class="card-body">
				 
                <div class="form-group">
                  <label>Tanggal</label>
                    <div class="input-group date" id="reservationdate" data-target-input="nearest" >
                        <input type="text" class="form-control datetimepicker-input col-md-4" name="TANGGAL_CMS"  data-target="#reservationdate"/>
                        <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>
				
				
				<div class="form-group">
                    <label>Jam</label>

                    <div class="input-group date" id="timepicker" data-target-input="nearest">
                      <input type="text" name="REG[JAM_CMS]" class="form-control datetimepicker-input col-md-4" data-target="#timepicker"/>
                      <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
                          <div class="input-group-text"><i class="far fa-clock"></i></div>
                      </div>
                      </div>
                    <!-- /.input group -->
                  </div>

                <div class="form-group" >
                    <label for="exampleInputEmail1">Deskripsi</label>
                    <input type="text" class="form-control col-md-12"  name="REG[DESKRIPSI_CMS]" wajib="yes"/>
                   </div>
				  
                <div class="form-group" >
                          <label for="exampleInputEmail1">Debit</label>
                          <input type="text" class="form-control col-md-12"  name="REG[DEBIT_CMS]" wajib="yes"/>
                </div>
				   
                <div class="form-group" >
                      <label for="exampleInputEmail1">Kredit</label>
                      <input type="text" class="form-control col-md-12"  name="REG[KREDIT_CMS]" wajib="yes"/>
                  </div>

                  <div class="form-group" >
                      <label for="exampleInputEmail1">Saldo</label>
                      <input type="text" class="form-control col-md-12"  name="REG[SALDO_CMS]" wajib="yes"/>
                  </div> 
      
                  <div class="form-group" >
                      <label for="exampleInputEmail1">Keterangan</label>
                  <?= form_dropdown('REG[KETERANGAN_CMS]', array('IJP'=> 'IJP', 
															'IJK'=> 'IJK',
                              'BD' => 'BD', 
															'BA' => 'BA', 
															'FBI' => 'FBI',
															'JG' => 'JG', 
															'KASBON' => 'KASBON', 
															'KLAIM' => 'KLAIM', 
															'KLAIM_COGAR' => 'KLAIM COGAR', 
															'KOR' => 'KOR',
															'PB' =>  'PB', 
															'PBY' => 'PBY', 
															'PEMBELIAN_SBN' => 'PEMBELIAN SBN',
															'PENCAIRAN_DEPOSITO' => 'PENCAIRAN DEPOSITO', 
															'PENEMPATAN_DEPOSITO' => 'PENEMPATAN DEPOSITO', 
															'PENEMPATAN_REKSA_DANA' => 'PENEMPATAN REKSA DANA', 
															'PENGEMBALIAN_KELEBIHAN_KLAIM' => 'PENGEMBALIAN KELEBIHAN KLAIM', 
															'PENJUALAN_REKSA_DANA' => 'PENJUALAN REKSA DANA', 
															'PINBUK_BNI' => 'PINBUK BNI', 
															'PINBUK_MANDIRI' => 'PINBUK MANDIRI', 
															'PJ' => 'PJ',
															'PK'=> 'PK', 
															'REAS' => 'REAS', 
															'SUBROGASI' => 'SUBROGASI', 
															'TOP_UP' => 'TOP UP'), '', 'id="namauser" class="form-control col-md-5"  wajib="yes"'); ?>
                    </div>

                    <div class="form-group" >
                      <label for="exampleInputEmail1">Nomor Rekening</label>
                      <input type="text" class="form-control col-md-12"  name="REG[NOREK_CMS]" wajib="yes"/>
                  </div>

                  <div class="form-group" >
                      <label for="exampleInputEmail1">Nama Bank Cabang</label>
                      <input type="text" class="form-control col-md-12"  name="REG[BANK_CMS]" wajib="yes"/>
                  </div>


                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" onclick="save_post('#fdokupload_'); return false;">Submit</button> &nbsp;<br/><p> <div class="msgtitle_">&nbsp;</div>
                </div>
              </form>
            </div>
            <!-- /.card -->
		</div>
	</div>	
	
	<script>
	
	$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });
    //Date range picker
    $('#reservation').daterangepicker({
		  format: 'YYYY-MM-DD'
	})
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

	  $('#reservationdate').datetimepicker({
        format: 'L'
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });
	
	$("#selectdriver").change(function(){
				var forms = $(this).val().split('||'); 
				$('#nopol').val(forms[1].toString());
				
			});

  })
</script>