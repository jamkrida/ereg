<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
			
            .autocomplete-suggestions {
                border: 1px solid #999;
                background: #FFF;
                overflow: auto;
            }
            .autocomplete-suggestion {
                padding: 2px 5px;
                white-space: nowrap;
                overflow: hidden;
            }
            .autocomplete-selected {
                background: #F0F0F0;
            }
            .autocomplete-suggestions strong {
                font-weight: normal;
                color: #3399FF;
            }
            .autocomplete-group {
                padding: 2px 5px;
            }
            .autocomplete-group strong {
                display: block;
                border-bottom: 1px solid #000;
            }
</style>
<div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Registrasi Keuangan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="fdokupload_" action="<?= $act;?>" method="post">
                <div class="card-body">
                 
				 <div class="form-group">
                    <label for="exampleInputEmail1">Jenis Registrasi</label>
                   <?= form_dropdown('REG[REG_JENIS_DOKUMEN]', $jnsreg, '', 'id="jnsproduk" class="form-control" style="width:520px;" wajib="yes"'); ?>
                  </div>
				  
				  <div class="form-group">
					  <label>Tanggal</label>
						<div class="input-group date" data-target-input="nearest" style="width:520px;">
							<input type="text" class="form-control" name="REG[TANGGAL_PENGAJUAN]" value="<?= date('Y-m-d');?>" style="width:300px;" data-target="#reservationdate" readonly/>
							<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
								<div class="input-group-text"><i class="fa fa-calendar"></i></div>
							</div>
						</div>
					</div>
				  
				  <div class="form-group kasbon" style="display:none;">
                    <label for="exampleInputEmail1">No. Reg Kasbon</label>
                     <?= form_dropdown('', $regnomor, '', 'id="selectkasbon" class="form-control select2" style="width:520px;" wajib="yes"'); ?>
					<input type="hidden" name="REG[REG_NOMOR_KASBON]" id="nomorkasbon">
				  </div>
				  
				  <div class="form-group kasbon"  style="display:none;">
                    <label for="exampleInputEmail1">Tanggal Kasbon</label>
                    <input type="text" class="form-control"  name="REG[TANGGAL_KASBON]" style="width:520px;" id="tglkasbon" readonly/>
                  </div>
				  
				  
				  <div class="form-group kasbon"  style="display:none;">
                    <label for="exampleInputEmail1">Uraian Kasbon</label>
                    <textarea class="form-control"  style="width:520px;" id="perihal123" readonly></textarea>
                  </div>
                 
				  <!--
				   <div class="form-group">
                    <label for="exampleInputEmail1">Pemohon</label>
                    <input type="text" class="form-control" id="proses" name="REG[REG_PEMOHON]" style="width:520px;" placeholder="Nama Pemohon" wajib="yes"/>
                  </div>
				  -->
				  <div class="form-group" id="penerima" style="display:none;">
                    <label for="exampleInputEmail1">Penerima</label>
                    <input type="text" class="form-control"  name="REG[REG_PENERIMA]" style="width:520px;" placeholder="Nama Penerima" wajib="yes"/>
                  </div>
				  
				   <div class="form-group norek" style="display:none;">
                    <label for="exampleInputEmail1">Nama Bank & Nomor Rekening</label>
                    <input type="text" class="form-control"  name="REG[REG_NOMOR_REKENING]" id="norekid" style="width:520px;" placeholder="Nomor Rekening" wajib="yes"/>
                   </div>
				  
				   <div class="form-group">
                    <label for="exampleInputEmail1">Jumlah</label>
                    <input type="text" class="form-control example1" id="rupiah" name="REG[REG_JUMLAH]" style="width:520px;" placeholder="Jumlah" wajib="yes"/>
                  </div>
				  
				   <div class="form-group kasbon" style="display:none;">
                    <label for="exampleInputEmail1">Jumlah Pemakaian</label>
                    <input type="text" class="form-control example1" id="pemakaian" name="REG[REG_PEMAKAIAN]" style="width:520px;" placeholder="Pemakaian" wajib="yes"/>
                  </div>
				  
				  <div class="form-group">
                    <label for="exampleInputEmail1">Rincian Harga</label>
					<table  width ="87%">
			
						<tr class="spec">
							<td width="41%">
								 <input type="text" class="form-control example1" name="REG_DETIL[REG_DETIL_INPUT][]" style="width:450px;"  placeholder="Rincian Pembelian">
							</td>
							<td width="41%">
								 <input type="text" class="form-control example1 angkanumerik" data-a-dec="," data-a-sep="." name="REG_DETIL[REG_DETIL_NOMINAL][]" placeholder="Nominal contoh : 2.000.000" style="width:450px;">
							</td>
							<td width="4%">
								<a class="btn btn-block btn-outline-primary" id="mycom" ><i class="fas fa-plus-square"></i></a>
							</td>
							
						</tr>
				
					</table>
					<div id="concoms"></div>
				  </div>
				  
				  <div class="form-group">
                    <label for="exampleInputEmail1">Perihal</label>
					<div class="mb-3">
					<textarea class="textarea"  name="REG[REG_PERIHAL]" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
					</div>
				  </div>
				  
				  <div class="form-group">
                    <label for="exampleInputEmail1">Catatan</label>
					<div class="mb-3">
					<textarea class="textarea" name="REG[REG_CATATAN]" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
					</div>
				  </div>
				  <br/>
				   <h5>&bull; <b>Penandatangan Pemohon</b></h5>
				   <hr>
				   <div class="row">
					   <div class="col-12 col-sm-12">
						<div class="form-group">
						  <label>Staff</label>
						   <input type="text" class="form-control" id="proses" name="REG[TTD_STAFF_PEMOHON]" value="<?php echo $this->session->userdata("USER_FULLNAME");?>" placeholder="Nama Staff" wajib="yes"/>
						</div>
						
					  </div>
					  <div class="col-12 col-sm-12">
					   <div class="form-group">
						<label for="exampleInputFile">Unit Kerja</label>
						<?= form_dropdown('REG[REG_UNIT_KERJA]', $departemen, '', 'id="proses" class="form-control" style="width:520px;" wajib="yes"'); ?>
					  </div>
					  </div>
					  
					  <div class="col-12 col-sm-4">
						<div class="form-group">
						  <label>Nama Atasan</label>
						   <?= form_dropdown('REG[TTD_MANAJER_PEMOHON]', $atasan, '', 'class="form-control select2" style="width: 100%;" wajib="yes"'); ?>
						  
						</div>
						<!-- /.form-group -->
					  </div>
					  <!-- /.col -->
					  <div class="col-12 col-sm-4">
						<div class="form-group">
						  <label>Jabatan</label>
						  <div class="select2-purple">
							 <?= form_dropdown('REG[TTD_JABATAN_MANAJER_PEMOHON]', $jabatan, '', 'class="form-control select2" style="width: 100%;" wajib="yes"'); ?>
						  </div>
						</div>
						<!-- /.form-group -->
					  </div>
					  <!-- /.col -->
					   <div class="col-12 col-sm-4">
						<div class="form-group">
						  <label>Status</label>
						  <div class="select2-purple">
							 <select class="form-control select2" name="REG[STATUS_TTD_MANAJER_PEMOHON]" style="width: 100%;">
							 <option >&nbsp;</option>
							<option value="Plt">Plt</option>
							<option value="Plh">Plh</option>
						  </select>
						  </div>
						</div>
						<!-- /.form-group -->
					  </div>
					  <!-- /.col -->
					  
					</div>
					<!-- /.row -->
					<br/>
					<div id="approval" style="display:none;">
					<h5>&bull; <b>Penandatangan Menyetujui</b></h5>
					<hr>
					   <div class="row">
						   <div class="col-12 col-sm-12">
							<div class="form-group">
							  <label>Staff Anggaran</label>
							  <input type="text" class="form-control" id="proses" name="REG[TTD_STAFF_APPROVAL]"  placeholder="Nama Staff" />
							</div>
							<!-- /.form-group -->
						  </div>	
						  <div class="col-12 col-sm-4">
							<div class="form-group">
							  <label>Kadiv</label>
							   <?= form_dropdown('REG[TTD_MANAJER_APPROVE]', $manajer, '', 'class="form-control select2" style="width: 100%;" wajib="yes"'); ?>
							</div>
							<!-- /.form-group -->
						  </div>
						  <!-- /.col -->
						  <div class="col-12 col-sm-4">
							<div class="form-group">
							  <label>Jabatan</label>
							  <div class="select2-purple">
								  <?= form_dropdown('REG[TTD_JABATAN_MANAJER_APPROVE]', $jabatan, '', 'class="form-control select2" style="width: 100%;" wajib="yes"'); ?>
							  </div>
							</div>
							<!-- /.form-group -->
						  </div>
						  <!-- /.col -->
						   <div class="col-12 col-sm-4">
							<div class="form-group">
							  <label>Status</label>
							  <div class="select2-purple">
								 <select class="form-control select2" name="REG[STATUS_TTD_MANAJER_APPROVE]" style="width: 100%;">
								 <option >&nbsp;</option>
								<option value="Plt">Plt</option>
								<option value="Plh">Plh</option>
							  </select>
							  </div>
							</div>
							<!-- /.form-group -->
						  </div>
						  <!-- /.col -->
						  
						</div>
						<!-- /.row -->
					</div>	
					<br/>
					<h5>&bull; <b>Penandatangan Pembayaran</b></h5>
				   <hr>
				   <div class="row">
					<!--		
					  <div class="col-12 col-sm-12">
						<div class="form-group">
						  <label>Kasir</label>
						  <input type="text" class="form-control" id="proses" name="REG[NAMA_KASIR]"  value="Fauziah" wajib="yes"/>
						</div>
					  </div>	
					  -->
					  <div class="col-12 col-sm-12">
						<div class="form-group">
						  <label>Staff Keuangan</label>
						  <?= form_dropdown('REG[STAFF_KEU]', $staffkeu, '29', 'class="form-control select2" style="width: 100%;" wajib="yes"'); ?>
						</div>
						<!-- /.form-group -->
					  </div>
					 
					  
					</div>
					<!-- /.row -->
                 
				 
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" onclick="save_post('#fdokupload_'); return false;">Submit</button> &nbsp;<br/><p> <div class="msgtitle_">&nbsp;</div>
                </div>
              </form>
            </div>
            <!-- /.card -->
		</div>
	</div>	
	<script type="text/javascript" src="<?= base_url();?>assets/js/jquery.ajaxfileupload.js?v=<?=date('YmdHis');?>"></script>
	<script>
		
	
	var rupiah = document.getElementById('rupiah');
		rupiah.addEventListener('keyup', function(e){
			// tambahkan 'Rp.' pada saat form di ketik
			// gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
			rupiah.value = formatRupiah(this.value, 'Rp. ');
		});
		
		var pemakaian = document.getElementById('pemakaian');
		    pemakaian.addEventListener('keyup', function(e){
			// tambahkan 'Rp.' pada saat form di ketik
			// gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
			pemakaian.value = formatPemakaian(this.value, 'Rp. ');
		});
 
		/* Fungsi formatRupiah */
		function formatRupiah(angka, prefix){
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
		}
		
		function formatPemakaian(angka, prefix){
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			pemakaian     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				pemakaian += separator + ribuan.join('.');
			}
 
			pemakaian = split[1] != undefined ? pemakaian + ',' + split[1] : pemakaian;
			return prefix == undefined ? pemakaian : (pemakaian ? '' + pemakaian : '');
		}
	
	$(function () {
    //Initialize Select2 Elements
    $('.select2').select2();

	 $('.textarea').summernote({
		
		 toolbar: [
			  ['style', ['style']],
			  ['font', ['bold', 'underline', 'clear']],
			  ['fontname', ['fontname']],
			  ['fontsize', ['fontsize']],
			  ['color', ['color']],
			  ['para', ['ul', 'ol', 'paragraph']],
			  ['table', ['table']],
			  //['insert', ['link', 'picture', 'video']],
			  ['view', ['fullscreen', 'codeview', 'help']],
			]
	 });
    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
	
	

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  });
	
	$(document).ready(function() {
		
			$( "#norekid" ).autocomplete({
				serviceUrl: "<?= site_url();?>/proses/get_norek",   // Kode php untuk prosesing data
				dataType: "JSON",           // Tipe data JSON
				onSelect: function (suggestion) {
					$( "#norekid" ).val("" + suggestion.norekid);
				}
			});
			
			
			$("#selectkasbon").change(function(){
				var forms = $(this).val().split('||'); 
				$('#tglkasbon').val(forms[0].toString());
				//alert(forms[1].toString()); return false;
				$('#perihal123').val(forms[1].toString());
				$('#nomorkasbon').val(forms[2].toString());
			});
			
			$("#jnsproduk").change(function(){
				if($(this).val()=='101'){
					$("#penerima").show();
					$(".kasbon").hide();
					$(".norek").show();
					$("#approval").show();
				}else if($(this).val()=='102'){
					$("#penerima").hide();
					$(".kasbon").hide();
					$(".norek").show();
					$("#approval").show();
				}else{
					$("#penerima").hide();
					$("#approval").hide();
					$(".kasbon").show();
					$(".norek").hide();
				}		
			});	
			
			 $('#reservationdate').datetimepicker({
				format: 'L'
			});
			
			$('.btnBrowse').click(function(){
				var id = $(this).attr('uploadid').toString();
				$('#upload'+id).click();
			});
			var interval;
			function applyAjaxFileUpload(element, nama){
				$(element).AjaxFileUpload({
					action: site + '/proses/upload_dok/<?= date("Ymd");?>/' + nama,
					onChange: function(filename) {
						// Create a span element to notify the user of an upload in progress
						var $span = $("<span />")
							.attr("class", $(this).attr("id"))
							.text("Uploading")
							.insertAfter($(this));
						
						$('#btn'+nama).hide();
						//$(this).remove();

						interval = window.setInterval(function() {
							var text = $span.text();
							if (text.length < 13) {
								$span.text(text + ".");
							} else {
								$span.text("Uploading");
							}
						}, 200);
					},
					onSubmit: function(filename) {
						return true;
					},
					onComplete: function(filename, response) {
						//alert('sii'); return false;
						window.clearInterval(interval);
						var $span = $("span." + $(this).attr("id")).text(""),
							$fileInput = $("<input />")
								.attr({
									type: "file",
									name: $(this).attr("name"),
									id: $(this).attr("id"),
									style: "display:none"
								});

						if (typeof(response.error) === "string") {
							$span.replaceWith($fileInput);
							applyAjaxFileUpload($fileInput);
							alert(response.error);
							return;
						}
						
						$("<a />")
							.attr("href", isUrl + 'dat/<?= date("Ymd");?>/' + '<?= date("Ymd")."_";?>' + filename.split(' ').join('_'))
							.attr("target", "blank_")
							.attr("class", "btn btn-mini btn-primary")
							.html('<i class="icon-search"></i> Preview')
							.appendTo($span);
						$("<span />").html('&nbsp; &nbsp;').appendTo($span);
						$("<a />")
							.attr("href", "#")
							.css('color', 'red')
							.attr("class", "btn btn-mini btn-danger")
							.html('<i class="icon-trash"></i> <span  style="color:white;">Hapus</span>')
							.bind("click", function(e) {
								//$span.replaceWith($fileInput);
								$span.remove();
								$('#btn'+nama).show();
								//applyAjaxFileUpload($fileInput);
							})
							.appendTo($span);
						$("#uppath"+nama).val(isUrl + 'dat/<?= date("Ymd")?>/' + '<?= date("Ymd")."_"; ?>' + filename.split(' ').join('_'));
					}
				});
			}
			
			applyAjaxFileUpload("#upload01", "01");
		});
		
		$(document).ready(function() {
		
			$('.angkanumerik').autoNumeric('init','commaDecimalCharDotSeparator');
		});	
		
		
		(function($){
				 
			  $.fn.addCom = function(){
				
				$( ".angkanumerik" ).keydown(function() {
				  $('.angkanumerik').autoNumeric('init');
				});
				var mycom = "<table width='87%'>"+
				 "  <tr>"+
				 "     <td width='41%'><input type='text' class='form-control' name='REG_DETIL[REG_DETIL_INPUT][]' style='width:450px;'></td>"+
				 "     <td width='41%'><input type='text' class='form-control angkanumerik angkanum' data-a-dec=',' data-a-sep='.' name='REG_DETIL[REG_DETIL_NOMINAL][]' style='width:450px;'></td>"+
				 "     <td width='4%'><button class='btn btn-danger btn-mini'><i class='fas fa-minus-square'></i></button></td>"+
				 "  </tr>"+
				 "</table>";
				 mycom = $("<div>"+mycom+"</div>");
				$("button", $(mycom)).click(function(){ 
					
					$(this).parent().parent().remove(); 
				});

				$(this).append(mycom);
						 
				  };
			})(jQuery);		

		$(function(){
			$("#mycom").click(function(){
				
				$('.angkanumerik').autoNumeric('init');
				$("#concoms").addCom();
				return false;
			});
			
		});
		
</script>