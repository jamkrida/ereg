<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script>
	function redir(namauser, reservation){
		var site = '<?= site_url();?>/home/cms/get_report';
		var namauser = $(namauser).val();
		var reservation = $(reservation).val();
		arrtgl = reservation.split(' - ');
		tglmulai = arrtgl[0].split("/");
		tglmulai = tglmulai[2]+'-'+tglmulai[0]+'-'+tglmulai[1]; 
		
		tglselesai = arrtgl[1].split("/");
		tglselesai = tglselesai[2]+'-'+tglselesai[0]+'-'+tglselesai[1];
		
		//var jenis = 'total_dok';
		location.href = site + "/" +  namauser + "/" + tglmulai + "/" + tglselesai;
		
	}
</script>
<div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Cash Flow CMS</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="<?= site_url();?>/home/get_laporan/excel" method="post">
                <div class="card-body">
                 
				 <div class="form-group">
                    <label for="exampleInputEmail1">Nama</label>
                   <?= form_dropdown('JENIS_DOKUMEN', array('IJP'=> 'IJP', 
															'IJK'=> 'IJK',
                              'BD' => 'BD', 
															'BA' => 'BA', 
															'FBI' => 'FBI',
															'JG' => 'JG', 
															'KASBON' => 'KASBON', 
															'KLAIM' => 'KLAIM', 
															'KLAIM_COGAR' => 'KLAIM COGAR', 
															'KOR' => 'KOR',
															'PB' =>  'PB', 
															'PBY' => 'PBY', 
															'PEMBELIAN_SBN' => 'PEMBELIAN SBN',
															'PENCAIRAN_DEPOSITO' => 'PENCAIRAN DEPOSITO', 
															'PENEMPATAN_DEPOSITO' => 'PENEMPATAN DEPOSITO', 
															'PENEMPATAN_REKSA_DANA' => 'PENEMPATAN REKSA DANA', 
															'PENGEMBALIAN_KELEBIHAN_KLAIM' => 'PENGEMBALIAN KELEBIHAN KLAIM', 
															'PENJUALAN_REKSA_DANA' => 'PENJUALAN REKSA DANA', 
															'PINBUK_BNI' => 'PINBUK BNI', 
															'PINBUK_MANDIRI' => 'PINBUK MANDIRI', 
															'PJ' => 'PJ',
															'PK'=> 'PK', 
															'REAS' => 'REAS', 
															'SUBROGASI' => 'SUBROGASI', 
															'TOP_UP' => 'TOP UP'), '', 'id="namauser" class="form-control col-md-5"  wajib="yes"'); ?>
                  </div>
				  
				  <div class="form-group">
                  <label>Tanggal</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control float-right  col-md-5" id="reservation" name="TANGGAL" >
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
				  
				  
   
				 
                </div>
                <!-- /.card-body -->

                 <div class="card-footer">
                  <a href="javascript:;" class="btn btn-primary" id="ok_" onclick="redir('#namauser', '#reservation');">
					Proses 
				  </a>
				 
                </div>
              </form>
            </div>
            <!-- /.card -->
		</div>
	</div>	
	
	<script>
	
	$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    
    $('[data-mask]').inputmask()

   
    //Date range picker
    $('#reservation').daterangepicker({
		  format: 'YYYY-MM-DD'
	})
    //Date range picker with time picker
   
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>