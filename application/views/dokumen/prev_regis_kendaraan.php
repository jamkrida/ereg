<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Registrasi Kendaraan</h3>
              </div>
           
              <form role="form" id="fdokupload_" action="<?= site_url();?>/home/registrasi_act/updatedriver" method="post">
                <div class="card-body">
                <input type="hidden" name="regid" value="<?= $datas['REG_ID']; ?>">
				        <div class="form-group">
                    <label for="exampleInputEmail1">Nama Pemohon</label>
                    <?= form_dropdown('REG[NAMA_PEMOHON]', $pemohon, $datas['NAMA_PEMOHON'], 'class="form-control select2 col-md-8" disabled '); ?>
                 </div>
				 
				         <div class="form-group">
                    <label for="exampleInputEmail1">Unit Kerja</label>
                   <?= form_dropdown('REG[UNIT_KERJA]', $departemen, $datas['UNIT_KERJA'], 'class="form-control select2 col-md-8" disabled'); ?>
                  </div>
				  
                <div class="form-group">
                          <label for="exampleInputEmail1">Atasan Pemohon</label>
                        <?= form_dropdown('REG[ATASAN_PEMOHON]', $manajer, $datas['ATASAN_PEMOHON'], 'class="form-control select2 col-md-8" disabled'); ?>
                </div>
				  
                <div class="form-group">
                    <label>Tanggal</label>
                      <div class="input-group date" data-target-input="nearest" >
                          <input type="text" class="form-control datetimepicker-input col-md-4" name="REG[TANGGAL_PERMOHONAN]" value="<?= $datas['TANGGAL_PERMOHONAN']; ?>" disabled data-target="#reservationdate"/>
                          <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                          </div>
                      </div>
                  </div>
				
				
			        	<div class="form-group">
                    <label>Waktu Penggunaan</label>

                    <div class="input-group date" id="timepicker" data-target-input="nearest">
                      <input type="text" name="REG[WAKTU_PERMOHONAN]" value="<?= $datas['WAKTU_PERMOHONAN']?>" class="form-control datetimepicker-input col-md-4" data-target="#timepicker" disabled/>
                      <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
                          <div class="input-group-text"><i class="far fa-clock"></i></div>
                      </div>
                      </div>
                    <!-- /.input group -->
                  </div>
				  
				         <div class="form-group" >
                    <label for="exampleInputEmail1">Keperluan</label>
                    <input type="text" class="form-control col-md-12"  name="REG[KEPERLUAN]" value = "<?= $datas['KEPERLUAN']?>" disabled/>
                   </div>
				   
				          <div class="form-group" >
                    <label for="exampleInputEmail1">Tujuan</label>
                    <input type="text" class="form-control col-md-12"  name="REG[TUJUAN]" value = "<?= $datas['TUJUAN']?>" disabled/>
                   </div>
					
                  <div class="form-group">
                      <label for="exampleInputEmail1">Jenis Kendaraan</label>
                    <?= form_dropdown('REG[JENIS_KENDARAAN]', $jns_kendaraan, '', 'id ="selectjnskendaraan" class="form-control col-md-5" wajib="yes"'); ?>
                  </div> 
            
                  <div class="form-group jkl" style="display:none;" >
                    <label for="exampleInputEmail1">Keterangan Kendaraan</label>
                    <input type="text" class="form-control col-md-5" id="jnskendaraan"  name="REG[JENIS_KENDARAAN_DESC]" placeholder="Grab/Gojek dll - mobil/motor"/>
      
                 </div>

				         <div class="form-group drv">
                    <label for="exampleInputEmail1">Driver</label>
                   <?= form_dropdown('REG[DRIVER_ID]', $driver, '', 'id ="selectdriver" class="form-control select2 col-md-5" '); ?>
                 </div>	
				   
				       <div class="form-group nopolis" >
                    <label for="exampleInputEmail1">No Polisi</label>
                    <input type="text" class="form-control col-md-5" id="nopol"  name="REG[NO_KENDARAAN]" wajib="yes"/>
                   </div>	
				  
                </div>
              

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" onclick="save_post('#fdokupload_'); return false;">Submit</button> &nbsp;<br/><p> <div class="msgtitle_">&nbsp;</div>
                </div>

              </form>
            </div>
            <!-- /.card -->
		</div>
	</div>	
	
	<script>
	
	$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });
    //Date range picker
    $('#reservation').daterangepicker({
		  format: 'YYYY-MM-DD'
	})
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

	  $('#reservationdate').datetimepicker({
        format: 'L'
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });
	
   $("#selectjnskendaraan").change(function(){
      var lab = $(this).val();
      if(lab == 2){
        $('.jkl').show();
        $('.drv').hide();
        $('#selectdriver').val('');
        $('.nopolis').hide();
        $('#nopol').hide();
        $('#nopol').val('');
      }else{
        $('.nopolis').show();
        $('#nopol').show();
        $('.jkl').hide();
        $('.drv').show();
        $('#jnskendaraan').val('');
      }
   }); 

	$("#selectdriver").change(function(){
				var forms = $(this).val().split('||'); 
				$('#nopol').val(forms[1].toString());
				
			});

  })
</script>