<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	
	<div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Laporan Cash FLow CMS</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
             
               <div class="card-footer">
                  <a href="<?= site_url();?>/prints/report_cms/<?= $uid;?>/<?= $awal;?>/<?= $akhir;?>" target="_blank" class="btn btn-success"><i class="fas fa-print"></i> Cetak PDF</a>
                </div>
				
				<div class="card-body">
				 <div class="table-responsive table-hover">
				  <table class="table m-0">
                  <thead>                  
                    <tr>
                     
                      <th>TANGGAL</th>
                      <th>DEKRIPSI</th>
                      <th>DEBIT</th>
					  <th>KREDIT</th>
					  <th>KETERANGAN</th>
					  <th>NOMOR REKENING</th>
					  
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
						foreach($arrisi as $datas){
							
						$db = str_replace('.00', '', $datas['DEBIT']);
						$db1 = str_replace(',', '', $db);
						$totaldb += $db1;	
						
						$kd = str_replace('.00', '', $datas['KREDIT']);
						$kd1 = str_replace(',', '', $kd);
						$totalkd += $kd1;	

					?>
					
					<tr>
                      <td><?= $datas['TANGGAL'];?></td>
                      <td><?= $datas['DESKRIPSI'];?></td>
					  <td><?= $datas['DEBIT'];?></td>
					  <td><?= $datas['KREDIT'];?></td>
                      <td><?= $datas['KETERANGAN'];?></td>
					  <td><?= $datas['NOMOR_REKENING'];?></td>
                    </tr>
                    <?php
						}
					?>
					<tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
					  <td><?= number_format(round($totaldb),2,',','.');?></td>
					  <td><?= number_format(round($totalkd),2,',','.');?></td>
                      <td>&nbsp;</td>
					  <td>&nbsp;</td>
                    </tr>
                  </tbody>
                </table>
				 
				</div>
                </div>
                <!-- /.card-body -->

              
			 
            </div>
            <!-- /.card -->
		</div>
	</div>

	
	
	<script type="text/javascript" src="<?= base_url();?>assets/js/jquery.ajaxfileupload.js?v=<?=date('YmdHis');?>"></script>
	<script>
	
	
	
	$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

	 
    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
	
	

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    $('#reservationdate2').datetimepicker({
		format: 'YYYY-MM-DD'
	});
	
	$('#reservationdate').datetimepicker({
		format: 'YYYY-MM-DD'
	});
	
	$('#reservationdate3').datetimepicker({
		format: 'YYYY-MM-DD'
	});
   
   

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  });
	
	$(document).ready(function() {
			
			 $('.textarea').summernote({
		 
		  
			 });
			$("#selectkasbon").change(function(){
				var forms = $(this).val().split('||'); 
				$('#tglkasbon').val(forms[0].toString());
				//alert(forms[1].toString()); return false;
				$('#perihal123').val(forms[1].toString());
				$('#nomorkasbon').val(forms[2].toString());
			});
			
			$("#jnsproduk").change(function(){
				if($(this).val()=='101'){
					$("#penerima").show();
					$(".kasbon").hide();
					$(".norek").show();
					$("#approval").show();
				}else if($(this).val()=='102'){
					$("#penerima").hide();
					$(".kasbon").hide();
					$(".norek").show();
					$("#approval").show();
				}else{
					$("#penerima").hide();
					$("#approval").hide();
					$(".kasbon").show();
					$(".norek").hide();
				}		
			});	
			
			$(".jnssurat").change(function(){
				if($(this).val()=='1.1' || $(this).val()=='1.2' || $(this).val()=='1.3' || $(this).val()=='1.4'){
					$(".notedjns").show();
					
				}else if($(this).val()=='2'){
					$(".notedjns").hide();
					
				}		
			});	
			
			$("#jsninstansi").change(function(){
				if($(this).val()=='101' || $(this).val()=='102' || $(this).val()=='103' || $(this).val()=='106' || $(this).val()=='202'){
					$(".nminstansi").show();
					$(".nminstansidesc").attr("placeholder", "Sebutkan.....");
				}else{
					
					$(".nminstansi").hide();
				}
			});	
			
			$('.btnBrowse').click(function(){
				var id = $(this).attr('uploadid').toString();
				$('#upload'+id).click();
			});
			var interval;
			function applyAjaxFileUpload(element, nama){
				$(element).AjaxFileUpload({
					action: site + '/proses/upload_dok/<?= date("Ymd");?>/' + nama,
					onChange: function(filename) {
						// Create a span element to notify the user of an upload in progress
						var $span = $("<span />")
							.attr("class", $(this).attr("id"))
							.text("Uploading")
							.insertAfter($(this));
						
						$('#btn'+nama).hide();
						//$(this).remove();

						interval = window.setInterval(function() {
							var text = $span.text();
							if (text.length < 13) {
								$span.text(text + ".");
							} else {
								$span.text("Uploading");
							}
						}, 200);
					},
					onSubmit: function(filename) {
						return true;
					},
					onComplete: function(filename, response) {
						//alert('sii'); return false;
						window.clearInterval(interval);
						var $span = $("span." + $(this).attr("id")).text(""),
							$fileInput = $("<input />")
								.attr({
									type: "file",
									name: $(this).attr("name"),
									id: $(this).attr("id"),
									style: "display:none"
								});

						if (typeof(response.error) === "string") {
							$span.replaceWith($fileInput);
							applyAjaxFileUpload($fileInput);
							alert(response.error);
							return;
						}
						
						$("<a />")
							.attr("href", isUrl + 'dat/<?= date("Ymd");?>/' + '<?= date("Ymd")."_";?>' + filename.split(' ').join('_'))
							.attr("target", "blank_")
							.attr("class", "btn btn-mini btn-primary")
							.html('<i class="icon-search"></i> Preview')
							.appendTo($span);
						$("<span />").html('&nbsp; &nbsp;').appendTo($span);
						$("<a />")
							.attr("href", "#")
							.css('color', 'red')
							.attr("class", "btn btn-mini btn-danger")
							.html('<i class="icon-trash"></i> <span  style="color:white;">Hapus</span>')
							.bind("click", function(e) {
								//$span.replaceWith($fileInput);
								$span.remove();
								$('#btn'+nama).show();
								//applyAjaxFileUpload($fileInput);
							})
							.appendTo($span);
						$("#uppath"+nama).val(isUrl + 'dat/<?= date("Ymd")?>/' + '<?= date("Ymd")."_"; ?>' + filename.split(' ').join('_'));
					}
				});
			}
			
			applyAjaxFileUpload("#upload01", "01");
		});
		
		$(document).ready(function() {
			
			$('.angkanumerik').autoNumeric('init');
		});	
		
		
		(function($){
				  
			  $.fn.addCom = function(){
				
				var mycom = "<table width='87%'>"+
				 "  <tr>"+
				 "     <td width='41%'><input type='text' class='form-control' name='REG_DETIL[REG_DETIL_INPUT][]' style='width:520px;'></td>"+
				 "     <td width='41%'><input type='text' class='form-control angkanumerik' name='REG_DETIL[REG_DETIL_NOMINAL][]' style='width:520px;'></td>"+
				 "     <td width='4%'><button class='btn btn-danger btn-mini'><i class='fas fa-minus-square'></i></button></td>"+
				 "  </tr>"+
				 "</table>";
				 mycom = $("<div>"+mycom+"</div>");
				$("button", $(mycom)).click(function(){ 
					
					$(this).parent().parent().remove(); 
				});

				$(this).append(mycom);
						 
				  };
			})(jQuery);		

		$(function(){
			$("#mycom").click(function(){
				$("#concoms").addCom();
				return false;
			});
		});
		
		
</script>