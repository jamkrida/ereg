<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="row">
          <!-- left column -->
          <div class="col-md-12">
		  <div class="card">
			<div class="card-header">
				<h3 class="card-title"><i class="fas fa-table"></i> <?= "Data CMS";?></i></h3>
			</div>

				 <div class="card-body">
					   <table class="table table-hover">
							<tr>
								<th>Tanggal & Jam Transaksi</th>
								<th>Deskripsi</th>
								<th>Kredit</th>
								<th>Debit</th>
							</tr>
							<tr>
								<td><?= $isi['TANGGAL_CMS'].' '.$isi['JAM_CMS']; ?></td>
								<td><?= $isi['DESKRIPSI_CMS']; ?></td>
								<td><?= $isi['DEBIT_CMS']; ?></td>
								<td><?= $isi['KREDIT_CMS']; ?></td>
							</tr>	
					   </table>
				</div>
			</div>
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Input Fix CMS</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="fdokupload_" action="<?= site_url();?>/home/registrasi_act/save_Fix_CMS" method="post">
                <input type="hidden" name="cmsid" value="<?= $cmsid; ?>">
				<div class="card-body">
				 
				 <div class="form-group">
                    <label for="exampleInputEmail1">Deskripsi</label>
                    <input type="text" class="form-control col-md-8" name="REG[DESKRIPSI_CMS]" value="<?= $isi['DESKRIPSI_CMS'];?>"  wajib="yes">
                 </div>
				 
				 <div class="form-group">
                    <label for="exampleInputEmail1">Loan Number</label>
                    <input type="text" class="form-control col-md-8" name="REG[LOAN_NUMBER]" value="<?= $loan_number;?>"  wajib="yes">
                 </div>
				 <?php 
					if($isi['CABANG_REKENING']==""){
						$cabang = 'KL Syariah Wahid Hasyim';
					}else{
						$cabang = $isi['CABANG_REKENING'];
					}
				 ?>		
				  <div class="form-group">
                    <label for="exampleInputEmail1">Cabang Rekening</label>
                    <input type="text" class="form-control col-md-8" name="REG[CABANG_REKENING]" value="<?= $cabang; ?>"  wajib="yes">
                 </div>

				  <div class="form-group">
                    <label for="exampleInputEmail1">Cabang Penerbitan</label>
                    <input type="text" class="form-control col-md-8" name="REG[CABANG_PENERBITAN]" value="<?= $isi['CABANG_PENERBITAN'];?>"  wajib="yes">
                 </div>	
				 
				 <div class="form-group">
                    <label for="exampleInputEmail1">Nomor SP</label>
                    <input type="text" class="form-control col-md-8" name="REG[NOMOR_SP]" value="<?= $isi['NOMOR_SP'];?>"  >
                 </div>	
				 
				 <div class="form-group">
                    <label for="exampleInputEmail1">Nomor SM</label>
                    <input type="text" class="form-control col-md-8" name="REG[NOMOR_SM]" value="<?= $isi['NOMOR_SM'];?>" >
                 </div>	
				 
				 <div class="form-group">
                    <label for="exampleInputEmail1">Status</label>
                   <?= form_dropdown('REG[STATUS]', $sts, $isi['STATUS'], 'id="jnsproduk" class="form-control col-md-8" wajib="yes"'); ?>
                  </div>	
				
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" onclick="save_post('#fdokupload_'); return false;">Submit</button> &nbsp;<br/><p> <div class="msgtitle_">&nbsp;</div>
                </div>
              </form>
            </div>
            <!-- /.card -->
		</div>
	</div>	
	
	