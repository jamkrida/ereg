<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

	
        
	 <div class="row">
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-info"><i class="fas fa-file-invoice"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Pembayaran</span>
                <span class="info-box-number"><?= $PEMBAYARAN;?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-success"><i class="fas fa-file-invoice"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Kasbon</span>
                <span class="info-box-number"><?= $KASBON;?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-warning"><i class="fas fa-file-invoice"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Pelunasan Kasbon</span>
                <span class="info-box-number"><?= $PELUNASAN_KASBON;?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-danger"><i class="fas fa-file-invoice-dollar"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Semua Dokumen</span>
                <span class="info-box-number"><?= $PELUNASAN_KASBON+$KASBON+$PEMBAYARAN; ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
		
		 <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Selamat Datang <b><?= $this->session->userdata('USER_NAME');?></b></h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>NAMA</th>
                      <th>NIK</th>
                      <th >JABATAN</th>
					  <th>DEPARTEMEN</th>
					  <th>ROLE</th>
					  <th>LAST LOGIN</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1.</td>
                      <td><?= $this->session->userdata('USER_FULLNAME');?></td>
                      <td>
                        <span class="badge bg-danger"><?= $this->session->userdata('USER_NIK');?></span>
                      </td>
                      <td><?= $this->session->userdata('NAMA_JABATAN');?></td>
					  <td><?= $this->session->userdata('NAMA_DEPARTEMEN');?></td>
					  <td><?= $this->session->userdata('NAMA_ROLE');?></td>
					  <td><?= $this->session->userdata('USER_LOGINDATE');?></td>
					  
                    </tr>
                    
                  </tbody>
                </table>
              </div>
             
            </div>
            <!-- /.card -->
			</div>
			</div>
		<?php 
		if($this->session->userdata('LAST_UPDATE_PASSWORD')==""){
		?>
		<script>
			alert("Silahkan Update Password");
			window.location = '<?= site_url(); ?>/home/password/edit';
		</script>
		
		<?php
		}
		?>