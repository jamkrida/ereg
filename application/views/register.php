<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PT. Jamkrida Jakarta :: Registrasi Daftar Rekanan</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="icon" type="image/png" href="<?= base_url();?>assets/img/jamkrida.png"/>
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?= base_url();?>plugins/fontawesome-free/css/all.min.css?v=<?=date('YmdHis')?>">
  <link rel="stylesheet" href="<?= base_url();?>assets/css/newtable.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= base_url();?>plugins/daterangepicker/daterangepicker.css">
  
  <link rel="stylesheet" href="<?= base_url();?>plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <link rel="stylesheet" href="<?= base_url();?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url();?>dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	
	<!--Summernote CSS-->
	 <link rel="stylesheet" href="<?= base_url();?>plugins/fontawesome-free/css/all.min.css">
     <!-- Ionicons -->
     <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	  <!-- Theme style -->
	 <link rel="stylesheet" href="<?= base_url();?>dist/css/adminlte.min.css">
	  <!-- summernote -->
	 <link rel="stylesheet" href="<?= base_url();?>plugins/summernote/summernote-bs4.css">
	  <!-- Google Font: Source Sans Pro -->
	 <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	 <link rel="stylesheet" href="<?= base_url();?>plugins/summernote/summernote-bs4.css">
	 <!--Summernote CSS-->
	 
	 
	 <link rel="stylesheet" href="<?= base_url();?>plugins/daterangepicker/daterangepicker.css">
	  <!-- iCheck for checkboxes and radio inputs -->
	  <link rel="stylesheet" href="<?= base_url();?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	  <!-- Bootstrap Color Picker -->
	  <link rel="stylesheet" href="<?= base_url();?>plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
	  <!-- Tempusdominus Bbootstrap 4 -->
	  <link rel="stylesheet" href="<?= base_url();?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
	  <!-- Select2 -->
	  <link rel="stylesheet" href="<?= base_url();?>plugins/select2/css/select2.min.css">
	  <link rel="stylesheet" href="<?= base_url();?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
	  <!-- Bootstrap4 Duallistbox -->
	  <link rel="stylesheet" href="<?= base_url();?>plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
	  <!-- Theme style -->
	  <link rel="stylesheet" href="<?= base_url();?>dist/css/adminlte.min.css">
	  <!-- Google Font: Source Sans Pro -->
	  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  
  
	<script src="<?= base_url();?>plugins/jquery/jquery.min.js?v=<?=date('YmdHis');?>"></script>
	<script src="<?= base_url();?>assets/js/newtable.js?v=<?=date('YmdHis');?>"></script>
	<script src="<?= base_url();?>assets/js/newtablehash.js?v=<?=date('YmdHis');?>"></script>
	<!-- Bootstrap -->
	<script src="<?= base_url();?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- overlayScrollbars -->
	<script src="<?= base_url();?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
	<!-- AdminLTE App -->
	<script src="<?= base_url();?>dist/js/adminlte.js"></script>
	<!-- OPTIONAL SCRIPTS -->
	<script src="<?= base_url();?>dist/js/demo.js"></script>

	<!-- PAGE PLUGINS -->
	<!-- jQuery Mapael -->
	<script src="<?= base_url();?>plugins/daterangepicker/daterangepicker.js"></script>
	
	<script src="<?= base_url();?>plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
	<script src="<?= base_url();?>plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
	<script src="<?= base_url();?>plugins/raphael/raphael.min.js"></script>
	<script src="<?= base_url();?>plugins/jquery-mapael/jquery.mapael.min.js"></script>
	<script src="<?= base_url();?>plugins/jquery-mapael/maps/usa_states.min.js"></script>
	<!-- ChartJS -->
	<script src="<?= base_url();?>plugins/chart.js/Chart.min.js"></script>
	<!-- PAGE SCRIPTS -->
	<script src="<?= base_url();?>dist/js/pages/dashboard2.js"></script>
	<script src="<?= base_url();?>assets/js/home.js?v=<?=date('YmdHis');?>"></script>
	<script type="text/javascript">
	 var site = "<?php echo site_url(); ?>";
	 var isUrl = "<?php echo base_url(); ?>";
	</script>
	
	<!-- datepicker-->
	<script src="<?= base_url();?>plugins/summernote/summernote-bs4.min.js"></script>
	<!--<script src="<?= base_url();?>plugins/jquery/jquery.min.js"></script>-->
	<!-- Bootstrap 4 -->
	<script src="<?= base_url();?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- Select2 -->
	<script src="<?= base_url();?>plugins/select2/js/select2.full.min.js"></script>
	<!-- Bootstrap4 Duallistbox -->
	<script src="<?= base_url();?>plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
	<!-- InputMask -->
	<script src="<?= base_url();?>plugins/moment/moment.min.js"></script>
	<script src="<?= base_url();?>plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
	<!-- date-range-picker -->
	<script src="<?= base_url();?>plugins/daterangepicker/daterangepicker.js"></script>
	<!-- bootstrap color picker -->
	<script src="<?= base_url();?>plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
	<!-- Tempusdominus Bootstrap 4 -->
	<script src="<?= base_url();?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
	<!-- Bootstrap Switch -->
	<script src="<?= base_url();?>plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
	
	<!-- AdminLTE App -->
	<!--<script src="<?= base_url();?>dist/js/adminlte.min.js"></script>-->
	
	<!-- AdminLTE for demo purposes -->
	<!--<script src="<?= base_url();?>dist/js/demo.js"></script>-->
	<!-- datepicker-->
	<script src="<?= base_url();?>assets/js/numeric/autoNumeric.js?v=<?=date('YmdHis');?>"></script>
	
	<!-- Summernote -->
	<script src="<?= base_url();?>plugins/summernote/summernote-bs4.min.js"></script>
	<!--Summernote JS-->
</head>
<body class="hold-transition register-page">
<div class="register-box">
<br>
  <div class="register-logo">
   <img src="<?= base_url();?>assets/img/jamkrida.png">
  </div>
		
  <div class="card">
    <div class="card-body register-card-body">
      <p class="login-box-msg">Registrasi Daftar Rekanan<br>PT Jamkrida Jakarta</p>

      <form id="fdokupload_" action="<?= site_url();?>/home/register_act/save_vendor" method="post">
         <div class="form-group">
			<label for="exampleInputEmail1">Nama Vendor <span style="color:red">*</span></label>
			<input type="text" name="REG[NAMA_VENDOR]" class="form-control" wajib="yes">
		  </div>
		<div class="form-group">
			<label for="exampleInputEmail1">Alamat Vendor <span style="color:red">*</span></label>
			<textarea class="form-control" name="REG[ALAMAT_VENDOR]" wajib="yes"></textarea>
		  </div>
		 <div class="form-group">
			<label for="exampleInputEmail1">Bidang Usaha <span style="color:red">*</span></label>
			<input type="text" name="REG[BIDANG_USAHA]" class="form-control" wajib="yes">
		  </div>
		  
		   <div class="form-group">
			<label for="exampleInputEmail1">Nama Pimpinan/ Direktur /PIC <span style="color:red">*</span></label>
			<input type="text" name="REG[NAMA_PIMPINAN]" class="form-control" wajib="yes">
		  </div>
		  
		  <div class="form-group">
			<label for="exampleInputEmail1">Email <span style="color:red">*</span></label>
			<input type="text" name="REG[EMAIL]" class="form-control" wajib="yes">
		  </div>
		  
		  <div class="form-group">
			<label for="exampleInputEmail1">No. HP <span style="color:red">*</span></label>
			<input type="text" name="REG[TELP]" class="form-control" wajib="yes">
		  </div>
		  
		  <div class="form-group">
			<label for="exampleInputEmail1">Nomor NIB <span style="color:red">*</span></label>
			<input type="text" name="REG[NIB]" class="form-control" wajib="yes">
		  </div>
		  
		  <div class="form-group">
					   <label class="control-label" for="form-field-1">Sertifikat NIB <span style="color:red">*</span></label>
					   <div class="controls">
						  <input style="display:none;" class="inputFiles" type="file" name="01" id="upload01" />
						  <input type="hidden"name="PATH[01]" id="uppath01">
						  <button type="button" id="btn01" uploadid="01" class="btnBrowse btn btn-small btn-success">Browse <i class="icon-search icon-on-right bigger-110"></i></button>
					   </div>
				  </div>
		  
		  <div class="form-group">
					   <label class="control-label" for="form-field-1">Scan KTP <span style="color:red">*</span></label>
					   <div class="controls">
						  <input style="display:none;" class="inputFiles" type="file" name="02" id="upload02" />
						  <input type="hidden"name="PATH[02]" id="uppath02">
						  <button type="button" id="btn02" uploadid="02" class="btnBrowse btn btn-small btn-success">Browse <i class="icon-search icon-on-right bigger-110"></i></button>
					   </div>
				  </div>

		  <div class="form-group">
					   <label class="control-label" for="form-field-1">Scan NPWP <span style="color:red">*</span></label>
					   <div class="controls">
						  <input style="display:none;" class="inputFiles" type="file" name="03" id="upload03" />
						  <input type="hidden"name="PATH[03]" id="uppath03">
						  <button type="button" id="btn03" uploadid="03" class="btnBrowse btn btn-small btn-success">Browse <i class="icon-search icon-on-right bigger-110"></i></button>
					   </div>
				  </div>	

		  <div class="form-group">
					   <label class="control-label" for="form-field-1">Scan Akta Pendirian <span style="color:red">*</span></label>
					   <div class="controls">
						  <input style="display:none;" class="inputFiles" type="file" name="04" id="upload04" />
						  <input type="hidden" name="PATH[04]" id="uppath04">
						  <button type="button" id="btn04" uploadid="04" class="btnBrowse btn btn-small btn-success">Browse <i class="icon-search icon-on-right bigger-110"></i></button>
					   </div>
				  </div>			
		<div class="row">
           <div class="col-8">
            <div class="icheck-primary">
				&nbsp;
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block" onclick="save_post('#fdokupload_'); return false;">Submit</button>  
			
          </div>
          <!-- /.col -->
        </div>
		<br>
		<div class="row">
			<div class="col-12">
				<div class="msgtitle_">&nbsp;</div>
			</div>
		</div>
      </form>

 
    </div> <!-- /.form-box -->
  
  </div><!-- /.card -->
  
</div><!-- /.register-box -->



<script type="text/javascript" src="<?= base_url();?>assets/js/jquery.ajaxfileupload.js?v=<?=date('YmdHis');?>"></script>
	<script>
	$(document).ready(function() {
			
			$('.btnBrowse').click(function(){
				var id = $(this).attr('uploadid').toString();
				$('#upload'+id).click();
			});
			var interval;
			function applyAjaxFileUpload(element, nama){
				$(element).AjaxFileUpload({
					action: site + '/proses/upload_vendor/vendor/<?= date("Ymd");?>/' + nama,
					onChange: function(filename) {
						// Create a span element to notify the user of an upload in progress
						var $span = $("<span />")
							.attr("class", $(this).attr("id"))
							.text("Uploading")
							.insertAfter($(this));
						
						$('#btn'+nama).hide();
						//$(this).remove();

						interval = window.setInterval(function() {
							var text = $span.text();
							if (text.length < 13) {
								$span.text(text + ".");
							} else {
								$span.text("Uploading");
							}
						}, 200);
					},
					onSubmit: function(filename) {
						return true;
					},
					onComplete: function(filename, response) {
						
						/*
						var chkname = filename.substr(0, 5);
						
						if(chkname=='PP.JJ'){
							filename = filename.replace('PP.JJ.','PP_JJ_');
						}else if(chkname=='PK.JJ'){
							filename = filename.replace('PK.JJ.','PK_JJ_');
						}else if(chkname=='PKB.J'){
							filename = filename.replace('PKB.JJ.','PKB_JJ_');
						}
						*/
						
						window.clearInterval(interval);
						var $span = $("span." + $(this).attr("id")).text(""),
							$fileInput = $("<input />")
								.attr({
									type: "file",
									name: $(this).attr("name"),
									id: $(this).attr("id"),
									style: "display:none"
								});

						if (typeof(response.error) === "string") {
							$span.replaceWith($fileInput);
							applyAjaxFileUpload($fileInput);
							alert(response.error);
							return;
						}
						
						$("<a />")
							.attr("href", isUrl + 'dat/vendor/<?= date("Ymd");?>/' + '<?= date("Ymd")."_";?>' + filename.split(' ').join('_'))
							.attr("target", "blank_")
							.attr("class", "btn btn-mini btn-primary")
							.html('<i class="icon-search"></i> Preview')
							.appendTo($span);
						$("<span />").html('&nbsp; &nbsp;').appendTo($span);
						$("<a />")
							.attr("href", "#")
							.css('color', 'red')
							.attr("class", "btn btn-mini btn-danger")
							.html('<i class="icon-trash"></i> <span  style="color:white;">Hapus</span>')
							.bind("click", function(e) {
								//$span.replaceWith($fileInput);
								$span.remove();
								$('#btn'+nama).show();
								//applyAjaxFileUpload($fileInput);
							})
							.appendTo($span);
						$("#uppath"+nama).val(isUrl + 'dat/vendor/<?= date("Ymd")?>/' + '<?= date("Ymd")."_"; ?>' + filename.split(' ').join('_'));
					}
				});
			}
			
			applyAjaxFileUpload("#upload01", "01");
			applyAjaxFileUpload("#upload02", "02");
			applyAjaxFileUpload("#upload03", "03");
			applyAjaxFileUpload("#upload04", "04");
			
		});
</script>
</body>
</html>
