<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Proses extends CI_Controller{
	
	
	function index($isajax=""){
		if(strtolower($_SERVER['REQUEST_METHOD'])!="post" || !$this->session->userdata('LOGGED')){
			redirect(base_url());
			exit();	
		}else{
		 
			$this->load->model('proses_act');			
			$ret = $this->proses_act->set_proses($isajax);	
			
			
		}
		if($isajax!="ajax"){
			redirect(base_url());
			exit();
		}
		echo $ret;
	}
	

	function cek_reg_kendaraan() {
        $sess = $this->session->userdata('LOGGED');
		if($this->session->userdata('USER_ROLE')=="03"){
			$query = "SELECT COUNT(REG_ID) JML FROM tx_kendaraan WHERE STATUS = '3' AND JENIS_KENDARAAN IS NOT NULL AND TTD IS NULL AND ATASAN_PEMOHON = '".$this->session->userdata('USER_ID')."' ORDER BY TANGGAL_PERMOHONAN";
			$arrjml = $this->db->query($query)->row_array();
		}else{
			$query = "SELECT COUNT(REG_ID) JML FROM tx_kendaraan WHERE STATUS = '2' AND JENIS_KENDARAAN IS NULL ORDER BY TANGGAL_PERMOHONAN";
			$arrjml = $this->db->query($query)->row_array();	
		}	
		

        $return = $arrjml['JML'];

        echo $return;
    }
	
	function get_norek(){
		$nama = $_GET['query'];
		
		$query = "SELECT * FROM tm_rekening WHERE NOMOR_REKENING LIKE '%".$nama."%'";
		$arrdata  = $this->db->query($query)->result_array();
		foreach($arrdata as $data) {
			$output['suggestions'][] = [
				'value' => $data['NOMOR_REKENING'].' (an '.$data['NAMA_REKENING'].')',
				'norekid'  => $data['NOMOR_REKENING'].' (an '.$data['NAMA_REKENING'].')'
			];
		}

		if (! empty($output)) {
			// Encode ke format JSON.
			echo json_encode($output);
		}
		
	}
	
	public function upload_dok($id="",$nama=""){
		
		#print_r($_POST); die();
		$dir = 'dat/';
		if(!is_dir($dir)) mkdir($dir);
		chmod($dir,0777);
		
		$dir .= $id."/";
		if(!is_dir($dir)){ 
			mkdir($dir);
		}
		chmod($dir,0777);
		
        $config['upload_path'] = $dir;
        $config['allowed_types'] = 'jpg|pdf|zip|jpeg|png|zip|rar|doc|docx|xls|xlsx';
        $config['max_size'] = 104857600;
	    #$config['file_name'] = str_replace(".","",$_FILES[$nama]['name']);
        $config['file_name'] = date("Ymd").'_'.str_replace(" ", "_", $_FILES[$nama]['name']);
       
		$this->load->library('upload', $config);
        if (!$this->upload->do_upload($nama)) {
            $error = $this->upload->display_errors();
            var_dump($this->upload->data());
            var_dump($error);
        }else{
            $myfile = fopen($dir . "/index.php", "w");
            $txt = "<?php if(!defined('BASEPATH'))exit('Mohon maaf, anda tidak bisa mengakses halaman ini'); ?>";
            fwrite($myfile, $txt);
            fclose($myfile);
			
			$data = $this->upload->data();           
		    $upload_info = $this->upload->data();
			chmod($upload_info['full_path'],0777);
        }
		
	} 
	
	public function upload_cms($id="",$nama=""){
		
		
		$dir = 'dat/';
		if(!is_dir($dir)) mkdir($dir);
		chmod($dir,0777);
		
		$dir .= 'cms/';
		if(!is_dir($dir)) mkdir($dir);
		chmod($dir,0777);
		
		$dir .= $id."/";
		if(!is_dir($dir)){ 
			mkdir($dir);
		}
		chmod($dir,0777);
		
        $config['upload_path'] = $dir;
        $config['allowed_types'] = 'jpg|pdf|zip|jpeg|png|zip|rar|doc|docx|xls|xlsx';
        $config['max_size'] = 104857600;
	   
        $config['file_name'] = date("Ymd").'_'.str_replace(" ", "_", $_FILES[$nama]['name']);
       
		$this->load->library('upload', $config);
        if (!$this->upload->do_upload($nama)) {
            $error = $this->upload->display_errors();
            var_dump($this->upload->data());
            var_dump($error);
        }else{
            $myfile = fopen($dir . "/index.php", "w");
            $txt = "<?php if(!defined('BASEPATH'))exit('Mohon maaf, anda tidak bisa mengakses halaman ini'); ?>";
            fwrite($myfile, $txt);
            fclose($myfile);
			
			$data = $this->upload->data();           
		    $upload_info = $this->upload->data();
			chmod($upload_info['full_path'],0777);
        }
		
	}
	
	
	public function upload_regis_kendaraan($id="",$nama=""){
		
		#print_r($_POST); die();
		$dir = 'dat/';
		if(!is_dir($dir)) mkdir($dir);
		chmod($dir,0777);
		
		$dir .= "kendaraan/";
		if(!is_dir($dir)){ 
			mkdir($dir);
		}
		chmod($dir,0777);
		
		$dir .= $id."/";
		if(!is_dir($dir)){ 
			mkdir($dir);
		}
		chmod($dir,0777);
		
        $config['upload_path'] = $dir;
        $config['allowed_types'] = 'jpg|pdf|zip|jpeg|png|zip|rar|doc|docx|xls|xlsx';
        $config['max_size'] = 104857600;
	    #$config['file_name'] = str_replace(".","",$_FILES[$nama]['name']);
        $config['file_name'] = date("Ymd").'_'.str_replace(" ", "_", $_FILES[$nama]['name']);
       
		$this->load->library('upload', $config);
        if (!$this->upload->do_upload($nama)) {
            $error = $this->upload->display_errors();
            var_dump($this->upload->data());
            var_dump($error);
        }else{
            $myfile = fopen($dir . "/index.php", "w");
            $txt = "<?php if(!defined('BASEPATH'))exit('Mohon maaf, anda tidak bisa mengakses halaman ini'); ?>";
            fwrite($myfile, $txt);
            fclose($myfile);
			
			$data = $this->upload->data();           
		    $upload_info = $this->upload->data();
			chmod($upload_info['full_path'],0777);
        }
		
	}
	
	
	public function upload_vendor($jenis="", $id="", $nama=""){
		
		#print_r($_POST); die();
		$dir = 'dat/';
		if(!is_dir($dir)) mkdir($dir);
		chmod($dir,0777);
		
		$dir .= $jenis."/";
		if(!is_dir($dir)){ 
			mkdir($dir);
		}
		chmod($dir,0777);
		
		$dir .= $id."/";
		if(!is_dir($dir)){ 
			mkdir($dir);
		}
		chmod($dir,0777);
		
        $config['upload_path'] = 'dat/'.$jenis.'/'.$id.'/';
        $config['allowed_types'] = 'jpg|pdf|zip|jpeg|png|zip|rar|doc|docx|xls|xlsx';
        $config['max_size'] = 104857600;
	    #$config['file_name'] = str_replace(".","",$_FILES[$nama]['name']);
        $config['file_name'] = date("Ymd").'_'.str_replace(" ", "_", $_FILES[$nama]['name']);
       
		$this->load->library('upload', $config);
        if (!$this->upload->do_upload($nama)) {
            $error = $this->upload->display_errors();
            var_dump($this->upload->data());
            var_dump($error);
        }else{
            $myfile = fopen($dir . "/index.php", "w");
            $txt = "<?php if(!defined('BASEPATH'))exit('Mohon maaf, anda tidak bisa mengakses halaman ini'); ?>";
            fwrite($myfile, $txt);
            fclose($myfile);
			
			$data = $this->upload->data();           
		    $upload_info = $this->upload->data();
			chmod($upload_info['full_path'],0777);
        }
		
	}
	
}
?>