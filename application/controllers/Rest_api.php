<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
require APPPATH . 'libraries/Format.php';
class Rest_api extends REST_Controller{
	
	 public function __construct(){
			parent::__construct();
	 }
	 
	 public function index_get(){
	// testing response
		$response['status']=200;
		$response['error']=false;
		$response['message']='Hai from response';
	// tampilkan response
		$this->response($response);
	}
	
	public function pembayaran_post(){
		$cbs =& get_instance();
		$cbs->load->model("main","main", true);
		
		$r = array('','I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII');
		$bln = date("n");
		
		$uid = $cbs->main->get_uraian("SELECT USER_ID FROM tm_user WHERE USER_NIK = '20180032'","USER_ID");
		
		$JENIS = 'PEMBAYARAN';
		$jml = $cbs->main->get_uraian("SELECT JUMLAH FROM tm_nomor WHERE JENIS_NOMOR = 'PEMBAYARAN'","JUMLAH");
		$jml = $jml+1;
		$jml = sprintf("%03d", $jml);
		$nomor = 'PP.JJ.'.$r[$bln].'-'.date("y").'-'.$jml;
		
		$no = $cbs->main->get_uraian("SELECT JUMLAH FROM tm_nomor WHERE JENIS_NOMOR = 'SURAT'","JUMLAH");
		$no = $no+1;
		$no = $code = sprintf("%03d", $no);
		$no = 'SE'.$no;
	
		
		
		$data = array('REG_ID' => $no,
					  'TANGGAL_PENGAJUAN' => date("Y-m-d"),
					  'REG_JENIS_DOKUMEN' => '101',
					  'REG_UNIT_KERJA' => '3',
					  'REG_PENERIMA' => $this->post('penerima'),
					  'REG_NOMOR_REKENING' => $this->post('norek'),
					  'REG_JUMLAH' => $this->post('jumlah'),
					  'REG_PEMAKAIAN_TERBILANG' => $this->post('jumlah_terbilang'),	
					  'REG_PERIHAL' => $this->post('perihal'),
					  'REG_CATATAN' => 'Rest API testing',		
					  'REG_NOMOR' => $nomor,
					  'TTD_MANAJER_PEMOHON' => '92',
					  'TTD_JABATAN_MANAJER_PEMOHON' => '3.1',
					  'TTD_STAFF_APPROVAL' => 'Mazidun',
					  'TTD_MANAJER_APPROVE' => '2',
					  'TTD_JABATAN_MANAJER_APPROVE' => '3.2',
					  'STAFF_KEU' => '29',
					  'USER_CREATE' => $uid,
					  'DATE_CREATE' => date("Y-m-d H:i:s")
					);		
		

        $insert = $this->db->insert('tx_reg', $data);
		
		if($insert){
			$this->response($data, 200);
			$this->db->simple_query("UPDATE tm_nomor SET JUMLAH = JUMLAH + 1 WHERE JENIS_NOMOR = 'SURAT'");
			$this->db->simple_query("UPDATE tm_nomor SET JUMLAH = JUMLAH + 1 WHERE JENIS_NOMOR = '".$JENIS."'");
			$this->response($data, 200);
		}else{
			$this->response(array('status' => 'fail', 502));
		}
		
	}
	
	public function user_get(){
	// testing response
		$response['status']=200;
		$response['error']=false;
		$response['user']['username']='erthru';
		$response['user']['email']='ersaka96@gmail.com';
		$response['user']['detail']['full_name']='Suprianto D';
		$response['user']['detail']['position']='Developer';
		$response['user']['detail']['specialize']='Android,IOS,WEB,Desktop';
	//tampilkan response
		$this->response($response);
	}
	
}	

?>