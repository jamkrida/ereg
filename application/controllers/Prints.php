<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prints extends CI_Controller {

	function index(){

		redirect(base_url());
	}
	
	function list_pembayaran($id=""){
		if($this->session->userdata("LOGGED")){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			ob_start(); 		
			
			require_once(APPPATH.'libraries/tcpdf/tcpdf.php');
				
			$this->load->model('print/print_act');
			$arrdata = $this->print_act->pembayaran($id);
			
			
			
			$filename = str_replace(' ','','FORM_REGISTRASI_PEMBAYARAN_' . $id . '.pdf');
			
			
			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, '', true, 'UTF-8', false);		
			$pdf->SetTitle('FORM PEMBAYARAN');

			#$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			#$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			$pdf->SetMargins(20, 25, 20, true);
			#$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			#$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			// set some language-dependent strings (optional)
			if (@file_exists(APPPATH.'libraries/tcpdf/examples/lang/eng.php')) {
				require_once(APPPATH.'libraries/tcpdf/examples/lang/eng.php');
				$pdf->setLanguageArray($l);
			}
			// ---------------------------------------------------------

			$pdf->SetFont('Times', '', 14);

			// add a page
			$pdf->AddPage('p','A4');

			// -- set new background ---

			// get the current page break margin
			$bMargin = $pdf->getBreakMargin();
			// get current auto-page-break mode
			$auto_page_break = $pdf->getAutoPageBreak();
			// disable auto-page-break
			$pdf->SetAutoPageBreak(false, 0);

			$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
			// set the starting point for the page content
			$pdf->setPageMark();

			// print_r($arrdata); die();

			$text = $this->load->view('print/pembayaran', $arrdata, true);
			$conv = htmlentities($text);
			$convstr = html_entity_decode($conv);
			// echo $convstr; die();
			// $pdf->writeHTML($convstr, true, 0, true, 0);
			$pdf->writeHTML($convstr, true, false, true, false, '');
			
			$pdf->Output($filename);			

		}else{
			redirect(base_url());
		}
	
	}
	
	
	function data_ulasan($id=""){
		if($this->session->userdata("LOGGED")){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			ob_start(); 		
			
			require_once(APPPATH.'libraries/tcpdf/tcpdf.php');
				
			$this->load->model('print/print_act');
			$arrdata = $this->print_act->driver($id);
			
			
			
			$filename = str_replace(' ','','LAPORAN PENILAIAN DRIVER' . $id . '.pdf');
			
			
			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, '', true, 'UTF-8', false);		
			$pdf->SetTitle('Penilaian Driver');


			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			$pdf->SetMargins(20, 25, 20, true);


			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			// set some language-dependent strings (optional)
			if (@file_exists(APPPATH.'libraries/tcpdf/examples/lang/eng.php')) {
				require_once(APPPATH.'libraries/tcpdf/examples/lang/eng.php');
				$pdf->setLanguageArray($l);
			}
			// ---------------------------------------------------------

			$pdf->SetFont('Times', '', 14);

			// add a page
			$pdf->AddPage('p','A4');

			// -- set new background ---

			// get the current page break margin
			$bMargin = $pdf->getBreakMargin();
			// get current auto-page-break mode
			$auto_page_break = $pdf->getAutoPageBreak();
			// disable auto-page-break
			$pdf->SetAutoPageBreak(false, 0);

			$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
			// set the starting point for the page content
			$pdf->setPageMark();

			

			$text = $this->load->view('print/ulasan_driver', $arrdata, true);
			$conv = htmlentities($text);
			$convstr = html_entity_decode($conv);
			
			$pdf->writeHTML($convstr, true, false, true, false, '');
			
			$pdf->Output($filename);			

		}else{
			redirect(base_url());
		}
	}
	
	function regis_kendaraan($id=""){
		if($this->session->userdata("LOGGED")){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			ob_start(); 		
			
			require_once(APPPATH.'libraries/tcpdf/tcpdf.php');
			
			$arraju = explode(",", $id);
			foreach($arraju as $a => $b){
				$c = explode('.', $b);
				$d[$a] = $c[0];
			
			}
			
			$aju = $d[0];
			
			$aju = $d[0];
			$cetak = implode('-', $d);
			
			

			$this->load->model('print/print_act');
			$arrdata = $this->print_act->cetak_surat_registrasi($cetak);
			
			
			
			$filename = str_replace(' ','','SURAT_PERMOHONAN_PENGGUNAAN_KENDARAAN_OPERASIONAL_' . $id . '.pdf');
			
			
			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, '', true, 'UTF-8', false);		
			$pdf->SetTitle('SURAT PERMOHONAN PENGGUNAAN KENDARAAN OPERASIONAL');

			#$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			#$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			$pdf->SetMargins(20, 15, 20, true);
			#$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			#$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			// set some language-dependent strings (optional)
			if (@file_exists(APPPATH.'libraries/tcpdf/examples/lang/eng.php')) {
				require_once(APPPATH.'libraries/tcpdf/examples/lang/eng.php');
				$pdf->setLanguageArray($l);
			}
			// ---------------------------------------------------------

			$pdf->SetFont('Times', '', 12);

			// add a page
			$pdf->AddPage('p','A4');

			// -- set new background ---

			// get the current page break margin
			$bMargin = $pdf->getBreakMargin();
			// get current auto-page-break mode
			$auto_page_break = $pdf->getAutoPageBreak();
			// disable auto-page-break
			$pdf->SetAutoPageBreak(false, 0);

			$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
			// set the starting point for the page content
			$pdf->setPageMark();

			// print_r($arrdata); die();

			$text = $this->load->view('print/surat_permohonan_penggunaan_kendaraan', $arrdata, true);
			$conv = htmlentities($text);
			$convstr = html_entity_decode($conv);
			// echo $convstr; die();
			// $pdf->writeHTML($convstr, true, 0, true, 0);
			$pdf->writeHTML($convstr, true, false, true, false, '');
			
			$pdf->Output($filename);			

		}else{
			redirect(base_url());
		}
	
	}
	
	function list_absen_rapat($id=""){
		
		
		#if($this->session->userdata("LOGGED")){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			ob_start(); 		
			
			require_once(APPPATH.'libraries/tcpdf/tcpdf.php');
				
			$this->load->model('print/print_act');
			
			$arrdata = $this->print_act->absen_rapat($id);
			
			
			
			$filename = str_replace(' ','','LIST_ABSENSI_RAPAT_' . $id . '.pdf');
			
			
			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, '', true, 'UTF-8', false);		
			$pdf->SetTitle('LIST ABSEN RAPAT');

			#$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			#$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			$pdf->SetMargins(20, 30, 20, true);
			#$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			#$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			// set some language-dependent strings (optional)
			if (@file_exists(APPPATH.'libraries/tcpdf/examples/lang/eng.php')) {
				require_once(APPPATH.'libraries/tcpdf/examples/lang/eng.php');
				$pdf->setLanguageArray($l);
			}
			// ---------------------------------------------------------

			$pdf->SetFont('Times', '', 10);

			// add a page
			$pdf->AddPage('p','A4');

			// -- set new background ---

			// get the current page break margin
			$bMargin = $pdf->getBreakMargin();
			// get current auto-page-break mode
			$auto_page_break = $pdf->getAutoPageBreak();
			// disable auto-page-break
			$pdf->SetAutoPageBreak(false, 0);

			$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
			// set the starting point for the page content
			$pdf->setPageMark();

			// print_r($arrdata); die();

			$text = $this->load->view('print/rapat', $arrdata, true);
			$conv = htmlentities($text);
			$convstr = html_entity_decode($conv);
			// echo $convstr; die();
			// $pdf->writeHTML($convstr, true, 0, true, 0);
			$pdf->writeHTML($convstr, true, false, true, false, '');
			
			$pdf->Output($filename);			

		#}else{
		#	redirect(base_url());
		#}
	
	}
	
	
	function list_kasbon($id=""){
		if($this->session->userdata("LOGGED")){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			ob_start(); 		
			
			require_once(APPPATH.'libraries/tcpdf/tcpdf.php');
				
			$this->load->model('print/print_act');
			$arrdata = $this->print_act->pembayaran($id);
			
			
			
			$filename = str_replace(' ','','FORM_REGISTRASI_PEMBAYARAN_' . $id . '.pdf');
			
			
			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, '', true, 'UTF-8', false);		
			$pdf->SetTitle('FORM PEMBAYARAN');

			#$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			#$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			$pdf->SetMargins(20, 30, 20, true);
			#$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			#$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			// set some language-dependent strings (optional)
			if (@file_exists(APPPATH.'libraries/tcpdf/examples/lang/eng.php')) {
				require_once(APPPATH.'libraries/tcpdf/examples/lang/eng.php');
				$pdf->setLanguageArray($l);
			}
			// ---------------------------------------------------------

			$pdf->SetFont('Times', '', 14);

			// add a page
			$pdf->AddPage('p','A4');

			// -- set new background ---

			// get the current page break margin
			$bMargin = $pdf->getBreakMargin();
			// get current auto-page-break mode
			$auto_page_break = $pdf->getAutoPageBreak();
			// disable auto-page-break
			$pdf->SetAutoPageBreak(false, 0);

			$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
			// set the starting point for the page content
			$pdf->setPageMark();

			// print_r($arrdata); die();

			$text = $this->load->view('print/kasbon', $arrdata, true);
			$conv = htmlentities($text);
			$convstr = html_entity_decode($conv);
			// echo $convstr; die();
			// $pdf->writeHTML($convstr, true, 0, true, 0);
			$pdf->writeHTML($convstr, true, false, true, false, '');
			
			$pdf->Output($filename);			

		}else{
			redirect(base_url());
		}
	
	}
	
		function list_pelunasan_kasbon($id=""){
		if($this->session->userdata("LOGGED")){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			ob_start(); 		
			
			require_once(APPPATH.'libraries/tcpdf/tcpdf.php');
				
			$this->load->model('print/print_act');
			$arrdata = $this->print_act->pembayaran($id);
			
			
			
			$filename = str_replace(' ','','FORM_REGISTRASI_PEMBAYARAN_' . $id . '.pdf');
			
			
			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, '', true, 'UTF-8', false);		
			$pdf->SetTitle('FORM PEMBAYARAN');

			#$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			#$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			$pdf->SetMargins(20, 30, 20, true);
			#$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			#$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			// set some language-dependent strings (optional)
			if (@file_exists(APPPATH.'libraries/tcpdf/examples/lang/eng.php')) {
				require_once(APPPATH.'libraries/tcpdf/examples/lang/eng.php');
				$pdf->setLanguageArray($l);
			}
			// ---------------------------------------------------------

			$pdf->SetFont('Times', '', 14);

			// add a page
			$pdf->AddPage('p','A4');

			// -- set new background ---

			// get the current page break margin
			$bMargin = $pdf->getBreakMargin();
			// get current auto-page-break mode
			$auto_page_break = $pdf->getAutoPageBreak();
			// disable auto-page-break
			$pdf->SetAutoPageBreak(false, 0);

			$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
			// set the starting point for the page content
			$pdf->setPageMark();

			// print_r($arrdata); die();

			$text = $this->load->view('print/pelunasan_kasbon', $arrdata, true);
			$conv = htmlentities($text);
			$convstr = html_entity_decode($conv);
			// echo $convstr; die();
			// $pdf->writeHTML($convstr, true, 0, true, 0);
			$pdf->writeHTML($convstr, true, false, true, false, '');
			
			$pdf->Output($filename);			

		}else{
			redirect(base_url());
		}
	
	}
	
	function stdr($id=""){
		
		#if($this->session->userdata("LOGGED")){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			ob_start(); 		
			
			require_once(APPPATH.'libraries/tcpdf/tcpdf.php');
			
			$arraju = explode(",", $id);
			foreach($arraju as $a => $b){
				$c = explode('.', $b);
				$d[$a] = $c[0];
			
			}
			
			$aju = $d[0];
			
			$aju = $d[0];
			$cetak = implode('-', $d);
			

			$this->load->model('print/print_act');
			$arrdata = $this->print_act->cetak_stdr($cetak);
			
			#print_r($arrdata); die();
			
			$filename = str_replace(' ','','SURAT_TANDA_DAFTAR_REKANAN_' . $id . '.pdf');
			
			
			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, '', true, 'UTF-8', false);		
			$pdf->SetTitle('SURAT PERMOHONAN KONSUMSI RAPAT');

			#$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			#$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			$pdf->SetMargins(28, 30, 28, true);
			#$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			#$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			// set some language-dependent strings (optional)
			if (@file_exists(APPPATH.'libraries/tcpdf/examples/lang/eng.php')) {
				require_once(APPPATH.'libraries/tcpdf/examples/lang/eng.php');
				$pdf->setLanguageArray($l);
			}
			// ---------------------------------------------------------

			$pdf->SetFont('Times', '', 12);

			// add a page
			$pdf->AddPage('p','A4');

			// -- set new background ---

			// get the current page break margin
			$bMargin = $pdf->getBreakMargin();
			// get current auto-page-break mode
			$auto_page_break = $pdf->getAutoPageBreak();
			// disable auto-page-break
			$pdf->SetAutoPageBreak(false, 0);

			$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
			// set the starting point for the page content
			$pdf->setPageMark();

			// print_r($arrdata); die();

			$text = $this->load->view('print/stdr', $arrdata, true);
			$conv = htmlentities($text);
			$convstr = html_entity_decode($conv);
			// echo $convstr; die();
			// $pdf->writeHTML($convstr, true, 0, true, 0);
			$pdf->writeHTML($convstr, true, false, true, false, '');
			
			$pdf->Output($filename);			

		#}else{
		#	redirect(base_url());
		#}
	
	}
	
	
	function report_absen($tahun="", $bulan=""){
		
		if($this->session->userdata("LOGGED")){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			ob_start(); 		
			
			require_once(APPPATH.'libraries/tcpdf/tcpdf.php');
						

			$this->load->model('print/print_act');
			$arrdata = $this->print_act->cetak_rekap_absen($tahun, $bulan);
			
			#print_r($arrdata); die();
			
			$filename = str_replace(' ','','REPORT_ABSENSI.pdf');
			
			
			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, '', true, 'UTF-8', false);		
			$pdf->SetTitle('REKAP ABSEN BULANAN');

			#$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			#$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			$pdf->SetMargins(12, 18, 20, true);
			#$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			#$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			// set some language-dependent strings (optional)
			if (@file_exists(APPPATH.'libraries/tcpdf/examples/lang/eng.php')) {
				require_once(APPPATH.'libraries/tcpdf/examples/lang/eng.php');
				$pdf->setLanguageArray($l);
			}
			// ---------------------------------------------------------

			$pdf->SetFont('helvetica  ', '', 10);

			// add a page
			$pdf->AddPage('p','A4');

			// -- set new background ---

			// get the current page break margin
			$bMargin = $pdf->getBreakMargin();
			// get current auto-page-break mode
			$auto_page_break = $pdf->getAutoPageBreak();
			// disable auto-page-break
			$pdf->SetAutoPageBreak(false, 0);

			$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
			// set the starting point for the page content
			$pdf->setPageMark();

			// print_r($arrdata); die();

			$text = $this->load->view('print/absen/rekap_absen', $arrdata, true);
			$conv = htmlentities($text);
			$convstr = html_entity_decode($conv);
			// echo $convstr; die();
			// $pdf->writeHTML($convstr, true, 0, true, 0);
			$pdf->writeHTML($convstr, true, false, true, false, '');
			
			$pdf->Output($filename);			

		}else{
			redirect(base_url());
		}
	
	}
	
	function report_absen_user($id="", $tahun="", $bulan=""){
		
		if($this->session->userdata("LOGGED")){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			ob_start(); 		
			
			require_once(APPPATH.'libraries/tcpdf/tcpdf.php');
						

			$this->load->model('print/print_act');
			$arrdata = $this->print_act->set_data_absen_user($id, $tahun, $bulan);
			
			#print_r($arrdata); die();
			
			$filename = str_replace(' ','','REPORT_ABSENSI.pdf');
			
			
			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, '', true, 'UTF-8', false);		
			$pdf->SetTitle('REKAP ABSEN BULANAN');

			#$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			#$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			$pdf->SetMargins(25, 30, 25, true);
			#$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			#$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			// set some language-dependent strings (optional)
			if (@file_exists(APPPATH.'libraries/tcpdf/examples/lang/eng.php')) {
				require_once(APPPATH.'libraries/tcpdf/examples/lang/eng.php');
				$pdf->setLanguageArray($l);
			}
			// ---------------------------------------------------------

			$pdf->SetFont('helvetica  ', '', 10);

			// add a page
			$pdf->AddPage('p','A4');

			// -- set new background ---

			// get the current page break margin
			$bMargin = $pdf->getBreakMargin();
			// get current auto-page-break mode
			$auto_page_break = $pdf->getAutoPageBreak();
			// disable auto-page-break
			$pdf->SetAutoPageBreak(false, 0);

			$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
			// set the starting point for the page content
			$pdf->setPageMark();

			// print_r($arrdata); die();

			$text = $this->load->view('print/absen/rekap_absen_user', $arrdata, true);
			$conv = htmlentities($text);
			$convstr = html_entity_decode($conv);
			// echo $convstr; die();
			// $pdf->writeHTML($convstr, true, 0, true, 0);
			$pdf->writeHTML($convstr, true, false, true, false, '');
			
			$pdf->Output($filename);			

		}else{
			redirect(base_url());
		}
	
	}


	function report_cms($id="", $awal="", $akhir=""){
		


		if($this->session->userdata("LOGGED")){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			ob_start(); 		
			
			require_once(APPPATH.'libraries/tcpdf/tcpdf.php');
						

			$this->load->model('print/print_act');
			$arrdata = $this->print_act->set_data_cms($id, $awal, $akhir);
			
			#print_r($arrdata); die();
			
			$filename = str_replace(' ','','REPORT_CMS.pdf');
			
			
			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, '', true, 'UTF-8', false);		
			$pdf->SetTitle('REKAP ABSEN BULANAN');

			#$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			#$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			$pdf->SetMargins(10, 30, 10, true);
			#$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			#$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

			// set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

			// set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

			// set some language-dependent strings (optional)
			if (@file_exists(APPPATH.'libraries/tcpdf/examples/lang/eng.php')) {
				require_once(APPPATH.'libraries/tcpdf/examples/lang/eng.php');
				$pdf->setLanguageArray($l);
			}
			// ---------------------------------------------------------

			$pdf->SetFont('helvetica  ', '', 10);

			// add a page
			$pdf->AddPage('l','F4');

			// -- set new background ---

			// get the current page break margin
			$bMargin = $pdf->getBreakMargin();
			// get current auto-page-break mode
			$auto_page_break = $pdf->getAutoPageBreak();
			// disable auto-page-break
			$pdf->SetAutoPageBreak(false, 0);

			$pdf->SetAutoPageBreak($auto_page_break, $bMargin);
			// set the starting point for the page content
			$pdf->setPageMark();

			// print_r($arrdata); die();

			$text = $this->load->view('print/rekap_cms', $arrdata, true);
			$conv = htmlentities($text);
			$convstr = html_entity_decode($conv);
			// echo $convstr; die();
			// $pdf->writeHTML($convstr, true, 0, true, 0);
			$pdf->writeHTML($convstr, true, false, true, false, '');
			
			$pdf->Output($filename);			

		}else{
			redirect(base_url());
		}
	
	}
	
}
