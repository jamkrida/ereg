<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends  CI_Controller {
	
	function verify($sessid="", $isajax=""){
		$ret = "";
		#echo $_SERVER['REQUEST_METHOD']; die('acc');

		if(strtolower($_SERVER['REQUEST_METHOD'])!="post")  $ret = "NO";
		$uid = $this->input->post('uid_');		
		$pwd = $this->input->post('pass_');
		/*|| $this->session->userdata('session_id')!=$sessid*/
		
		if($ret==""){
			
			$this->load->model('user_act');
			$hasil = $this->user_act->login($uid, $pwd);
			if($hasil==1)
				$ret = "YES";
			else if($hasil==2)
				$ret = "AKT";
			else if($hasil==3)
				$ret = "NTA";
			else
				$ret = "NO";
		}
		if($isajax!="ajax"){
			redirect(base_url());
			exit();
		}
		echo $ret;
	}
	
	function forgot($sessid="", $isajax=""){
		$ret = "";
		if(strtolower($_SERVER['REQUEST_METHOD'])!="post") $ret = "NO";
		$uid = $this->input->post('uid_');
		$npwp = $this->input->post('npwp_');
		if($ret==""){
			$this->load->model('user_act');
			$ret = $this->user_act->forgot($uid, $npwp);
		}
		if($isajax!="ajax"){
			redirect(base_url());
			exit();
		}
		echo $ret;
	}
	

	

	
	
}