<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');


class Download extends CI_Controller {
    
    function files($id="",$filename="") {
		
		$cbs =& get_instance();
		$cbs->load->model("main","main", true);
		$path = $cbs->main->get_uraian("SELECT REG_PATHFILE FROM tx_reg WHERE REG_NOMOR = '$id' ","REG_PATHFILE");
		
	   #$data = base64_decode($path);
		$imginfo = getimagesize($path);
		
		header("Content-type: $imginfo[mime]");
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: '.filesize($path));
		ob_clean();
		flush();
		readfile($path);
		exit;
    }
	
	
	function files_kendaraan($id="",$filename="") {
		
		$cbs =& get_instance();
		$cbs->load->model("main","main", true);
		$path = $cbs->main->get_uraian("SELECT PATH_FILE FROM tx_kendaraan_doc WHERE REG_ID = '$id' ","PATH_FILE");
		
	   #$data = base64_decode($path);
		$imginfo = getimagesize($path);
		
		header("Content-type: $imginfo[mime]");
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: '.filesize($path));
		ob_clean();
		flush();
		readfile($path);
		exit;
    }
	
	function qrcode(){
        require_once(APPPATH.'libraries/phpqrcode/qrlib.php');
		
		$tempdir = "temp/"; //Nama folder tempat menyimpan file qrcode
		
		if (!file_exists($tempdir)) //Buat folder bername temp
		mkdir($tempdir);

			//ambil logo
		$logopath="https://cdn.pixabay.com/photo/2018/05/08/18/25/facebook-3383596_960_720.png";

		 //isi qrcode jika di scan
		 $codeContents = 'https://www.facebook.com'; 

		 //simpan file qrcode
		 QRcode::png($codeContents, $tempdir.'qrwithlogo.png', QR_ECLEVEL_H, 10,4);


		 // ambil file qrcode
		 $QR = imagecreatefrompng($tempdir.'qrwithlogo.png');

		 // memulai menggambar logo dalam file qrcode
		 $logo = imagecreatefromstring(file_get_contents($logopath));
		 
		 imagecolortransparent($logo , imagecolorallocatealpha($logo , 0, 0, 0, 127));
		 imagealphablending($logo , false);
		 imagesavealpha($logo , true);

		 $QR_width = imagesx($QR);
		 $QR_height = imagesy($QR);

		 $logo_width = imagesx($logo);
		 $logo_height = imagesy($logo);

		 // Scale logo to fit in the QR Code
		 $logo_qr_width = $QR_width/8;
		 $scale = $logo_width/$logo_qr_width;
		 $logo_qr_height = $logo_height/$scale;

		 imagecopyresampled($QR, $logo, $QR_width/2.3, $QR_height/2.3, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);

		 // Simpan kode QR lagi, dengan logo di atasnya
		 imagepng($QR,$tempdir.'qrwithlogo.png');

		  //menampilkan file qrcode 
		 echo '<div align="center"><h2>Create QR Code With Logo PHP</h2>';
		 echo '<img src="'.$tempdir.'qrwithlogo.png'.'" />';
		 echo '<br><a href="https://www.maribelajarcoding.com">maribelajarcoding.com</a><div>';
	
	}	
	
	function vendor($id="",$subid="",$filename="") {
		
		$cbs =& get_instance();
		$cbs->load->model("main","main", true);
		
		if($subid=="01"){
			$path = $cbs->main->get_uraian("SELECT FILE_NIB FROM tm_vendor WHERE ID_VENDOR = '$id' ","FILE_NIB");		
		}else if($subid=="02"){
			$path = $cbs->main->get_uraian("SELECT FILE_KTP FROM tm_vendor WHERE ID_VENDOR = '$id' ","FILE_KTP");
		}else if($subid=="03"){
			$path = $cbs->main->get_uraian("SELECT FILE_NPWP FROM tm_vendor WHERE ID_VENDOR = '$id' ","FILE_NPWP");
		}else if($subid=="04"){
			$path = $cbs->main->get_uraian("SELECT FILE_AKTA_PENDIRI FROM tm_vendor WHERE ID_VENDOR = '$id' ","FILE_AKTA_PENDIRI");
		}
	
		
	   #$data = base64_decode($path);
		$imginfo = getimagesize($path);
		
		header("Content-type: $imginfo[mime]");
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: '.filesize($path));
		ob_clean();
		flush();
		readfile($path);
		exit;
    }

	 
   


}

?>