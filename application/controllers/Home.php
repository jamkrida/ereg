<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	var $content = "";
	var $judul = "";
	var $aktif;
	
	function __construct(){
		parent::__construct();
		$this->load->model('act_model');
	}
	
	public function index(){
		#$this->load->view('cms');
		if($this->session->userdata('LOGGED')){
			$appname = 'Aplikasi Investasi Jamkrida Jakarta';
				
			$this->load->model("proses_act");
			$counter = $this->proses_act->get_counter();
			
			if($this->content==""){
				$this->load->model("proses_act");
				$counter = $this->proses_act->get_counter();
				#print_r($counter); die();	
				$this->content = $this->load->view('welcome', $counter, true);
				
				$this->judul = "Selamat Datang";
				$this->aktif = "home";
				$_SESSION['menu'] = '';
			}
			
			
			
			$data = array('_appname_' => $appname,
						  '_content_' => $this->content,
						  '_judul_' => $this->judul,
						  'counter' => $counter,
						  'aktif' => $this->aktif
						);
						
			
			$this->parser->parse('cms', $data);
		}else{
			$this->session->sess_destroy();
			$this->load->view("login");
		}
	}
	
	function register(){
		
		$this->load->view("register");
	}
	
	function forms($tgl="", $id=""){
		
		$this->load->view("form_register");
	}
	
	function register_ok(){
		
		$this->load->view("register_ok");
	}
	
	function register_ok_new(){
		
		$this->load->view("register_ok_new");
	}
	
	
	function admin_act($action="", $isajax=""){
		if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){ #Administrator, Operator Insert
			redirect(base_url());
			exit();
		}else{
			$this->load->model("user_act");
			$ret = $this->user_act->set_login($action, $isajax);
		}
		
		if($isajax!="ajax"){
			redirect(base_url());
			exit();
		}
		echo $ret;
	}
	
	function dokumen($menu="", $submenu="", $id="", $subid=""){
		if($this->session->userdata('LAST_UPDATE_PASSWORD')==""){
			print('<script>alert("Silahkan Ubah Password"); window.location.href = "'.site_url().'/home/password/edit";</script>');
		}
		if($menu=="new"){
			#print_r($_SESSION); die();
			$_SESSION['menu'] = 'newact';	
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->get_dokumen();
			
			$this->content = $this->load->view('dokumen/input_file', $arrdata, true);
		}else if($menu=="regSurat"){
			$_SESSION['menu'] = $menu;	
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->get_reg_surat();
			$this->content = $this->load->view('dokumen/registrasi_surat', $arrdata, true);
		}else if($menu=="inputRapat"){
			$_SESSION['menu'] = $menu;	
			#$this->load->model('registrasi_act');
			#$arrdata = $this->registrasi_act->get_dokumen();
			
			$this->content = $this->load->view('dokumen/input_rapat', '', true);
		}else if($menu=="inputStok"){
			$_SESSION['menu'] = $menu;	
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->get_stok();
			$this->content = $this->load->view('dokumen/input_stok', $arrdata, true);
		}else if($menu=="inputCMS"){
			$_SESSION['menu'] = $menu;	
			
			$this->content = $this->load->view('dokumen/input_cms', '', true);
		}else if($menu=="SetFixCMS"){
		
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->get_cms($submenu);
			$this->content = $this->load->view('dokumen/input_fix_cms', $arrdata, true);
		}else if($menu=="kendaraan"){
			$_SESSION['menu'] = 'newreg';	
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->get_kendaraan();
			
			$this->content = $this->load->view('dokumen/regis_kendaraan', $arrdata, true);
		}else if($menu=="regCMS"){
			$_SESSION['menu'] = 'regCMS';	
			#$this->load->model('registrasi_act');
			#$arrdata = $this->registrasi_act->get_kendaraan();
			
			$this->content = $this->load->view('dokumen/regis_cms', '', true);
		}else if($menu=="edit_pembayaran"){
			$_SESSION['menu'] = 'newact';	
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->get_dokumen($menu, $submenu);
			$this->content = $this->load->view('dokumen/edit_pembayaran', $arrdata, true);
		}else if($menu=="new_upload"){
			$_SESSION['menu'] = 'newact_upload';	
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->get_dokumen();
			$this->content = $this->load->view('dokumen/upload_klaim', $arrdata, true);
		}else if($menu=="upload_pembayaran"){
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->get_dokumen_upload($menu, $submenu);
			$this->content = $this->load->view('dokumen/upload', $arrdata, true);
		}else if($menu=="update_driver"){
			$this->load->model('registrasi_act');
			#echo $menu.'='.$submenu; die();
			$arrdata = $this->registrasi_act->get_kendaraan($menu, $submenu);
			$this->content = $this->load->view('dokumen/prev_regis_kendaraan', $arrdata, true);
		}else if($menu=="upload_regis_kendaraan"){
			$this->load->model('registrasi_act');
			
			$arrdata = $this->registrasi_act->list_upload_kendaraan($submenu);
			$arrdata = array_merge ($arrdata , $this->registrasi_act->get_dokumen_upload_kendaraan($menu, $submenu));
			#$arrdata = $this->registrasi_act->get_dokumen_upload_kendaraan($menu, $submenu);
			$this->content = $this->load->view('dokumen/upload_regis_kendaraan', $arrdata, true);
		}else if($menu=="edit_kasbon"){
			$_SESSION['menu'] = 'newact';	
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->get_dokumen($menu, $submenu);
			$this->content = $this->load->view('dokumen/edit_kasbon', $arrdata, true);
		}else if($menu=="edit_pelunasan_kasbon"){
			$_SESSION['menu'] = 'newact';	
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->get_dokumen($menu, $submenu);
			$this->content = $this->load->view('dokumen/edit_pelunasan_kasbon', $arrdata, true);
		}else if($menu=="list"){  	
			$_SESSION['menu'] = $id;	
			$this->content = $this->load->view($submenu.'/'.$id, $arrdata, true);		
		}else if($menu=="listdata"){  	
			$_SESSION['menu'] = $id;	
			
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->list_registrasi($id);
			$this->content = $this->load->view('table', $arrdata, true);			
		}else if($menu=="listregis"){  	
			$_SESSION['menu'] = $submenu;	
			
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->list_registrasi_kendaraan($submenu);
			$this->content = $this->load->view('table', $arrdata, true);			
		}else if($menu=="listCMS"){  	
			$_SESSION['menu'] = $submenu;	
			
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->list_cms($submenu);
			$this->content = $this->load->view('table', $arrdata, true);			
		}else if($menu=="listRegus"){  	
			$_SESSION['menu'] = $submenu;	
			
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->list_regis($submenu);
			$this->content = $this->load->view('table', $arrdata, true);			
		}else if($menu=="listNominatif"){  	
			$_SESSION['menu'] = $submenu;	
			
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->list_nominatif($submenu);
			$this->content = $this->load->view('table', $arrdata, true);			
		}else if($menu=="report"){
			$_SESSION['menu'] = 'reportAct';	
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->get_dokumen();
			$this->content = $this->load->view('dokumen/report', $arrdata, true);
		}else if($menu=="CashFLow"){
			$_SESSION['menu'] = $menu;	
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->get_dokumen();
			$this->content = $this->load->view('dokumen/report_cashflow', $arrdata, true);
		}else if($menu=="report_kendaraan"){
			$_SESSION['menu'] = 'reporkendaraan';	
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->get_dokumen();
			$this->content = $this->load->view('dokumen/report_kendaraan', $arrdata, true);
		}else if($menu=="reportRekanan"){
			$_SESSION['menu'] = 'reportRekanan';	
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->get_dokumen();
			$this->content = $this->load->view('dokumen/reportRekanan', $arrdata, true);
		}
		$this->index();	
	}
	
	function cancled($doc="", $id=""){
	
		$this->db->where('REG_NOMOR', $id);
		
		$isi = array('STATUS' => '2');
		
		$this->db->update('tx_reg',$isi);
			
		redirect(site_url('home/dokumen/list/datatables/'.$doc));	
		
		
	}
	
	function ajaxTable($menu = '', $menuId= ''){
		
		$this->act_model->ajaxTable($menu, $menuId);
	}
	
	function user($menu="", $submenu=""){
		
		if($menu=="list"){  
			$_SESSION['menu'] = 'listuser';
			$this->load->model('user_act');
			$arrdata = $this->user_act->list_user($menu);
			$this->content = $this->load->view('table', $arrdata, true);
			$this->index();		
		}else if($menu=="list_driver"){
			$_SESSION['menu'] = 'driver';
			$this->load->model('user_act');
			$arrdata = $this->user_act->list_user_driver($menu);
			$this->content = $this->load->view('table', $arrdata, true);
			$this->index();	
		}else if($menu =="listRekanan" || $menu =="listSetujuiRekanan" || $menu =="listTolak"){
			
			$_SESSION['menu'] = $menu;
			$this->load->model('user_act');
			$arrdata = $this->user_act->list_rekanan($menu);
			$this->content = $this->load->view('table', $arrdata, true);
			$this->index();	
		}else if($menu=="detil"){
			$this->load->model('user_act');
			$arrdata = $this->user_act->get_detil_upload($menu, $submenu);
			
			$this->load->view('add_file', $arrdata);
			
		}else if($menu =="new"){
			
			$this->load->model('user_act');
			$arrdata = $this->user_act->get_user($menu);
			$this->content = $this->load->view('add_user', $arrdata, true);
			$this->index();	
		}
		
		
	}
	
	function rate($uid=""){
		$query = "SELECT A.*, B.*, C.USER_FULLNAME AS PEMOHON, (SELECT COUNT(REG_ID) FROM tx_ratting_driver WHERE REG_ID = A.REG_ID) JML 
					FROM tx_kendaraan A 
					LEFT JOIN tm_user B ON B.USER_ID = A.DRIVER_ID 
					LEFT JOIN tm_user C ON C.USER_ID = A.USER_CREATE
					WHERE A.REG_ID = '".$uid."' ";
		$arrdata['sess'] = $this->db->query($query)->row_array();
		$jml = (int)$arrdata['sess']['JML'];
		if($jml==0){
			$isi = array('REG_ID' => $uid, 
						 'DRIVER_ID' => $arrdata['sess']['DRIVER_ID'],
						 'USER_NILAI' => $arrdata['sess']['USER_CREATE'],
						 );
			$this->db->insert('tx_ratting_driver', $isi);			 
		}
		
		
		$this->content = $this->load->view('rate', $arrdata, true);
		$this->index();	
	}
	
	function cms($menu="", $submenu="", $id="", $subid=""){
		if($menu=="get_report"){
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->get_report_cashflow($submenu, $id, $subid);		
			$this->content = $this->load->view('dokumen/get_cashflow', $arrdata, true);
			
		}
		$this->index();	
	}
	
	function ulasan($action="", $isajax="") {
		if($action=="setulasan"){
			
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();	
			}else{ 	
				
				$this->load->model('user_act');
				$ret = $this->user_act->set_ulasan($action, $isajax);
			}

			if($isajax!="ajax"){
				redirect(base_url());
				exit();
			}
			echo $ret;	
			
			
		}
	}
	
	function password($action="", $isajax="") {
		if($action=="edit") {
			$_SESSION['menu'] = 'pass';
			$this->content = $this->load->view('password', '', true);
			$this->index();	
		}else if($action=="setpassword"){
			
			if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
				redirect(base_url());
				exit();	
			}else{ 	
				#print_r($_POST); die('asAS');
				$this->load->model('user_act');
				$ret = $this->user_act->ubahpassword($action, $isajax);
			}

			if($isajax!="ajax"){
				redirect(base_url());
				exit();
			}
			echo $ret;	
			
			
		}
	}

	function get_laporan($param="", $tahun="", $bulan=""){
		
		if($param == "excel"){
			$jenis = $this->input->post('JENIS_DOKUMEN');
			$tanggal = $this->input->post('TANGGAL');
		
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->set_data_excel($jenis, $tanggal);
			
			if($jenis=="101"){
				$ret = $this->load->view('laporan/pembayaran', $arrdata ,true);
				$headers = '';
				#$file = 'Laporan_pembayaran.xlsx'
				header("Cache-Control: no-cache, no-store, must-revalidate");
				header("Content-Type: application/vnd.ms-excel");
				header("Content-Disposition: attachment; filename=\"LAPORAN PEMBAYARAN.xls\"");
				
				echo "$headers\n$ret";
				
			}else if($jenis =="102"){
				$ret = $this->load->view('laporan/kasbon', $arrdata ,true);
				$headers = '';
				header("Cache-Control: no-cache, no-store, must-revalidate");
				header("Content-Type: application/vnd.ms-excel");
				header("Content-Disposition: attachment; filename=\"LAPORAN KASBON.xls\"");
				echo "$headers\n$ret";
				
			}else if($jenis =="103"){
				$ret = $this->load->view('laporan/pelunasan_kasbon', $arrdata ,true);
				$headers = '';
				header("Cache-Control: no-cache, no-store, must-revalidate");
				header("Content-Type: application/vnd.ms-excel");
				header("Content-Disposition: attachment; filename=\"LAPORAN PELUNASAN KASBON.xls\"");
				echo "$headers\n$ret";
				
			}
			
		}else if($param == "excel_kendaraan"){
			$np = $this->input->post('NO_POLISI');
			$tanggal = $this->input->post('TANGGAL');
		
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->set_data_kendaraan($tanggal, $np);
			$ret = $this->load->view('laporan/kendaraan', $arrdata ,true);
			
			$headers = '';
			header("Cache-Control: no-cache, no-store, must-revalidate");
			header("Content-Type: application/vnd.ms-excel");
			header("Content-Disposition: attachment; filename=\"LAPORAN PENGGUNAAN KENDARAAN.xls\"");
			echo "$headers\n$ret";
		}else if($param == "excelRekanan"){
			$jenis = $this->input->post('STATUS');
			$tanggal = $this->input->post('TANGGAL');
		
			$this->load->model('registrasi_act');
			$arrdata = $this->registrasi_act->set_data_rekanan($jenis, $tanggal);
			$ret = $this->load->view('laporan/rekanan', $arrdata ,true);
			
			$headers = '';
			header("Cache-Control: no-cache, no-store, must-revalidate");
			header("Content-Type: application/vnd.ms-excel");
			header("Content-Disposition: attachment; filename=\"LAPORAN REKANAN.xls\"");
			echo "$headers\n$ret";
		}
		
	}
	
	function registrasi_act($action="", $isajax=""){ 		
		if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
			redirect(base_url());
			exit();	
		}else{ 	
			#print_r($_POST); die('asAS');
			$this->load->model('registrasi_act');
			$ret = $this->registrasi_act->set_pengajuan($action, $isajax);
		}

		if($isajax!="ajax"){
			redirect(base_url());
			exit();
		}
		echo $ret;	
	}
	
	function sendd(){
		
		$this->load->model('main');
		$email   = 'ramdhaniwijatniko@gmail.com';
		
		$nama  	 = 'Ramdhani Wijatniko';
		$subject = "Email Notifikasi";
		$isi     = 'Pengajuan Notifikasi Anda dengan Reg ID <b>'.'EREG100215VR11900069'.'</b> telah disetujui.';
		if($this->main->send_mail($email, $nama, $subject, $isi)){
			#$this->main->send_mailbpom($email, $nama, $subject, $isi);	
			 echo 'Success 1';
			}else{
				
				#$this->main->send_mail($email, $nama, $subject, $isi);
				echo 'Alternative';
			}
		die();	
			

	}
	
	function register_act($action="", $isajax=""){ 		
		if(strtolower($_SERVER['REQUEST_METHOD'])!="post"){
			redirect(base_url());
			exit();	
		}else{ 	
			
			$this->load->model('registrasi_act');
			$ret = $this->registrasi_act->set_vendor($action, $isajax);
		}

		if($isajax!="ajax"){
			redirect(base_url());
			exit();
		}
		echo $ret;	
	}
	
	function logout(){
		#UPDATE LAST LOGIN
		$data = array('LAST_LOGOUT' => $this->session->userdata('USER_LOGINDATE'));
		$this->db->where('USER_ID', $this->session->userdata('USER_ID'));
		$this->db->update('tm_user', $data);
				
		#DESTROY
		$this->session->sess_destroy();
		$url = explode('/', base_url());
		$this->load->helper('url');
		redirect(base_url());  
		
	}
	
}
