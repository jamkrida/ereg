<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Registrasi_act extends CI_Model{

	function get_dokumen($menu= "", $submenu=""){
		
		$cbs =& get_instance();
		$cbs->load->model("main","main", true);
		
		$jnsreg = $cbs->main->get_combobox("SELECT REFF_CODE, REFF_DESC FROM tb_reff WHERE REFF_TAB = 'JENIS_REGISTRASI'","REFF_CODE","REFF_DESC", TRUE);
		
		$kendaraan = $cbs->main->get_combobox("SELECT A.NO_POLISI, CONCAT(B.USER_FULLNAME,'-',A.NO_POLISI) NAMA 
											   FROM tm_kendaraan A LEFT JOIN tm_user B ON B.USER_ID = A.DRIVER_ID WHERE A.DRIVER_ID IS NOT NULL 
											   UNION ALL
											   SELECT 'DRIVER_ONLINE' , 'Gojek/Gocorp' ","NO_POLISI","NAMA", TRUE);	
		
		
		#print_r($_SESSION); die();
		$jabatan = $cbs->main->get_combobox("SELECT REFF_CODE, REFF_DESC FROM tb_reff WHERE REFF_TAB = 'JABATAN' AND (LENGTH(REFF_CODE) = 3 OR LENGTH(REFF_CODE) = 1 OR REFF_CODE = '14')","REFF_CODE","REFF_DESC", TRUE);
		
		$atasan = $cbs->main->get_combobox("SELECT USER_ID, USER_FULLNAME FROM tm_user WHERE USER_ROLE IN ('03','06','08') AND USER_STATUS = '1'","USER_ID","USER_FULLNAME", TRUE);
		
		$manajer = $cbs->main->get_combobox("SELECT USER_ID, USER_FULLNAME FROM tm_user WHERE USER_ROLE = '03'","USER_ID","USER_FULLNAME", TRUE);
		
		$staffkeu = $cbs->main->get_combobox("SELECT USER_ID, USER_FULLNAME FROM tm_user WHERE USER_ROLE = '02' AND USER_JABATAN = '3.2.1' AND USER_ID = '10'","USER_ID","USER_FULLNAME", TRUE);	
		
		if($submenu==""){
			#AND REG_NOMOR NOT IN (SELECT REG_NOMOR_KASBON FROM tx_reg WHERE REG_JENIS_DOKUMEN = '103')
			$regnomor = $cbs->main->get_combobox("SELECT CONCAT(TANGGAL_PENGAJUAN,'||',REG_PERIHAL,'||', REG_NOMOR) AS ID , REG_NOMOR FROM tx_reg WHERE REG_JENIS_DOKUMEN = '102'","ID","REG_NOMOR", TRUE);
		}else{
			$regnomor = $cbs->main->get_combobox("SELECT CONCAT(TANGGAL_PENGAJUAN,'||', REG_NOMOR) AS ID , REG_NOMOR FROM tx_reg WHERE REG_JENIS_DOKUMEN = '102'","ID","REG_NOMOR", TRUE);
					
		}
			
		
	    $departemen = $cbs->main->get_combobox("SELECT REFF_CODE, REFF_DESC FROM tb_reff WHERE REFF_TAB = 'DEPARTEMEN'","REFF_CODE","REFF_DESC", TRUE);		
	    $act = site_url().'/home/registrasi_act/savedokumen';	
		
		
		
	   if(($menu=="edit_pembayaran" || $menu=="edit_kasbon" || $menu=="edit_pelunasan_kasbon") && $submenu !=""){
			   $act = site_url().'/home/registrasi_act/'.$menu;
			   $query = "SELECT * FROM tx_reg WHERE REG_NOMOR = '".$submenu."'";
			   $res = $cbs->main->get_result($query);
				if($res){
					foreach($query->result_array() as $datas){
						$sess = $datas;
						
					}
					
						$comit = "SELECT * FROM tx_reg_detil WHERE REG_ID = '".$sess['REG_ID']."' ";
					
						$comspec = $cbs->main->get_result($comit);
						if($comspec){
							foreach($comit->result_array() as $arrs){
									$arrdetil[] = $arrs;
									
							}					
						}
					
				} #RES----
		}
	
		$arrdata = array('jnsreg' => $jnsreg,
						 'sess' => $sess,
						 'arrdetil' => $arrdetil,
						 'departemen' => $departemen,
						 'manajer' => $manajer,
						 'kendaraan' => $kendaraan,
						 'atasan' => $atasan,
						 'regnomor' => $regnomor,
						 'jabatan' => $jabatan,
						 'staffkeu' => $staffkeu,
						 'act' => $act
						 );
						
		return $arrdata;
	}
	
	function get_dokumen_upload($menu= "", $submenu=""){
		
		$cbs =& get_instance();
		$cbs->load->model("main","main", true);
		
		 $query = "SELECT * FROM tx_reg WHERE REG_NOMOR = '".$submenu."'";
		   $res = $cbs->main->get_result($query);
			if($res){
				foreach($query->result_array() as $datas){
					$sess = $datas;
					
				}
				
			}	
	    $act = site_url().'/home/registrasi_act/savedokumenupload';	
	
		$arrdata = array('sess' => $sess,
						 'act' => $act
						 );
						
		return $arrdata;
	}
	
	function get_dokumen_upload_kendaraan($menu= "", $submenu=""){
		
		$cbs =& get_instance();
		$cbs->load->model("main","main", true);
		#echo $menu.'='.$submenu; die();
		 $query = "SELECT * FROM tx_kendaraan WHERE REG_ID = '".$submenu."'";
		   $res = $cbs->main->get_result($query);
			if($res){
				foreach($query->result_array() as $datas){
					$sess = $datas;
					
				}
				
			}	
	    $act = site_url().'/home/registrasi_act/saveuploadkendaraan';	
	
		$arrdata = array('sess' => $sess,
						 'act' => $act
						 );
						
		return $arrdata;
	}
	
	function get_reg_surat(){
		$cbs =& get_instance();
		$cbs->load->model("main","main", true);
		$jnsregsurat = $cbs->main->get_combobox("SELECT REFF_CODE, REFF_DESC FROM tb_reff WHERE REFF_TAB = 'JENIS_REG_SURAT'","REFF_CODE","REFF_DESC", TRUE);
		$jns_surat = $cbs->main->get_combobox("SELECT REFF_CODE, REFF_DESC FROM tb_reff WHERE REFF_TAB = 'JENIS_SURAT'","REFF_CODE","REFF_DESC", TRUE);
		$dispo = $cbs->main->get_combobox("SELECT REFF_CODE, CONCAT(REFF_CODE,' - ',REFF_DESC) JABATAN FROM tb_reff WHERE REFF_TAB = 'JABATAN_NEW' AND REFF_CODE IN ('1','1.1','1.2','2','2.1','2.1','3.1','3.2')","REFF_CODE","JABATAN", TRUE);
	    $type = $cbs->main->get_combobox("SELECT REFF_CODE, REFF_DESC FROM tb_reff WHERE REFF_TAB = 'TIPE_SURAT'","REFF_CODE","REFF_DESC", TRUE);
		$act = site_url().'/home/registrasi_act/saveregsurat';	
	
		$arrdata = array(
						 'jnsregsurat' => $jnsregsurat,
						 'jns_surat' => $jns_surat,
						 'dispo' => $dispo,
						 'type' => $type,
						 'act' => $act
						 );
						
		return $arrdata;
		
	}
	
	function get_stok($menu= "", $submenu=""){
		
		$cbs =& get_instance();
		$cbs->load->model("main","main", true);
		#echo $menu.'='.$submenu; die();
		 /*
		 $query = "SELECT * FROM tx_kendaraan WHERE REG_ID = '".$submenu."'";
		   $res = $cbs->main->get_result($query);
			if($res){
				foreach($query->result_array() as $datas){
					$sess = $datas;
					
				}
				
			}
		*/	
		$jnsstok = $cbs->main->get_combobox("SELECT REFF_CODE, REFF_DESC FROM tb_reff WHERE REFF_TAB = 'JENIS_STOK'","REFF_CODE","REFF_DESC", TRUE);
		$satuan = $cbs->main->get_combobox("SELECT REFF_CODE, REFF_DESC FROM tb_reff WHERE REFF_TAB = 'SATUAN'","REFF_CODE","REFF_DESC", TRUE);	
	    $act = site_url().'/home/registrasi_act/savestok';	
	
		$arrdata = array('jnsstok' => $jnsstok,
						 'satuan' => $satuan,
						 'act' => $act
						 );
						
		return $arrdata;
	}
	
	
	function get_kendaraan($menu= "", $submenu=""){
		
		$cbs =& get_instance();
		$cbs->load->model("main","main", true);
		
		$chk = (int)$cbs->main->get_uraian("SELECT COUNT(A.REG_ID) JML FROM tx_ratting_driver A LEFT JOIN tx_kendaraan B ON B.REG_ID = A.REG_ID WHERE A.USER_NILAI = '".$this->session->userdata('USER_ID')."' AND A.NILAI IS NULL AND B.STATUS = '1'","JML");
		
		if($chk > 0){
			
			$aju = $cbs->main->get_uraian("SELECT A.REG_ID FROM tx_ratting_driver A LEFT JOIN tx_kendaraan B ON B.REG_ID = A.REG_ID WHERE A.USER_NILAI = '".$this->session->userdata('USER_ID')."' AND A.NILAI IS NULL AND B.STATUS = '1' LIMIT 1","REG_ID");
			redirect(site_url('home/rate/'.$aju));
		}
		
		if($this->session->userdata('USER_ROLE')=="01" || $this->session->userdata('USER_ID')=="49"){
			$departemen = $cbs->main->get_combobox("SELECT REFF_CODE, REFF_DESC FROM tb_reff WHERE REFF_TAB = 'DEPARTEMEN'","REFF_CODE","REFF_DESC", TRUE);
			$pemohon = $cbs->main->get_combobox("SELECT USER_ID, USER_FULLNAME FROM tm_user WHERE USER_ID NOT IN (SELECT DRIVER_ID FROM tm_kendaraan WHERE DRIVER_ID IS NOT NULL) ORDER BY 2","USER_ID","USER_FULLNAME", TRUE);
		}else if( $this->session->userdata('USER_ID')=="53"){
			$departemen = $cbs->main->get_combobox("SELECT REFF_CODE, REFF_DESC FROM tb_reff WHERE REFF_TAB = 'DEPARTEMEN' AND REFF_CODE = '".$this->session->userdata('USER_DEPARTEMEN')."'","REFF_CODE","REFF_DESC", FALSE);
			$pemohon = $cbs->main->get_combobox("SELECT USER_ID, USER_FULLNAME FROM tm_user WHERE USER_ID NOT IN (SELECT DRIVER_ID FROM tm_kendaraan WHERE DRIVER_ID IS NOT NULL) 
													UNION ALL
													SELECT USER_ID, USER_FULLNAME FROM tm_user WHERE  USER_ID = '".$this->session->userdata('USER_ID')."'","USER_ID","USER_FULLNAME", TRUE);
			
		}else{
			$departemen = $cbs->main->get_combobox("SELECT REFF_CODE, REFF_DESC FROM tb_reff WHERE REFF_TAB = 'DEPARTEMEN' AND REFF_CODE = '".$this->session->userdata('USER_DEPARTEMEN')."'","REFF_CODE","REFF_DESC", FALSE);
			$pemohon = $cbs->main->get_combobox("SELECT USER_ID, USER_FULLNAME FROM tm_user WHERE USER_ID NOT IN (SELECT DRIVER_ID FROM tm_kendaraan WHERE DRIVER_ID IS NOT NULL) AND 
											     USER_ID = '".$this->session->userdata('USER_ID')."'","USER_ID","USER_FULLNAME", FALSE);
		}	
		
		if($this->session->userdata('USER_ID')=="79"){
			$manajer = $cbs->main->get_combobox("SELECT USER_ID, USER_FULLNAME FROM tm_user WHERE USER_ID = '".$this->session->userdata('USER_ID')."'","USER_ID","USER_FULLNAME", TRUE);
		}else{
			$manajer = $cbs->main->get_combobox("SELECT USER_ID, USER_FULLNAME FROM tm_user WHERE USER_ROLE IN ('03','06') ORDER BY 2","USER_ID","USER_FULLNAME", TRUE);
		}
			
		
		$jam = date('H'); 
		$hari = date('d'); 
		
		if($this->session->userdata('USER_ROLE')=="01" || $this->session->userdata('USER_ID')=="49" || $this->session->userdata('USER_ID')=="53"){
			
			$driver = $cbs->main->get_combobox("SELECT CONCAT(A.USER_ID,'||',B.MERK_KENDARAAN,' - ',B.NO_POLISI) ID, CONCAT(A.USER_FULLNAME,' (',B.NO_POLISI,')') USER 
														FROM tm_user A LEFT JOIN tm_kendaraan B ON B.DRIVER_ID = A.USER_ID WHERE B.STATUS = '0'","ID","USER", TRUE);
			
		}else{
			$driver = $cbs->main->get_combobox("SELECT CONCAT(A.USER_ID,'||',B.MERK_KENDARAAN,' - ',B.NO_POLISI) ID, CONCAT(A.USER_FULLNAME,' (',B.NO_POLISI,')') USER 
														FROM tm_user A LEFT JOIN tm_kendaraan B ON B.DRIVER_ID = A.USER_ID WHERE B.STATUS = '0'","ID","USER", TRUE);
			/*
			if ($hari % 2 == 0){ 
				
				if($jam >= 10 && $jam < 16){
					$driver = $cbs->main->get_combobox("SELECT CONCAT(A.USER_ID,'||',B.MERK_KENDARAAN,' - ',B.NO_POLISI) ID, CONCAT(A.USER_FULLNAME,' (',B.NO_POLISI,')') USER 
														FROM tm_user A LEFT JOIN tm_kendaraan B ON B.DRIVER_ID = A.USER_ID WHERE B.STATUS = '0'","ID","USER", TRUE);
				}else{
				$driver = $cbs->main->get_combobox("SELECT CONCAT(A.USER_ID,'||',B.MERK_KENDARAAN,' - ',B.NO_POLISI) ID, CONCAT(A.USER_FULLNAME,' (',B.NO_POLISI,')') USER 
													FROM tm_user A LEFT JOIN tm_kendaraan B ON B.DRIVER_ID = A.USER_ID WHERE B.STATUS = '0' AND SUBSTRING(B.NO_POLISI,3,4) %2 = 0","ID","USER", TRUE);
				}
			}else {
				if($jam >= 10 && $jam <= 16){
					$driver = $cbs->main->get_combobox("SELECT CONCAT(A.USER_ID,'||',B.MERK_KENDARAAN,' - ',B.NO_POLISI) ID, CONCAT(A.USER_FULLNAME,' (',B.NO_POLISI,')') USER 
														FROM tm_user A LEFT JOIN tm_kendaraan B ON B.DRIVER_ID = A.USER_ID WHERE B.STATUS = '0'","ID","USER", TRUE);
				}else{
					$driver = $cbs->main->get_combobox("SELECT CONCAT(A.USER_ID,'||',B.MERK_KENDARAAN,' - ',B.NO_POLISI) ID, CONCAT(A.USER_FULLNAME,' (',B.NO_POLISI,')') USER 
														FROM tm_user A LEFT JOIN tm_kendaraan B ON B.DRIVER_ID = A.USER_ID WHERE B.STATUS = '0' AND SUBSTRING(B.NO_POLISI,3,4) %2 <> 0","ID","USER", TRUE);
				}	
				
			}
			*/
		}
		if($menu=="update_driver" && $submenu !=""){
			$jns_kendaraan = $cbs->main->get_combobox("SELECT REFF_CODE, REFF_DESC FROM tb_reff WHERE REFF_TAB = 'JENIS_KENDARAAN' ","REFF_CODE","REFF_DESC", TRUE);
			$query = "SELECT * FROM tx_kendaraan WHERE REG_ID = '".$submenu."'";
			$res = $this->db->query($query)->result_array();	
			foreach($res as $isi){
				$datas = $isi;
			}

			$act = site_url().'/home/registrasi_act/updatedriver';	
	
			$arrdata = array('pemohon' => $pemohon,
							'datas' => $datas,
							'jns_kendaraan' => $jns_kendaraan,
							'departemen' => $departemen,
							'manajer' => $manajer,
							'driver' => $driver,
							'act' => $act, 
							'regid' => $submenu
							);
		}else{
			$act = site_url().'/home/registrasi_act/savedokumenupload';	
		
			$arrdata = array('pemohon' => $pemohon,
							'departemen' => $departemen,
							'manajer' => $manajer,
							'driver' => $driver,
							'act' => $act
							);
		}
	    
		#print_r($arrdata); die();				
		return $arrdata;
	}
	
	function get_detildeposito($id=""){
		$cbs =& get_instance();
		$cbs->load->model("main","main", true);
		$month = date('n');
		$year = date('Y');
		$arrid = explode('.', $id);
		$query = "SELECT A.*,C.MULAI_TEMPO AS MTEMPO, C.JATUH_TEMPO AS JTEMPO,  DATE_FORMAT(B.DATE_BEGIN, '%Y-%c-%e') BEGIN, DATE_FORMAT(B.DATE_END, '%Y-%c-%e') ENDS, B.DAY AS 'DTL_DAY', B.DATE_BEGIN, B.DATE_END,
				  B.INTEREST_RECEIVED_AMOUNT_TERBILANG, B.INTEREST_RECEIVED_AMOUNT, B.INTEREST_RECEIVED_DATE, C.BUNGA AS BUNGAS
						FROM tx_header_deposito A 
				  LEFT JOIN tx_calculation_date B ON B.ID_DEPOSITO = A.ID_DEPOSITO
				  LEFT JOIN tx_periode_deposito C ON C.ID_DEPOSITO = A.ID_DEPOSITO AND C.MULAI_TEMPO = B.MULAI_TEMPO				 
						WHERE A.ID_DEPOSITO = '".$arrid[0]."' AND B.INTEREST_RECEIVED_AMOUNT <> ''";
				 
				
		$ress = $cbs->main->get_result($query);
		if($ress){
			foreach($query->result_array() as $datas){
				$row[] = $datas;
				
			}
			
		}
		$arrdata = array('sess' => $row);
		return $arrdata;
	}
	
	function get_periode_deposito($menu="", $id="", $subid=""){
		$cbs =& get_instance();
		$cbs->load->model("main","main", true);
		
		
		$act = site_url().'/home/registrasi_act/saveperiode';
		$caption = "Simpan";
		$icon = '<i class="icon-save"></i>';
		
		$arrid = explode('.', $id);
		
		$depositoid = $arrid[0];
		$nmdeposito = $arrid[1];
		$bunga = $cbs->main->get_uraian("SELECT BUNGA FROM tx_header_deposito WHERE ID_DEPOSITO = '".$depositoid."'","BUNGA");
		$jangkawaktu = $cbs->main->get_uraian("SELECT CONCAT(A.JML_JANGKA_WAKTU, ' ',B.REFF_DESC, ' ', C.REFF_DESC) JANGAWAKTU 
													FROM tx_header_deposito A
											   LEFT JOIN tb_reff B ON B.REFF_CODE = A.TYPE_JANGKA_WAKTU AND B.REFF_TAB = 'JANGKA_WAKTU'
											   LEFT JOIN tb_reff C ON C.REFF_CODE = A.ARO_JANGKA_WAKTU AND C.REFF_TAB = 'ARO' 
													WHERE A.ID_DEPOSITO = '".$depositoid."'","JANGAWAKTU");
													
													
		$tglpenempatan = $cbs->main->get_uraian("SELECT DATE_FORMAT(TANGGAL_PENEMPATAN, '%Y-%m-%d') AS TANGGAL_PENEMPATAN FROM tx_header_deposito WHERE ID_DEPOSITO = '".$depositoid."'","TANGGAL_PENEMPATAN");
		
		if($menu=="edit"){
			
			
			
			$act = site_url().'/home/registrasi_act/editperiode';
			$query = "SELECT * FROM tx_periode_deposito WHERE ID_DEPOSITO = '".$depositoid."' AND PERIODE_ID = '".$subid."'";
			
			$res = $cbs->main->get_result($query);
			if($res){
				foreach($query->result_array() as $datas){
					$rows = $datas;
					
				}
				
			}
		}
		$arrdata = array('depositoid' => $depositoid,
						 'nmdeposito' => $nmdeposito,
						 'sess' => $rows,
						 'bunga' => $bunga,
						 'icon'=> $icon,
						 'tglpenempatan' => $tglpenempatan,
						 'jangkawaktu' => $jangkawaktu,
						 'act' => $act,
						 'caption' => $caption
						 );
						
		return $arrdata;
	}
	
	function set_data_rekanan($jenis="", $tanggal=""){
		$cbs =& get_instance();
		$cbs->load->model("main", "main", true);
		
		$tanggal = explode(' - ', $tanggal);
		
		if($jenis=="1" || $jenis=="0") $addwhere = 'AND A.STATUS = "'.$jenis.'"';
		
		
		$awal = date_create($tanggal[0]);
		$awal = date_format($awal,"Y-m-d");
		
		$akhir = date_create($tanggal[1]);
		$akhir = date_format($akhir,"Y-m-d");
		
		$query = "SELECT A.* FROM tm_vendor A	
				 WHERE A.DATE_CREATE BETWEEN '".$awal."' AND '".$akhir."' $addwhere ORDER BY A.DATE_CREATE ASC";
				  
		
		$ress = $cbs->main->get_result($query);
		if($ress){
			foreach($query->result_array() as $rows){
				
				$dataP[] = $rows; 
				
			}
			
		}
		
		
		$bulanarr = array('','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November', 'Desember');
		$tglind1 = date_create($tanggal[0]);
		$tglind1 = date_format($tglind1,"Y-n-d");
		$tglind1 = explode('-', $tglind1);
		$tglind1 = $tglind1[2].' '.$bulanarr[$tglind1[1]].' '.$tglind1[0];
		
		$tglind2 = date_create($tanggal[1]);
		$tglind2 = date_format($tglind2,"Y-n-d");
		$tglind2 = explode('-', $tglind2);
		$tglind2 = $tglind2[2].' '.$bulanarr[$tglind2[1]].' '.$tglind2[0];
		
		
		$arrdata = array(
						
						 'akhir' => $tglind2,
						 'awal' => $tglind1,
						 'datas' => $dataP
						 );
		return $arrdata;
	
	}
	
	function set_data_kendaraan($tanggal="", $np=""){
		$cbs =& get_instance();
		$cbs->load->model("main", "main", true);
		
		$tanggal = explode(' - ', $tanggal);
		
		
		if($np!=""){
			if($np=="DRIVER_ONLINE"){
				$addwhere = " AND A.DRIVER_ID = '0'";
			}else{
				$addwhere = "AND A.NO_KENDARAAN LIKE '%".$np."%' ";
			}
		}else{
			$addwhere = "";
		}
		
		$awal = date_create($tanggal[0]);
		$awal = date_format($awal,"Y-m-d");
		
		$akhir = date_create($tanggal[1]);
		$akhir = date_format($akhir,"Y-m-d");
		
		
		$query = "SELECT A.REG_ID, CONCAT(A.TANGGAL_PERMOHONAN,'#', A.WAKTU_PERMOHONAN) AS 'TGL_PEMAKAIAN' , 
					(CASE WHEN A.JENIS_KENDARAAN = '2' THEN A.JENIS_KENDARAAN_DESC
						  ELSE A.NO_KENDARAAN 
					 END) AS NO_KENDARAAN, 
					(CASE 
						WHEN A.JENIS_KENDARAAN = '2' THEN 'Driver Online' 
						ELSE C.USER_FULLNAME
					END) AS DRIVER, B.USER_FULLNAME AS USER, A.TUJUAN, A.KEPERLUAN, 
					(CASE
						WHEN D.NILAI = '4' THEN 'Sangat baik'
						WHEN D.NILAI = '3' THEN 'Baik'
						WHEN D.NILAI = '2' THEN 'Cukup'
						WHEN D.NILAI = '1' THEN 'Kurang'
						ELSE '-'
					END) AS PENILAIAN, IFNULL(D.CATATAN,'-') AS CTTN_DRIVER	
						FROM tx_kendaraan A
					LEFT JOIN tm_user B ON B.USER_ID = A.NAMA_PEMOHON
					LEFT JOIN tm_user C ON C.USER_ID = A.DRIVER_ID
					LEFT JOIN tx_ratting_driver D ON D.REG_ID = A.REG_ID	
						WHERE A.TANGGAL_PERMOHONAN BETWEEN '".$awal."' AND '".$akhir."' 
						 $addwhere ORDER BY A.TANGGAL_PERMOHONAN";
		
		
		$ress = $cbs->main->get_result($query);
		if($ress){
			foreach($query->result_array() as $rows){
				
				$dataP[] = $rows; 
				
			}
			
		}
		
		
		$bulanarr = array('','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November', 'Desember');
		$tglind1 = date_create($tanggal[0]);
		$tglind1 = date_format($tglind1,"Y-n-d");
		$tglind1 = explode('-', $tglind1);
		$tglind1 = $tglind1[2].' '.$bulanarr[$tglind1[1]].' '.$tglind1[0];
		
		$tglind2 = date_create($tanggal[1]);
		$tglind2 = date_format($tglind2,"Y-n-d");
		$tglind2 = explode('-', $tglind2);
		$tglind2 = $tglind2[2].' '.$bulanarr[$tglind2[1]].' '.$tglind2[0];
		
		
		$arrdata = array(
						
						 'akhir' => $tglind2,
						 'awal' => $tglind1,
						 'datas' => $dataP
						 );
		return $arrdata;
	
	}
	
	
	
	function set_data_excel($jenis="", $tanggal=""){
		$cbs =& get_instance();
		$cbs->load->model("main", "main", true);
		
		$tanggal = explode(' - ', $tanggal);
		
		
		$awal = date_create($tanggal[0]);
		$awal = date_format($awal,"Y-m-d");
		
		$akhir = date_create($tanggal[1]);
		$akhir = date_format($akhir,"Y-m-d");
		
		$query = "SELECT A.REG_NOMOR, DATE_FORMAT(A.TANGGAL_PENGAJUAN, '%d/%m/%Y') TGL, A.REG_PERIHAL, B.REFF_DESC AS UNIT_KERJA, A.REG_NOMOR_KASBON ,
				  A.REG_JUMLAH, A.TTD_STAFF_PEMOHON, A.REG_JUMLAH_TERBILANG, A.REG_PEMAKAIAN_TERBILANG
						FROM tx_reg A 
				  LEFT JOIN tb_reff B ON B.REFF_CODE = A.REG_UNIT_KERJA AND B.REFF_TAB = 'DEPARTEMEN'
						WHERE A.REG_JENIS_DOKUMEN = '".$jenis."' 
				 AND (A.STATUS IS NULL OR A.STATUS = '1')	
				 AND A.TANGGAL_PENGAJUAN BETWEEN '".$awal."' AND '".$akhir."' ORDER BY DATE_CREATE ASC";
				  
			  
		$ress = $cbs->main->get_result($query);
		if($ress){
			foreach($query->result_array() as $rows){
				
				$dataP[] = $rows; 
				
			}
			
		}
		
		
		$bulanarr = array('','Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November', 'Desember');
		$tglind1 = date_create($tanggal[0]);
		$tglind1 = date_format($tglind1,"Y-n-d");
		$tglind1 = explode('-', $tglind1);
		$tglind1 = $tglind1[2].' '.$bulanarr[$tglind1[1]].' '.$tglind1[0];
		
		$tglind2 = date_create($tanggal[1]);
		$tglind2 = date_format($tglind2,"Y-n-d");
		$tglind2 = explode('-', $tglind2);
		$tglind2 = $tglind2[2].' '.$bulanarr[$tglind2[1]].' '.$tglind2[0];
		
		
		$arrdata = array(
						
						 'akhir' => $tglind2,
						 'awal' => $tglind1,
						 'datas' => $dataP
						 );
		return $arrdata;
		
	
		
	}
	
	function get_detil_periode_deposito($menu="", $id="", $subid=""){
		$cbs =& get_instance();
		$cbs->load->model("main","main", true);
		
		
		$act = site_url().'/home/registrasi_act/savedetilperiode';
		$caption = "Simpan";
		$icon = '<i class="icon-save"></i>';
		
		$arrid = explode('.', $id);
		
		$seri = $arrid[0];
		$id = $arrid[1];
		$bunga = $cbs->main->get_uraian("SELECT BUNGA FROM tx_header_deposito WHERE ID_DEPOSITO = '".$id."'","BUNGA");
		$jangkawaktu = $cbs->main->get_uraian("SELECT CONCAT(A.JML_JANGKA_WAKTU, ' ',B.REFF_DESC, ' ', C.REFF_DESC) JANGAWAKTU 
													FROM tx_header_deposito A
											   LEFT JOIN tb_reff B ON B.REFF_CODE = A.TYPE_JANGKA_WAKTU AND B.REFF_TAB = 'JANGKA_WAKTU'
											   LEFT JOIN tb_reff C ON C.REFF_CODE = A.ARO_JANGKA_WAKTU AND C.REFF_TAB = 'ARO' 
													WHERE A.ID_DEPOSITO = '".$id."'","JANGAWAKTU");
													
		$isi = "SELECT * FROM tx_header_deposito WHERE ID_DEPOSITO = '".$id."'";
		$arrisi = $this->db->query($isi)->row_array();

		$isi2 = "SELECT * FROM tx_calculation_date WHERE SERI = '".$seri."'";
		$arrdetil = $this->db->query($isi2)->row_array();	
		$tglpenempatan = $cbs->main->get_uraian("SELECT DATE_FORMAT(TANGGAL_PENEMPATAN, '%Y-%m-%d') AS TANGGAL_PENEMPATAN FROM tx_header_deposito WHERE ID_DEPOSITO = '".$id."'","TANGGAL_PENEMPATAN");
		
		
		$arrdata = array('seri' => $seri,
						 'arrisi' => $arrisi,
						 'arrdetil' => $arrdetil,
						 'bunga' => $bunga,
						 'icon'=> $icon,
						 'tglpenempatan' => $tglpenempatan,
						 'jangkawaktu' => $jangkawaktu,
						 'act' => $act,
						 'caption' => $caption
						 );
						
		return $arrdata;
	}
	
	function set_vendor($action="", $isajax=""){
		$cbs =& get_instance();
		$cbs->load->model("main","main", true);
		
		if($action=="save_vendor"){
			$data = $cbs->main->post_to_query($this->input->post('REG'));
			
	
			$data = array(
							  'FILE_NIB' => str_replace(base_url(),'',$_POST['PATH']['01']),		
							  'FILE_NPWP' => str_replace(base_url(),'',$_POST['PATH']['02']),						  
							  'FILE_KTP' => str_replace(base_url(),'',$_POST['PATH']['03']),
							  'FILE_AKTA_PENDIRI' => str_replace(base_url(),'',$_POST['PATH']['04']),
							  'DATE_CREATE' => date("Y-m-d H:i:s")
						 );
			$data = array_merge($cbs->main->post_to_query($this->input->post('REG')), $data);
			if($this->db->insert('tm_vendor',$data)){
				return "MSG#OK#Simpan Dokumen Berhasil#".site_url().'/home/register_ok';
			}else{
				return "MSG#ERR#Simpan Pengajuan Gagal";
			} 	
		}else if($action=="save_rapat"){
			#print_r($_POST); die();
			#$dir = "upload/";
			$dir = 'dat/';
			if(!is_dir($dir)) mkdir($dir);
			chmod($dir,0777);
			
			$dir .= "ttd/";
			if(!is_dir($dir)){ 
				mkdir($dir);
			}
			chmod($dir,0777);
		
			$image_parts = explode(";base64,", $_POST['signed']);
				
			$image_type_aux = explode("image/", $image_parts[0]);
			  
			$image_type = $image_type_aux[1];
			  
			$image_base64 = base64_decode($image_parts[1]);
			
			if($_POST['signed']==""){	
				$file = null;
			}else{
				$file = $dir . uniqid() . '.'.$image_type;
			}
			$idrapat = $this->input->post('rapatid');
			$data = $cbs->main->post_to_query($this->input->post('reg'));
			$data = array('ID_RAPAT' => $idrapat,
						  'TTD' => $file,
						  'DATE_CREATE' => date("Y-m-d H:i:s")
						 );
			$data = array_merge($cbs->main->post_to_query($this->input->post('reg')), $data);
			file_put_contents($file, $image_base64);
			
			if($this->db->insert('t_detil_rapat',$data)){
				
				return "MSG#OK#Simpan Berhasil#".site_url().'/home/register_ok_new';
			}else{
				return "MSG#ERR#Simpan Pengajuan Gagal";
			} 	
			
			
			
		}	
	}
	
	function set_pengajuan($action="", $isajax=""){
		if($this->session->userdata("LOGGED")){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			
			if($action=="savedokumen"){
				
				$data = $cbs->main->post_to_query($this->input->post('REG'));
				
				$r = array('','I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII');
				$bln = date("n");			
				
				if($data['REG_JENIS_DOKUMEN']=="101"){
					$JENIS = 'PEMBAYARAN';
					$jml = $cbs->main->get_uraian("SELECT JUMLAH FROM tm_nomor WHERE JENIS_NOMOR = 'PEMBAYARAN'","JUMLAH");
					$jml = $jml+1;
					$jml = sprintf("%03d", $jml);
					$nomor = 'PP.JJ.'.$r[$bln].'-'.date("y").'-'.$jml;
					$url = 'pembayaran';
				}else if($data['REG_JENIS_DOKUMEN']=="102"){
					$JENIS = 'KASBON';
					$jml = $cbs->main->get_uraian("SELECT JUMLAH FROM tm_nomor WHERE JENIS_NOMOR = 'KASBON'","JUMLAH");
					$jml = $jml+1;
					$jml = sprintf("%03d", $jml);
					$nomor = 'PKB.JJ.'.$r[$bln].'-'.date("y").'-'.$jml;
					$url = 'kasbon';
				}else if($data['REG_JENIS_DOKUMEN']=="103"){
					$JENIS = 'PELUNASAN_KASBON';
					$jml = $cbs->main->get_uraian("SELECT JUMLAH FROM tm_nomor WHERE JENIS_NOMOR = 'PELUNASAN_KASBON'","JUMLAH");
					$jml = $jml+1;
					$jml = sprintf("%03d", $jml);
					$nomor = 'PK.JJ.'.$r[$bln].'-'.date("y").'-'.$jml;
					$url = 'pelunasan_kasbon';
				}
				
				
					$no = $cbs->main->get_uraian("SELECT JUMLAH FROM tm_nomor WHERE JENIS_NOMOR = 'SURAT'","JUMLAH");
					$no = $no+1;
					$no = $code = sprintf("%03d", $no);
					$no = 'SE'.$no;
				
				$date=date_create($data['TANGGAL_PENGAJUAN']);
				$tgl = date_format($date,"Y-m-d");
				
				$data = array('REG_ID' => $no,
							  'TANGGAL_PENGAJUAN' => $tgl,	
							  'REG_JUMLAH_TERBILANG' => str_replace('.','',$data['REG_JUMLAH']),
							  'REG_PEMAKAIAN_TERBILANG' => str_replace('.','',$data['REG_PEMAKAIAN']),							  
							  'REG_NOMOR' => $nomor,	
							  'USER_CREATE' => $this->session->userdata('USER_ID'),
							  'DATE_CREATE' => date("Y-m-d H:i:s")
							);
				
				$data = array_merge($cbs->main->post_to_query($this->input->post('REG')), $data);
								
		
				if($this->db->insert('tx_reg',$data)){
					
					$com = $this->input->post('REG_DETIL');
					$arrkey = array_keys($com);
					for($i=0;$i<count($com[$arrkey[0]]);$i++){
						$arrcom = array('REG_ID' => $no);								 
							for($j=0;$j<count($arrkey);$j++){								
								$arrcom[$arrkey[$j]] = $com[$arrkey[$j]][$i];
							}
						 
						$this->db->insert('tx_reg_detil', $arrcom); #Insert tx_spec
					}
					
					$this->db->simple_query("UPDATE tm_nomor SET JUMLAH = JUMLAH + 1 WHERE JENIS_NOMOR = 'SURAT'");
					$this->db->simple_query("UPDATE tm_nomor SET JUMLAH = JUMLAH + 1 WHERE JENIS_NOMOR = '".$JENIS."'");
								
					return "MSG#OK#Simpan Dokumen Berhasil#".site_url().'/home/dokumen/listdata/newtable/'.$url;
					
				}else{
					return "MSG#ERR#Simpan Pengajuan Gagal";
				} 
					
					
				
			}else if($action =="edit_pembayaran"){
				$regid = $this->input->post('regid');
				
				$data = $cbs->main->post_to_query($this->input->post('REG'));
				
				$chkklaim = $cbs->main->get_uraian("SELECT FILE_UPLOAD FROM tx_reg WHERE REG_ID = '".$regid."'", "FILE_UPLOAD");
				$chkfile = strlen($chkklaim);
				if($chkfile > 20){
					$data = array(
						'REG_JUMLAH_TERBILANG' => str_replace(',','',$data['REG_JUMLAH']),						  
						'USER_UPDATE' => $this->session->userdata('USER_ID'),
						'DATE_UPDATE' => date("Y-m-d H:i:s")
					  );
				}else{
					$data = array(
						'REG_JUMLAH_TERBILANG' => str_replace('.','',$data['REG_JUMLAH']),						  
						'USER_UPDATE' => $this->session->userdata('USER_ID'),
						'DATE_UPDATE' => date("Y-m-d H:i:s")
					  );
				} 
				
				
				$data = array_merge($cbs->main->post_to_query($this->input->post('REG')), $data);
						
				$this->db->where('REG_ID', $regid);
				if($this->db->update('tx_reg', $data)){

					$this->db->simple_query("DELETE FROM tx_reg_detil WHERE REG_ID = '$regid'");
					$com = $this->input->post('REG_DETIL');
					$arrkey = array_keys($com);
					for($i=0;$i<count($com[$arrkey[0]]);$i++){
						$arrcom = array('REG_ID' => $regid);								 
							for($j=0;$j<count($arrkey);$j++){								
								$arrcom[$arrkey[$j]] = $com[$arrkey[$j]][$i];
							}
						 
						$this->db->insert('tx_reg_detil', $arrcom); #Insert tx_spec
					}
						
					return "MSG#OK#Ubah Dokumen Berhasil#".site_url().'/home/dokumen/listdata/newtable/pembayaran';
				}
				return "MSG#ERR#Simpan Pengajuan Gagal";
			}else if($action =="edit_kasbon"){
				$regid = $this->input->post('regid');
				
				$data = $cbs->main->post_to_query($this->input->post('REG'));
				$data = array(
							  'REG_JUMLAH_TERBILANG' => str_replace('.','',$data['REG_JUMLAH']),						  
							  'USER_UPDATE' => $this->session->userdata('USER_ID'),
							  'DATE_UPDATE' => date("Y-m-d H:i:s")
							);
				
				$data = array_merge($cbs->main->post_to_query($this->input->post('REG')), $data);
						
				$this->db->where('REG_ID', $regid);
				if($this->db->update('tx_reg', $data)){
					
					$this->db->simple_query("DELETE FROM tx_reg_detil WHERE REG_ID = '$regid'");
					$com = $this->input->post('REG_DETIL');
					$arrkey = array_keys($com);
					for($i=0;$i<count($com[$arrkey[0]]);$i++){
						$arrcom = array('REG_ID' => $regid);								 
							for($j=0;$j<count($arrkey);$j++){								
								$arrcom[$arrkey[$j]] = $com[$arrkey[$j]][$i];
							}
						 
						$this->db->insert('tx_reg_detil', $arrcom); #Insert tx_spec
					}
					
					return "MSG#OK#Ubah Dokumen Berhasil#".site_url().'/home/dokumen/listdata/newtable/kasbon';
				}
				return "MSG#ERR#Simpan Pengajuan Gagal";
			}else if($action =="edit_pelunasan_kasbon"){
				$regid = $this->input->post('regid');
				
				$data = $cbs->main->post_to_query($this->input->post('REG'));
				$data = array(
							  'REG_JUMLAH_TERBILANG' => str_replace('.','',$data['REG_JUMLAH']),
							  'REG_PEMAKAIAN_TERBILANG' => str_replace('.','',$data['REG_PEMAKAIAN']),			
							  'USER_UPDATE' => $this->session->userdata('USER_ID'),
							  'DATE_UPDATE' => date("Y-m-d H:i:s")
							);
				
				$data = array_merge($cbs->main->post_to_query($this->input->post('REG')), $data);
						
				$this->db->where('REG_ID', $regid);
				if($this->db->update('tx_reg', $data)){

					$this->db->simple_query("DELETE FROM tx_reg_detil WHERE REG_ID = '$regid'");
					$com = $this->input->post('REG_DETIL');
					$arrkey = array_keys($com);
					for($i=0;$i<count($com[$arrkey[0]]);$i++){
						$arrcom = array('REG_ID' => $regid);								 
							for($j=0;$j<count($arrkey);$j++){								
								$arrcom[$arrkey[$j]] = $com[$arrkey[$j]][$i];
							}
						 
						$this->db->insert('tx_reg_detil', $arrcom); #Insert tx_spec
					}
					
					return "MSG#OK#Ubah Dokumen Berhasil#".site_url().'/home/dokumen/listdata/newtable/pelunasan_kasbon';
				}
				return "MSG#ERR#Simpan Pengajuan Gagal";
			}else if($action=="saveuser"){
				$data = $cbs->main->post_to_query($this->input->post('USER'));
				$data = array(
							  'USER_PASSWORD' => md5('pwd_')
							 
							);
				
				$data = array_merge($cbs->main->post_to_query($this->input->post('USER')), $data);
				
				if($this->db->insert('tm_user', $data)){					
					return "MSG#OK#Simpan Data Berhasil#".site_url().'/home/user/list';
				}
				return "MSG#ERR#Simpan Data Gagal";		
				
			}else if($action=="saverapat"){
				$data = $cbs->main->post_to_query($this->input->post('REG'));
				$data = array(
							  'TANGGAL' => date("Y-m-d H:i:s"),
							  'USER_CREATE' => $this->session->userdata("USER_ID")
							);
				
				$data = array_merge($cbs->main->post_to_query($this->input->post('REG')), $data);
				
				if($this->db->insert('t_rapat', $data)){					
					return "MSG#OK#Simpan Data Berhasil#".site_url().'/home/dokumen/listdata/newtable/rapat';
				}
				
				return "MSG#ERR#Simpan Data Gagal";	
			}else if($action=="savekendaraan"){
				#print_r($_POST); die();
				$aju = $cbs->main->get_aju_regis();
				
				$dir = 'dat/';
				if(!is_dir($dir)) mkdir($dir);
				chmod($dir,0777);
				
				$dir .= "ttd/";
				if(!is_dir($dir)){ 
					mkdir($dir);
				}
				chmod($dir,0777);
			
				$image_parts = explode(";base64,", $_POST['signed']);
					
				$image_type_aux = explode("image/", $image_parts[0]);
				  
				$image_type = $image_type_aux[1];
				  
				$image_base64 = base64_decode($image_parts[1]);
				
				if($_POST['signed']==""){	
					$file = null;
				}else{
					$file = $dir . uniqid() . '.'.$image_type;
				}
				
				
				$data = $cbs->main->post_to_query($this->input->post('REG'));
				
				
				#$date=date_create($data['TANGGAL_PERMOHONAN']);
				#$tgl = date_format($date,"Y-m-d");
				#$tgl = date('Y-m-d', strtotime($data['TANGGAL_PERMOHONAN']));
				
				$arrdriver = explode('||', $data['DRIVER_ID']);
				
				$chkdriver = (int)$cbs->main->get_uraian("SELECT STATUS FROM tm_kendaraan WHERE STATUS = '1' AND DRIVER_ID= '".$arrdriver[0]."' ","STATUS");
				if($chkdriver > 0) return "MSG#ERR#Driver tidak tersedia";	
				
				$data = array('REG_ID' => $aju,
							  #'TANGGAL_PERMOHONAN' => $tgl,
							  'TANGGAL_PENGAJUAN' => date("Y-m-d H:i:s"),	
							  #'DRIVER_ID' => $arrdriver[0],
							  'STATUS' => '2',
							  'USER_TTD' => $file,
							  'USER_CREATE' => $this->session->userdata("USER_ID")
							);
				
				$data = array_merge($cbs->main->post_to_query($this->input->post('REG')), $data);
				#print_r($data); die();
				#$this->db->insert('tx_kendaraan', $data);
				#echo $this->db->last_query(); die();
				file_put_contents($file, $image_base64);
				if($this->db->insert('tx_kendaraan', $data)){	
					$this->db->where('DRIVER_ID', $arrdriver[0]);
					$this->db->update('tm_kendaraan', array('STATUS' => '1'));
					
					$arrlog = array('REG_ID' => $aju, 
									'DRIVER_ID' => $arrdriver[0], 
									'TGL_MULAI' => date("Y-m-d H:i:s")
									);
					$this->db->insert('tx_log_driver', $arrlog);				
					
					$this->db->simple_query("UPDATE tm_nomor SET JUMLAH = JUMLAH + 1 WHERE JENIS_NOMOR = 'REGIS_KENDARAAN'");
					return "MSG#OK#Simpan Data Berhasil#".site_url().'/home/dokumen/listregis/new';
				}
				
				return "MSG#ERR#Simpan Data Gagal";	
			}else if($action=="updatedriver"){
				$regid = $this->input->post('regid');
				$data = $cbs->main->post_to_query($this->input->post('REG'));
				$arrdriver = explode('||', $data['DRIVER_ID']);
				
				$jml = (int)$cbs->main->get_uraian("SELECT LENGTH(TTD) JML FROM tx_kendaraan WHERE REG_ID = '".$regid."'", "JML");
				$chkuser = (int)$cbs->main->get_uraian("SELECT COUNT(REG_ID) JML FROM tx_ratting_driver WHERE REG_ID = '".$regid."'", "JML");
				$user = $cbs->main->get_uraian("SELECT USER_CREATE FROM tx_kendaraan WHERE REG_ID = '".$regid."'", "USER_CREATE");
				$jnsken = $data['JENIS_KENDARAAN'];
				
				
				if($jnsken == "1"){
					if($chkuser > 0){
						$rp1 = array('DRIVER_ID' => $arrdriver[0], 
									 'USER_NILAI' => $user
									 );
						$this->db->where('REG_ID', $regid);
						$this->db->update('tx_ratting_driver', $rp1);
					}else{
						$rp1 = array('REG_ID' => $regid,
									 'DRIVER_ID' => $arrdriver[0], 
									 'USER_NILAI' => $user
									);
						$this->db->insert('tx_ratting_driver', $rp1);			
					}
				}
				
				
				if($jml > 0){
					$data = array(
								  'DRIVER_ID' => $arrdriver[0]
								);
				}else{
					$data = array(
								  'DRIVER_ID' => $arrdriver[0], 
								  'STATUS' => '3'		
								);
					
				}
				
				
				$data = array_merge($cbs->main->post_to_query($this->input->post('REG')), $data);
			
				$this->db->where('REG_ID', $regid);
				if($this->db->update('tx_kendaraan', $data)){
					
					$this->db->where('DRIVER_ID', $arrdriver[0]);
					$this->db->update('tm_kendaraan', array('STATUS' => '1'));
				
					return "MSG#OK#Ubah Data Berhasil#".site_url().'/home/dokumen/listregis/new';

				}			
				return "MSG#ERR#Simpan Data Gagal";	
			
			}else if($action=="set_available" || $action=="not_set_available"){
				$ret = "MSG#Ubah Status Gagal.";	
				
				if($action=="set_available") $stts = '0';
				if($action=="not_set_available") $stts = '1';
				
				foreach($this->input->post('tb_chk') as $chkitem){
					$arrchk = explode(".", $chkitem);
					 
					$this->db->where('DRIVER_ID',$arrchk[0]);
					if($this->db->update('tm_kendaraan',array('STATUS'=>$stts))) $ret = "MSG#Ubah Status Berhasil.#";
				
				}
				
				if($isajax!="ajax"){
					redirect(base_url());
					exit();
				}
				return $ret;
				
			}else if($action=="acc"){
				$ret = "MSG#Ubah Status Gagal.";
				foreach($this->input->post('tb_chk') as $chkitem){
					$arrchk = explode(".", $chkitem);
					$ttd = 'ttd_'.$this->session->userdata('USER_ID').'.jpg';
					
					$this->db->where('REG_ID',$arrchk[0]);
					if($this->db->update('tx_kendaraan',array('STATUS' => '2', 'TTD' => $ttd, 'TGL_TTD' => date("Y-m-d H:i:s")))) $ret = "MSG#Ubah Status Berhasil.#";
				
				}
				
				if($isajax!="ajax"){
					redirect(base_url());
					exit();
				}
				return $ret;
				
			}else if($action=="set_user_aktif"){
				$ret = "MSG#Ubah Status Gagal.";	
				
				
				foreach($this->input->post('tb_chk') as $chkitem){
					$arrchk = explode(".", $chkitem);
					 
					$this->db->where('USER_ID',$arrchk[0]);
					if($this->db->update('tm_user',array('USER_STATUS'=>'1'))) $ret = "MSG#Ubah Status Berhasil.#";
				
				}
				
				if($isajax!="ajax"){
					redirect(base_url());
					exit();
				}
				return $ret;
				
			}else if($action=="set_aktif"){
				$ret = "MSG#Ubah Status Gagal.";	
				
				
				foreach($this->input->post('tb_chk') as $chkitem){
					$arrchk = explode(".", $chkitem);
					 
					 $user = "SELECT * FROM tm_vendor WHERE VENDOR_ID = '".$arrchk[0]."' ";
					 $arruser = $this->db->query($user)->row_array();
						$url = 'http://36.37.74.34:7979/ereg/index.php';
						$email = $arruser['EMAIL'];
						$nama  	 = $arruser['NAMA_PIMPINAN'];
						$subject = "Email Notifikasi";
						$isi     = 'Surat Tanda Daftar Rekanan sudah disetujui, File dapa di download <a href="'.$url.'/prints/stdr/'.$arrchk[0].'.'.rand().'" target="_blank">disini</a>';
						if($this->main->send_mail($email, $nama, $subject, $isi)){
							#$this->main->send_mailbpom($email, $nama, $subject, $isi);		 
						}else{
							$ret = "MSG#Ubah Status Gagal.";
							#$this->main->send_mail($email, $nama, $subject, $isi);
						
						}
					 
					$this->db->where('VENDOR_ID',$arrchk[0]);
					if($this->db->update('tm_vendor',array('STATUS'=>'1', 'USER_APROVE' => $this->session->userdata('USER_ID'), 'DATE_APROVE' => date("Y-m-d H:i:s")))) $ret = "MSG#Ubah Status Berhasil.#";
				
				}
				
				if($isajax!="ajax"){
					redirect(base_url());
					exit();
				}
				return $ret;
				
			}else if($action=="tolakuser"){
				$ret = "MSG#Ubah Status Gagal.";	
				
				
				foreach($this->input->post('tb_chk') as $chkitem){
					$arrchk = explode(".", $chkitem);
					 
					$this->db->where('VENDOR_ID',$arrchk[0]);
					if($this->db->update('tm_vendor',array('STATUS'=>'0'))) $ret = "MSG#Ubah Status Berhasil.#";
				
				}
				
				if($isajax!="ajax"){
					redirect(base_url());
					exit();
				}
				return $ret;
				
			}else if($action=="deleteuser"){
				$ret = "MSG#Ubah Status Gagal.";	
				
				
				foreach($this->input->post('tb_chk') as $chkitem){
					$arrchk = explode(".", $chkitem);
					 
					$this->db->where('USER_ID',$arrchk[0]);
					if($this->db->delete('tm_user')) $ret = "MSG#Hapus User Berhasil.#";
				
				}
				
				if($isajax!="ajax"){
					redirect(base_url());
					exit();
				}
				return $ret;
				
			}else if($action=="cancled"){
				$ret = "MSG#Ubah Status Gagal.";	
				
				
				foreach($this->input->post('tb_chk') as $chkitem){
					
					 
					$this->db->where('REG_NOMOR',$chkitem);
					if($this->db->update('tx_reg',array('STATUS'=>'2'))) $ret = "MSG#Ubah Status Berhasil.#";
					
				}
			
				
				if($isajax!="ajax"){
					redirect(base_url());
					exit();
				}
				return $ret;
				
			}else if($action=="deletefile"){
				$ret = "MSG#Hapus File Gagal.";	
				
				
				foreach($this->input->post('tb_chk') as $chkitem){
					
					 
					$this->db->where('DOC_ID',$chkitem);
					if($this->db->delete('tx_kendaraan_doc')) $ret = "MSG#Hapus File Berhasil.#";
					
				}
			
				
				if($isajax!="ajax"){
					redirect(base_url());
					exit();
				}
				return $ret;
				
			}else if($action=="cancleddriver" || $action =="approvedriver"){
				$ret = "MSG#Ubah Status Gagal.";	
				
				
				foreach($this->input->post('tb_chk') as $chkitem){
					if($action=="approvedriver"){
						$stts = '1';
						$user = NULL;
					}else{
						$user = $this->session->userdata('USER_ID');
						$stts = '0';
					} 
					
					$driverid = $cbs->main->get_uraian("SELECT DRIVER_ID FROM tx_kendaraan WHERE REG_ID = '".$chkitem."'","DRIVER_ID");	
					
					if($action=="cancleddriver"){
						$this->db->where('REG_ID', $chkitem);
						$this->db->delete('tx_ratting_driver');
					}
						
					 
					$this->db->where('REG_ID',$chkitem);
					if($this->db->update('tx_kendaraan',array('STATUS' => $stts, 'USER_CANCEL' => $user))){
						
						
						
						$this->db->where('REG_ID', $chkitem);
						$this->db->update('tx_log_driver', array('TGL_SELESAI' => date('Y-m-d H:i:s')));
						
						$this->db->where('DRIVER_ID', $driverid);
						$this->db->update('tm_kendaraan', array('STATUS' => '0'));
						
						$ret = "MSG#Ubah Status Berhasil.#";	
					} 
					
				}
			
				
				if($isajax!="ajax"){
					redirect(base_url());
					exit();
				}
				return $ret;
			}else if ($action=="hidecms"){
				$ret = "MSG#Hapus CMS Gagal.";	
				
				if($this->session->userdata('USER_JABATAN')=="3.2.2"){
				
					foreach($this->input->post('tb_chk') as $chkitem){

						$stts = '0';	
						
						$this->db->where('CMS_ID',$chkitem);
						if($this->db->update('tx_cms',array('STATUS' => $stts))){
							

							$ret = "MSG#Ubah Status Berhasil.#";	
						} 
						
					}
				
				}else{
					$ret = "MSG#Hapus CMS Gagal.";	
				}
				
				if($isajax!="ajax"){
					redirect(base_url());
					exit();
				}
				return $ret;
			}else if($action=="save_Fix_CMS"){
				$cmsid = $this->input->post('cmsid');
				$data = $cbs->main->post_to_query($this->input->post('REG'));
				
				$this->db->where('CMS_ID', $cmsid);
				#$this->db->update('tx_cms', $data);
				#echo $this->db->last_query(); die();
				if($this->db->update('tx_cms', $data)){
					return "MSG#OK#Simpan Data Berhasil#".site_url().'/home/dokumen/listCMS/CMSnewSyr';
				}
				
			
			}else if($action=="saveregsurat"){
				$data = $cbs->main->post_to_query($this->input->post('REG'));
				
				$data = array(
							 
							  'DATE_CREATE' => date("Y-m-d H:i:s"),
							  'USER_CREATE' => $this->session->userdata("USER_ID")
							);
				$data = array_merge($cbs->main->post_to_query($this->input->post('REG')), $data);
				
				if($this->db->insert('tx_reg_surat', $data)){
					return "MSG#OK#Simpan Data Berhasil#".site_url();
				}
				
				return "MSG#ERR#Simpan Data Gagal";	
			}else if($action=="saveuploadkendaraan"){
				
				$file = $this->input->post('PATH');
				$ket = $this->input->post('KETERANGAN');
				$jarak = $this->input->post('JARAK');
				$regid = $this->input->post('regnomor');
				
				$isi = array(
							 'REG_ID' => $regid,
							 'PATH_FILE' => str_replace(base_url(), '', $file),
							 'KETERANGAN' => $ket, 
							 'JARAK' => $jarak,
							 'DATE_UPLOAD' => date("Y-m-d H:i:s")
							 );
							 
				if($this->db->insert('tx_kendaraan_doc', $isi)){
					
					return "MSG#OK#Simpan Data Berhasil#".site_url().'/home/dokumen/listregis/new';
				}
				
				
			}else if($action=="savedokumenupload"){
				
				$file = $this->input->post('PATH');
				$namafile = $this->input->post('REG_NAMAFILE');
				$regid = $this->input->post('regnomor');
				
				$type = $cbs->main->get_uraian("SELECT REG_JENIS_DOKUMEN FROM tx_reg WHERE REG_NOMOR = '".$regid."' ","REG_JENIS_DOKUMEN");
				
				if($type=="101") $link = 'pembayaran';
				if($type=="102") $link = 'kasbon';
				if($type=="103") $link = 'pelunasan_kasbon';
				
				$isi = array('REG_PATHFILE' => str_replace(base_url(), '', $file),
							 'REG_NAMAFILE' => $namafile, 
							 'STATUS' => '1',
							 'REG_TGL_UPLOAD' => date("Y-m-d")
							 );
				$this->db->where('REG_NOMOR', $regid);
				if($this->db->update('tx_reg', $isi)){
					
					return "MSG#OK#Simpan Data Berhasil#".site_url().'/home/dokumen/listdata/newtable/'.$link;
				}	
				
			}else if($action=="saveregcms"){
				
			
				$tglcms = $this->input->post('TANGGAL_CMS');
				$data = $cbs->main->post_to_query($this->input->post('REG'));
				
				$date=date_create($tglcms);
				$tgl = date_format($date,"Y-m-d");
			
				
				
				$data = array(
							  'TANGGAL_CMS' => $tgl,
							  'DATE_UPLOAD' => date("Y-m-d H:i:s"),
							  'USER_UPLOAD' => $this->session->userdata("USER_ID")
							);
				
				$data = array_merge($cbs->main->post_to_query($this->input->post('REG')), $data);
			
				if($this->db->insert('tx_cms', $data)){	
					
					return "MSG#OK#Simpan Data Berhasil#".site_url().'/home/dokumen/listCMS/CMSnew';
				}
				
				return "MSG#ERR#Simpan Data Gagal";	
			}else if($action=="saveCMSNew"){
				require_once(APPPATH.'libraries/PHPSpreadsheet/vendor/autoload.php');
				$path = $this->input->post('PATH');
				$jnsdok = $this->input->post('jns_dokumen');
				$ket = $this->input->post('CATATAN');
				
				$targetPath = str_replace(base_url(), '', $path);

				$xt = explode('/', $targetPath);
				$namafile = $xt[3];
				
				$exten = explode('.', $namafile);

				#echo $path.'='.$jnsdok.'='.$namafile; die($exten[1]);
				
				if($exten[1]=="xlsx"){
					$path = FCPATH.$targetPath;		
					$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);

				}else if($exten[1]=="xls"){
					$path = FCPATH.$targetPath;			
					$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);
				
				}else{
					return "MSG#ERR#upload file xlxs atau xls";
				}
				
				foreach($spreadsheet->getWorksheetIterator() as $worksheet)
				{
					$worksheet = $spreadsheet->getSheet(0);
					$highestRow = $worksheet->getHighestRow();
					$highestColumn = $worksheet->getHighestColumn(); 
				}
				
				if($jnsdok=="CMS"){
					
					if($highestRow <=1 ){
						return "MSG#ERR#Data Di Dalam Excel Kosong. Cek Excel yang Diupload";
					}else{
						for($row=2; $row<=$highestRow; $row++){
							$isi1 = $worksheet->getCellByColumnAndRow(1, $row)->getFormattedValue();
							$deskripsi = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
							$debit = $worksheet->getCellByColumnAndRow(3, $row)->getFormattedValue();
							$kredit = $worksheet->getCellByColumnAndRow(4, $row)->getFormattedValue();
							$saldo = $worksheet->getCellByColumnAndRow(5, $row)->getFormattedValue();
							$ket_cms = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
							$rek = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
							
							$waktu = strtotime($isi1);
							$tanggal = date('Y-m-d', $waktu);
							$jam = date('H:i:s', $waktu);
							
							#echo $tanggal.'='.$isi2.'='.$jam; die();
							
							$isi = array('TANGGAL_CMS' => $tanggal, 
										 'JAM_CMS' => $jam, 
										 'DESKRIPSI_CMS' => $deskripsi,
										 'DEBIT_CMS' => $debit, 
										 'KREDIT_CMS' => $kredit, 
										 'SALDO_CMS' => $saldo, 
										 'KETERANGAN_CMS' => $ket_cms, 
										 'NOREK_CMS' => $rek, 
										 'BANK_CMS' => '-', 
										 'NAMA_FILE' => $namafile, 
										 'DATE_UPLOAD' => date("Y-m-d H:i:s"), 
										 'CATATAN' => $ket,
										 'STATUS' => '5',	
										 'USER_UPLOAD' => $this->session->userdata('USER_ID')
										);
							
							$this->db->insert('tx_cms', $isi);
						}
						
					}
				}else{
					if($highestRow <=1 ){
						return "MSG#ERR#Data Di Dalam Excel Kosong. Cek Excel yang Diupload";
					}else{

						for($row=2; $row<=$highestRow; $row++){
								 #number_format($isi2,2,".",",")
								 $loan_number = $worksheet->getCellByColumnAndRow(2, $row)->getValue();			 
								 $namadebitur = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
								 #$tgl_lahir = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
								 $tgl_lahir = $worksheet->getCellByColumnAndRow(4, $row)->getFormattedValue();
								 $jekel = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
								 $alamat = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
								 $pembiayaan = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
								 $plafon = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
								 $nilaiK = $worksheet->getCellByColumnAndRow(9, $row)->getCalculatedValue();
								 $usia = $worksheet->getCellByColumnAndRow(10, $row)->getCalculatedValue();
								 $instansi = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
								 $margin = $worksheet->getCellByColumnAndRow(12, $row)->getCalculatedValue();
								 $jwbulan = $worksheet->getCellByColumnAndRow(13, $row)->getCalculatedValue();
								 $tgl_realisasi = $worksheet->getCellByColumnAndRow(14, $row)->getFormattedValue();
								 $tgl_jatuh_tempo = $worksheet->getCellByColumnAndRow(15, $row)->getFormattedValue();
								 $ijk = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
								 $persen_ijk = $worksheet->getCellByColumnAndRow(17, $row)->getCalculatedValue();
								 $nik = $worksheet->getCellByColumnAndRow(18, $row)->getFormattedValue();
								 
								 if($tgl_lahir!=""){
									 $waktu = strtotime($tgl_lahir);
									 $tgl_lahir = date('Y-m-d', $waktu);
									 #$tgl_lahir=date_create($tgl_lahir);
									# $tgl_lahir = date_format($tgl_lahir,"Y-m-d");
								 }else{
									 $tgl_lahir= null;
								 }
								 
								 if($tgl_realisasi!=""){
									 $tgl_realisasi=date_create($tgl_realisasi);
									 $tgl_realisasi = date_format($tgl_realisasi,"Y-m-d");
								 }else{
									 $tgl_realisasi= null;
								 }
								 
								 if($tgl_jatuh_tempo!=""){
									 $tgl_jatuh_tempo=date_create($tgl_jatuh_tempo);
									 $tgl_jatuh_tempo = date_format($tgl_jatuh_tempo,"Y-m-d");
								 }else{
									 $tgl_jatuh_tempo= null;
								 }
								 
								 $margin = str_replace(',','.', $margin);
								 $margin = str_replace('%','', $margin);
								 
								 $persen_ijk = str_replace(',','.', $persen_ijk);
								 $persen_ijk = str_replace('%','', $persen_ijk);
								 
									
								 
								 $datas = array('LOAN_NUMBER' => $loan_number,
												'NAMA_DEBITUR' => $namadebitur, 
												'TGL_LAHIR' =>  $tgl_lahir,
												'JENIS_KELAMIN' =>  $jekel,
												'ALAMAT_DEBITUR' => $alamat,
												'PEMBIAYAAN' => $pembiayaan,
												'PLAFON_PEMBIAYAAN' => $plafon,
												'NILAI_KAFALAH' => $nilaiK,
												'USIA' => $usia,
												'INSTANSI' => $instansi,
												'MARGIN' =>$margin,
												'JW_BULAN' => $jwbulan,
												'TGL_REALISASI' => $tgl_realisasi,
												'TGL_JATUH_TEMPO' => $tgl_jatuh_tempo,
												'IJK' => $ijk,
												'PERSEN_IJK' => $persen_ijk,
												'NIK' => $nik
												
											);
										
								$this->db->insert('tx_nominatif', $datas);
									
							}
							
						}
				 #TUTUP KONDISI
				}
				
				return "MSG#OK#Simpan data cms berhasil#".site_url();
				
				
			}else if($action=="saveuploadklaim"){
				require_once(APPPATH.'libraries/PHPSpreadsheet/vendor/autoload.php');
				
				$path = $this->input->post('PATH');
				$jns = $this->input->post('REG_JENIS_DOKUMEN');
				$tgl = $this->input->post('TANGGAL_PENGAJUAN');

				$no = $cbs->main->get_uraian("SELECT JUMLAH FROM tm_nomor WHERE JENIS_NOMOR = 'SURAT'","JUMLAH");
				$no = $no+1;
				$no = $code = sprintf("%03d", $no);
				$no = 'SE'.$no;

				$targetPath = str_replace(base_url(), '', $path);

				$xt = explode('/', $targetPath);
				$namafile = $xt[2];
				
				$exten = explode('.', $namafile);

				#echo $path.'='.$jns.'='.$tgl; die($exten[1]);
				
				if($exten[1]=="xlsx"){
					$path = FCPATH.$targetPath;		
					$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);

				}else if($exten[1]=="xls"){
					$path = FCPATH.$targetPath;			
					$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);
				
				}else{
					return "MSG#ERR#upload file xlxs atau xls";
				}

				foreach($spreadsheet->getWorksheetIterator() as $worksheet)
				{
					$worksheet = $spreadsheet->getSheet(0);
					$highestRow = $worksheet->getHighestRow();
					$highestColumn = $worksheet->getHighestColumn(); 
				}

				
				if($highestRow <=1 ){
					return "MSG#ERR#Data Di Dalam Excel Kosong. Cek Excel yang Diupload";
				}else{

					for($row=2; $row<=$highestRow; $row++){
							 
						     $isi1 = $worksheet->getCellByColumnAndRow(3, $row)->getValue();

							 $isi2 = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
							
							 $norek = $worksheet->getCellByColumnAndRow(17, $row)->getValue();

							 $datas = array('REG_ID' => $no,
								 			'REG_DETIL_INPUT' => $isi1.' '.$norek, 
											'REG_DETIL_NOMINAL' =>  number_format($isi2,2,".",",")
										);
							$this->db->insert('tx_reg_detil', $datas);				
						}	 
				}
				
				$r = array('','I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII');
				$bln = date("n");
				$JENIS = 'PEMBAYARAN';
				$jml = $cbs->main->get_uraian("SELECT JUMLAH FROM tm_nomor WHERE JENIS_NOMOR = 'PEMBAYARAN'","JUMLAH");
				$jml = $jml+1;
				$jml = sprintf("%03d", $jml);
				$nomor = 'PP.JJ.'.$r[$bln].'-'.date("y").'-'.$jml;

				$date=date_create($tgl);
				$tgl = date_format($date,"Y-m-d");
				
				$jml_bayar = $cbs->main->get_uraian("SELECT FORMAT(SUM(REPLACE(REG_DETIL_NOMINAL,',','')),2) JML FROM tx_reg_detil WHERE REG_ID = '".$no."' ","JML");
						
				$jml_bayar_terbilang = $cbs->main->get_uraian("SELECT SUM(REPLACE(REG_DETIL_NOMINAL,',','')) JML FROM tx_reg_detil WHERE REG_ID = '".$no."' ","JML");

				$data = array('REG_ID' => $no,
							  'REG_JENIS_DOKUMEN' => $jns,
							  'TANGGAL_PENGAJUAN' => $tgl,								 							  
							  'REG_NOMOR' => $nomor,
							  'REG_JUMLAH' => $jml_bayar,
							  'REG_JUMLAH_TERBILANG' => $jml_bayar_terbilang, 	
							  'USER_CREATE' => $this->session->userdata('USER_ID'),
							  'DATE_CREATE' => date("Y-m-d H:i:s"), 
							  'FILE_UPLOAD' => $targetPath
							);

				if($this->db->insert('tx_reg',$data)){
		
					
					$this->db->simple_query("UPDATE tm_nomor SET JUMLAH = JUMLAH + 1 WHERE JENIS_NOMOR = 'SURAT'");
					$this->db->simple_query("UPDATE tm_nomor SET JUMLAH = JUMLAH + 1 WHERE JENIS_NOMOR = '".$JENIS."'");
								
					return "MSG#OK#Simpan Dokumen Berhasil#".site_url().'/home/dokumen/edit_pembayaran/'.$nomor;
					
				}else{
					return "MSG#ERR#Simpan Pengajuan Gagal";
				} 			

			}else if($action=="saveCMS"){

				#require(APPPATH.'libraries/PHPSpreadsheet/vendor/autoload.php');	
				require_once(APPPATH.'libraries/PHPSpreadsheet/vendor/autoload.php');
				
				$path = $this->input->post('PATH');
				$ket = $this->input->post('CATATAN');
				$targetPath = str_replace(base_url(), '', $path);
				
				$xt = explode('/', $targetPath);
				$namafile = $xt[3];
				
				$exten = explode('.', $namafile);
				#print_r($exten); die();
			
				if($exten[1]=="xlsx"){
					$path = FCPATH.$targetPath;		
					$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);
					
					#$Reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
				}else if($exten[1]=="xls"){
					$path = FCPATH.$targetPath;			
					$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($path);
					
					#$Reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
				}else{
					return "MSG#ERR#upload file xlxs atau xls";
				}
				
				foreach($spreadsheet->getWorksheetIterator() as $worksheet)
				{
					$worksheet = $spreadsheet->getSheet(0);
					$highestRow = $worksheet->getHighestRow();
					$highestColumn = $worksheet->getHighestColumn(); 
				}

				
				if($highestRow <=1 ){
					return "MSG#ERR#Data Di Dalam Excel Kosong. Cek Excel yang Diupload";
				}else{

					for($row=2; $row<=$highestRow; $row++){
							 
						     $isi1 = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
							 #$date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($isi1);

							 $isi2 = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
							 #$jam = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($isi2);

							 $datas = array('NAMA_DEBITUR' => $isi1, 
											'TOTAL_JML' => $isi2
										);
							print_r($datas);				
						}	 
				}
				die('asu');
			
				/*	
				$spreadSheet = $Reader->load(FCPATH.$targetPath);
				
				$excelSheet = $spreadSheet->getActiveSheet();
				$spreadSheetAry = $excelSheet->toArray();
				$sheetCount = count($spreadSheetAry);
			

				for ($i = 1; $i <= $sheetCount; $i ++) {
					
						$date=date_create($spreadSheetAry[$i][0]);
						$tgl_cms =  date_format($date,"Y-m-d");
						$isi = array('TANGGAL_CMS' => $tgl_cms, 
									'JAM_CMS' => $spreadSheetAry[$i][1], 
									'DESKRIPSI_CMS' => $spreadSheetAry[$i][2],
									'DEBIT_CMS' => $spreadSheetAry[$i][3], 
									'KREDIT_CMS' => $spreadSheetAry[$i][4], 
									'SALDO_CMS' => $spreadSheetAry[$i][5], 
									'KETERANGAN_CMS' => $spreadSheetAry[$i][6], 
									'NOREK_CMS' => $spreadSheetAry[$i][7], 
									'BANK_CMS' => $spreadSheetAry[$i][8], 
									'NAMA_FILE' => $namafile, 
									'DATE_UPLOAD' => date("Y-m-d H:i:s"), 
									'CATATAN' => $ket, 
									'USER_UPLOAD' => $this->session->userdata('USER_ID')
									);
								
						$this->db->insert('tx_cms', $isi);	
					
				} 
				*/
			
				
				return "MSG#OK#Simpan data cms berhasil#".site_url();
			}
			
		}else{
			redirect(base_url());
			exit();
		}
	}
	
	
	function list_registrasi($id="", $ispreview=FALSE){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			
			if($this->session->userdata('LOGGED')){
				
				$this->load->library('newtable');
			if($id=="pembayaran" || $id=="kasbon"){
					$this->newtable->hiddens(array('REG_ID','REG_NOMOR','DATE_CREATE'));
					$this->newtable->orderby(3);
				}else{
					$this->newtable->hiddens(array('REG_ID','REG_NOMOR','DATE_CREATE'));
					$this->newtable->orderby(3);
					#$this->newtable->hiddens(array('REG_ID','REG_NOMOR'));
					#$this->newtable->orderby(1);
				}	
				
				if($id=="rapat"){
					$this->newtable->orderby(4);
					$this->newtable->sortby('DESC');
					$this->newtable->keys(array("ID_RAPAT"));
				}else{
					$this->newtable->keys(array("REG_NOMOR"));
					#$this->newtable->orderby(1);
					$this->newtable->sortby('DESC');
				}
					
				$this->newtable->action(site_url()."/home/dokumen/listdata/newtable/".$id);
				$this->newtable->cidb($this->db);
				$this->newtable->ciuri($this->uri->segment_array());
				#$this->newtable->orderby(1);
				#$this->newtable->sortby('DESC');
				
				if($ispreview){
					$this->newtable->rowcount("ALL");
					$this->newtable->show_chk(FALSE);
					$this->newtable->show_search(FALSE);
				}
				
				if($id=="pembayaran"){
					$this->newtable->hiddens(array('REG_ID','REG_NOMOR'));		
					$this->newtable->search(array(array('A.REG_NOMOR', 'NOMOR REGIS'), array('A.TANGGAL_PENGAJUAN','TANGGAL PENGAJUAN'),array('A.REG_JUMLAH','JUMLAH'),array('A.REG_PERIHAL','PERIHAL'), array('A.REG_PEMOHON', 'PEMOHON'), array('C.REFF_DESC','UNIT KERJA')));
					if($this->session->userdata('USER_DEPARTEMEN')=="4" || $this->session->userdata('USER_DEPARTEMEN')=="2" || $this->session->userdata('USER_ROLE')=="01"){
						if($this->session->userdata('USER_DEPARTEMEN')=="2"){
							$prosesnya = array(
										   'Cancled' => array('POST', site_url().'/home/registrasi_act/cancled/ajax', 'N'),
										   #'Upload Dokumen' => array('GET', site_url().'/home/dokumen/upload_pembayaran', '1'),
										   'Print Surat' => array('GETNEW', site_url()."/prints/list_pembayaran", 'N','btn-preview.png'),
										   'Edit Pembayaran' => array('GET', site_url().'/home/dokumen/edit_pembayaran', '1','edit-btn.png')
										   );
							
						}else{
							$prosesnya = array(
										   'Cancled' => array('POST', site_url().'/home/registrasi_act/cancled/ajax', 'N'),
										   'Upload Dokumen' => array('GET', site_url().'/home/dokumen/upload_pembayaran', '1'),
										   'Print Surat' => array('GETNEW', site_url()."/prints/list_pembayaran", 'N','btn-preview.png'),
										   'Edit Pembayaran' => array('GET', site_url().'/home/dokumen/edit_pembayaran', '1','edit-btn.png')
										   );
								
						}
						if($this->session->userdata('USER_ROLE')=="01" || $this->session->userdata('USER_JABATAN')=="3.2.1"){
							$adduserwhere = '';	
						}else{
							$adduserwhere = "AND A.USER_CREATE = '".$this->session->userdata("USER_ID")."' ";				   
						}			   
					}else{
						$prosesnya = array(
										'Cancled' => array('POST', site_url().'/home/registrasi_act/cancled/ajax', 'N'),
										'Print Surat' => array('GETNEW', site_url()."/prints/list_pembayaran", 'N','btn-preview.png'),
										'Edit Pembayaran' => array('GET', site_url().'/home/dokumen/edit_pembayaran', '1','edit-btn.png')
											);
						$adduserwhere = "AND A.USER_CREATE = '".$this->session->userdata("USER_ID")."' ";					
						
					}				   
					$this->newtable->menu($prosesnya);
					
					if($this->session->userdata('USER_ROLE')=="01"){
						$query = "SELECT A.REG_ID, A.REG_NOMOR, A.DATE_CREATE, A.REG_NOMOR AS 'REGISTRASI ID', A.TANGGAL_PENGAJUAN AS TANGGAL, B.REFF_DESC AS 'JENIS REGISTRASI' , C.REFF_DESC AS 'UNIT KERJA', 
							  CONCAT('Rp. ',A.REG_JUMLAH) AS 'JUMLAH', IFNULL(A.REG_PEMOHON,D.USER_FULLNAME) AS PEMOHON, A.REG_PERIHAL AS PERIHAL, 
							  (CASE WHEN A.STATUS IS NULL THEN CONCAT('On process<div> create by:',E.USER_FULLNAME,'</div>')
									WHEN A.STATUS = '1' THEN CONCAT('Uploaded <div><a href=\"".base_url()."','index.php/download/files/',A.REG_NOMOR,'/',REPLACE(A.REG_PATHFILE,CONCAT('dat/',DATE_FORMAT(A.REG_TGL_UPLOAD,'%Y%m%d'),'/'), ''),'\" target=\"_blank\">[Preview]</a></div><div> Tgl : ',A.REG_TGL_UPLOAD,'</div>')
									ELSE '<span style=\"color:red;\">Canceled</span>' 
							   END) STATUS							    
									FROM tx_reg A 
							  LEFT JOIN tb_reff B ON B.REFF_CODE = A.REG_JENIS_DOKUMEN AND B.REFF_TAB = 'JENIS_REGISTRASI'
							  LEFT JOIN tb_reff C ON C.REFF_CODE = A.REG_UNIT_KERJA AND C.REFF_TAB = 'DEPARTEMEN'
							  LEFT JOIN tm_user D ON D.USER_ID = A.TTD_MANAJER_PEMOHON
							  LEFT JOIN tm_user E ON E.USER_ID = A.USER_CREATE
									WHERE A.REG_JENIS_DOKUMEN = '101' AND A.STATUS IS NULL $adduserwhere";
					}else{
						$query = "SELECT A.REG_ID, A.REG_NOMOR, A.DATE_CREATE, A.REG_NOMOR AS 'REGISTRASI ID', A.TANGGAL_PENGAJUAN AS TANGGAL, B.REFF_DESC AS 'JENIS REGISTRASI' , C.REFF_DESC AS 'UNIT KERJA', 
							  CONCAT('Rp. ',A.REG_JUMLAH) AS 'JUMLAH', IFNULL(A.REG_PEMOHON,D.USER_FULLNAME) AS PEMOHON, A.REG_PERIHAL AS PERIHAL, 
							  (CASE WHEN A.STATUS IS NULL THEN 'On process'
									WHEN A.STATUS = '1' THEN CONCAT('Uploaded <div><a href=\"".base_url()."','index.php/download/files/',A.REG_NOMOR,'/',REPLACE(A.REG_PATHFILE,CONCAT('dat/',DATE_FORMAT(A.REG_TGL_UPLOAD,'%Y%m%d'),'/'), ''),'\" target=\"_blank\">[Preview]</a></div><div> Tgl : ',A.REG_TGL_UPLOAD,'</div>')
									ELSE '<span style=\"color:red;\">Canceled</span>' 
							   END) STATUS							    
									FROM tx_reg A 
							  LEFT JOIN tb_reff B ON B.REFF_CODE = A.REG_JENIS_DOKUMEN AND B.REFF_TAB = 'JENIS_REGISTRASI'
							  LEFT JOIN tb_reff C ON C.REFF_CODE = A.REG_UNIT_KERJA AND C.REFF_TAB = 'DEPARTEMEN'
							  LEFT JOIN tm_user D ON D.USER_ID = A.TTD_MANAJER_PEMOHON
									WHERE A.REG_JENIS_DOKUMEN = '101' AND A.STATUS IS NULL $adduserwhere";	
					}
					
									
					$this->newtable->columns(array("A.REG_ID","A.REG_NOMOR","A.TANGGAL_PENGAJUAN", "B.REFF_DESC", "C.REFF_DESC", "(CASE WHEN A.STATUS IS NULL THEN 'On process'
									WHEN A.STATUS = '1' THEN 'Uploaded' ELSE 'Cancled' END)"));				
				}else if($id=="NewPayment"){
					$this->newtable->hiddens(array('REG_ID','REG_NOMOR'));		
					$this->newtable->search(array(array('A.REG_NOMOR', 'NOMOR REGIS'), array('A.TANGGAL_PENGAJUAN','TANGGAL PENGAJUAN'),array('A.REG_JUMLAH','JUMLAH'),array('A.REG_PERIHAL','PERIHAL'), array('A.REG_PEMOHON', 'PEMOHON'), array('C.REFF_DESC','UNIT KERJA')));
					if($this->session->userdata('USER_DEPARTEMEN')=="4" || $this->session->userdata('USER_DEPARTEMEN')=="2" || $this->session->userdata('USER_ROLE')=="01"){
						if($this->session->userdata('USER_DEPARTEMEN')=="2"){
							$prosesnya = array(
										   'Cancled' => array('POST', site_url().'/home/registrasi_act/cancled/ajax', 'N'),
										   #'Upload Dokumen' => array('GET', site_url().'/home/dokumen/upload_pembayaran', '1'),
										   'Print Surat' => array('GETNEW', site_url()."/prints/list_pembayaran", 'N','btn-preview.png'),
										   'Edit Pembayaran' => array('GET', site_url().'/home/dokumen/edit_pembayaran', '1','edit-btn.png')
										   );
							
						}else{
							$prosesnya = array(
										   'Cancled' => array('POST', site_url().'/home/registrasi_act/cancled/ajax', 'N'),
										   'Upload Dokumen' => array('GET', site_url().'/home/dokumen/upload_pembayaran', '1'),
										   'Print Surat' => array('GETNEW', site_url()."/prints/list_pembayaran", 'N','btn-preview.png'),
										   'Edit Pembayaran' => array('GET', site_url().'/home/dokumen/edit_pembayaran', '1','edit-btn.png')
										   );
								
						}
						if($this->session->userdata('USER_ROLE')=="01" || $this->session->userdata('USER_JABATAN')=="3.2.1"){
							$adduserwhere = '';	
						}else{
							$adduserwhere = "AND A.USER_CREATE = '".$this->session->userdata("USER_ID")."' ";				   
						}			   
					}else{
						$prosesnya = array(
										'Cancled' => array('POST', site_url().'/home/registrasi_act/cancled/ajax', 'N'),
										'Print Surat' => array('GETNEW', site_url()."/prints/list_pembayaran", 'N','btn-preview.png'),
										'Edit Pembayaran' => array('GET', site_url().'/home/dokumen/edit_pembayaran', '1','edit-btn.png')
											);
						$adduserwhere = "AND A.USER_CREATE = '".$this->session->userdata("USER_ID")."' ";					
						
					}				   
					$this->newtable->menu($prosesnya);
					
					if($this->session->userdata('USER_ROLE')=="01"){
						$query = "SELECT A.REG_ID, A.REG_NOMOR, A.DATE_CREATE, A.REG_NOMOR AS 'REGISTRASI ID', A.TANGGAL_PENGAJUAN AS TANGGAL, B.REFF_DESC AS 'JENIS REGISTRASI' , C.REFF_DESC AS 'UNIT KERJA', 
							  CONCAT('Rp. ',A.REG_JUMLAH) AS 'JUMLAH', IFNULL(A.REG_PEMOHON,D.USER_FULLNAME) AS PEMOHON, A.REG_PERIHAL AS PERIHAL, 
							  (CASE WHEN A.STATUS IS NULL THEN CONCAT('On process<div> create by:',E.USER_FULLNAME,'</div>')
									WHEN A.STATUS = '1' THEN CONCAT('Uploaded <div><a href=\"".base_url()."','index.php/download/files/',A.REG_NOMOR,'/',REPLACE(A.REG_PATHFILE,CONCAT('dat/',DATE_FORMAT(A.REG_TGL_UPLOAD,'%Y%m%d'),'/'), ''),'\" target=\"_blank\">[Preview]</a></div><div> Tgl : ',A.REG_TGL_UPLOAD,'</div>')
									ELSE '<span style=\"color:red;\">Canceled</span>' 
							   END) STATUS							    
									FROM tx_reg A 
							  LEFT JOIN tb_reff B ON B.REFF_CODE = A.REG_JENIS_DOKUMEN AND B.REFF_TAB = 'JENIS_REGISTRASI'
							  LEFT JOIN tb_reff C ON C.REFF_CODE = A.REG_UNIT_KERJA AND C.REFF_TAB = 'DEPARTEMEN'
							  LEFT JOIN tm_user D ON D.USER_ID = A.TTD_MANAJER_PEMOHON
							  LEFT JOIN tm_user E ON E.USER_ID = A.USER_CREATE
									WHERE A.REG_JENIS_DOKUMEN = '101' AND A.REG_JUMLAH_TERBILANG >= '10000000' AND A.STATUS IS NULL $adduserwhere";
					}else{
						$query = "SELECT A.REG_ID, A.REG_NOMOR, A.DATE_CREATE, A.REG_NOMOR AS 'REGISTRASI ID', A.TANGGAL_PENGAJUAN AS TANGGAL, B.REFF_DESC AS 'JENIS REGISTRASI' , C.REFF_DESC AS 'UNIT KERJA', 
							  CONCAT('Rp. ',A.REG_JUMLAH) AS 'JUMLAH', IFNULL(A.REG_PEMOHON,D.USER_FULLNAME) AS PEMOHON, A.REG_PERIHAL AS PERIHAL, 
							  (CASE WHEN A.STATUS IS NULL THEN 'On process'
									WHEN A.STATUS = '1' THEN CONCAT('Uploaded <div><a href=\"".base_url()."','index.php/download/files/',A.REG_NOMOR,'/',REPLACE(A.REG_PATHFILE,CONCAT('dat/',DATE_FORMAT(A.REG_TGL_UPLOAD,'%Y%m%d'),'/'), ''),'\" target=\"_blank\">[Preview]</a></div><div> Tgl : ',A.REG_TGL_UPLOAD,'</div>')
									ELSE '<span style=\"color:red;\">Canceled</span>' 
							   END) STATUS							    
									FROM tx_reg A 
							  LEFT JOIN tb_reff B ON B.REFF_CODE = A.REG_JENIS_DOKUMEN AND B.REFF_TAB = 'JENIS_REGISTRASI'
							  LEFT JOIN tb_reff C ON C.REFF_CODE = A.REG_UNIT_KERJA AND C.REFF_TAB = 'DEPARTEMEN'
							  LEFT JOIN tm_user D ON D.USER_ID = A.TTD_MANAJER_PEMOHON
									WHERE A.REG_JENIS_DOKUMEN = '101' AND A.REG_JUMLAH_TERBILANG >= '10000000' AND A.STATUS IS NULL $adduserwhere";	
					}
					
									
					$this->newtable->columns(array("A.REG_ID","A.REG_NOMOR","A.TANGGAL_PENGAJUAN", "B.REFF_DESC", "C.REFF_DESC", "(CASE WHEN A.STATUS IS NULL THEN 'On process'
									WHEN A.STATUS = '1' THEN 'Uploaded' ELSE 'Cancled' END)"));	
				}else if($id=="kasbon"){
					$this->newtable->hiddens(array('REG_ID','REG_NOMOR'));		
					$this->newtable->search(array(array('A.REG_NOMOR', 'NOMOR REGIS'), array('A.REG_PERIHAL','PERIHAL'), array('A.REG_PEMOHON', 'PEMOHON'), array('C.REFF_DESC','UNIT KERJA')));
					if($this->session->userdata('USER_DEPARTEMEN')=="4" || $this->session->userdata('USER_DEPARTEMEN')=="2" || $this->session->userdata('USER_ROLE')=="01"){
						
						if($this->session->userdata('USER_DEPARTEMEN')=="2"){
							$prosesnya = array(
										   'Cancled' => array('POST', site_url().'/home/registrasi_act/cancled/ajax', 'N'),
										   #'Upload Dokumen' => array('GET', site_url().'/home/dokumen/upload_pembayaran', '1'),
										   'Print Surat' => array('GETNEW', site_url()."/prints/list_kasbon", 'N','btn-preview.png'),
										   'Edit Kasbon' => array('GET', site_url().'/home/dokumen/edit_kasbon', '1','edit-btn.png')
										   );
							
						}else{
							$prosesnya = array(
										   'Cancled' => array('POST', site_url().'/home/registrasi_act/cancled/ajax', 'N'),
										   'Upload Dokumen' => array('GET', site_url().'/home/dokumen/upload_pembayaran', '1'),
										   'Print Surat' => array('GETNEW', site_url()."/prints/list_kasbon", 'N','btn-preview.png'),
										   'Edit Kasbon' => array('GET', site_url().'/home/dokumen/edit_kasbon', '1','edit-btn.png')
										   );
								
						}
						
						if($this->session->userdata('USER_ROLE')=="01" || $this->session->userdata('USER_JABATAN')=="3.2.1"){
							$adduserwhere = '';	
						}else{
							$adduserwhere = "AND A.USER_CREATE = '".$this->session->userdata("USER_ID")."' ";				   
						}				   
					}else{
							$prosesnya = array(
										   'Cancled' => array('POST', site_url().'/home/registrasi_act/cancled/ajax', 'N'),
										   'Print Surat' => array('GETNEW', site_url()."/prints/list_kasbon", 'N','btn-preview.png'),
										   'Edit Kasbon' => array('GET', site_url().'/home/dokumen/edit_kasbon', '1','edit-btn.png')
										   );

						$adduserwhere = "AND A.USER_CREATE = '".$this->session->userdata("USER_ID")."' ";	
					}
					
					$this->newtable->menu($prosesnya);
					
					$query = "SELECT A.REG_ID, A.REG_NOMOR,A.DATE_CREATE,A.REG_NOMOR AS 'REGISTRASI ID', A.TANGGAL_PENGAJUAN AS TANGGAL, B.REFF_DESC AS 'JENIS REGISTRASI' , C.REFF_DESC AS 'UNIT KERJA', 
							  CONCAT('Rp. ',A.REG_JUMLAH) AS 'JUMLAH', A.REG_PEMOHON AS PEMOHON, A.REG_PERIHAL AS PERIHAL, 
							  (CASE WHEN A.STATUS IS NULL THEN CONCAT('On process<div> create by:',E.USER_FULLNAME,'</div>')
									WHEN A.STATUS = '1' THEN CONCAT('Uploaded <div><a href=\"".base_url()."','index.php/download/files/',A.REG_NOMOR,'/',REPLACE(A.REG_PATHFILE,CONCAT('dat/',DATE_FORMAT(A.REG_TGL_UPLOAD,'%Y%m%d'),'/'), ''),'\" target=\"_blank\">[Preview]</a></div><div> Tgl : ',A.REG_TGL_UPLOAD,'</div>')
									ELSE '<span style=\"color:red;\">Canceled</span>' 
							   END) STATUS							    
									FROM tx_reg A 
							  LEFT JOIN tb_reff B ON B.REFF_CODE = A.REG_JENIS_DOKUMEN AND B.REFF_TAB = 'JENIS_REGISTRASI'
							  LEFT JOIN tb_reff C ON C.REFF_CODE = A.REG_UNIT_KERJA AND C.REFF_TAB = 'DEPARTEMEN'
							  LEFT JOIN tm_user E ON E.USER_ID = A.USER_CREATE
									WHERE A.REG_JENIS_DOKUMEN = '102' AND A.STATUS IS NULL $adduserwhere";
									
					$this->newtable->columns(array("A.REG_ID","A.REG_NOMOR","A.TANGGAL_PENGAJUAN", "B.REFF_DESC", "C.REFF_DESC", "(CASE WHEN A.STATUS IS NULL THEN 'On process'
									WHEN A.STATUS = '1' THEN 'Uploaded' ELSE 'Canceled' END)"));	
					
				}else if($id=="pelunasan_kasbon"){
					$this->newtable->hiddens(array('REG_ID','REG_NOMOR'));		
					$this->newtable->search(array(array('A.REG_NOMOR', 'NOMOR REGIS'), array('A.REG_PERIHAL','PERIHAL'), array('A.REG_PEMOHON', 'PEMOHON'), array('C.REFF_DESC','UNIT KERJA'),
					array('A.REG_NOMOR_KASBON','NOMOR KASBON'), array('A.TANGGAL_KASBON','TANGGAL KASBON')));
					if($this->session->userdata('USER_DEPARTEMEN')=="4" || $this->session->userdata('USER_DEPARTEMEN')=="2" || $this->session->userdata('USER_ROLE')=="01"){
							if($this->session->userdata('USER_DEPARTEMEN')=="2"){
							$prosesnya = array(
										   'Cancled' => array('POST', site_url().'/home/registrasi_act/cancled/ajax', 'N'),
										   #'Upload Dokumen' => array('GET', site_url().'/home/dokumen/upload_pembayaran', '1'),
										   'Print Surat' => array('GETNEW', site_url()."/prints/list_pelunasan_kasbon", 'N','btn-preview.png'),
										   'Edit Pelunasan Kasbon' => array('GET', site_url().'/home/dokumen/edit_pelunasan_kasbon', '1','edit-btn.png')
										   );
							
							}else{
								$prosesnya = array(
											   'Cancled' => array('POST', site_url().'/home/registrasi_act/cancled/ajax', 'N'),
											   'Upload Dokumen' => array('GET', site_url().'/home/dokumen/upload_pembayaran', '1'),
											   'Print Surat' => array('GETNEW', site_url()."/prints/list_pelunasan_kasbon", 'N','btn-preview.png'),
											   'Edit Pelunasan Kasbon' => array('GET', site_url().'/home/dokumen/edit_pelunasan_kasbon', '1','edit-btn.png')
											   );
									
							}
							if($this->session->userdata('USER_ROLE')=="01" || $this->session->userdata('USER_JABATAN')=="3.2.1"){
								$adduserwhere = '';	
							}else{
								$adduserwhere = "AND A.USER_CREATE = '".$this->session->userdata("USER_ID")."' ";				   
							}					   
					}else{
							$prosesnya = array(
											   'Canceled' => array('POST', site_url().'/home/registrasi_act/cancled/ajax', 'N'),
											   'Print Surat' => array('GETNEW', site_url()."/prints/list_pelunasan_kasbon", 'N','btn-preview.png'),
											   'Edit Pelunasan Kasbon' => array('GET', site_url().'/home/dokumen/edit_pelunasan_kasbon', '1','edit-btn.png')
											   );
											   
							$adduserwhere = "AND A.USER_CREATE = '".$this->session->userdata("USER_ID")."' ";	
						
					}				   
					$this->newtable->menu($prosesnya);
					
					$query = "SELECT A.REG_ID, A.REG_NOMOR, A.DATE_CREATE,A.REG_NOMOR AS 'REGISTRASI ID', A.TANGGAL_PENGAJUAN AS TANGGAL,
							  CONCAT(A.REG_NOMOR_KASBON,'<div>', A.TANGGAL_KASBON,'</div>') AS 'PENGAJUAN KASBON',
							  B.REFF_DESC AS 'JENIS REGISTRASI' , C.REFF_DESC AS 'UNIT KERJA', 
							  CONCAT('Rp. ',A.REG_JUMLAH) AS 'JUMLAH', A.REG_PEMOHON AS PEMOHON, A.REG_PERIHAL AS PERIHAL, 
							  (CASE WHEN A.STATUS IS NULL THEN CONCAT('On process<div> create by:',E.USER_FULLNAME,'</div>')
									WHEN A.STATUS = '1' THEN CONCAT('Uploaded <div><a href=\"".base_url()."','index.php/download/files/',A.REG_NOMOR,'/',REPLACE(A.REG_PATHFILE,CONCAT('dat/',DATE_FORMAT(A.REG_TGL_UPLOAD,'%Y%m%d'),'/'), ''),'\" target=\"_blank\">[Preview]</a></div><div> Tgl : ',A.REG_TGL_UPLOAD,'</div>')
									ELSE '<span style=\"color:red;\">Canceled</span>' 
							   END) STATUS							    
									FROM tx_reg A 
							  LEFT JOIN tb_reff B ON B.REFF_CODE = A.REG_JENIS_DOKUMEN AND B.REFF_TAB = 'JENIS_REGISTRASI'
							  LEFT JOIN tb_reff C ON C.REFF_CODE = A.REG_UNIT_KERJA AND C.REFF_TAB = 'DEPARTEMEN'
							  LEFT JOIN tm_user E ON E.USER_ID = A.USER_CREATE
									WHERE A.REG_JENIS_DOKUMEN = '103' AND A.STATUS IS NULL $adduserwhere";
									
					$this->newtable->columns(array("A.REG_ID","A.REG_NOMOR","A.TANGGAL_PENGAJUAN","CONCAT(A.REG_NOMOR_KASBON,'<div>', A.TANGGAL_KASBON,'</div>')", "B.REFF_DESC", "C.REFF_DESC", "(CASE WHEN A.STATUS IS NULL THEN 'On process'
									WHEN A.STATUS = '1' THEN 'Uploaded' ELSE 'Cancled' END)"));	
				}else if($id=="uploaded"){
					$this->newtable->hiddens(array('REG_ID','REG_NOMOR'));		
					$prosesnya = array(
									  
									   'Print Surat' => array('GETNEW', site_url()."/prints/list_pelunasan_kasbon", 'N','btn-preview.png')
									   );
					$this->newtable->menu($prosesnya);
					
					$query = "SELECT A.REG_ID, A.REG_NOMOR,A.REG_NOMOR AS 'REGISTRASI ID', A.TANGGAL_PENGAJUAN AS TANGGAL, B.REFF_DESC AS 'JENIS REGISTRASI' , C.REFF_DESC AS 'UNIT KERJA', 
							  CONCAT('Rp. ',A.REG_JUMLAH) AS 'JUMLAH', A.REG_PEMOHON AS PEMOHONA, A.REG_PERIHAL AS PERIHAL, 
							  (CASE WHEN A.STATUS IS NULL THEN 'On process'
									WHEN A.STATUS = '1' THEN CONCAT('Uploaded <div><a href=\"".base_url()."','index.php/download/files/',A.REG_NOMOR,'/',REPLACE(A.REG_PATHFILE,CONCAT('dat/',DATE_FORMAT(A.REG_TGL_UPLOAD,'%Y%m%d'),'/'), ''),'\" target=\"_blank\">[Preview]</a></div><div> Tgl : ',A.REG_TGL_UPLOAD,'</div>')
									ELSE '<span style=\"color:red;\">Canceled</span>' 
							   END) STATUS							    
									FROM tx_reg A 
							  LEFT JOIN tb_reff B ON B.REFF_CODE = A.REG_JENIS_DOKUMEN AND B.REFF_TAB = 'JENIS_REGISTRASI'
							  LEFT JOIN tb_reff C ON C.REFF_CODE = A.REG_UNIT_KERJA AND C.REFF_TAB = 'DEPARTEMEN'
									WHERE A.STATUS = '1'";
									
					$this->newtable->columns(array("A.REG_ID","A.REG_NOMOR","A.TANGGAL_PENGAJUAN", "B.REFF_DESC", "C.REFF_DESC", "(CASE WHEN A.STATUS IS NULL THEN 'On process'
									WHEN A.STATUS = '1' THEN 'Uploaded' ELSE 'Cancled' END)"));	
				}else if($id=="canceled"){
					$this->newtable->hiddens(array('REG_ID','REG_NOMOR'));		
					$prosesnya = array(
									   
									   'Print Surat' => array('GETNEW', site_url()."/prints/list_pelunasan_kasbon", 'N','btn-preview.png')
									   
									   );
									   
					if($this->session->userdata('USER_ROLE')=="01" || $this->session->userdata('USER_JABATAN')=="3.2.1"){
						$adduserwhere = '';	
					}else{
						$adduserwhere = "AND A.USER_CREATE = '".$this->session->userdata("USER_ID")."' ";				   
					}		
					
					$this->newtable->menu($prosesnya);
					
					$query = "SELECT A.REG_ID, A.REG_NOMOR,A.REG_NOMOR AS 'REGISTRASI ID', A.TANGGAL_PENGAJUAN AS TANGGAL, B.REFF_DESC AS 'JENIS REGISTRASI' , C.REFF_DESC AS 'UNIT KERJA', 
							  CONCAT('Rp. ',A.REG_JUMLAH) AS 'JUMLAH', A.REG_PEMOHON AS PEMOHONA, A.REG_PERIHAL AS PERIHAL, 
							  (CASE WHEN A.STATUS IS NULL THEN 'On process'
									WHEN A.STATUS = '1' THEN CONCAT('Uploaded <div><a href=\"".base_url()."','index.php/download/files/',A.REG_NOMOR,'/',REPLACE(A.REG_PATHFILE,CONCAT('dat/',DATE_FORMAT(A.REG_TGL_UPLOAD,'%Y%m%d'),'/'), ''),'\" target=\"_blank\">[Preview]</a></div><div> Tgl : ',A.REG_TGL_UPLOAD,'</div>')
									ELSE '<span style=\"color:red;\">Canceled</span>' 
							   END) STATUS							    
									FROM tx_reg A 
							  LEFT JOIN tb_reff B ON B.REFF_CODE = A.REG_JENIS_DOKUMEN AND B.REFF_TAB = 'JENIS_REGISTRASI'
							  LEFT JOIN tb_reff C ON C.REFF_CODE = A.REG_UNIT_KERJA AND C.REFF_TAB = 'DEPARTEMEN'
									WHERE A.STATUS = '2' $adduserwhere";
									
					$this->newtable->columns(array("A.REG_ID","A.REG_NOMOR","A.TANGGAL_PENGAJUAN", "B.REFF_DESC", "C.REFF_DESC", "(CASE WHEN A.STATUS IS NULL THEN 'On process'
									WHEN A.STATUS = '1' THEN 'Uploaded' ELSE 'Cancled' END)"));	
				}else if($id=="rapat"){
					$this->newtable->search(array(array('A.AGENDA', 'AGENDA'),array('A.SUB_AGENDA','SUB JUDUL'), array('A.TANGGAL', 'TANGGAL')));
					$this->newtable->hiddens(array('ID_RAPAT'));		
					$prosesnya = array( 
									   'Print Daftar Hadir' => array('GETNEW', site_url()."/prints/list_absen_rapat", 'N','btn-preview.png')
									   
									   );
					$this->newtable->menu($prosesnya);
					
					$query = "SELECT A.ID_RAPAT, A.AGENDA AS 'JUDUL', IFNULL(A.SUB_AGENDA, '-') AS 'SUB JUDUL', 
							  DATE_FORMAT(A.TANGGAL, '%Y-%m-%d') AS TANGGAL,
							  CONCAT('<a href=\"".base_url()."','index.php/home/forms/',DATE_FORMAT(A.TANGGAL,'%Y-%m-%d'),'/',A.ID_RAPAT,'\" target=\"_blank\">[Link Forms]</a>') AS LINK 
							  FROM t_rapat A WHERE A.USER_CREATE = '".$this->session->userdata("USER_ID")."' ";
									
					$this->newtable->columns(array("A.ID_RAPAT","A.AGENDA","A.SUB_AGENDA"));	
				}
				
				
				if($id=="pelunasan_kasbon") $id = 'pelunasan kasbon';
				$judul = "List Registrasi ".ucwords($id);
				$this->newtable->use_ajax(FALSE);
				$this->newtable->js_file('<script type="text/javascript" src="'.base_url().'assets/js/newtable.js"></script>');
				$tabel = $this->newtable->generate($query);
				if($ispreview){
					$tabel = "<script type=\"text/javascript\" src=\"".base_url()."assets/js/newtable.js\"></script>".$tabel;
					$arrdata = $tabel;
				}else{
					$arrdata = array("htmltable" => $tabel,
									 "judul" => $judul
									 );
				}
				return $arrdata;
					
			}
		redirect(base_url());
		exit();		
	}
	
	
	function list_registrasi_kendaraan($id="", $ispreview=FALSE){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
				
			if($this->session->userdata('LOGGED')){
				
				$this->load->library('newtable');
			
					$this->newtable->hiddens(array('REG_ID','REG_NOMOR','DATE_CREATE'));
					$this->newtable->orderby(1);
				
					$this->newtable->keys(array("REG_ID"));
					$this->newtable->sortby('DESC');
				
					
					$this->newtable->action(site_url()."/home/dokumen/listregis/".$id);
					$this->newtable->cidb($this->db);
					$this->newtable->ciuri($this->uri->segment_array());
				
				
				if($ispreview){
					$this->newtable->rowcount("ALL");
					$this->newtable->show_chk(FALSE);
					$this->newtable->show_search(FALSE);
				}
				if($this->session->userdata('USER_ROLE')=="01" || $this->session->userdata('USER_ID')=="37" || $this->session->userdata('USER_ID')=="29" || $this->session->userdata('USER_ID')=="13" || $this->session->userdata('USER_ID')=="33"){
					$addwhere = '';
				}else if($this->session->userdata('USER_ROLE')=='03'){
					$addwhere = "AND ATASAN_PEMOHON = '".$this->session->userdata('USER_ID')."' ";
					
				}else{
					$addwhere = "AND A.USER_CREATE = '".$this->session->userdata('USER_ID')."'";
				}
				
				if($id=="new"){
					$this->newtable->hiddens(array('REG_ID'));		
					$this->newtable->search(array(array('A.REG_ID', 'NOMOR REGIS'),array('A.TUJUAN','TUJUAN'), array('A.KEPERLUAN', 'KEPERLUAN')));
					if($this->session->userdata('USER_DEPARTEMEN')=="5" || $this->session->userdata('USER_ROLE')=="01"){
						if($this->session->userdata('USER_DEPARTEMEN')=="2"){
							$prosesnya = array(
										   #'Cancled/Tolak' => array('POST', site_url().'/home/registrasi_act/cancleddriver/ajax', 'N'),
										   'Print Surat' => array('GETNEW', site_url()."/prints/regis_kendaraan", 'N','btn-preview.png')
										   );
							
						}else{
							
							if($this->session->userdata('USER_ROLE')=="01" || $this->session->userdata('USER_ID')=="13" || $this->session->userdata('USER_ID')=="33" || $this->session->userdata('USER_ID')=="29"){
								$prosesnya = array(
											   'Input Driver' => array('GET', site_url().'/home/dokumen/update_driver', '1'),	
											   'Cancled/Tolak' => array('POST', site_url().'/home/registrasi_act/cancleddriver/ajax', 'N'),
											   'Proses Selesai' => array('POST', site_url().'/home/registrasi_act/approvedriver/ajax', 'N'),
											   'Upload Dokumen' => array('GET', site_url().'/home/dokumen/upload_regis_kendaraan', '1'),
											   'Print Surat' => array('GETNEW', site_url()."/prints/regis_kendaraan", 'N','btn-preview.png')
											 
											   );	
							}else{
								$prosesnya = array(
												#'Cancled/Tolak' => array('POST', site_url().'/home/registrasi_act/cancleddriver/ajax', 'N'),
												'Print Surat' => array('GETNEW', site_url()."/prints/regis_kendaraan", 'N','btn-preview.png')
												);
								
							}	
							
								
						}
								   
									   
					}else{
						$prosesnya = array(
										#'Cancled' => array('POST', site_url().'/home/registrasi_act/cancleddriver/ajax', 'N'),
										'Print Surat' => array('GETNEW', site_url()."/prints/regis_kendaraan", 'N','btn-preview.png')
										
											);
										
						
					}				   
					$this->newtable->menu($prosesnya);
					
					$query = "SELECT A.REG_ID, CONCAT(A.REG_ID,'<div> create by ', G.USER_FULLNAME,'</div><div>',IFNULL(A.TANGGAL_PENGAJUAN,'-'),'</div>') AS 'REGISTRASI ID', CONCAT(B.USER_FULLNAME,'<div>', C.REFF_DESC,'</div>') AS 'NAMA PEMOHON' , 
							  CONCAT(A.TANGGAL_PERMOHONAN,' ', A.WAKTU_PERMOHONAN) AS TANGGAL, 
							  D.USER_FULLNAME AS 'ATASAN', A.KEPERLUAN, A.TUJUAN, 
							  (CASE WHEN A.JENIS_KENDARAAN = '1' THEN CONCAT(E.USER_FULLNAME,'<div>',A.NO_KENDARAAN,'</div>')
							  		ELSE A.JENIS_KENDARAAN_DESC
							   END) DRIVER,
							  (CASE 
								WHEN A.TGL_TTD IS NOT NULL THEN CONCAT('Disetujui - ', D.USER_FULLNAME,'<div>',A.TGL_TTD,'</div>')
								ELSE F.REFF_DESC END) AS STATUS
									FROM tx_kendaraan A 
							  LEFT JOIN tm_user B ON B.USER_ID = A.NAMA_PEMOHON
							  LEFT JOIN tb_reff C ON C.REFF_CODE = A.UNIT_KERJA AND C.REFF_TAB = 'DEPARTEMEN'
							  LEFT JOIN tm_user D ON D.USER_ID = A.ATASAN_PEMOHON
							  LEFT JOIN tm_user E ON E.USER_ID = A.DRIVER_ID
							  LEFT JOIN tb_reff F ON F.REFF_CODE = A.STATUS AND F.REFF_TAB = 'STATUS_KENDARAAN'
							  LEFT JOIN tm_user G ON G.USER_ID = A.USER_CREATE 	
								WHERE A.STATUS = '2' $addwhere";
								
					$this->newtable->columns(array("A.REG_ID","CONCAT(B.USER_FULLNAME,'<div>', C.REFF_DESC,'</div>')","CONCAT(A.TANGGAL_PERMOHONAN,' ', A.WAKTU_PERMOHONAN)", "D.USER_FULLNAME", 
					"A.KEPERLUAN", "A.TUJUAN","CONCAT(E.USER_FULLNAME,'<div>',A.NO_KENDARAAN,'</div>')"));				
				}else if($id=="approve"){
					$this->newtable->hiddens(array('REG_ID'));		
					$this->newtable->search(array(array('A.REG_ID', 'NOMOR REGIS'),array('A.TUJUAN','TUJUAN'), array('A.KEPERLUAN', 'KEPERLUAN')));
					
					if($this->session->userdata('USER_ROLE')=="01" || $this->session->userdata('USER_ID')=="13" || $this->session->userdata('USER_ID')=="33"|| $this->session->userdata('USER_ID')=="37" || $this->session->userdata('USER_ID')=="29"){
						$addwhere = '';
					}else if($this->session->userdata('USER_ROLE')=='03'){
						$addwhere = "AND ATASAN_PEMOHON = '".$this->session->userdata('USER_ID')."' ";
						
					}else{
						$addwhere = "AND A.USER_CREATE = '".$this->session->userdata('USER_ID')."'";
					}
					
					$prosesnya = array(
										#'Cancled' => array('POST', site_url().'/home/registrasi_act/cancled/ajax', 'N'),
										'Print Surat' => array('GETNEW', site_url()."/prints/regis_kendaraan", 'N','btn-preview.png')
									  );
					$this->newtable->menu($prosesnya);
					$query = "SELECT A.REG_ID, CONCAT(A.REG_ID,'<div> create by ', G.USER_FULLNAME,'</div>') AS 'REGISTRASI ID' , CONCAT(B.USER_FULLNAME,'<div>', C.REFF_DESC,'</div>') AS NAMA , 
							  CONCAT(A.TANGGAL_PERMOHONAN,' ', A.WAKTU_PERMOHONAN) AS TANGGAL, 
							  D.USER_FULLNAME AS 'ATASAN', A.KEPERLUAN, A.TUJUAN, 
							  (CASE WHEN A.JENIS_KENDARAAN = '1' THEN CONCAT(E.USER_FULLNAME,'<div>',A.NO_KENDARAAN,'</div>')
							  		ELSE A.JENIS_KENDARAAN_DESC
							   END) DRIVER,
							  (CASE 
								WHEN A.TGL_TTD IS NOT NULL THEN CONCAT('Selesai <div>disetujui; ',A.TGL_TTD,'</div>')
								ELSE F.REFF_DESC 
							   END) AS STATUS
									FROM tx_kendaraan A 
							  LEFT JOIN tm_user B ON B.USER_ID = A.NAMA_PEMOHON
							  LEFT JOIN tb_reff C ON C.REFF_CODE = A.UNIT_KERJA AND C.REFF_TAB = 'DEPARTEMEN'
							  LEFT JOIN tm_user D ON D.USER_ID = A.ATASAN_PEMOHON
							  LEFT JOIN tm_user E ON E.USER_ID = A.DRIVER_ID
							  LEFT JOIN tb_reff F ON F.REFF_CODE = A.STATUS AND F.REFF_TAB = 'STATUS_KENDARAAN' 
							  LEFT JOIN tm_user G ON G.USER_ID = A.USER_CREATE	
								WHERE A.STATUS = '1' $addwhere";
								
					$this->newtable->columns(array("A.REG_ID","CONCAT(B.USER_FULLNAME,'<div>', C.REFF_DESC,'</div>')","CONCAT(A.TANGGAL_PERMOHONAN,' ', A.WAKTU_PERMOHONAN)", "D.USER_FULLNAME", 
					"A.KEPERLUAN", "A.TUJUAN","CONCAT(E.USER_FULLNAME,'<div>',A.NO_KENDARAAN,'</div>')"));	
				}else if($id=="accreg"){
					$this->newtable->hiddens(array('REG_ID'));		
					$this->newtable->search(array(array('A.REG_ID', 'NOMOR REGIS'),array('A.TUJUAN','TUJUAN'), array('A.KEPERLUAN', 'KEPERLUAN')));
					
					if($this->session->userdata('USER_ROLE')=='03' || $this->session->userdata('USER_JABATAN')=='14.1'){
						$addwhere = "AND ATASAN_PEMOHON = '".$this->session->userdata('USER_ID')."' ";
						$prosesnya = array(
										'Acc' => array('POST', site_url().'/home/registrasi_act/acc/ajax', 'N'),
										'Print Surat' => array('GETNEW', site_url()."/prints/regis_kendaraan", 'N','btn-preview.png')
									  );
					}else if($this->session->userdata('USER_ROLE')=='01' || $this->session->userdata('USER_ID')=="13" || $this->session->userdata('USER_ID')=="33" || $this->session->userdata('USER_ID')=="29"){
						$addwhere = "";
						if($this->session->userdata('USER_ID')=="13" || $this->session->userdata('USER_ID')=="33" || $this->session->userdata('USER_ID')=="29"){
							$prosesnya = array(
										'Input Driver' => array('GET', site_url().'/home/dokumen/update_driver', '1'),
										'Cancled/Tolak' => array('POST', site_url().'/home/registrasi_act/cancleddriver/ajax', 'N'),
										'Print Surat' => array('GETNEW', site_url()."/prints/regis_kendaraan", 'N','btn-preview.png')
									  );
						}else{
							$prosesnya = array(
										'Acc' => array('POST', site_url().'/home/registrasi_act/acc/ajax', 'N'),
										'Cancled/Tolak' => array('POST', site_url().'/home/registrasi_act/cancleddriver/ajax', 'N'),
										'Print Surat' => array('GETNEW', site_url()."/prints/regis_kendaraan", 'N','btn-preview.png')
									  );	
						}
						
					}else{
						$addwhere = "AND A.USER_CREATE = '".$this->session->userdata('USER_ID')."'";
						$prosesnya = array(
										#'Cancled' => array('POST', site_url().'/home/registrasi_act/cancled/ajax', 'N'),
										'Print Surat' => array('GETNEW', site_url()."/prints/regis_kendaraan", 'N','btn-preview.png')
									  );
					}
					$this->newtable->menu($prosesnya);
					$query = "SELECT A.REG_ID, CONCAT(A.REG_ID,'<div> create by ', G.USER_FULLNAME,'</div>') AS 'REGISTRASI ID' , CONCAT(B.USER_FULLNAME,'<div>', C.REFF_DESC,'</div>') AS NAMA , 
							  CONCAT(A.TANGGAL_PERMOHONAN,' ', A.WAKTU_PERMOHONAN) AS TANGGAL, 
							  D.USER_FULLNAME AS 'ATASAN', A.KEPERLUAN, A.TUJUAN, 
							  (CASE WHEN A.JENIS_KENDARAAN = '1' THEN CONCAT(E.USER_FULLNAME,'<div>',A.NO_KENDARAAN,'</div>')
							  		ELSE A.JENIS_KENDARAAN_DESC
							   END) DRIVER,
							  F.REFF_DESC AS STATUS
									FROM tx_kendaraan A 
							  LEFT JOIN tm_user B ON B.USER_ID = A.NAMA_PEMOHON
							  LEFT JOIN tb_reff C ON C.REFF_CODE = A.UNIT_KERJA AND C.REFF_TAB = 'DEPARTEMEN'
							  LEFT JOIN tm_user D ON D.USER_ID = A.ATASAN_PEMOHON
							  LEFT JOIN tm_user E ON E.USER_ID = A.DRIVER_ID
							  LEFT JOIN tb_reff F ON F.REFF_CODE = A.STATUS AND F.REFF_TAB = 'STATUS_KENDARAAN' 
							  LEFT JOIN tm_user G ON G.USER_ID = A.USER_CREATE	
								WHERE A.STATUS = '3' $addwhere";
								
					$this->newtable->columns(array("A.REG_ID","CONCAT(B.USER_FULLNAME,'<div>', C.REFF_DESC,'</div>')","CONCAT(A.TANGGAL_PERMOHONAN,' ', A.WAKTU_PERMOHONAN)", "D.USER_FULLNAME", 
					"A.KEPERLUAN", "A.TUJUAN","CONCAT(E.USER_FULLNAME,'<div>',A.NO_KENDARAAN,'</div>')"));	
				}else if($id=="prosesReg"){
					$this->newtable->hiddens(array('REG_ID'));		
					$this->newtable->search(array(array('A.REG_ID', 'NOMOR REGIS'),array('A.TUJUAN','TUJUAN'), array('A.KEPERLUAN', 'KEPERLUAN')));
					
					$prosesnya = array(
										#'Cancled' => array('POST', site_url().'/home/registrasi_act/cancled/ajax', 'N'),
										'Print Surat' => array('GETNEW', site_url()."/prints/regis_kendaraan", 'N','btn-preview.png')
									  );
					$this->newtable->menu($prosesnya);
					$query = "SELECT A.REG_ID, A.REG_ID AS 'REGISTRASI ID', CONCAT(B.USER_FULLNAME,'<div>', C.REFF_DESC,'</div>') AS NAMA , 
							  CONCAT(A.TANGGAL_PERMOHONAN,' ', A.WAKTU_PERMOHONAN) AS TANGGAL, 
							  D.USER_FULLNAME AS 'ATASAN', A.KEPERLUAN, A.TUJUAN, CONCAT(E.USER_FULLNAME,'<div>',A.NO_KENDARAAN,'</div>') DRIVER,
							  F.REFF_DESC AS STATUS
									FROM tx_kendaraan A 
							  LEFT JOIN tm_user B ON B.USER_ID = A.NAMA_PEMOHON
							  LEFT JOIN tb_reff C ON C.REFF_CODE = A.UNIT_KERJA AND C.REFF_TAB = 'DEPARTEMEN'
							  LEFT JOIN tm_user D ON D.USER_ID = A.ATASAN_PEMOHON
							  LEFT JOIN tm_user E ON E.USER_ID = A.DRIVER_ID
							  LEFT JOIN tb_reff F ON F.REFF_CODE = A.STATUS AND F.REFF_TAB = 'STATUS_KENDARAAN' 	
								WHERE A.STATUS = '3' $addwhere";
								
					$this->newtable->columns(array("A.REG_ID","CONCAT(B.USER_FULLNAME,'<div>', C.REFF_DESC,'</div>')","CONCAT(A.TANGGAL_PERMOHONAN,' ', A.WAKTU_PERMOHONAN)", "D.USER_FULLNAME", 
					"A.KEPERLUAN", "A.TUJUAN","CONCAT(E.USER_FULLNAME,'<div>',A.NO_KENDARAAN,'</div>')"));	
				}else if($id=="denied"){
					$this->newtable->hiddens(array('REG_ID'));		
					$this->newtable->search(array(array('A.REG_ID', 'NOMOR REGIS'),array('A.TUJUAN','TUJUAN'), array('A.KEPERLUAN', 'KEPERLUAN')));
					$this->newtable->hiddens(array('REG_ID'));		
					$this->newtable->search(array(array('A.REG_ID', 'NOMOR REGIS'),array('A.TUJUAN','TUJUAN'), array('A.KEPERLUAN', 'KEPERLUAN')));
					
					$prosesnya = array(
										#'Cancled' => array('POST', site_url().'/home/registrasi_act/cancled/ajax', 'N'),
										'Print Surat' => array('GETNEW', site_url()."/prints/regis_kendaraan", 'N','btn-preview.png')
									  );
					$this->newtable->menu($prosesnya);
					$query = "SELECT A.REG_ID, A.REG_ID AS 'REGISTRASI ID', CONCAT(B.USER_FULLNAME,'<div>', C.REFF_DESC,'</div>') AS NAMA , 
							  CONCAT(A.TANGGAL_PERMOHONAN,' ', A.WAKTU_PERMOHONAN) AS TANGGAL, 
							  D.USER_FULLNAME AS 'ATASAN', A.KEPERLUAN, A.TUJUAN, 
							  (CASE WHEN A.JENIS_KENDARAAN = '1' THEN CONCAT(E.USER_FULLNAME,'<div>',A.NO_KENDARAAN,'</div>')
							  		ELSE A.JENIS_KENDARAAN_DESC
							   END) DRIVER,
							  F.REFF_DESC AS STATUS
									FROM tx_kendaraan A 
							  LEFT JOIN tm_user B ON B.USER_ID = A.NAMA_PEMOHON
							  LEFT JOIN tb_reff C ON C.REFF_CODE = A.UNIT_KERJA AND C.REFF_TAB = 'DEPARTEMEN'
							  LEFT JOIN tm_user D ON D.USER_ID = A.ATASAN_PEMOHON
							  LEFT JOIN tm_user E ON E.USER_ID = A.DRIVER_ID
							  LEFT JOIN tb_reff F ON F.REFF_CODE = A.STATUS AND F.REFF_TAB = 'STATUS_KENDARAAN' 	
								WHERE A.STATUS = '0' $addwhere";
								
					$this->newtable->columns(array("A.REG_ID","CONCAT(B.USER_FULLNAME,'<div>', C.REFF_DESC,'</div>')","CONCAT(A.TANGGAL_PERMOHONAN,' ', A.WAKTU_PERMOHONAN)", "D.USER_FULLNAME", 
					"A.KEPERLUAN", "A.TUJUAN","CONCAT(E.USER_FULLNAME,'<div>',A.NO_KENDARAAN,'</div>')"));
				}
				
				
				
				$judul = "List Registrasi Kendaraan";
				$this->newtable->use_ajax(FALSE);
				$this->newtable->js_file('<script type="text/javascript" src="'.base_url().'assets/js/newtable.js"></script>');
				$tabel = $this->newtable->generate($query);
				if($ispreview){
					$tabel = "<script type=\"text/javascript\" src=\"".base_url()."assets/js/newtable.js\"></script>".$tabel;
					$arrdata = $tabel;
				}else{
					$arrdata = array("htmltable" => $tabel,
									 "judul" => $judul
									 );
				}
				return $arrdata;
					
			}
		redirect(base_url());
		exit();		
	}
	
	function get_report_cashflow($menu="", $id="", $subid=""){
	
		$cbs =& get_instance();
		$cbs->load->model("main","main", true);

		$act = site_url().'/home/registrasi_act/saveabsen';
		
		$isi = "SELECT A.CMS_ID, CONCAT(A.TANGGAL_CMS,' ', A.JAM_CMS) AS TANGGAL, A.DESKRIPSI_CMS AS DESKRIPSI, 
			  A.DEBIT_CMS AS DEBIT, A.KREDIT_CMS AS KREDIT, A.SALDO_CMS AS SALDO, 
			  A.KETERANGAN_CMS AS KETERANGAN, CONCAT(A.NOREK_CMS,'-', A.BANK_CMS) AS 'NOMOR_REKENING'
				 FROM tx_cms A
			  LEFT JOIN tm_user B ON B.USER_ID = A.USER_UPLOAD
			  WHERE A.KETERANGAN_CMS LIKE '%".$menu."%'
			  AND  A.TANGGAL_CMS >= '".$id."' and A.TANGGAL_CMS <= '".$subid."' AND A.STATUS IS NULL ORDER BY A.CMS_ID";
			
		
		$arrisi = $this->db->query($isi)->result_array();									 
	
		$arrdata = array(						 						 
						 'arrisi' => $arrisi,
						 'uid' => $menu,
						 'awal' => $id,
						 'akhir' => $subid
						 );
		return $arrdata;
	}

	function list_regis($id="", $ispreview=FALSE){
		$cbs =& get_instance();
		$cbs->load->model("main","main", true);
				
		if($this->session->userdata('LOGGED')){
			
			$this->load->library('newtable');
			$this->newtable->hiddens(array('REG_ID'));
			$this->newtable->orderby(1);
		
			$this->newtable->keys(array("REG_ID"));
			$this->newtable->sortby('DESC');
		
			
			$this->newtable->action(site_url()."/home/dokumen/listRegus/".$id);
			$this->newtable->cidb($this->db);
			$this->newtable->ciuri($this->uri->segment_array());
			
			
		if($ispreview){
			$this->newtable->rowcount("ALL");
			$this->newtable->show_chk(FALSE);
			$this->newtable->show_search(FALSE);
		}
	
				
		if($id=="listS2M"){
			$judul = "List Registrasi";
			$this->newtable->hiddens(array('REG_ID'));		
			$this->newtable->search(array(array('A.PERIHAL_SURAT', 'PERIHAL'),array('A.TUJUAN_SURAT','UNTUK'), 
			array('A.NOMOR_SURAT', 'NOMOR SURAT'), array('A.TANGGAL_SURAT', 'TANGGAL SURAT'), array('A.NOTE_SURAT', 'NOTE')));
			
			$prosesnya = array(
								#'Fix CMS' => array('GET', site_url().'/home/dokumen/SetFixCMS', '1'),
								'Hapus' => array('POST', site_url().'/home/registrasi_act/deleteregsurat/ajax', 'N')
						
							);
			
			$this->newtable->menu($prosesnya);
			
			
			
			$query = "SELECT A.REG_ID, B.REFF_DESC AS 'JENIS REG SURAT', C.REFF_DESC AS 'JENIS SURAT', A.TANGGAL_SURAT AS TANGGAL,
						A.NOMOR_SURAT AS 'NOMOR SURAT', A.PERIHAL_SURAT AS PERIHAL, 
						(CASE 
							WHEN A.ASAL_SURAT = '' THEN '-' 
							ELSE A.ASAL_SURAT 
						END) AS DARI,
						A.TUJUAN_SURAT AS UNTUK,
						(CASE
							WHEN A.TIPE_SURAT = '0' THEN '-'
							ELSE D.REFF_DESC
						END) AS 'Copy/Asli/Tembusan',
						A.DISPOSISI_SURAT AS DISPO,
						A.JAM_OUT AS 'OUT',
						A.NOTE_SURAT AS NOTE
							FROM tx_reg_surat A
						LEFT JOIN tb_reff B ON B.REFF_CODE = A.JENIS_REG_SURAT AND B.REFF_TAB = 'JENIS_REG_SURAT'
						LEFT JOIN tb_reff C ON C.REFF_CODE = A.JENIS_SURAT AND C.REFF_TAB = 'JENIS_SURAT'
						LEFT JOIN tb_reff D ON D.REFF_CODE = A.TIPE_SURAT AND D.REFF_TAB = 'TIPE_SURAT'
							WHERE A.JENIS_SURAT IN ('1','2','3')
						";
						
						
				$this->newtable->columns(array("A.REG_ID", "B.REFF_DESC", "C.REFF_DESC", "A.TANGGAL_SURAT", 
			"A.NOMOR_SURAT", "A.PERIHAL_SURAT","(CASE 
							WHEN A.ASAL_SURAT = '' THEN '-' 
							ELSE A.ASAL_SURAT 
						END)","A.TUJUAN_SURAT","(CASE
							WHEN A.TIPE_SURAT = '0' THEN '-'
							ELSE D.REFF_DESC
						END)",
			"A.DISPOSISI_SURAT","A.JAM_OUT","A.NOTE_SURAT"));		
		}else if($id=="listPembayaran"){
			$judul = "List Pembayaran";
			$this->newtable->hiddens(array('REG_ID'));		
			$this->newtable->search(array(array('A.PERIHAL_SURAT', 'PERIHAL'),array('A.TUJUAN_SURAT','UNTUK'), 
			array('A.NOMOR_SURAT', 'NOMOR SURAT'), array('A.TANGGAL_SURAT', 'TANGGAL SURAT'), array('A.NOTE_SURAT', 'NOTE')));
			
			$prosesnya = array(
								#'Fix CMS' => array('GET', site_url().'/home/dokumen/SetFixCMS', '1'),
								'Hapus' => array('POST', site_url().'/home/registrasi_act/deleteregsurat/ajax', 'N')
						
							);
			
			$this->newtable->menu($prosesnya);
			
			
			
			$query = "SELECT A.REG_ID, B.REFF_DESC AS 'JENIS REG SURAT', A.TANGGAL_SURAT AS TANGGAL,
						A.NAMA_PEMOHON AS 'NAMA PEMOHON', A.TUJUAN_PEMBAYARAN AS 'TUJUAN PEMBAYARAN', 
						A.TOTAL_PEMBAYARAN AS TOTAL,
						A.DISPOSISI_SURAT AS DISPO,
						A.JAM_OUT AS 'OUT',
						A.NOTE_SURAT AS NOTE
							FROM tx_reg_surat A
						LEFT JOIN tb_reff B ON B.REFF_CODE = A.JENIS_REG_SURAT AND B.REFF_TAB = 'JENIS_REG_SURAT'
						LEFT JOIN tb_reff C ON C.REFF_CODE = A.JENIS_SURAT AND C.REFF_TAB = 'JENIS_SURAT'
						LEFT JOIN tb_reff D ON D.REFF_CODE = A.TIPE_SURAT AND D.REFF_TAB = 'TIPE_SURAT'
							WHERE A.JENIS_REG_SURAT IN ('P')
						";
						
						
				$this->newtable->columns(array("A.REG_ID", "B.REFF_DESC",  "A.TANGGAL_SURAT", 
			"A.NAMA_PEMOHON", "A.TUJUAN_PEMBAYARAN","A.TOTAL_PEMBAYARAN","A.TUJUAN_SURAT","A.DISPOSISI_SURAT","A.JAM_OUT","A.NOTE_SURAT"));	
		
		
		}else if($id=="listOverTime"){
			$judul = "List SPKL& Laporan Kerja Lembur";
			$this->newtable->hiddens(array('REG_ID'));		
			$this->newtable->search(array(array('A.PERIHAL_SURAT', 'PERIHAL'),array('A.TUJUAN_SURAT','UNTUK'), 
			array('A.NOMOR_SURAT', 'NOMOR SURAT'), array('A.TANGGAL_SURAT', 'TANGGAL SURAT'), array('A.NOTE_SURAT', 'NOTE')));
			
			$prosesnya = array(
								#'Fix CMS' => array('GET', site_url().'/home/dokumen/SetFixCMS', '1'),
								'Hapus' => array('POST', site_url().'/home/registrasi_act/deleteregsurat/ajax', 'N')
						
							);
			
			$this->newtable->menu($prosesnya);
			
			
			
			$query = "SELECT A.REG_ID, B.REFF_DESC AS 'JENIS REG SURAT', C.REFF_DESC AS 'JENIS SURAT', A.TANGGAL_SURAT AS TANGGAL,
						A.NAMA_PEMOHON AS 'NAMA PEMOHON', A.UNIT_KERJA AS 'UNIT KERJA', 
						A.PERIHAL_SURAT AS ALASAN,
						A.TANGGAL_LEMBUR AS 'TGL LEMBUR',
						A.JAM_OUT AS 'OUT',
						A.NOTE_SURAT AS NOTE
							FROM tx_reg_surat A
						LEFT JOIN tb_reff B ON B.REFF_CODE = A.JENIS_REG_SURAT AND B.REFF_TAB = 'JENIS_REG_SURAT'
						LEFT JOIN tb_reff C ON C.REFF_CODE = A.JENIS_SURAT AND C.REFF_TAB = 'JENIS_SURAT'
						LEFT JOIN tb_reff D ON D.REFF_CODE = A.TIPE_SURAT AND D.REFF_TAB = 'TIPE_SURAT'
							WHERE A.JENIS_REG_SURAT IN ('L')
						";
						
						
				$this->newtable->columns(array("A.REG_ID", "B.REFF_DESC",  "A.TANGGAL_SURAT", 
			"A.NAMA_PEMOHON", "A.TUJUAN_PEMBAYARAN","A.TOTAL_PEMBAYARAN","A.TUJUAN_SURAT","A.DISPOSISI_SURAT","A.JAM_OUT","A.NOTE_SURAT"));	
		
		
		}
				
				
				
				
		$this->newtable->use_ajax(FALSE);
		$this->newtable->js_file('<script type="text/javascript" src="'.base_url().'assets/js/newtable.js"></script>');
		$tabel = $this->newtable->generate($query);
		if($ispreview){
			$tabel = "<script type=\"text/javascript\" src=\"".base_url()."assets/js/newtable.js\"></script>".$tabel;
			$arrdata = $tabel;
		}else{
			$arrdata = array("htmltable" => $tabel,
							 "judul" => $judul
							 );
		}
		return $arrdata;
					
			}
		redirect(base_url());
		exit();		
	}
	
	function list_cms($id="", $ispreview=FALSE){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
				
			if($this->session->userdata('LOGGED')){
				
				$this->load->library('newtable');
			
					$this->newtable->hiddens(array('CMS_ID'));
					$this->newtable->orderby(1);
				
					$this->newtable->keys(array("CMS_ID"));
					$this->newtable->sortby('DESC');
				
					
					$this->newtable->action(site_url()."/home/dokumen/listCMS/".$id);
					$this->newtable->cidb($this->db);
					$this->newtable->ciuri($this->uri->segment_array());
				
				
				if($ispreview){
					$this->newtable->rowcount("ALL");
					$this->newtable->show_chk(FALSE);
					$this->newtable->show_search(FALSE);
				}
				
				#if($this->session->userdata('USER_ROLE')=="01" || $this->session->userdata('USER_ID')=="49"){
				#	$addwhere = '';
				#}else{
				#	$addwhere = "AND A.USER_CREATE = '".$this->session->userdata('USER_ID')."'";
				#}
				
				if($id=="CMSnewSyr"){
					$judul = "List CMS Syariah";
					$this->newtable->hiddens(array('CMS_ID'));		
					$this->newtable->search(array(array('A.DESKRIPSI_CMS', 'DESKRIPSI'),array('A.KETERANGAN_CMS','KETERANGAN'), 
					array('A.NOREK_CMS', 'NOMOR REKENING'), array('A.TANGGAL_CMS', 'TANGGAL CMS'), array('A.BANK_CMS', 'BANK')));
					
					$prosesnya = array(
										'Fix CMS' => array('GET', site_url().'/home/dokumen/SetFixCMS', '1'),
										'Cancled/Hide' => array('POST', site_url().'/home/registrasi_act/hidecms/ajax', 'N')
								
									);
					
					$this->newtable->menu($prosesnya);
					
					$query = "SELECT A.CMS_ID, CONCAT(A.TANGGAL_CMS,'<div>', A.JAM_CMS,'</div>') AS TANGGAL, A.DESKRIPSI_CMS AS DESKRIPSI, 
							  A.DEBIT_CMS AS DEBIT, A.KREDIT_CMS AS KREDIT, A.SALDO_CMS AS SALDO, 
							  A.KETERANGAN_CMS AS KETERANGAN, CONCAT(A.NOREK_CMS,'<div>', IFNULL(A.BANK_CMS,'-'), '</div>') AS 'NOMOR REKENING',
							  CONCAT(B.USER_FULLNAME,'<div>', A.DATE_UPLOAD,'</div>') AS 'NAMA/TGL UPLOAD' 
								 FROM tx_cms A
							  LEFT JOIN tm_user B ON B.USER_ID = A.USER_UPLOAD
							  WHERE A.KETERANGAN_CMS = 'IJK' AND A.STATUS = '5'";
								
					$this->newtable->columns(array("A.CMS_ID","CONCAT(A.TANGGAL_CMS,'<div>', A.JAM_CMS,'</div>')","A.DESKRIPSI_CMS", "A.DEBIT_CMS", 
					"A.KREDIT_CMS", "A.KREDIT_CMS","A.SALDO_CMS","A.KETERANGAN_CMS","CONCAT(A.NOREK_CMS,'<div>', A.BANK_CMS, '</div>')",
					"CONCAT(B.USER_FULLNAME, '<div>', A.DATE_UPLOAD,'</div>')"));				
				}else if($id =="CMS_fix"){
					$judul = "List CMS Fix Syariah";
					$this->newtable->hiddens(array('CMS_ID'));		
					$this->newtable->search(array(array('A.DESKRIPSI_CMS', 'DESKRIPSI'),array('A.KETERANGAN_CMS','KETERANGAN'), 
					array('A.NOREK_CMS', 'NOMOR REKENING'), array('A.TANGGAL_CMS', 'TANGGAL CMS'), array('A.BANK_CMS', 'BANK')));
					
					$prosesnya = array(
									'Cancled/Hide' => array('POST', site_url().'/home/registrasi_act/hidecms/ajax', 'N')
								
									);
					
					$this->newtable->menu($prosesnya);
					
					$query = "SELECT A.CMS_ID, CONCAT(A.TANGGAL_CMS,'<div>', A.JAM_CMS,'</div>') AS TANGGAL, A.DESKRIPSI_CMS AS DESKRIPSI, 
							  A.DEBIT_CMS AS DEBIT, A.KREDIT_CMS AS KREDIT, CONCAT(A.CABANG_REKENING,'<div>', A.CABANG_PENERBITAN,'</div>') AS 'CABANG REKENING & PENERBITAN', 
							  A.KETERANGAN_CMS AS KETERANGAN, A.LOAN_NUMBER AS 'LOAN NUMBER',
							  CONCAT(B.USER_FULLNAME,'<div>', A.DATE_UPLOAD,'</div>') AS 'NAMA/TGL UPLOAD' 
								 FROM tx_cms A
							  LEFT JOIN tm_user B ON B.USER_ID = A.USER_UPLOAD
							  WHERE A.KETERANGAN_CMS = 'IJK' AND A.STATUS = '1'";
								
					$this->newtable->columns(array("A.CMS_ID","CONCAT(A.TANGGAL_CMS,'<div>', A.JAM_CMS,'</div>')","A.DESKRIPSI_CMS", "A.DEBIT_CMS", 
					"A.KREDIT_CMS", "A.KREDIT_CMS","A.SALDO_CMS","A.KETERANGAN_CMS","CONCAT(A.NOREK_CMS,'<div>', A.BANK_CMS, '</div>')",
					"CONCAT(B.USER_FULLNAME, '<div>', A.DATE_UPLOAD,'</div>')"));	
				}
				
				
				
				
				$this->newtable->use_ajax(FALSE);
				$this->newtable->js_file('<script type="text/javascript" src="'.base_url().'assets/js/newtable.js"></script>');
				$tabel = $this->newtable->generate($query);
				if($ispreview){
					$tabel = "<script type=\"text/javascript\" src=\"".base_url()."assets/js/newtable.js\"></script>".$tabel;
					$arrdata = $tabel;
				}else{
					$arrdata = array("htmltable" => $tabel,
									 "judul" => $judul
									 );
				}
				return $arrdata;
					
			}
		redirect(base_url());
		exit();		
	}
	
	
	function list_nominatif($id="", $ispreview=FALSE){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
				
			if($this->session->userdata('LOGGED')){
				
				$this->load->library('newtable');
			
					$this->newtable->hiddens(array('LOAN_NUMBER'));
					$this->newtable->orderby(1);
				
					$this->newtable->keys(array("LOAN_NUMBER"));
					$this->newtable->sortby('DESC');
				
					
					$this->newtable->action(site_url()."/home/dokumen/listNominatif/".$id);
					$this->newtable->cidb($this->db);
					$this->newtable->ciuri($this->uri->segment_array());
				
				
				if($ispreview){
					$this->newtable->rowcount("ALL");
					$this->newtable->show_chk(FALSE);
					$this->newtable->show_search(FALSE);
				}
				
				
				if($id=="NominatifNEW"){
					$judul = "List Nominatif";
					$this->newtable->hiddens(array('CMS_ID'));		
					$this->newtable->search(array(array('A.NAMA_DEBITUR', 'NAMA DEBITUR'),array('A.LOAN_NUMBER', 'LOAN NUMBER'),array('A.ALAMAT_DEBITUR','ALAMAT'), 
					array('A.INSTANSI', 'INSTANSI'), array('A.NIK', 'NIK')));
					
					$prosesnya = array(
										'Fix CMS' => array('GET', site_url().'/home/dokumen/SetFixCMS', '1'),
										'Cancled/Hide' => array('POST', site_url().'/home/registrasi_act/hidecms/ajax', 'N')
								
									);
					
					$this->newtable->menu($prosesnya);
					
					$query = "SELECT A.LOAN_NUMBER, CONCAT(A.NAMA_DEBITUR,'<div> Loan Number : ', A.LOAN_NUMBER,'</div>') AS 'NAMA DEBITUR', 
							  A.NIK, A.INSTANSI, A.ALAMAT_DEBITUR AS 'ALAMAT DEBITUR', A.PEMBIAYAAN, FORMAT(A.NILAI_KAFALAH, 2,'de_DE') AS 'NILAI KAFALAH' 
									FROM tx_nominatif A";
								
					$this->newtable->columns(array("A.LOAN_NUMBER","CONCAT(A.NAMA_DEBITUR,'<div> Loan Number : ', A.LOAN_NUMBER,'</div>')","A.NIK", "A.INSTANSI", 
					"A.ALAMAT_DEBITUR", "A.PEMBIAYAAN","FORMAT(A.NILAI_KAFALAH, 2,'de_DE')"));				
				}
				
				
				
				
				$this->newtable->use_ajax(FALSE);
				$this->newtable->js_file('<script type="text/javascript" src="'.base_url().'assets/js/newtable.js"></script>');
				$tabel = $this->newtable->generate($query);
				if($ispreview){
					$tabel = "<script type=\"text/javascript\" src=\"".base_url()."assets/js/newtable.js\"></script>".$tabel;
					$arrdata = $tabel;
				}else{
					$arrdata = array("htmltable" => $tabel,
									 "judul" => $judul
									 );
				}
				return $arrdata;
					
			}
		redirect(base_url());
		exit();		
	}
	
	
	function list_upload_kendaraan($id="", $ispreview=FALSE){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			
			if($this->session->userdata('LOGGED')){
				
				$this->load->library('newtable');
				$this->newtable->columns(array("A.DOC_ID","A.REG_ID", "B.KETERANGAN", "A.JARAK","CONCAT('<a class=\" btn btn-small btn-mini btn-primary\" href=\"".base_url()."','index.php/download/files_kendaraan/',A.ID_DEPOSITO,'/',A.DOC_ID,'/',REPLACE(A.PATH_FILE,CONCAT('dat/kendaraan/',DATE_FORMAT(A.DATE_UPLOAD,'%Y%m%d'),'/'), ''),'\" target=\"_blank\">
						  <span class=\"icon-search\"></span> Preview</a>')"));		  
				$this->newtable->hiddens(array('DOC_ID'));
				$this->newtable->search(array(array('A.KETERANGAN', 'KETERANGAN')));
				$this->newtable->action(site_url()."/home/deposito/input_periode/list/".$id);
				$this->newtable->cidb($this->db);
				$this->newtable->ciuri($this->uri->segment_array());
				$this->newtable->orderby(4);
				$this->newtable->sortby('DESC');
				$this->newtable->keys(array("DOC_ID"));
				if($ispreview){
					$this->newtable->rowcount("ALL");
					$this->newtable->show_chk(FALSE);
					$this->newtable->show_search(FALSE);
				}
				$prosesnya = array(
								   'Hapus' => array('POST', site_url().'/home/registrasi_act/deletefile/ajax', 'N')
								   );
				$this->newtable->menu($prosesnya);
				
				$arrid = explode('.', $id);
				
				$query= "SELECT A.DOC_ID, A.REG_ID AS 'REG ID',  A.KETERANGAN, A.JARAK AS 'JARAK',
						CONCAT('<a  href=\"".base_url()."','index.php/download/files_kendaraan/',A.REG_ID,'/',A.DOC_ID,'/',REPLACE(A.PATH_FILE,CONCAT('dat/kendaraan/',DATE_FORMAT(A.DATE_UPLOAD,'%Y%m%d'),'/'), ''),'\" target=\"_blank\">
						  <span class=\"icon-search\"></span> Preview</a>') AS 'FILE'
							FROM tx_kendaraan_doc A WHERE  A.REG_ID = '".$arrid[0]."' ";
						
				$judul = "List Dokumen";
				$this->newtable->use_ajax(FALSE);
				$this->newtable->js_file('<script type="text/javascript" src="'.base_url().'assets/js/newtable.js"></script>');
				$tabel = $this->newtable->generate($query);
				if($ispreview){
					$tabel = "<script type=\"text/javascript\" src=\"".base_url()."assets/js/newtable.js\"></script>".$tabel;
					$arrdata = $tabel;
				}else{
					$arrdata = array("htmltable" => $tabel,
									 "judul" => $judul
									 );
				}
				return $arrdata;
					
			}
		redirect(base_url());
		exit();		
	}
	
	function list_periode_deposito($id="", $ispreview=FALSE){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			
			if($this->session->userdata('LOGGED')){
				
				$this->load->library('newtable');
				$this->newtable->columns(array("A.PERIODE_ID","A.ID_DEPOSITO", "A.MULAI_TEMPO", "B.JATUH_TEMPO", "A.KETERANGAN","C.DATE_BEGIN","C.DATE_END","C.DAY"));		  
				$this->newtable->hiddens(array('PERIODE_ID', 'ID_DEPOSITO'));
				$this->newtable->search(array(array('A.MULAI_TEMPO', 'MULAI TEMPO'), array('A.JATUH_TEMPO', 'JATUH TEMPO'), array('A.TAX', 'TAX')));
				$this->newtable->action(site_url()."/home/deposito/input_periode/list/".$id);
				$this->newtable->cidb($this->db);
				$this->newtable->ciuri($this->uri->segment_array());
				$this->newtable->orderby(4);
				$this->newtable->sortby('DESC');
				$this->newtable->keys(array("PERIODE_ID"));
				if($ispreview){
					$this->newtable->rowcount("ALL");
					$this->newtable->show_chk(FALSE);
					$this->newtable->show_search(FALSE);
				}
				$prosesnya = array('Ubah' => array('GET', site_url().'/home/deposito/input_periode/edit/'. $id, '1'),
								   'Hapus' => array('POST', site_url().'/home/registrasi_act/delete/ajax', 'N')
								   );
				$this->newtable->menu($prosesnya);
				
				$arrid = explode('.', $id);
				
				$month = date('m');
				$year = date('Y');
				
				$query= "SELECT A.PERIODE_ID, A.ID_DEPOSITO, A.MULAI_TEMPO AS 'MULAI TEMPO', A.JATUH_TEMPO AS 'JATUH TEMPO', A.KETERANGAN, 
					     CONCAT(A.TAX,'%') TAX, 
						 CONCAT('Rp. ',A.INTEREST_RECEIVED_AMOUNT,'<div>',A.INTEREST_RECEIVED_DATE,'</div>') AS 'INTEREST RECEIVED',
						 CONCAT('Rp. ',A.DIVESTMENT_AMOUNT,'<div>',A.DIVESTMENT_DATE,'</div>') AS 'DIVESTMENT'
							FROM tx_periode_deposito A
						 LEFT JOIN tx_calculation_date B ON B.ID_DEPOSITO = A.ID_DEPOSITO AND MONTH(B.DATE_BEGIN) = $month AND YEAR(B.DATE_BEGIN) = $year	
						 WHERE A.ID_DEPOSITO = '".$arrid[0]."' ";
								
				$judul = "Daftar Periode Deposito";
				$this->newtable->use_ajax(FALSE);
				$this->newtable->js_file('<script type="text/javascript" src="'.base_url().'js/newtable.js"></script>');
				$tabel = $this->newtable->generate($query);
				if($ispreview){
					$tabel = "<script type=\"text/javascript\" src=\"".base_url()."js/newtable.js\"></script>".$tabel;
					$arrdata = $tabel;
				}else{
					$arrdata = array("htmltable" => $tabel);
				}
				return $arrdata;
					
			}
		redirect(base_url());
		exit();		
	}
	
	
	function list_detil_periode_deposito($id="", $ispreview=FALSE){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			
			if($this->session->userdata('LOGGED')){
				
				$this->load->library('newtable');
				$this->newtable->columns(array("A.SERI","A.ID_DEPOSITO", "A.KETERANGAN","A.DATE_BEGIN","A.DATE_END","A.DAY"));		  
				$this->newtable->hiddens(array('SERI', 'ID_DEPOSITO'));
				$this->newtable->search(array(array('A.MULAI_TEMPO', 'MULAI TEMPO'), array('A.JATUH_TEMPO', 'JATUH TEMPO'), array('A.TAX', 'TAX')));
				$this->newtable->action(site_url()."/home/deposito/input_periode/list/".$id);
				$this->newtable->cidb($this->db);
				$this->newtable->ciuri($this->uri->segment_array());
				$this->newtable->orderby(1);
				$this->newtable->sortby('ASC');
				$this->newtable->keys(array("SERI", "ID_DEPOSITO"));
				if($ispreview){
					$this->newtable->rowcount("ALL");
					$this->newtable->show_chk(FALSE);
					$this->newtable->show_search(FALSE);
				}
				$prosesnya = array('Ubah Interest Received' => array('GET', site_url()."/home/deposito/set_periode_detil/list", '1','edit-btn.png')
								   );
				$this->newtable->menu($prosesnya);
				
				$arrid = explode('.', $id);
				
				$month = date('m');
				$year = date('Y');
				
				$query= "SELECT DISTINCT A.SERI, A.ID_DEPOSITO, A.DATE_BEGIN AS 'BEGIN', A.DATE_END AS 'END', B.KETERANGAN, 
					     CONCAT(A.TAX,'%') TAX, CONCAT(A.DAY,' Hari') JUMLAH,
						 CONCAT('Rp. ',A.INTEREST_RECEIVED_AMOUNT,'<div>',A.INTEREST_RECEIVED_DATE,'</div>') AS 'INTEREST RECEIVED',
						 CONCAT('Rp. ',A.DIVESTMENT_AMOUNT,'<div>',A.DIVESTMENT_DATE,'</div>') AS 'DIVESTMENT'
							FROM tx_calculation_date A
						 LEFT JOIN tx_periode_deposito B ON B.ID_DEPOSITO = A.ID_DEPOSITO 	
						    WHERE A.ID_DEPOSITO = '".$arrid[0]."' ";
								
				$judul = "Daftar Detil Periode Deposito";
				$this->newtable->use_ajax(FALSE);
				$this->newtable->js_file('<script type="text/javascript" src="'.base_url().'js/newtable.js"></script>');
				$tabel = $this->newtable->generate($query);
				if($ispreview){
					$tabel = "<script type=\"text/javascript\" src=\"".base_url()."js/newtable.js\"></script>".$tabel;
					$arrdata = $tabel;
				}else{
					$arrdata = array("htmltable" => $tabel,
									"judul" => $judul);
				}
				return $arrdata;
					
			}
		redirect(base_url());
		exit();		
	}
	
		
	
	function set_laporan($param = ""){
		$cbs =& get_instance();
		$cbs->load->model("main", "main", true);
		
		
		$jenis = 'D.'.$this->input->post('JENIS');		
		
		$bulan = $this->input->post('BULAN');
		
		$tahun = $this->input->post('TAHUN');
		
		$query = "SELECT A.ID_DEPOSITO, A.INVESTEE, A.NOMOR_DEPOSITO, CONCAT('Rp.', A.PENEMPATAN) AS PENEMPATAN, 
				A.JML_JANGKA_WAKTU, DATE_FORMAT(A.TANGGAL_PENEMPATAN,'%d/%m/%Y') AS 'TANGGAL_PENEMPATAN',
				CONCAT(D.BUNGA,'%') AS BUNGA,
				DATE_FORMAT(D.MULAI_TEMPO,'%d/%m/%Y') AS MULAI_TEMPO, DATE_FORMAT(D.JATUH_TEMPO,'%d/%m/%Y') JATUH_TEMPO,
				DATE_FORMAT(D.DATE_BEGIN,'%d/%m/%Y') DATE_BEGIN, DATE_FORMAT(D.DATE_END,'%d/%m/%Y') DATE_END,
				CONCAT(B.REFF_DESC, ' ', C.REFF_DESC) AS JANGKA_WAKTU, D.TAX, D.DAY,
				DATE_FORMAT(D.DATE_BEGIN, '%Y-%c-%e') BEGIN, DATE_FORMAT(D.DATE_END, '%Y-%c-%e') ENDS, 
				D.INTEREST_RECEIVED_DATE, D.INTEREST_RECEIVED_AMOUNT, D.DIVESTMENT_DATE, D.DIVESTMENT_AMOUNT, 
				A.PENEMPATAN_TERBILANG, D.INTEREST_RECEIVED_AMOUNT_TERBILANG, D.KETERANGAN 		
					FROM tx_header_deposito A
				LEFT JOIN tb_reff B ON B.REFF_CODE = A.TYPE_JANGKA_WAKTU AND B.REFF_TAB = 'JANGKA_WAKTU'
				LEFT JOIN tb_reff C ON C.REFF_CODE = A.ARO_JANGKA_WAKTU AND C.REFF_TAB = 'ARO'
				LEFT JOIN tx_periode_deposito D ON D.ID_DEPOSITO = A.ID_DEPOSITO 
					WHERE MONTH($jenis) = $bulan AND YEAR($jenis) = $tahun";
		
		$data = $cbs->main->get_result($query);	
		if($data){
			foreach ($query->result_array() as $row) {
				$sess[] = $row;
				
				$arrdata = array(
							'sess'=> $sess
							);
			} 
			
		}
		return $arrdata;
	
	}
	
	function get_dokupload($id=""){
		$cbs =& get_instance();
		$cbs->load->model("main", "main", true);
		if($this->session->userdata('LOGGED')){
		$arrid = explode('.', $id);
		$act = site_url().'/home/registrasi_act/saveupload';
		$iddeposito = $arrid[0];
		$arrdata = array('act' => $act,
						 'iddeposito' => $iddeposito
						);
		return $arrdata;
		}	
		
	}
	
	function get_cms($id=""){
		$cbs =& get_instance();
		$cbs->load->model("main", "main", true);
		if($this->session->userdata('LOGGED')){
		$sts = $cbs->main->get_combobox("SELECT REFF_CODE, REFF_DESC FROM tb_reff WHERE REFF_TAB = 'STATUS_CMS'","REFF_CODE","REFF_DESC", TRUE);
		$act = site_url().'/home/registrasi_act/saveupload';
		
		$query= "SELECT * FROM tx_cms WHERE CMS_ID = '".$id."' ";
		$isi = $this->db->query($query)->row_array();
		$loan_number = preg_replace( '/[^\d]/', '', $isi['DESKRIPSI_CMS']);
		
		$arrdata = array('act' => $act,
						 'sts' => $sts,
						 'isi' => $isi,
						 'loan_number' => $loan_number,
						 'cmsid' => $id
						);
		return $arrdata;
		}	
		
	}
	
	function list_permohonan_new($menu=""){
		$cbs =& get_instance();
		$cbs->load->model("main", "main", true);
		
		$this->load->library('newtable30');
		$table = $this->newtable30;
		
		$query = "SELECT ID_DEPOSITO, NOMOR_DEPOSITO, INVESTEE, PENEMPATAN_TERBILANG, MULAI_TEMPO, JATUH_TEMPO FROM tx_header_deposito";
		
		$table->action(site_url()."/home/dokumen_new/list/proses");
		
		$table->columns(array("ID_DEPOSITO", "NOMOR_DEPOSITO",  "INVESTEE", "INVESTEEINVESTEE", "MULAI_TEMPO","JATUH_TEMPO"));
		
		$table->width(array('ID_DEPOSITO' => 400, 'NOMOR_DEPOSITO' => 150, 'INVESTEE' => 150, 'INVESTEE' => 150));
		
		$table->language("ID");
		
		$table->search(array(array('NOMOR_DEPOSITO', 'NOMOR DEPOSITO'), array('INVESTEE',' NAMA BANK')));
		
		
		$table->detail(site_url()."/home/action/detil");
		
		/* Inisialisasi Obyek Database */
		$table->cidb($this->db);
		
		/* Inisialisasi Obyek URL */
		$table->ciuri($this->uri->segment_array());
		
		/* Default Order Saat Halaman Web Diakses. Diisi Dengan Nomor Urut Kolom */
		$table->orderby(3);
		
		/* Default Sort Saat Halaman Web Diakses. Diisi Dengan ASC Atau DESC */
		$table->sortby("DESC");
		
		
		$table->keys(array('help_topic_id', 'NAMA'));
		
		$table->show_chk(TRUE);
		
		$table->use_ajax(TRUE);
		
		$table->js_file('<script type="text/javascript" src="'.base_url().'assets/js/newtable.js"></script>');
		
		$table->show_search(TRUE);
		
		
		$table->menu(array('Tambah' => array('GET', site_url()."/home/action/new", '0', 'home'),
						   'Ubah' => array('GET', site_url()."/home/action/edit", '1', 'sub-edit.png'),
						   'Hapus' => array('POST', site_url()."/home/action/hapus", 'N', 'sub-delete.png'),
						   'Unduh Template' => array('GETNEW', site_url()."/home/action/template", '0'),
						   'Print' => array('GETNEW', site_url()."/home/action/print", '1')));
		
		/* Generate Tabel */
		$view = $table->generate($query);
		$arrdata = array(
							 "htmltable" => $view
						 );
			//print_r($arrdata);
			return $arrdata;
		
		
	}
	
	function list_permohonan($doc=""){
		$cbs =& get_instance();
		$cbs->load->model("main", "main", true);
		#if($this->session->userdata('LOGGED')){
			$this->load->library('newtable');
			$this->newtable->action(site_url()."/home/dokumen/list/$doc");
			$this->newtable->orderby(3);		
			$this->newtable->sortby("ASC");	
			
			 if($doc =="proses"){
				$_SESSION['menu'] = '02proses_';
				$judul = "List Permohonan Deposito";
				if($this->session->userdata('USER_ROLE')=='04'){
				$prosesnya = array(
								   'Interest Received' => array('GET', site_url()."/home/deposito/input_periode_detil/list", '1','edit-btn.png')
								   );	
					
				}else{
				$prosesnya = array('Edit' => array('GET', site_url()."/home/deposito/edit", '1'),
								   'Periode Deposito' => array('GET', site_url()."/home/deposito/input_periode/list", '1')
								   #'Print Draft Surat' => array('GETNEW', site_url()."/prints/surat_permohonan", 'N')
								   );
				}
				$this->newtable->detail(site_url()."/home/deposito/detil");
				
				$query = "SELECT A.ID_DEPOSITO, A.NOMOR_DEPOSITO, CONCAT(A.ID_DEPOSITO,'<div>', IFNULL(A.DATE_INSERT,'-'),'</div>') AS 'DEPOSITO ID', CONCAT(A.INVESTEE,' - ',D.REFF_DESC,'<div>',A.NOMOR_DEPOSITO,'</div>') AS 'INVESTEE DAN NOMOR DEPOSITO' , 
						  CONCAT('Rp.', A.PENEMPATAN) AS PENEMPATAN, CONCAT(A.BUNGA,'%') AS BUNGA, CONCAT(A.JML_JANGKA_WAKTU, ' ',B.REFF_DESC, ' ', C.REFF_DESC) AS 'JANGKA WAKTU', 
						  DATE_FORMAT(A.TANGGAL_PENEMPATAN,'%d-%m-%Y') AS 'TANGGAL PENEMPATAN', DATE_FORMAT(A.MULAI_TEMPO,'%d-%m-%Y') AS 'MULAI TEMPO', 
						  DATE_FORMAT(A.JATUH_TEMPO,'%d-%m-%Y') AS 'JATUH TEMPO', CONCAT(A.TAX,'%') AS TAX		
								FROM tx_header_deposito A
						  LEFT JOIN tb_reff B ON B.REFF_CODE = A.TYPE_JANGKA_WAKTU AND B.REFF_TAB = 'JANGKA_WAKTU'
						  LEFT JOIN tb_reff C ON C.REFF_CODE = A.ARO_JANGKA_WAKTU AND C.REFF_TAB = 'ARO'
						  LEFT JOIN tb_reff D ON D.REFF_CODE = A.INVESTEE_TYPE AND D.REFF_TAB = 'INVESTEE_TYPE'
								WHERE DATEDIFF(A.JATUH_TEMPO, NOW()) > 0";
				
				$this->newtable->columns(array( "A.ID_DEPOSITO","A.NOMOR_DEPOSITO", "CONCAT(A.ID_DEPOSITO,'<div>', A.DATE_INSERT,'</div>')", "CONCAT(A.INVESTEE,'-',D.REFF_DESC,'<div>',A.NOMOR_DEPOSITO,'</div>')","CONCAT('Rp.', A.PENEMPATAN)",
											    "CONCAT(A.JML_JANGKA_WAKTU, ' ',B.REFF_DESC, ' ', C.REFF_DESC)","DATE_FORMAT(A.TANGGAL_PENEMPATAN,'%d-%m-%Y')","DATE_FORMAT(A.MULAI_TEMPO,'%d-%m-%Y')",
												"DATE_FORMAT(A.JATUH_TEMPO,'%d-%m-%Y')","CONCAT(A.TAX,'%')","A.DAY"));
				$this->newtable->hiddens("ID_DEPOSITO");
			}else if($doc =="jatuh_tempo"){
				$_SESSION['menu'] = '02jatuhtempo_';
				
				$judul = "List Jatuh Tempo";
				if($this->session->userdata('USER_ROLE')=='04'){
				$prosesnya = array(
								   'Interest Received' => array('GET', site_url()."/home/deposito/input_periode_detil/list", '1','edit-btn.png')
								   );	
					
				}else{
				$prosesnya = array('Edit' => array('GET', site_url()."/home/deposito/edit", '1'),
								   'Periode Deposito' => array('GET', site_url()."/home/deposito/input_periode/list", '1')
								   );
				}
				
				
				$query = "SELECT A.ID_DEPOSITO, A.NOMOR_DEPOSITO, CONCAT(A.ID_DEPOSITO,'<div>', IFNULL(A.DATE_INSERT,'-'),'</div>') AS 'DEPOSITO ID', CONCAT(A.INVESTEE,' - ',D.REFF_DESC,'<div>',A.NOMOR_DEPOSITO,'</div>') AS 'INVESTEE DAN NOMOR DEPOSITO' , 
						  CONCAT('Rp.', A.PENEMPATAN) AS PENEMPATAN, CONCAT(A.BUNGA,'%') AS BUNGA, CONCAT(A.JML_JANGKA_WAKTU, ' ',B.REFF_DESC, ' ', C.REFF_DESC) AS 'JANGKA WAKTU', 
						  DATE_FORMAT(A.TANGGAL_PENEMPATAN,'%d-%m-%Y') AS 'TANGGAL PENEMPATAN', DATE_FORMAT(A.MULAI_TEMPO,'%d-%m-%Y') AS 'MULAI TEMPO', 
						  DATE_FORMAT(A.JATUH_TEMPO,'%d-%m-%Y') AS 'JATUH TEMPO', CONCAT(A.TAX,'%') AS TAX		
								FROM tx_header_deposito A
						  LEFT JOIN tb_reff B ON B.REFF_CODE = A.TYPE_JANGKA_WAKTU AND B.REFF_TAB = 'JANGKA_WAKTU'
						  LEFT JOIN tb_reff C ON C.REFF_CODE = A.ARO_JANGKA_WAKTU AND C.REFF_TAB = 'ARO'
						   LEFT JOIN tb_reff D ON D.REFF_CODE = A.INVESTEE_TYPE AND D.REFF_TAB = 'INVESTEE_TYPE'
								WHERE DATEDIFF(A.JATUH_TEMPO, NOW()) < 7 AND DATEDIFF(A.JATUH_TEMPO, NOW()) >= 0 ";
								
							
				
				$this->newtable->columns(array( "A.ID_DEPOSITO","A.NOMOR_DEPOSITO", "CONCAT(A.ID_DEPOSITO,'<div>', A.DATE_INSERT,'</div>')", "CONCAT(A.INVESTEE,' - ',D.REFF_DESC,'<div>',A.NOMOR_DEPOSITO,'</div>')","CONCAT('Rp.', A.PENEMPATAN)",
											    "CONCAT(A.JML_JANGKA_WAKTU, ' ',B.REFF_DESC, ' ', C.REFF_DESC)","DATE_FORMAT(A.TANGGAL_PENEMPATAN,'%d-%m-%Y')","DATE_FORMAT(A.MULAI_TEMPO,'%d-%m-%Y')",
												"DATE_FORMAT(A.JATUH_TEMPO,'%d-%m-%Y')","CONCAT(A.TAX,'%')","A.DAY"));
				$this->newtable->hiddens("ID_DEPOSITO");
			}else if($doc =="expired"){
				$_SESSION['menu'] = '02expired_';
				
				$judul = "List Expired";
				if($this->session->userdata('USER_ROLE')=='04'){
				$prosesnya = array(
								   'Interest Received' => array('GET', site_url()."/home/deposito/input_periode_detil/list", '1','edit-btn.png')
								   );	
					
				}else{
				$prosesnya = array('Edit' => array('GET', site_url()."/home/deposito/edit", '1'),
								   'Periode Deposito' => array('GET', site_url()."/home/deposito/input_periode/list", '1')
								   );
				}
				
				$this->newtable->detail(site_url()."/home/deposito/detil");
				$query = "SELECT A.ID_DEPOSITO, A.NOMOR_DEPOSITO, CONCAT(A.ID_DEPOSITO,'<div>', IFNULL(A.DATE_INSERT,'-'),'</div>') AS 'DEPOSITO ID', CONCAT(A.INVESTEE,' - ',D.REFF_DESC,'<div>',A.NOMOR_DEPOSITO,'</div>') AS 'INVESTEE DAN NOMOR DEPOSITO' , 
						  CONCAT('Rp.', A.PENEMPATAN) AS PENEMPATAN, CONCAT(A.BUNGA,'%') AS BUNGA, CONCAT(A.JML_JANGKA_WAKTU, ' ',B.REFF_DESC, ' ', C.REFF_DESC) AS 'JANGKA WAKTU', 
						  DATE_FORMAT(A.TANGGAL_PENEMPATAN,'%d-%m-%Y') AS 'TANGGAL PENEMPATAN', DATE_FORMAT(A.MULAI_TEMPO,'%d-%m-%Y') AS 'MULAI TEMPO', 
						  DATE_FORMAT(A.JATUH_TEMPO,'%d-%m-%Y') AS 'JATUH TEMPO', CONCAT(A.TAX,'%') AS TAX		
								FROM tx_header_deposito A
						  LEFT JOIN tb_reff B ON B.REFF_CODE = A.TYPE_JANGKA_WAKTU AND B.REFF_TAB = 'JANGKA_WAKTU'
						  LEFT JOIN tb_reff C ON C.REFF_CODE = A.ARO_JANGKA_WAKTU AND C.REFF_TAB = 'ARO'
						   LEFT JOIN tb_reff D ON D.REFF_CODE = A.INVESTEE_TYPE AND D.REFF_TAB = 'INVESTEE_TYPE'
								WHERE DATEDIFF(A.JATUH_TEMPO, NOW()) < 0";
								
							
				
				$this->newtable->columns(array( "A.ID_DEPOSITO","A.NOMOR_DEPOSITO", "CONCAT(A.ID_DEPOSITO,'<div>', A.DATE_INSERT,'</div>')", "CONCAT(A.INVESTEE,' - ', D.REFF_DESC,'<div>',A.NOMOR_DEPOSITO,'</div>')","CONCAT('Rp.', A.PENEMPATAN)",
											    "CONCAT(A.JML_JANGKA_WAKTU, ' ',B.REFF_DESC, ' ', C.REFF_DESC)","DATE_FORMAT(A.TANGGAL_PENEMPATAN,'%d-%m-%Y')","DATE_FORMAT(A.MULAI_TEMPO,'%d-%m-%Y')",
												"DATE_FORMAT(A.JATUH_TEMPO,'%d-%m-%Y')","CONCAT(A.TAX,'%')","A.DAY"));
				$this->newtable->hiddens("ID_DEPOSITO");
			}else if($doc =="all"){
				$_SESSION['menu'] = '02all_';
				
				$judul = "List Expired";
				if($this->session->userdata('USER_ROLE')=='04'){
				$prosesnya = array(
								   'Interest Received' => array('GET', site_url()."/home/deposito/input_periode_detil/list", '1','edit-btn.png')
								   );	
					
				}else{
				$prosesnya = array('Preview' => array('GET', site_url()."/home/deposito/preview", '1','btn-preview.png'),
								   'Edit' => array('GET', site_url()."/home/deposito/edit", '1'),
								   'Periode Deposito' => array('GET', site_url()."/home/deposito/input_periode/list", '1','edit-btn.png'),
								   'Interest Received' => array('GET', site_url()."/home/deposito/input_periode_detil/list", '1','edit-btn.png'),
								   'Upload Dokumen Pendukung' =>  array('GET', site_url()."/home/deposito/dokumen_upload", '1')
								   );
				}
				
				$this->newtable->detail(site_url()."/home/deposito/detil");
				$query = "SELECT A.ID_DEPOSITO, A.NOMOR_DEPOSITO, CONCAT(A.ID_DEPOSITO,'<div>', IFNULL(A.DATE_INSERT,'-'),'</div>') AS 'DEPOSITO ID', 
						  A.INVESTEE AS 'INVESTEE' , A.NOMOR_DEPOSITO AS 'NOMOR DEPOSITO',
						  CONCAT('Rp.', A.PENEMPATAN) AS PENEMPATAN, CONCAT(A.BUNGA,'%') AS BUNGA, 
						  CONCAT(A.JML_JANGKA_WAKTU, ' ',B.REFF_DESC, ' ', C.REFF_DESC) AS 'JANGKA WAKTU', 
						  DATE_FORMAT(A.TANGGAL_PENEMPATAN,'%d-%m-%Y') AS 'TANGGAL PENEMPATAN', 
						  DATE_FORMAT(A.MULAI_TEMPO,'%d-%m-%Y') AS 'MULAI TEMPO', 
						  DATE_FORMAT(A.JATUH_TEMPO,'%d-%m-%Y') AS 'JATUH TEMPO', CONCAT(A.TAX,'%') AS TAX		
								FROM tx_header_deposito A
						  LEFT JOIN tb_reff B ON B.REFF_CODE = A.TYPE_JANGKA_WAKTU AND B.REFF_TAB = 'JANGKA_WAKTU'
						  LEFT JOIN tb_reff C ON C.REFF_CODE = A.ARO_JANGKA_WAKTU AND C.REFF_TAB = 'ARO'
						  LEFT JOIN tb_reff D ON D.REFF_CODE = A.INVESTEE_TYPE AND D.REFF_TAB = 'INVESTEE_TYPE'";
								
							
				
				$this->newtable->columns(array( "A.ID_DEPOSITO","A.NOMOR_DEPOSITO", "CONCAT(A.ID_DEPOSITO,'<div>', A.DATE_INSERT,'</div>')", "CONCAT(A.INVESTEE,'<div>',A.NOMOR_DEPOSITO,'</div>')","CONCAT('Rp.', A.PENEMPATAN)",
											    "CONCAT(A.JML_JANGKA_WAKTU, ' ',B.REFF_DESC, ' ', C.REFF_DESC)","DATE_FORMAT(A.TANGGAL_PENEMPATAN,'%d-%m-%Y')","DATE_FORMAT(A.MULAI_TEMPO,'%d-%m-%Y')",
												"DATE_FORMAT(A.JATUH_TEMPO,'%d-%m-%Y')","CONCAT(A.TAX,'%')","A.DAY"));
				$this->newtable->hiddens("ID_DEPOSITO");
			}
			
			$this->newtable->search(array(array('A.INVESTEE','NAMA BANK'),array('A.ID_DEPOSITO','DEPOSITO ID'), array('A.NOMOR_DEPOSITO','NOMOR DEPOSITO'), 
			array("CONCAT(A.JML_JANGKA_WAKTU, ' ',B.REFF_DESC, ' ', C.REFF_DESC)","JANGKA WAKTU"), array("DATE_FORMAT(A.TANGGAL_PENEMPATAN,'%d-%m-%Y')","TANGGAL PENEMPATAN"), 
			array("DATE_FORMAT(A.MULAI_TEMPO,'%d-%m-%Y')","MULAI TEMPO"), array("DATE_FORMAT(A.JATUH_TEMPO,'%d-%m-%Y')","JATUH TEMPO")));
			$this->newtable->cidb($this->db);
			$this->newtable->ciuri($this->uri->segment_array());
			$this->newtable->show_chk(TRUE);
			$this->newtable->hiddens(array("ID_DEPOSITO", "NOMOR_DEPOSITO"));
		    $this->newtable->keys(array("ID_DEPOSITO", "NOMOR_DEPOSITO"));			
			$this->newtable->menu($prosesnya);
			$this->newtable->use_ajax(FALSE);
			$this->newtable->js_file('<script type="text/javascript" src="'.base_url().'assets/js/newtable.js"></script>');
			$tabel = $this->newtable->generate($query);
			$arrdata = array("judul" => $judul,
							 "htmltable" => $tabel
							 );
			//print_r($arrdata);
			return $arrdata;
			
		#}
		
	}
	

		
}	
?>