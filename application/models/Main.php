<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Main extends CI_Model{

	/*function post_to_query($array, $except=""){
		$data = array();
		foreach($array as $a => $b){
			if(is_array($except)){
				if(!in_array($a, $except)) $data[$a] = strtoupper($b);
			}else{
				$data[$a] = ($b=='') ? NULL : strtoupper($b);
			}
		}
		return $data;
	}*/
	
	function post_to_query($array, $except = "") {
		$data = array();
		foreach ($array as $a => $b) {
		  if (is_array($except)) {
			if (!in_array($a, $except))
			  $data[$a] = $b;
		  }else {
			$data[$a] = $b;
		  }
		}
		return $data;
	}
	
	function get_result(&$query, $db=""){
			if($db=="") $db = $this->db;
			$data = $db->query($query);
			if($data->num_rows() > 0){
				$query = $data;
			}else{
				return false;
			}
			return true;
	}
	
	function get_uraian($query, $select){
		$data = $this->db->query($query);
		if($data->num_rows() > 0){
			$row = $data->row();
			return $row->$select;
		}else{
			return "";
		}
		return 1;
	}
	
	
	
	function get_tanggalind($tgl=""){
	
		$arrbulan = array("","Januari","Februari","Maret","April","Mei","Juni","Juli",
							 "Agustus","September","Oktober","November","Desember");
		
		
		
		$blnind = explode("-", $tgl);
		
		$tglind = $blnind[2].' '.$arrbulan[$blnind[1]].' '.$blnind[0];
		
		return $tglind;
	}
	
	
		function send_mail($email, $nama, $subject, $isi){
			$this->load->library('My_PHPMailer');  
			$nama_email = $nama;
			$email = explode(';', $email);
			$mail = new PHPMailer();
			$mail->IsSMTP();
			#$mail->Host = "srv42.niagahoster.com";
			$mail->Host = "smtp.gmail.com";
			$mail->SMTPAuth = true;
			$mail->Port = 465;
			$mail->SMTPSecure = "ssl";
			$mail->SMTPDebug = 0;
			$mail->Username = "it.jamkridajkt@gmail.com";
			$mail->Password = "monasmenjamin1011";
			$mail->SetFrom('it.jamkridajkt@gmail.com', 'Administrator e-Office Jamkrida');
			#$mail->AddReplyTo('aero@pom.go.id', 'Administrator Aero');
			#$mail->AddAddress($email, $nama_email);
			foreach($email as $arremail){
				$mail->AddAddress($arremail, $nama_email);
			}
			$mail->IsHTML(true);
			$mail->Subject = $subject;
			$body = '<html>
			<body style="background: #ffffff; color: #000000; font-family: arial; font-size: 13px; margin: 20px; color: #363636;">
			<table style="margin-bottom: 2px">
			<tr style="font-size: 13px; color: #0b1d90; font-weight: 700; font-family: arial;">
			<td width="41" style="margin: 0 0 6px 10px;">
			<img src="'.base_url().'assets/img/jamkrida.gif" style="vertical-align: middle;"/>
			</td>
			<td style="font-family: arial; vertical-align: middle; color: #153f6f;">
			'.$subject.'<br/>
			<span style="color: #858585; font-size: 10px; text-decoration: none;">
			e-Office
			</span>
			</td>
			</tr>
			</table>
			<div style="background-color: #D4FFD4; margin-top: 4px; margin-bottom: 10px; padding: 5px; font-family: Verdana; font-size: 11px; text-align:justify;">
			'.$isi.'
			
			</div>
			<div style="border-top: 1px solid #dcdcdc; clear: both; font-size: 11px; margin-top: 10px; padding-top: 5px;">
			<div style="font-family: arial; font-size: 10px; color: #a7aaab;">
			PT. Jamkrida Jakarta<br>Perusahaan Penjaminan Kredit Daerah Jakarta
			</div>
			<a style="text-decoration: none; font-family: arial; font-size: 10px; font-weight: normal;" href="http://jamkrida-jakarta.co.id/">jamkrida-jakarta.co.id</a>
			</div>
			</body>
			</html>';	
			$mail->Body = $body;
			return $mail->send();

		}
	
	
	function get_haritanggal($tgl=""){
		
		$tgl=date_create($tgl);
		$tgl =  date_format($tgl,"Y-n-d");
		$daftar_hari = array(
							 'Sunday' => 'Minggu',
							 'Monday' => 'Senin',
							 'Tuesday' => 'Selasa',
							 'Wednesday' => 'Rabu',
							 'Thursday' => 'Kamis',
							 'Friday' => 'Jumat',
							 'Saturday' => 'Sabtu'
							);
		
		$date = $tgl;
		$namahari = date('l', strtotime($date));
		
				
		$arrbulan = array("","Januari","Februari","Maret","April","Mei","Juni","Juli",
							 "Agustus","September","Oktober","November","Desember");
		$blnind = explode("-", $date);
		
			
		$arrbln = $daftar_hari[$namahari];
		$dates =  $arrbln.', '.$blnind[2].' '.$arrbulan[$blnind[1]].' '.$blnind[0];
		
		return $dates;
	}
	
	function get_aju($jenis=""){
		
		$chk = $this->get_uraian("SELECT JUMLAH FROM tm_nomor WHERE JENIS = 'NOMOR_DEPOSITO' ","JUMLAH");
		$chk = $chk +1;
		$code = sprintf("%03d", $chk);
		
		return $code;
	}
	
	function get_aju_regis($jenis=""){
		
		$chk = $this->get_uraian("SELECT JUMLAH FROM tm_nomor WHERE JENIS_NOMOR = 'REGIS_KENDARAAN' ","JUMLAH");
		$chk = $chk +1;
		$code = sprintf("%03d", $chk);
		$code = 'REG'.date("Y").$code;
		return $code;
	}
	
	function set_aju(){
			
			$this->db->simple_query("UPDATE tm_nomor SET JUMLAH = JUMLAH + 1 WHERE JENIS = 'NOMOR_DEPOSITO'");
			return true;
		}
	
	function get_prosesbox(&$status = "", $type ="", $role = ""){
	
	
		$query = "SELECT CASE 
			WHEN  A.PROSES_TYPE = '1' THEN 'one'
			WHEN  A.PROSES_TYPE = '2' THEN 'two'
			WHEN  A.PROSES_TYPE = '3' THEN 'three'	
			ELSE 'zero' END AS 'PROSES', A.PROSES_SESUDAH AS SESUDAH, B.REFF_DESC AS URAIAN, 
				  C.REFF_DESC AS STATUS  
						FROM ts_status A
				  LEFT JOIN tb_reff B ON B.REFF_CODE = A.PROSES_TYPE AND B.REFF_TAB = 'PROSES'
				  LEFT JOIN tb_reff C ON C.REFF_CODE = A.PROSES_SESUDAH AND C.REFF_TAB = 'STATUS'
						WHERE A.PROSES_SEBELUM = '$status' AND A.ROLE = '".$this->session->userdata('USER_ROLE')."' 
				  AND A.KLASIFIKASI_ID = '$type'";
		
		#echo $query; die();
		$data = $this->db->query($query);
		if($data->num_rows() > 0){
			$proses = array("", "");
			$arrsts = array("", "");
			foreach($data->result_array() as $row){	
				$arrsts[0] = $row['SESUDAH'];
				$arrsts[1] = $row['STATUS'];
				$proses[1] = $row['URAIAN'];
				if($proses[0]==$row['PROSES']){
					$combobox[$arrsts[0]] = "&nbsp; &nbsp;&nbsp;".$arrsts[1];
				}else{
					$combobox[$row['PROSES']] = $proses[1];
					$combobox[$arrsts[0]] = "&nbsp; &nbsp;&nbsp;".$arrsts[1];
				}
				$proses[0] = $row['PROSES'];
				$arrdis[] = $proses[0];
			}
			
			#$status = $arrdis;
		}
		#print_r($status);
		#print_r($combobox); die();
		return $combobox;
		
	}
	
	function get_combobox($query, $key, $value, $empty = FALSE, &$disable = ""){
			$combobox = array();
			
			#print_r($combobox); die();
			$data = $this->db->query($query);
			#echo $query; die();
			if($empty) $combobox[""] = "&nbsp;";
			if($data->num_rows() > 0){
				$kodedis = "";
				$arrdis = array();
				foreach($data->result_array() as $row){
					if(is_array($disable)){
						if($kodedis==$row[$disable[0]]){
							if(!array_key_exists($row[$key], $combobox)) $combobox[$row[$key]] = str_replace("'", "\'", "&nbsp; &nbsp;&nbsp;".$row[$value]);
						}else{
							if(!array_key_exists($row[$disable[0]], $combobox)) $combobox[$row[$disable[0]]] = $row[$disable[1]];
							if(!array_key_exists($row[$key], $combobox)) $combobox[$row[$key]] = str_replace("'", "\'", "&nbsp; &nbsp;&nbsp;".$row[$value]);
						}
						$kodedis = $row[$disable[0]];
						if(!in_array($kodedis, $arrdis)) $arrdis[] = $kodedis;
					}else{
						$combobox[$row[$key]] = str_replace("'", "\'", $row[$value]);
					}
				}
				$disable = $arrdis;
			}
			
			#print_r($combobox); die();
			return $combobox;
			
			
	}
	
	
	
	

		
}	
?>