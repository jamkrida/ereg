<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Act_model extends CI_Model
{

    function queryListCustomer($param1 = '')
	{
       
		if($param1=="list_pembayaran"){
         $this->db->select("A.REG_NOMOR, A.TANGGAL_PENGAJUAN, A.REG_PERIHAL, A.REG_NAMAFILE, A.REG_TGL_UPLOAD, B.REFF_DESC AS UNIT_KERJA,  CONCAT('Rp. ',A.REG_JUMLAH,',00') AS JUMLAH, A.REG_PEMOHON, (CASE WHEN A.STATUS = '1' THEN A.STATUS WHEN A.STATUS = '2' THEN 'CANCLED' ELSE 'ON PROCESS' END) STATUS, A.REG_PATHFILE")
		 		->join("tb_reff B", "B.REFF_CODE = A.REG_UNIT_KERJA AND B.REFF_TAB = 'DEPARTEMEN'", "LEFT")
				->where("A.REG_JENIS_DOKUMEN = '101'")
		 		->group_by("A.REG_NOMOR")
		 		->from('tx_reg A');
				
		}else if($param1=="list_kasbon"){
			 $this->db->select("A.REG_NOMOR, A.TANGGAL_PENGAJUAN,  A.REG_NAMAFILE, A.REG_TGL_UPLOAD, (CASE WHEN A.STATUS = '1' THEN A.STATUS WHEN A.STATUS = '2' THEN 'CANCLED' ELSE 'ON PROCESS' END) STATUS, A.REG_PATHFILE, A.REG_PERIHAL, B.REFF_DESC AS UNIT_KERJA,  CONCAT('Rp. ',A.REG_JUMLAH,',00') AS JUMLAH, A.REG_PEMOHON")
		 		->join("tb_reff B", "B.REFF_CODE = A.REG_UNIT_KERJA AND B.REFF_TAB = 'DEPARTEMEN'", "LEFT")
				->where("A.REG_JENIS_DOKUMEN = '102'")
		 		->group_by("A.REG_NOMOR")
		 		->from('tx_reg A');
			
		}else if($param1=="list_pelunasan_kasbon"){
			 $this->db->select("A.REG_NOMOR, A.TANGGAL_PENGAJUAN, A.REG_NAMAFILE, A.REG_TGL_UPLOAD, (CASE WHEN A.STATUS = '1' THEN A.STATUS WHEN A.STATUS = '2' THEN 'CANCLED' ELSE 'ON PROCESS' END) STATUS, A.REG_PATHFILE , A.REG_PERIHAL, B.REFF_DESC AS UNIT_KERJA,  CONCAT('Rp. ',A.REG_JUMLAH,',00') AS JUMLAH, A.REG_PEMOHON")
		 		->join("tb_reff B", "B.REFF_CODE = A.REG_UNIT_KERJA AND B.REFF_TAB = 'DEPARTEMEN'", "LEFT")
				->where("A.REG_JENIS_DOKUMEN = '103'")
		 		->group_by("A.REG_NOMOR")
		 		->from('tx_reg A');
			
		}			
	}
    
    function ajaxTable($menu = '', $menuId = '', $param1=''){
		
		$cbs =& get_instance();
		$cbs->load->model("main","main", true);
		
        if($menu == 'list_pembayaran'){

            $column_order_stok 	= array('REG_NOMOR', 'TANGGAL_PENGAJUAN');
			$column_search_stok = array('REG_NOMOR', 'TANGGAL_PENGAJUAN');
			$order_stok 		= array('TANGGAL_PENGAJUAN' => 'DESC');
			
			$this->queryListCustomer($menu);
			
			$this->actmain->search_order($column_order_stok, $column_search_stok, $order_stok);
			
			$this->actmain->paging();
			$get    = $this->db->get();
			
		
			$count  = $get->num_rows();
			$hasil  = $get->result();
				
			$this->queryListCustomer($menu);
			
			$countAll   = $this->db->count_all_results();

			$no 	= 0;
			$tabel  = array();
			foreach ($hasil as $value) {
				$date=date_create($value->REG_TGL_UPLOAD);
				$folder =  date_format($date,"Ymd");
				$folder = 'dat/'.$folder.'/';
				$file = str_replace($folder, '', $value->REG_PATHFILE);
				
				
                $row["REG_NOMOR"] = $value->REG_NOMOR;
                $row["TANGGAL_PENGAJUAN"] = $value->TANGGAL_PENGAJUAN;
				$row["REG_PERIHAL"] = $value->REG_PERIHAL;
				$row["UNIT_KERJA"] = $value->UNIT_KERJA;
				$row["JUMLAH"] = $value->JUMLAH;
				$row["REG_PEMOHON"] = $value->REG_PEMOHON;
				
				$jabatan = $this->session->userdata('USER_JABATAN');
				
				if($value->STATUS=='1'){
					
					$row['STATUS'] =  '<span style="font-size:11px;">status : <a href="'.site_url().'/download/files/'.$value->REG_NOMOR.'/'.$file.'" class="btn btn-xs btn-primary" target="_blank">Uploaded</a><br>Ket : '.$value->REG_NAMAFILE.'</span>';
				}else{
					$row["STATUS"] = $value->STATUS;
				}
				
				$uid = $cbs->main->get_uraian("SELECT USER_CREATE FROM tx_reg WHERE REG_NOMOR = '".$value->REG_NOMOR."'","USER_CREATE");
				#<a href="'.site_url().'/home/dokumen/upload_pembayaran/'.$value->REG_NOMOR.'" ><i class="fas fa-file-upload"></i> Upload</a></span>
				
				if($jabatan =="3.2.1"){
					if($value->STATUS=="1"){
						$row["option"] = '<span style="font-size:11px;"><a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a>';
					}else{
						$row["option"] = '<span style="font-size:11px;"><a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a> || <a href="'.site_url().'/home/dokumen/edit_pembayaran/'.$value->REG_NOMOR.'" ><i class="far fa-edit"></i> Edit</a>';	
					}	
					
				}else if($jabatan=="3.2.3"){
					#<a href="'.site_url().'/home/dokumen/edit_pembayaran/'.$value->REG_NOMOR.'" ><i class="far fa-edit"></i> Edit</a>
					if($value->STATUS=="1"){
							$row["option"] = '<span style="font-size:11px;"><a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a>';
						}else{
							$row["option"] = '<span style="font-size:11px;"><a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a> || <a href="'.site_url().'/home/dokumen/upload_pembayaran/'.$value->REG_NOMOR.'" ><i class="fas fa-file-upload"></i> Upload</a></span>';	
						}
					
				}else{
					if($value->STATUS=='1'){
						$row["option"] = '<span style="font-size:11px;"><a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a>';
					}else if($value->STATUS =='CANCLED'){
						$row["option"] = '-';
					}else{
						
						$row["option"] = '<span style="font-size:11px;"><a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a> || <br><a href="'.site_url().'/home/cancled/pembayaran/'.$value->REG_NOMOR.'" onclick="return confirm(\'Apakah anda yakin akan membatalkan ?\')"> [batal] </a><span style="font-size:11px;">';	
					}	
					
					#if($uid ==$this->session->userdata('USER_ID')){
					#	if($value->STATUS=="1"){
					#		$row["option"] = '<span style="font-size:11px;"><a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a>';
					#	}else{
					#		$row["option"] = '<span style="font-size:11px;"><a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a> || <a href="'.site_url().'/home/dokumen/edit_pembayaran/'.$value->REG_NOMOR.'" ><i class="far fa-edit"></i> Edit</a> || <a href="'.site_url().'/home/dokumen/upload_pembayaran/'.$value->REG_NOMOR.'" ><i class="fas fa-file-upload"></i> Upload</a></span>';	
					#	}
					#}else{	
					#}	
					
				}
				
                

				$tabel[] = $row;
            }
            
			$output = array(
				"draw" 					=> $_POST['draw'],
				"iTotalRecords" 		=> $count,
				"iTotalDisplayRecords" 	=> $countAll,
				"data" 					=> $tabel
			);
			echo json_encode($output);
       
	   }else if($menu == 'list_kasbon'){
		    $column_order_stok 	= array('REG_NOMOR', 'TANGGAL_PENGAJUAN');
			$column_search_stok = array('REG_NOMOR', 'TANGGAL_PENGAJUAN');
			$order_stok 		= array('TANGGAL_PENGAJUAN' => 'DESC');
			
			$this->queryListCustomer($menu);
			
			$this->actmain->search_order($column_order_stok, $column_search_stok, $order_stok);
			
			$this->actmain->paging();
			$get    = $this->db->get();
			
		
			$count  = $get->num_rows();
			$hasil  = $get->result();
				
			$this->queryListCustomer($menu);
			
			$countAll   = $this->db->count_all_results();

			$no 	= 0;
			$tabel  = array();
			foreach ($hasil as $value) {
				$date=date_create($value->REG_TGL_UPLOAD);
				$folder =  date_format($date,"Ymd");
				$folder = 'dat/'.$folder.'/';
				$file = str_replace($folder, '', $value->REG_PATHFILE);
				
				
                $row["REG_NOMOR"] = $value->REG_NOMOR;
                $row["TANGGAL_PENGAJUAN"] = $value->TANGGAL_PENGAJUAN;
				$row["REG_PERIHAL"] = $value->REG_PERIHAL;
				$row["UNIT_KERJA"] = $value->UNIT_KERJA;
				$row["JUMLAH"] = $value->JUMLAH;
				$row["REG_PEMOHON"] = $value->REG_PEMOHON;
				
				if($value->STATUS=='1'){
					
					$row['STATUS'] =  '<span style="font-size:11px;">status : <a href="'.site_url().'/download/files/'.$value->REG_NOMOR.'/'.$file.'" class="btn btn-xs btn-primary" target="_blank">Uploaded</a><br>Ket : '.$value->REG_NAMAFILE.'</span>';
				}else{
					$row["STATUS"] = $value->STATUS;
				}
				$jabatan = $this->session->userdata('USER_JABATAN');
				
				if($jabatan =="3.2.1"){
					if($value->STATUS=="1"){
						$row["option"] = '<span style="font-size:11px;"><a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a></span>';	
					}else{
						
						$row["option"] = '<span style="font-size:11px;"><a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a> || <a href="'.site_url().'/home/dokumen/edit_kasbon/'.$value->REG_NOMOR.'" ><i class="far fa-edit"></i> Edit</a></span>';	
					}	
				}else if($jabatan=="3.2.3"){
					if($value->STATUS=="1"){
						$row["option"] = '<span style="font-size:11px;"><a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a></span>';	
					}else{
						
						$row["option"] = '<span style="font-size:11px;"><a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a> ||  <a href="'.site_url().'/home/dokumen/upload_pembayaran/'.$value->REG_NOMOR.'" ><i class="fas fa-file-upload"></i> Upload</a></span>';	
					}	
					
				}else{
					#$row["option"] = '<a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a>';
					
					if($value->STATUS=='1'){
						$row["option"] = '<span style="font-size:11px;"><a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a>';
					}else if($value->STATUS =='CANCLED'){
						$row["option"] = '-';
					}else{
						
						$row["option"] = '<span style="font-size:11px;"><a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a> || <br><a href="'.site_url().'/home/cancled/kasbon/'.$value->REG_NOMOR.'" onclick="return confirm(\'Apakah anda yakin akan membatalkan ?\')"> [batal] </a><span style="font-size:11px;">';	
					}	
				}	
				#$uid = $cbs->main->get_uraian("SELECT USER_CREATE FROM tx_reg WHERE REG_NOMOR = '".$value->REG_NOMOR."'","USER_CREATE");
				#if($uid ==$this->session->userdata('USER_ID')){
					
				#	if($value->STATUS=="1"){
				#		$row["option"] = '<a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a>';	
				#	}else{
				#		
				#		$row["option"] = '<a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a> || <a href="'.site_url().'/home/dokumen/edit_kasbon/'.$value->REG_NOMOR.'" ><i class="far fa-edit"></i> Edit</a>';	
				#	}	
					
				#}else{
				#	$row["option"] = '<a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a>';	
				#}	
                

				$tabel[] = $row;
            }
            
			$output = array(
				"draw" 					=> $_POST['draw'],
				"iTotalRecords" 		=> $count,
				"iTotalDisplayRecords" 	=> $countAll,
				"data" 					=> $tabel
			);
			echo json_encode($output);
		}else if($menu == 'list_pelunasan_kasbon'){
		    $column_order_stok 	= array('REG_NOMOR', 'TANGGAL_PENGAJUAN');
			$column_search_stok = array('REG_NOMOR', 'TANGGAL_PENGAJUAN');
			$order_stok 		= array('TANGGAL_PENGAJUAN' => 'DESC');
			
			$this->queryListCustomer($menu);
			
			$this->actmain->search_order($column_order_stok, $column_search_stok, $order_stok);
			
			$this->actmain->paging();
			$get    = $this->db->get();
			
		
			$count  = $get->num_rows();
			$hasil  = $get->result();
				
			$this->queryListCustomer($menu);
			
			$countAll   = $this->db->count_all_results();

			$no 	= 0;
			$tabel  = array();
			foreach ($hasil as $value) {
				$date=date_create($value->REG_TGL_UPLOAD);
				$folder =  date_format($date,"Ymd");
				$folder = 'dat/'.$folder.'/';
				$file = str_replace($folder, '', $value->REG_PATHFILE);
				
                $row["REG_NOMOR"] = $value->REG_NOMOR;
                $row["TANGGAL_PENGAJUAN"] = $value->TANGGAL_PENGAJUAN;
				$row["REG_PERIHAL"] = $value->REG_PERIHAL;
				$row["UNIT_KERJA"] = $value->UNIT_KERJA;
				$row["JUMLAH"] = $value->JUMLAH;
				$row["REG_PEMOHON"] = $value->REG_PEMOHON;
				
				if($value->STATUS=='1'){
					
					$row['STATUS'] =  '<span style="font-size:11px;">status : <a href="'.site_url().'/download/files/'.$value->REG_NOMOR.'/'.$file.'" class="btn btn-xs btn-primary" target="_blank">Uploaded</a><br>Ket : '.$value->REG_NAMAFILE.'</span>';
				}else{
					$row["STATUS"] = $value->STATUS;
				}
				$jabatan = $this->session->userdata('USER_JABATAN');
				$uid = $cbs->main->get_uraian("SELECT USER_CREATE FROM tx_reg WHERE REG_NOMOR = '".$value->REG_NOMOR."'","USER_CREATE");
				
				if($jabatan =="3.2.1"){
					if($value->STATUS=="1"){
						
						$row["option"] = '<span style="font-size:11px;"><a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a></span>';
					}else{
						
						$row["option"] = '<span style="font-size:11px;"><a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a> || <a href="'.site_url().'/home/dokumen/edit_pelunasan_kasbon/'.$value->REG_NOMOR.'" ><i class="far fa-edit"></i> Edit</a></span>';	
					}
				}else if($jabatan =="3.2.3"){
					if($value->STATUS=="1"){
						
						$row["option"] = '<span style="font-size:11px;"><a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a></span>';
					}else{
						
						$row["option"] = '<span style="font-size:11px;"><a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a> ||  <a href="'.site_url().'/home/dokumen/upload_pembayaran/'.$value->REG_NOMOR.'" ><i class="fas fa-file-upload"></i> Upload</a></span>';	
					}
				}else{
					
					#$row["option"] = '<span style="font-size:11px;"><a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a></span>';
					if($value->STATUS=='1'){
						$row["option"] = '<span style="font-size:11px;"><a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a>';
					}else if($value->STATUS =='CANCLED'){
						$row["option"] = '-';
					}else{
						
						$row["option"] = '<span style="font-size:11px;"><a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a> || <br><a href="'.site_url().'/home/cancled/pelunasan_kasbon/'.$value->REG_NOMOR.'" onclick="return confirm(\'Apakah anda yakin akan membatalkan ?\')"> [batal] </a><span style="font-size:11px;">';	
					}
				}		
				
				#if($uid ==$this->session->userdata('USER_ID')){
					
				#	if($value->STATUS=="1"){						
				#		$row["option"] = '<a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a>';
				#	}else{		
				#		$row["option"] = '<a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a> || <a href="'.site_url().'/home/dokumen/edit_pelunasan_kasbon/'.$value->REG_NOMOR.'" ><i class="far fa-edit"></i> Edit</a>';	
				#	}	
				#}else{
				#	$row["option"] = '<a href="'.site_url().'/prints/'.$menu.'/'.$value->REG_NOMOR.'" target="_blank"><i class="fas fa-print"></i> print</a>';
				#}
				
				$tabel[] = $row;
            }
            
			$output = array(
				"draw" 					=> $_POST['draw'],
				"iTotalRecords" 		=> $count,
				"iTotalDisplayRecords" 	=> $countAll,
				"data" 					=> $tabel
			);
			echo json_encode($output);
		}
        
    }


}