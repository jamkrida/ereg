<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class print_act extends CI_Model{

	function pembayaran($aju=""){
		if($this->session->userdata("LOGGED")){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			
			$query = "SELECT A.REG_NOMOR, A.REG_ID, DATE_FORMAT(A.TANGGAL_PENGAJUAN,'%Y-%c-%d') TANGGAL_PENGAJUAN, B.REFF_DESC AS UNIT_KERJA, 
					  A.REG_PEMOHON, A.REG_PENERIMA, A.REG_NOMOR_REKENING, A.REG_PERIHAL, A.REG_JUMLAH, A.REG_CATATAN, A.TTD_STAFF_PEMOHON,
					  C.USER_FULLNAME AS MANAJER_PEMOHON, D.REFF_DESC AS JABATAN_MANAJER_PEMOHON, A.STATUS_TTD_MANAJER_PEMOHON, A.TTD_STAFF_APPROVAL,
					  (CASE WHEN A.STATUS_TTD_MANAJER_APPROVE = 'Plt' THEN CONCAT(A.STATUS_TTD_MANAJER_APPROVE,'. ') WHEN A.STATUS_TTD_MANAJER_APPROVE = 'Plh' THEN CONCAT(A.STATUS_TTD_MANAJER_APPROVE,'. ') ELSE '' END) AS PLT, E.USER_FULLNAME AS MANAJER_APPROVE, A.NAMA_KASIR, F.USER_FULLNAME AS STAFF_KEU,
					  DATE_FORMAT(A.TANGGAL_KASBON,'%Y-%c-%d') AS TANGGAL_KASBON, A.REG_NOMOR_KASBON, 
					  A.REG_PEMAKAIAN, A.REG_PEMAKAIAN_TERBILANG, A.REG_JUMLAH_TERBILANG, A.FILE_UPLOAD, 
					  IFNULL(H.REFF_LONG_DESC,'Staf') AS 'JABATAN_STAF'
							FROM tx_reg A
					  LEFT JOIN tb_reff B ON B.REFF_CODE = A.REG_UNIT_KERJA AND B.REFF_TAB = 'DEPARTEMEN'
					  LEFT JOIN tm_user C ON C.USER_ID = A.TTD_MANAJER_PEMOHON
					  LEFT JOIN tb_reff D ON D.REFF_CODE = A.TTD_JABATAN_MANAJER_PEMOHON AND D.REFF_TAB = 'JABATAN'
					  LEFT JOIN tm_user E ON E.USER_ID = A.TTD_MANAJER_APPROVE
					  LEFT JOIN tm_user F ON F.USER_ID = A.STAFF_KEU
					  LEFT JOIN tm_user G ON G.USER_ID = A.USER_CREATE
					  LEFT JOIN tb_reff H ON H.REFF_CODE = G.USER_JABATAN AND H.REFF_TAB = 'JABATAN'	
							WHERE A.REG_NOMOR = '".$aju."'";
							
			
			$res = $cbs->main->get_result($query);
			if($res){
				foreach($query->result_array() as $row){
										
					$detil = $this->db->query("SELECT A.*, REPLACE(A.REG_DETIL_NOMINAL, '.', '') NM FROM tx_reg_detil A WHERE LENGTH(A.REG_DETIL_INPUT) > 1 AND LENGTH(A.REG_DETIL_NOMINAL) > 1 AND A.REG_ID = '".$row['REG_ID']."'")->result_array();	
					$date = $row['TANGGAL_PENGAJUAN'];
					$jmlklaim = $cbs->main->get_uraian("SELECT FORMAT(SUM(REPLACE(REG_DETIL_NOMINAL,',','')), 2) JML FROM tx_reg_detil WHERE REG_ID = '".$row['REG_ID']."' ","JML");
					$tglkasbon = $cbs->main->get_tanggalind($row['TANGGAL_KASBON']);
					$tglnow = $cbs->main->get_tanggalind($date);
					
					$arrdata = array( 'sess' => $row,
									  'detil' => $detil,
									  'tglind' => $tglnow,
									  'jmlklaim' => $jmlklaim,
									  'tglkasbon' => $tglkasbon
									  
									 );
				}
				
				return $arrdata;
			}	
		
		} #------------CHKLOGGED--------------#
		redirect(base_url());
		exit();
	}
	
	
	function driver($id=""){
			
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			
			$arrid = explode('.', $id);
			
			$user = "SELECT * FROM tm_user WHERE USER_ID = '".$arrid[0]."'";	
			$sess = $this->db->query($user)->row_array();
			
		    $kn = "SELECT * FROM tm_kendaraan WHERE DRIVER_ID = '".$arrid[0]."'";	
			$isi = $this->db->query($kn)->row_array();
			
			$avg = (int)$cbs->main->get_uraian("SELECT ROUND(IFNULL(AVG(NILAI),0)) JML FROM tx_ratting_driver WHERE DRIVER_ID = '".$arrid[0]."' AND NILAI IS NOT NULL","JML");
			
			if($avg==4) $ket ='Sangat Baik';
			else if($avg==3) $ket = 'Baik';
			else if($avg==2) $ket = 'Cukup';
			else if($avg==1) $ket = 'Kurang';
			
			$arrdata = array( 'sess' => $sess,
							  'isi' => $isi,
							  'avg' => $avg,
							  'ket' => $ket	
									 
									 );
			return $arrdata;	
	}
	
	function absen_rapat($id=""){
				
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			
			$query = "SELECT A.*, B.* 
						FROM t_detil_rapat A
					  LEFT JOIN t_rapat B ON B.ID_RAPAT = A.ID_RAPAT
					    WHERE A.ID_RAPAT = '".$id."'";
							
			
			$res = $cbs->main->get_result($query);
			if($res){
				foreach($query->result_array() as $row){
										
					$rows[] = $row;
					$agenda = $cbs->main->get_uraian("SELECT AGENDA FROM t_rapat WHERE ID_RAPAT = '".$id."'","AGENDA");
					$subagenda = $cbs->main->get_uraian("SELECT SUB_AGENDA FROM t_rapat WHERE ID_RAPAT = '".$id."'","SUB_AGENDA");
					$date = $cbs->main->get_uraian("SELECT DATE_FORMAT(TANGGAL, '%Y-%c-%d') TGL FROM t_rapat WHERE ID_RAPAT = '".$id."'","TGL");
					$tglrapat = $cbs->main->get_tanggalind($date);
					
					$arrdata = array( 'sess' => $rows,
									  'agenda' => $agenda,
									  'subagenda' => $subagenda,
									  'tglrapat' => $tglrapat
									 );
									
				}
				
				return $arrdata;
			}	
		
		
		redirect(base_url());
		exit();
	}


	function set_data_absen_user($id="", $tahun="", $bulan=""){
		$cbs =& get_instance();
		$cbs->load->model("main", "main", true);
		
		$query = "SELECT A.ABSEN_NAME, A.ABSEN_TANGGAL, CONCAT(LEFT(A.ABSEN_MASUK,5),' - ', LEFT(A.ABSEN_PULANG,5)) JAM_KERJA, LEFT(A.ABSEN_MASUK,5) AS ABSEN_MASUK, LEFT(A.ABSEN_PULANG,5) AS ABSEN_PULANG, 
				  A.ABSEN_SCAN_MASUK, A.ABSEN_SCAN_PULANG,
				  A.ABSEN_WFH_WAO, A.KETERANGAN, ROUND(TIME_TO_SEC(TIMEDIFF(A.ABSEN_SCAN_MASUK,A.ABSEN_MASUK))/60) MNT, ROUND(TIME_TO_SEC(TIMEDIFF(A.ABSEN_PULANG, A.ABSEN_SCAN_PULANG))/60) PC
					FROM tx_absen A
				  LEFT JOIN tm_user_absen B ON B.ABSEN_ID = A.ABSEN_NO_ID
					WHERE A.ABSEN_NO_ID = '$id' AND MONTH(A.ABSEN_TANGGAL) = '$bulan'  AND A.ABSEN_TAHUN = '$tahun' ORDER BY A.ABSEN_TANGGAL ASC";
		#echo $query; die();
		$ress = $cbs->main->get_result($query);
		if($ress){
			foreach($query->result_array() as $rows){
				
				$dataabsen[] = $rows; 
				$nama = $rows['ABSEN_NAME'];
			}
			
		}
		#print_r($dataabsen); die();
		
		if($bulan =="01") $blnind = 'Januari';
		if($bulan =="02") $blnind = 'Februari';
		if($bulan =="03") $blnind = 'Maret';
		if($bulan =="04") $blnind = 'April';
		if($bulan =="05") $blnind = 'Mei';
		if($bulan =="06") $blnind = 'Juni';
		if($bulan =="07") $blnind = 'Juli';
		if($bulan =="08") $blnind = 'Agustus';
		if($bulan =="09") $blnind = 'September';
		if($bulan =="10") $blnind = 'Oktober';
		if($bulan =="11") $blnind = 'November';
		if($bulan =="12") $blnind = 'Desember';
		
		$arrdata = array(
						 'tahun' => $tahun,
						 'nama' => $nama,
						 'bulan' => $bulan,
						 'blnind' => $blnind,
						 'dataabsen' => $dataabsen,
						 'id' => $id
						 );
		return $arrdata;
		
		
	}


	function set_data_cms($id="", $awal="", $akhir=""){
		$cbs =& get_instance();
		$cbs->load->model("main", "main", true);
		
		$query = "SELECT A.CMS_ID, CONCAT(A.TANGGAL_CMS,' ', A.JAM_CMS) AS TANGGAL, A.DESKRIPSI_CMS AS DESKRIPSI, 
		A.DEBIT_CMS AS DEBIT, A.KREDIT_CMS AS KREDIT, A.SALDO_CMS AS SALDO, 
		A.KETERANGAN_CMS AS KETERANGAN, CONCAT(A.NOREK_CMS,'-', A.BANK_CMS) AS 'NOMOR_REKENING'
		   FROM tx_cms A
		LEFT JOIN tm_user B ON B.USER_ID = A.USER_UPLOAD
		WHERE A.KETERANGAN_CMS LIKE '%".$id."%'
		AND  A.TANGGAL_CMS >= '".$awal."' and A.TANGGAL_CMS <= '".$akhir."' AND A.STATUS IS NULL ORDER BY A.CMS_ID";
		
		$ress = $cbs->main->get_result($query);
		if($ress){
			foreach($query->result_array() as $rows){
				
				$datacms[] = $rows; 
				
			}
			
		}
	
		
		$arrdata = array(
	
						 'datacms' => $datacms,
						 'id' => $id
						 );
		return $arrdata;
		
		
	}
	
	function cetak_rekap_absen($tahun="", $bulan=""){
		if($this->session->userdata("LOGGED")){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);					
		
		$query="SELECT A.ABSEN_ID, CONCAT(A.USER_FULLNAME,' (', (CASE WHEN A.STATUS = '1' THEN 'KT' WHEN A.STATUS = '2' THEN 'CK' WHEN A.STATUS = '3' THEN 'IKWT' ELSE 'MG' END),')') AS USER_FULLNAME, 	
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND (ABSEN_SCAN_MASUK <> '00:00:00' OR ABSEN_SCAN_PULANG <> '00:00:00')) JML_MASUK,
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND ABSEN_SCAN_MASUK = '00:00:00' AND ABSEN_SCAN_PULANG = '00:00:00') JML_TDK_MASUK,
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO') WAO,
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WFH') WFH,
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND ABSEN_SCAN_MASUK > ABSEN_MASUK) JML_TERLAMBAT,
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND ABSEN_SCAN_PULANG < ABSEN_PULANG AND ABSEN_SCAN_PULANG <> '00:00:00') JML_PULANG_CEPAT,
				(SELECT SUM(ROUND(TIME_TO_SEC(TIMEDIFF(ABSEN_SCAN_MASUK,ABSEN_MASUK))/60)) FROM tx_absen WHERE ABSEN_NO_ID = A.ABSEN_ID AND MONTH(ABSEN_TANGGAL) = '$bulan'  AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND ABSEN_SCAN_MASUK > ABSEN_MASUK) MENIT_TERLAMBAT,
				(SELECT SUM(ROUND(TIME_TO_SEC(TIMEDIFF(ABSEN_PULANG, ABSEN_SCAN_PULANG))/60)) FROM tx_absen WHERE ABSEN_NO_ID = A.ABSEN_ID AND MONTH(ABSEN_TANGGAL) = '$bulan'  AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND ABSEN_SCAN_PULANG < ABSEN_PULANG  AND ABSEN_SCAN_PULANG <> '00:00:00') MENIT_PULANG_CEPAT	
					FROM tm_user_absen A WHERE A.ABSEN_ID IS NOT NULL AND A.STATUS IN ('1') ORDER BY USER_ORDER ASC, ABSEN_ID ASC";
				
				
				$ress = $cbs->main->get_result($query);
				if($ress){
					foreach($query->result_array() as $rows){
						
						$dataabsen[] = $rows; 
					}
					
				}
				
				
		$query1="SELECT A.ABSEN_ID, CONCAT(A.USER_FULLNAME,' (', (CASE WHEN A.STATUS = '1' THEN 'KT' WHEN A.STATUS = '2' THEN 'CK' WHEN A.STATUS = '3' THEN 'IKWT' ELSE 'MG' END),')') AS USER_FULLNAME, 	
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND (ABSEN_SCAN_MASUK <> '00:00:00' OR ABSEN_SCAN_PULANG <> '00:00:00')) JML_MASUK,
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND ABSEN_SCAN_MASUK = '00:00:00' AND ABSEN_SCAN_PULANG = '00:00:00') JML_TDK_MASUK,
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO') WAO,
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WFH') WFH,
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND ABSEN_SCAN_MASUK > ABSEN_MASUK) JML_TERLAMBAT,
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND ABSEN_SCAN_PULANG < ABSEN_PULANG AND ABSEN_SCAN_PULANG <> '00:00:00') JML_PULANG_CEPAT,
				(SELECT SUM(ROUND(TIME_TO_SEC(TIMEDIFF(ABSEN_SCAN_MASUK,ABSEN_MASUK))/60)) FROM tx_absen WHERE ABSEN_NO_ID = A.ABSEN_ID AND MONTH(ABSEN_TANGGAL) = '$bulan'  AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND ABSEN_SCAN_MASUK > ABSEN_MASUK) MENIT_TERLAMBAT,
				(SELECT SUM(ROUND(TIME_TO_SEC(TIMEDIFF(ABSEN_PULANG, ABSEN_SCAN_PULANG))/60)) FROM tx_absen WHERE ABSEN_NO_ID = A.ABSEN_ID AND MONTH(ABSEN_TANGGAL) = '$bulan'  AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND ABSEN_SCAN_PULANG < ABSEN_PULANG  AND ABSEN_SCAN_PULANG <> '00:00:00') MENIT_PULANG_CEPAT	
					FROM tm_user_absen A WHERE A.ABSEN_ID IS NOT NULL AND A.STATUS IN ('2') ORDER BY USER_ORDER ASC, ABSEN_ID ASC";	
		
		$ressck = $cbs->main->get_result($query1);
		if($ressck){
			foreach($query1->result_array() as $rowsck){
				
				$dataabsenck[] = $rowsck; 
			}
			
		}
		
		$query2="SELECT A.ABSEN_ID, CONCAT(A.USER_FULLNAME,' (', (CASE WHEN A.STATUS = '1' THEN 'KT' WHEN A.STATUS = '2' THEN 'CK' WHEN A.STATUS = '3' THEN 'IKWT' ELSE 'MG' END),')') AS USER_FULLNAME, 	
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND (ABSEN_SCAN_MASUK <> '00:00:00' OR ABSEN_SCAN_PULANG <> '00:00:00')) JML_MASUK,
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND ABSEN_SCAN_MASUK = '00:00:00' AND ABSEN_SCAN_PULANG = '00:00:00') JML_TDK_MASUK,
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO') WAO,
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WFH') WFH,
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND ABSEN_SCAN_MASUK > ABSEN_MASUK) JML_TERLAMBAT,
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND ABSEN_SCAN_PULANG < ABSEN_PULANG AND ABSEN_SCAN_PULANG <> '00:00:00') JML_PULANG_CEPAT,
				(SELECT SUM(ROUND(TIME_TO_SEC(TIMEDIFF(ABSEN_SCAN_MASUK,ABSEN_MASUK))/60)) FROM tx_absen WHERE ABSEN_NO_ID = A.ABSEN_ID AND MONTH(ABSEN_TANGGAL) = '$bulan'  AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND ABSEN_SCAN_MASUK > ABSEN_MASUK) MENIT_TERLAMBAT,
				(SELECT SUM(ROUND(TIME_TO_SEC(TIMEDIFF(ABSEN_PULANG, ABSEN_SCAN_PULANG))/60)) FROM tx_absen WHERE ABSEN_NO_ID = A.ABSEN_ID AND MONTH(ABSEN_TANGGAL) = '$bulan'  AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND ABSEN_SCAN_PULANG < ABSEN_PULANG  AND ABSEN_SCAN_PULANG <> '00:00:00') MENIT_PULANG_CEPAT	
					FROM tm_user_absen A WHERE A.ABSEN_ID IS NOT NULL AND A.STATUS IN ('3') ORDER BY USER_ORDER ASC, ABSEN_ID ASC";

		$resskk = $cbs->main->get_result($query2);
		if($resskk){
			foreach($query2->result_array() as $rowskk){
				
				$dataabsenkk[] = $rowskk; 
			}
			
		}

		$query3="SELECT A.ABSEN_ID, CONCAT(A.USER_FULLNAME,' (', (CASE WHEN A.STATUS = '1' THEN 'KT' WHEN A.STATUS = '2' THEN 'CK' WHEN A.STATUS = '3' THEN 'IKWT' ELSE 'MG' END),')') AS USER_FULLNAME, 	
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND (ABSEN_SCAN_MASUK <> '00:00:00' OR ABSEN_SCAN_PULANG <> '00:00:00')) JML_MASUK,
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND ABSEN_SCAN_MASUK = '00:00:00' AND ABSEN_SCAN_PULANG = '00:00:00') JML_TDK_MASUK,
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO') WAO,
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WFH') WFH,
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND ABSEN_SCAN_MASUK > ABSEN_MASUK) JML_TERLAMBAT,
				(SELECT COUNT(ABSEN_NO_ID) FROM tx_absen WHERE MONTH(ABSEN_TANGGAL) = '$bulan' AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND ABSEN_SCAN_PULANG < ABSEN_PULANG AND ABSEN_SCAN_PULANG <> '00:00:00') JML_PULANG_CEPAT,
				(SELECT SUM(ROUND(TIME_TO_SEC(TIMEDIFF(ABSEN_SCAN_MASUK,ABSEN_MASUK))/60)) FROM tx_absen WHERE ABSEN_NO_ID = A.ABSEN_ID AND MONTH(ABSEN_TANGGAL) = '$bulan'  AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND ABSEN_SCAN_MASUK > ABSEN_MASUK) MENIT_TERLAMBAT,
				(SELECT SUM(ROUND(TIME_TO_SEC(TIMEDIFF(ABSEN_PULANG, ABSEN_SCAN_PULANG))/60)) FROM tx_absen WHERE ABSEN_NO_ID = A.ABSEN_ID AND MONTH(ABSEN_TANGGAL) = '$bulan'  AND ABSEN_TAHUN = '$tahun' AND ABSEN_NO_ID = A.ABSEN_ID AND ABSEN_WFH_WAO = 'WAO' AND ABSEN_SCAN_PULANG < ABSEN_PULANG  AND ABSEN_SCAN_PULANG <> '00:00:00') MENIT_PULANG_CEPAT	
					FROM tm_user_absen A WHERE A.ABSEN_ID IS NOT NULL AND A.STATUS IN ('4') ORDER BY USER_ORDER ASC, ABSEN_ID ASC";				
		
		$ressmg = $cbs->main->get_result($query3);
		if($ressmg){
			foreach($query3->result_array() as $rowsmg){
				
				$dataabsenmg[] = $rowsmg; 
			}
			
		}	
		
				

				#print_r($dataabsen); die();
				
		if($bulan =="01") $blnind = 'Januari';
		if($bulan =="02") $blnind = 'Februari';
		if($bulan =="03") $blnind = 'Maret';
		if($bulan =="04") $blnind = 'April';
		if($bulan =="05") $blnind = 'Mei';
		if($bulan =="06") $blnind = 'Juni';
		if($bulan =="07") $blnind = 'Juli';
		if($bulan =="08") $blnind = 'Agustus';
		if($bulan =="09") $blnind = 'September';
		if($bulan =="10") $blnind = 'Oktober';
		if($bulan =="11") $blnind = 'November';
		if($bulan =="12") $blnind = 'Desember';
				
				
				#$act = site_url().'/home/registrasi_act/cetak';
				
		$arrdata = array(
						 'tahun' => $tahun,
						 'bulan' => $bulan,
						 'blnind' => $blnind,
						 'dataabsen' => $dataabsen,
						 'dataabsenck' => $dataabsenck,
						 'dataabsenkk' => $dataabsenkk,
						 'dataabsenmg' => $dataabsenmg
						 );
		return $arrdata;
		
		}
		redirect(base_url());
		exit();
	}
	
	function cetak_surat_registrasi($aju=""){
		if($this->session->userdata("LOGGED")){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			
			$arraju = explode(".", $aju);
			$id = $arraju[0];
			
			$query = "SELECT A.REG_ID, A.REG_ID AS 'REGISTRASI ID', B.USER_FULLNAME AS NAMA_PEMOHON, 
					  C.REFF_DESC AS NAMA_DIV ,A.TANGGAL_PERMOHONAN, A.WAKTU_PERMOHONAN, 
							 DATE_FORMAT(A.TANGGAL_PERMOHONAN, '%Y-%c-%d') AS TGLIND, 
							 D.USER_FULLNAME AS 'ATASAN', A.KEPERLUAN, A.TUJUAN, 
							 E.USER_FULLNAME AS NAMA_DRIVER,A.NO_KENDARAAN, A.JENIS_KENDARAAN, 
							 A.JENIS_KENDARAAN_DESC AS KETERANGAN, A.TTD, A.USER_TTD,
							 F.USER_FULLNAME AS ATASAN
									FROM tx_kendaraan A 
							  LEFT JOIN tm_user B ON B.USER_ID = A.NAMA_PEMOHON
							  LEFT JOIN tb_reff C ON C.REFF_CODE = A.UNIT_KERJA AND C.REFF_TAB = 'DEPARTEMEN'
							  LEFT JOIN tm_user D ON D.USER_ID = A.ATASAN_PEMOHON
							  LEFT JOIN tm_user E ON E.USER_ID = A.DRIVER_ID
							  LEFT JOIN tm_user F ON F.USER_ID = A.ATASAN_PEMOHON
									WHERE A.REG_ID = '".$id."'";
							
			
			$res = $cbs->main->get_result($query);
			if($res){
				foreach($query->result_array() as $row){
					
					$tglpermohonan = $cbs->main->get_haritanggal($row['TANGGAL_PERMOHONAN']);
					
					
					
					#$idjabatan = substr($row['USER_JABATAN'],0,2);
					
					#$atasan = $cbs->main->get_uraian("SELECT USER_FULLNAME FROM tm_user WHERE USER_JABATAN = '".$idjabatan."' AND USER_ROLE = '03'","USER_FULLNAME");
					
					
					
					$date = $row['TGLIND'];
					
					
					$tglnow = $cbs->main->get_tanggalind($date);
					
					$arrdata = array( 'sess' => $row,
									  'tglind' => $tglnow,
									  'tglpermohonan' => $tglpermohonan
									  
									 );
				}
				
				return $arrdata;
			}	
		
		} #------------CHKLOGGED--------------#
		redirect(base_url());
		exit();
	}
	

	function cetak_stdr($aju=""){
		#if($this->session->userdata("LOGGED")){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			
			$arraju = explode(".", $aju);
			$id = $arraju[0];
			
			$query = "SELECT A.VENDOR_ID, A.NAMA_VENDOR, A.NIB , 
						 A.ALAMAT_VENDOR AS 'ALAMAT_PERUSAHAAN', A.NAMA_PIMPINAN, A.EMAIL, A.TELP, A.BIDANG_USAHA , A.DATE_CREATE,
						 DATE_FORMAT(A.DATE_APROVE, '%Y-%n-%d') AS DATE_APROVE	
						 FROM tm_vendor A	
					  WHERE A.VENDOR_ID = '".$id."'";
							
			
			$res = $cbs->main->get_result($query);
			if($res){
				foreach($query->result_array() as $row){
					
					
					if($row['DATE_APROVE']=="") $date = date("Y-n-d");
					  else $date = $row['DATE_APROVE'];	
					
					
					$tglnow = $cbs->main->get_tanggalind($date);
					
					$arrdata = array( 'sess' => $row,
									  'tglind' => $tglnow
									  
									 );
				}
				
				return $arrdata;
			}	
		
		#} #------------CHKLOGGED--------------#
		#redirect(base_url());
		#exit();
	}



}
?>