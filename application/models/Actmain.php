<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Actmain extends CI_Model
{

    function paging()
	{
		if (isset($_POST['length']) && $_POST['length'] < 1) {
			$_POST['length'] = '10';
		} else {
			$_POST['length'] = $_POST['length'];
		}
		if (isset($_POST['start']) && $_POST['start'] > 1) {
			$_POST['start'] = $_POST['start'];
		}
		$this->db->limit($_POST['length'], $_POST['start']);
	}

	function search_order($column_order_stok, $column_search_stok, $order_stok)
	{
		$i = 0;
		if ($_POST['search']['value'] != "") {
			foreach ($column_search_stok as $item) {
				if ($_POST['search']['value']) {
					if ($i === 0) {
						$this->db->group_start();
						$this->db->like('lower(' . $item . ')', strtolower($_POST['search']['value']));
					} else {
						$this->db->or_like('lower(' . $item . ')', strtolower($_POST['search']['value']));
					}

					if (count($column_search_stok) - 1 == $i)
						$this->db->group_end();
				}
				$i++;
			}
		}
		if (isset($_POST['order'])) {
			$this->db->order_by($column_order_stok[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($order_stok)) {
			$order = $order_stok;
			$this->db->order_by(key($order), $order[key($order)]);
		}
    }


}