<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_act extends CI_Model{

	function login($uid_="", $pwd_="", $adm=FALSE){
		
			$akc =& get_instance();
			$akc->load->model("main","main", true);
			
			if($adm){
				$pass = $pwd_;
			}else{
				$pass = md5($pwd_);
			}
		
			
			$sql = "SELECT A.USER_ID, A.USER_NAME , A.USER_FULLNAME, A.USER_PASSWORD, A.USER_JABATAN, A.USER_DEPARTEMEN, A.USER_ROLE, 
									  B.REFF_DESC AS 'NAMA_DEPARTEMEN', C.REFF_DESC AS 'NAMA_JABATAN', D.REFF_DESC AS 'NAMA_ROLE', 
									  A.USER_STATUS, A.USER_NIK, A.LAST_UPDATE_PASSWORD
											FROM tm_user A
									  LEFT JOIN tb_reff B ON B.REFF_CODE = A.USER_DEPARTEMEN AND B.REFF_TAB = 'DEPARTEMEN'
									  LEFT JOIN tb_reff C ON C.REFF_CODE = A.USER_JABATAN AND C.REFF_TAB = 'JABATAN'
									  LEFT JOIN tb_reff D ON D.REFF_CODE = A.USER_ROLE AND D.REFF_TAB = 'ROLE'
											WHERE A.USER_NAME = ?";
											
			$data = $this->db->query($sql, array($uid_));							
											
			if($data->num_rows() > 0){
					foreach($data->result_array() as $row){
						$datses['LOGGED'] = true;
						$datses['IP'] = $_SERVER['REMOTE_ADDR'];
						$datses['USER_ID'] = $row['USER_ID'];
						$datses['USER_PASS'] = $pass;
						$datses['USER_NIK'] = $row['USER_NIK'];
						$datses['USER_NAME'] = $uid_;
						$datses['USER_FULLNAME'] = $row['USER_FULLNAME'];
						$datses['USER_PASSWORD'] = $row['USER_PASSWORD'];
						$datses['USER_JABATAN'] = $row['USER_JABATAN'];
						$datses['USER_DEPARTEMEN'] = $row['USER_DEPARTEMEN'];
						$datses['USER_ROLE'] = $row['USER_ROLE'];
						$datses['NAMA_ROLE'] = $row['NAMA_ROLE'];
						$datses['NAMA_JABATAN'] = $row['NAMA_JABATAN'];
						$datses['NAMA_DEPARTEMEN'] = $row['NAMA_DEPARTEMEN'];
						$datses['USER_LOGINDATE'] = date('Y-m-d H:i:s');
						$datses['USER_STATUS'] = $row['USER_STATUS'];
						$datses['LAST_UPDATE_PASSWORD'] = $row['LAST_UPDATE_PASSWORD'];
						
					}
					
					
					
					if($datses['USER_PASSWORD'] == $datses['USER_PASS']){
						if($datses['USER_STATUS']=="1"){												
							#LOG
							#$logdata['LOG_BY'] = $datses['USER_ID'];
							#$logdata['LOG_DATE'] = date('Y-m-d H:i:s');
							#$logdata['LOG_DESC'] = 'Login Aplikasi';
							#$logdata['LOG_IP'] = $_SERVER['REMOTE_ADDR'];
							#$this->db->insert('tx_log', $logdata);			
							#LAST LOGIN
							$this->db->simple_query("UPDATE tm_user SET LAST_LOGIN = '".date('Y-m-d H:i:s')."' WHERE USER_ID = '".$datses['USER_ID']."'");
							#REGISTER SESSION
							$this->session->set_userdata($datses);
							
							return 1;
						}else if($datses['USER_STATUS']=="0"){
							return 2;
						}
							
					}else{
						return 0;
					}
					
			}	
	
	}
		
	
	function set_login($action="", $isajax=""){
		if($this->session->userdata('USER_ROLE')=="01"){
			$akc =& get_instance();
			$akc->load->model("main","main", true);
			
			if($action =="login"){
				$id = $this->input->post('tb_chk');
				
				$arrid = explode('.', $id[0]);
				
				$query = "SELECT USER_NAME, USER_PASSWORD FROM db_registrasi.tm_user WHERE USER_ID = ".$arrid[0];
				
				$res = $akc->main->get_result($query);
				if($res){
					foreach($query->result_array() as $a){
						$data = $a;
					}
				}
				
				$uid = $data['USER_NAME'];
				$pass = $data['USER_PASSWORD'];
			
				$hasil = $this->user_act->login($uid, $pass, TRUE);
				if($hasil==1) return "MSG#Login Berhasil.#".base_url();
				else return "MSG#Login Gagal.";		
				
				
			}
			
		}
		redirect(base_url());
		exit();
	}	
		
	function set_ulasan($action="", $isajax=""){
		if($this->session->userdata('LOGGED')){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			if($action=="setulasan"){	
				$ulasan = $this->input->post('r1');
				$regid = $this->input->post('regid');
				$catatan = $this->input->post('catatan');
				
				
				$data = array('NILAI'=> $ulasan, 
							  'CATATAN' => $catatan, 
							  'LAST_UPDATE' => date("Y-m-d H:i:s")
							  );
				$this->db->where('REG_ID', $regid);
				if($this->db->update('tx_ratting_driver', $data)){
					return "MSG#OK#Simpan Ulasan Berhasil#".site_url().'/home/dokumen/kendaraan';
				}	
			}
			
		}	
	}	
		
	function ubahpassword($action="", $isajax=""){
		if($this->session->userdata('LOGGED')){			
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			if($action=="setpassword"){	

				$old_pass = $this->input->post('old_pass');
				
				$new_pass = $this->input->post('new_pass');
				
				$confirm_pass = $this->input->post('confirm_pass');
				
				
				
				if(md5($old_pass) == md5($new_pass)) return "MSG#ERR#Password lama dan password baru tidak boleh sama";
				if(md5($confirm_pass) != md5($new_pass)) return "MSG#ERR#Konfirmasi password tidak sesuai";
				$this->db->where('USER_ID', $this->session->userdata('USER_ID'));
				$datas = array('USER_PASSWORD' => md5($new_pass), 
							   'LAST_UPDATE_PASSWORD' => date("Y-m-d H:i:s")
							  );
					 
				if($this->db->update('tm_user', $datas)){					
					return "MSG#OK#Ubah Password Berhasil#".site_url().'/home/logout';
				}
				return "MSG#ERR#Ubah Password Gagal...!";
			}else if($action=="ubahprofil"){
				$isi = $cbs->main->post_to_query($this->input->post('USER'));
				
				$this->db->where('USER_ID', $this->session->userdata('USER_ID'));
				if($this->db->update('tm_user', $isi)){					
					return "MSG#OK#Ubah Profile Berhasil#".site_url().'/home/logout';
				}
				return "MSG#ERR#Ubah Profile Gagal...!";
			} #Last-Action
			
		} #Logged
		
	} #Function
	
	
	function get_user($menu= "", $submenu=""){
		$cbs =& get_instance();
		$cbs->load->model("main","main", true);
		if($this->session->userdata('LOGGED')){
			$jabatan = $cbs->main->get_combobox("SELECT REFF_CODE, REFF_DESC FROM tb_reff WHERE REFF_TAB = 'JABATAN'","REFF_CODE","REFF_DESC", TRUE);
			$departemen = $cbs->main->get_combobox("SELECT REFF_CODE, REFF_DESC FROM tb_reff WHERE REFF_TAB = 'DEPARTEMEN'","REFF_CODE","REFF_DESC", TRUE);	
			$role = $cbs->main->get_combobox("SELECT REFF_CODE, REFF_DESC FROM tb_reff WHERE REFF_TAB = 'ROLE'","REFF_CODE","REFF_DESC", TRUE);		
		    $act = site_url().'/home/registrasi_act/saveuser';	
			
			$arrdata = array(
							 'departemen' => $departemen,
							 'jabatan' => $jabatan,
							 'role' => $role,
							 'act' => $act
						 );
						
			return $arrdata;
		}
	}
	
	function get_detil_upload($menu= "", $submenu=""){
		$cbs =& get_instance();
		$cbs->load->model("main","main", true);
		if($this->session->userdata('LOGGED')){
			$arrid = explode('.', $submenu);	
		    #$act = site_url().'/home/registrasi_act/saveuser';	
			$file = "SELECT * FROM tm_vendor WHERE VENDOR_ID = '".$arrid[0]."' ";
			$arrfile = $this->db->query($file)->row_array();
			
			$arrdata = array(
							 'arrfile' => $arrfile,
							 'tgl' => $arrid[1],
							 'id' => $arrid[0]
						 );
						
			return $arrdata;
		}
	}
	
	function list_user($id="", $ispreview=FALSE){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			
			if($this->session->userdata('LOGGED')){
				
				$this->load->library('newtable');
				$this->newtable->columns(array("A.USER_ID","A.USER_NAME", "A.USER_FULLNAME","A.USER_PASSWORD","B.REFF_DESC","C.REFF_DESC","D.REFF_DESC","A.USER_STATUS","A.USER_NIK"));		  
				$this->newtable->hiddens(array('USER_ID', 'USER_PASSWORD','USER_STATUS'));
				$this->newtable->search(array(array('A.USER_NAME', 'USER ID'), array('A.USER_FULLNAME', 'NAMA'), array('B.REFF_DESC', 'NAMA DEPARTEMEN')));
				$this->newtable->action(site_url()."/home/user/list");
				$this->newtable->cidb($this->db);
				$this->newtable->ciuri($this->uri->segment_array());
				$this->newtable->orderby(1);
				$this->newtable->sortby('ASC');
				$this->newtable->keys(array("USER_ID", "USER_STATUS"));
				if($ispreview){
					$this->newtable->rowcount("ALL");
					$this->newtable->show_chk(FALSE);
					$this->newtable->show_search(FALSE);
				}
				$prosesnya = array('Add User' => array('GET', site_url()."/home/user/new", '0'),
								   'Login' => array('POST', site_url().'/home/admin_act/login/ajax', '1'),	
								   'Aktifkan User' => array('POST', site_url().'/home/registrasi_act/set_user_aktif/ajax', 'N'),
								   'Delete' => array('POST', site_url().'/home/registrasi_act/deleteuser/ajax', 'N')
								   );
				$this->newtable->menu($prosesnya);
				
				
				$query= "SELECT A.USER_ID, A.USER_NAME AS 'USER ID' , A.USER_FULLNAME AS NAMA, A.USER_PASSWORD,  
						B.REFF_DESC AS 'NAMA DEPARTEMEN', C.REFF_DESC AS 'NAMA JABATAN', D.REFF_DESC AS 'ROLE', A.USER_STATUS, A.USER_NIK AS NIK,
						(CASE WHEN A.USER_STATUS = '1' THEN '<i class=\"fas fa-check\" style=\"color:green;\"></i> Aktif' ELSE '<i class=\"far fa-times-circle\" style=\"color:red;\"></i> Belum Aktif' END) 'STATUS USER'
							FROM tm_user A
						LEFT JOIN tb_reff B ON B.REFF_CODE = A.USER_DEPARTEMEN AND B.REFF_TAB = 'DEPARTEMEN'
						LEFT JOIN tb_reff C ON C.REFF_CODE = A.USER_JABATAN AND C.REFF_TAB = 'JABATAN'
						LEFT JOIN tb_reff D ON D.REFF_CODE = A.USER_ROLE AND D.REFF_TAB = 'ROLE'";
								
				$judul = "List User";
				$this->newtable->use_ajax(FALSE);
				$this->newtable->js_file('<script type="text/javascript" src="'.base_url().'assets/js/newtable.js"></script>');
				$tabel = $this->newtable->generate($query);
				if($ispreview){
					$tabel = "<script type=\"text/javascript\" src=\"".base_url()."assets/js/newtable.js\"></script>".$tabel;
					$arrdata = $tabel;
				}else{
					$arrdata = array("htmltable" => $tabel,
									"judul" => $judul);
				}
				return $arrdata;
					
			}
		redirect(base_url());
		exit();		
	}
	
	
	function list_user_driver($id="", $ispreview=FALSE){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			
			if($this->session->userdata('LOGGED')){
				
				$this->load->library('newtable');
				$this->newtable->columns(array("A.USER_ID","A.USER_NAME", "A.USER_FULLNAME","A.USER_PASSWORD","B.REFF_DESC","C.REFF_DESC","D.REFF_DESC","A.USER_STATUS","A.USER_NIK"));		  
				$this->newtable->hiddens(array('USER_ID', 'USER_PASSWORD','USER_STATUS'));
				$this->newtable->search(array(array('A.USER_NAME', 'USER ID'), array('A.USER_FULLNAME', 'NAMA'), array('B.REFF_DESC', 'NAMA DEPARTEMEN')));
				$this->newtable->action(site_url()."/home/user/list");
				$this->newtable->cidb($this->db);
				$this->newtable->ciuri($this->uri->segment_array());
				$this->newtable->orderby(1);
				$this->newtable->sortby('ASC');
				$this->newtable->keys(array("USER_ID", "USER_STATUS"));
				if($ispreview){
					$this->newtable->rowcount("ALL");
					$this->newtable->show_chk(FALSE);
					$this->newtable->show_search(FALSE);
				}
				$prosesnya = array(
								   #'Add User' => array('GET', site_url()."/home/user/new", '0'),
								   'Print Ulasan' => array('GETNEW', site_url()."/prints/data_ulasan", 'N','btn-preview.png'),
								   'available driver' => array('POST', site_url().'/home/registrasi_act/set_available/ajax', 'N'),
								   'not available driver' => array('POST', site_url().'/home/registrasi_act/not_set_available/ajax', 'N')
								   );
				$this->newtable->menu($prosesnya);
				
				
				$query= "SELECT A.USER_ID, A.USER_NAME AS 'USER ID' , A.USER_FULLNAME AS NAMA, A.USER_PASSWORD,  
						B.REFF_DESC AS 'NAMA DEPARTEMEN', C.REFF_DESC AS 'NAMA JABATAN', D.REFF_DESC AS 'ROLE', A.USER_STATUS, 
						CONCAT(E.MERK_KENDARAAN ,'<div>', E.NO_POLISI,'</div>') AS MOBIL,
						(CASE 
							WHEN E.STATUS = '0' THEN '<i class=\"fas fa-check\" style=\"color:green;\"></i> available' 
							ELSE '<i class=\"far fa-times-circle\" style=\"color:red;\"></i> not available' 
						END) 'STATUS DRIVER'
							FROM tm_user A
						LEFT JOIN tb_reff B ON B.REFF_CODE = A.USER_DEPARTEMEN AND B.REFF_TAB = 'DEPARTEMEN'
						LEFT JOIN tb_reff C ON C.REFF_CODE = A.USER_JABATAN AND C.REFF_TAB = 'JABATAN'
						LEFT JOIN tb_reff D ON D.REFF_CODE = A.USER_ROLE AND D.REFF_TAB = 'ROLE'
						LEFT JOIN tm_kendaraan E ON E.DRIVER_ID = A.USER_ID
							WHERE E.DRIVER_ID IS NOT NULL";
								
				$judul = "List User";
				$this->newtable->use_ajax(FALSE);
				$this->newtable->js_file('<script type="text/javascript" src="'.base_url().'assets/js/newtable.js"></script>');
				$tabel = $this->newtable->generate($query);
				if($ispreview){
					$tabel = "<script type=\"text/javascript\" src=\"".base_url()."assets/js/newtable.js\"></script>".$tabel;
					$arrdata = $tabel;
				}else{
					$arrdata = array("htmltable" => $tabel,
									"judul" => $judul);
				}
				return $arrdata;
					
			}
		redirect(base_url());
		exit();		
	}
	
	function list_rekanan($id="", $ispreview=FALSE){
			$cbs =& get_instance();
			$cbs->load->model("main","main", true);
			
			if($this->session->userdata('LOGGED')){
				
				$this->load->library('newtable');
				$this->newtable->columns(array("A.VENDOR_ID","CONCAT(A.NAMA_VENDOR,'<div> NIB :', A.NIB, '</div>')", "A.ALAMAT_VENDOR","A.NAMA_PIMPINAN","A.BIDANG_USAHA"));		  
				$this->newtable->hiddens(array('VENDOR_ID', 'DATE_CREATE'));
				$this->newtable->search(array(array('A.NAMA_VENDOR', 'NAMA VENDOR'), array('A.NIB', 'NIB'), array('A.ALAMAT_VENDOR', 'ALAMAT')));
				$this->newtable->action(site_url()."/home/user/listRekanan");
				$this->newtable->cidb($this->db);
				$this->newtable->ciuri($this->uri->segment_array());
				$this->newtable->orderby(1);
				$this->newtable->sortby('ASC');
				$this->newtable->keys(array("VENDOR_ID", "DATE_CREATE"));
				$this->newtable->detail(site_url()."/home/user/detil");
				if($ispreview){
					$this->newtable->rowcount("ALL");
					$this->newtable->show_chk(FALSE);
					$this->newtable->show_search(FALSE);
				}
				
				if($id=="listRekanan"){
					$judul = "List Vendor Baru";
					$prosesnya = array('Print Preview' => array('GETNEW', site_url()."/prints/stdr", 'N','btn-preview.png'),
									   'Setujui' => array('POST', site_url().'/home/registrasi_act/set_aktif/ajax', 'N'),
									   'Tolak' => array('POST', site_url().'/home/registrasi_act/tolakuser/ajax', 'N')
									   );
					$this->newtable->menu($prosesnya);
				
				
					$query= "SELECT A.VENDOR_ID, CONCAT(A.NAMA_VENDOR,'<div> NIB :', A.NIB, '</div><div>Tgl daftar :',A.DATE_CREATE, '</div>') AS 'NAMA PERUSAHAAN', 
							 A.ALAMAT_VENDOR AS 'ALAMAT PERUSAHAAN', CONCAT(A.NAMA_PIMPINAN,'<div>', A.EMAIL,'</div><div>', A.TELP,'</div>') AS 'NAMA PIMPINAN', A.BIDANG_USAHA AS 'BIDANG USAHA', A.DATE_CREATE, 
							 (CASE 
								WHEN A.STATUS IS NULL THEN '<i class=\"far fa-question-circle\" style=\"color:red;\"></i> Belum Aktif'
								WHEN A.STATUS = '1' THEN '<i class=\"fas fa-check\" style=\"color:green;\"></i> Disetujui'
								WHEN A.STATUS = '0' THEN '<i class=\"far fa-times-circle\" style=\"color:red;\"></i> Ditolak' END) STATUS						 	
							 FROM tm_vendor A WHERE A.STATUS IS NULL";
					
				}else if($id=="listSetujuiRekanan"){
					$judul = "List Vendor Disetujui";
					$prosesnya = array('Print Preview' => array('GETNEW', site_url()."/prints/stdr", 'N','btn-preview.png'),
									   'Tolak' => array('POST', site_url().'/home/registrasi_act/tolakuser/ajax', 'N')
									   );
					$this->newtable->menu($prosesnya);
					
					
					$query= "SELECT A.VENDOR_ID, CONCAT(A.NAMA_VENDOR,'<div> NIB :', A.NIB, '</div><div>Tgl daftar :',A.DATE_CREATE, '</div>') AS 'NAMA PERUSAHAAN', 
							 A.ALAMAT_VENDOR AS 'ALAMAT PERUSAHAAN', CONCAT(A.NAMA_PIMPINAN,'<div>', A.EMAIL,'</div><div>', A.TELP,'</div>') AS 'NAMA PIMPINAN', A.BIDANG_USAHA AS 'BIDANG USAHA', A.DATE_CREATE, 
							 (CASE 
								WHEN A.STATUS IS NULL THEN '<i class=\"far fa-question-circle\" style=\"color:red;\"></i> Belum Aktif'
								WHEN A.STATUS = '1' THEN '<i class=\"fas fa-check\" style=\"color:green;\"></i> Disetujui'
								WHEN A.STATUS = '0' THEN '<i class=\"far fa-times-circle\" style=\"color:red;\"></i> Ditolak' END) STATUS						 	
							 FROM tm_vendor A WHERE A.STATUS = '1'";
				}else if($id=="listTolak"){
					$judul = "List Vendor Ditolak";
					$prosesnya = array('Print Preview' => array('GETNEW', site_url()."/prints/stdr", 'N','btn-preview.png'),
									   'Setujui' => array('POST', site_url().'/home/registrasi_act/set_aktif/ajax', 'N')
									  );
					$this->newtable->menu($prosesnya);
					
					
					$query= "SELECT A.VENDOR_ID, CONCAT(A.NAMA_VENDOR,'<div> NIB :', A.NIB, '</div><div>Tgl daftar :',A.DATE_CREATE, '</div>') AS 'NAMA PERUSAHAAN', 
							 A.ALAMAT_VENDOR AS 'ALAMAT PERUSAHAAN', CONCAT(A.NAMA_PIMPINAN,'<div>', A.EMAIL,'</div><div>', A.TELP,'</div>') AS 'NAMA PIMPINAN', A.BIDANG_USAHA AS 'BIDANG USAHA', A.DATE_CREATE, 
							 (CASE 
								WHEN A.STATUS IS NULL THEN '<i class=\"far fa-question-circle\" style=\"color:red;\"></i> Belum Aktif'
								WHEN A.STATUS = '1' THEN '<i class=\"fas fa-check\" style=\"color:green;\"></i> Disetujui'
								WHEN A.STATUS = '0' THEN '<i class=\"far fa-times-circle\" style=\"color:red;\"></i> Ditolak' END) STATUS						 	
							 FROM tm_vendor A WHERE A.STATUS = '0'";
				}
				
								
				
				$this->newtable->use_ajax(FALSE);
				$this->newtable->js_file('<script type="text/javascript" src="'.base_url().'assets/js/newtable.js"></script>');
				$tabel = $this->newtable->generate($query);
				if($ispreview){
					$tabel = "<script type=\"text/javascript\" src=\"".base_url()."assets/js/newtable.js\"></script>".$tabel;
					$arrdata = $tabel;
				}else{
					$arrdata = array("htmltable" => $tabel,
									"judul" => $judul);
				}
				return $arrdata;
					
			}
		redirect(base_url());
		exit();		
	}

		
}	
?>