var showtime = true;
var unload = 0;
var theurl;
var klik = false;

function fctrim(str){
	str = str.replace(/^\s+/, '');
	for(var i = str.length - 1; i >= 0; i--){
		if(/\S/.test(str.charAt(i))){
			str = str.substring(0, i + 1);
			break;
		}
	}
	return str;
}

$(document).ready(function(){
	$("input, textarea, select").focus(function(){
			if($(this).attr('wajib')=="yes"){
				$(".msgtitle_").fadeOut('slow');
				$(this).removeClass('wajib');
			}
		});
});

function save_post(formid){
	$(".msgtitle_").hide();
	$(".msgtitle_").css('color', 'blue');
	$(".msgtitle_").html('Verifikasi Data..');
	$(".msgtitle_").fadeIn('slow');
	var notvalid = 0;
	if(klik) return false;
	$.each($("input:visible, select:visible, textarea:visible"), function(){
		if($(this).attr('wajib')){
			if($(this).attr('wajib')=="yes" && ($(this).val()=="" || $(this).val()==null)){
				$(this).addClass('wajib');
				notvalid++;
			}
		}
	});
	if(notvalid==0 && (formid=='#fpassword_' || formid=='#fnewuser_' || formid=='#fnewpendaftar_')){
		if($('#konfirmasi').val()!=$('#pwd').val()){
			$('#konfirmasi').addClass('wajib');
			notvalid = -1;
		}
	}
	if(notvalid==-1){
		$(".msgtitle_").css('color', 'red');
		$(".msgtitle_").html('Konfirmasi Password Harus Sama');
		$(".msgtitle_").fadeIn('slow');
		return false;
	}else if(notvalid>0){
		$(".msgtitle_").css('color', 'red');
		$(".wajib").css('border-color', 'red');
		//$(".msgtitle_").html('Ada ' + notvalid + ' Kolom Yang Harus Diisi');
		$(".msgtitle_").html('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> Ada <strong>' + notvalid + '</strong> Kolom Yang Harus Diisi</div>');
		$(".msgtitle_").fadeIn('slow');
		return false;
	}
	klik = true;
	$.ajax({
		type: 'POST',
		url: $(formid).attr('action') + '/ajax',
		data: $(formid).serialize(),
		success: function(data){
			//alert(data);return false;
			if(data.search("MSG")>=0){
				arrdata = data.split('#');
				if(arrdata[1]=="OK"){
					$(".msgtitle_").css('color', 'green');
					$(".msgtitle_").html(arrdata[2]);
				}else{
					$(".msgtitle_").css('color', 'red');
					$(".msgtitle_").html(arrdata[2]);
				}
				if(arrdata.length>3){
					if(data.search("ALERT")>=0){
						alert(arrdata[5]);
					}
					setTimeout(function(){location.href = arrdata[3];}, 2000);
					return false;
				}
			}else{
				$(".msgtitle_").css('color', 'red');
				$(".msgtitle_").html('Proses Gagal.');
			}
			klik = false;
		}
	});
	return false;
}


function kembali(url){
	location.href = url;
}

function set_datetimeclock(id) {
    var sekarang = new Date();
    var tanggal = sekarang.getDate();
    var hari = sekarang.getDay();
    if (hari === 0){
        hari = 'Minggu';
    }if (hari === 1){
        hari = 'Senin';
    }if (hari === 2){
        hari = 'Selasa';
    }if (hari === 3){
        hari = 'Rabu';
    }if (hari === 4){
        hari = 'Kamis';
    }if (hari === 5){
        hari = 'Jumat';
    }if (hari === 6){
        hari = 'Sabtu';
    }
    var bulan = sekarang.getMonth();
    if (bulan === 0){
        bulan = 'Januari';
    }if (bulan === 1){
        bulan = 'Februari';
    }if (bulan === 2){
        bulan = 'Maret';
    }if (bulan === 3){
        bulan = 'April';
    }if (bulan === 4){
        bulan = 'Mei';
    }if (bulan === 5){
        bulan = 'Juni';
    }if (bulan === 6){
        bulan = 'Juli';
    }if (bulan === 7){
        bulan = 'Agustus';
    }if (bulan === 8){
        bulan = 'September';
    }if (bulan === 9){
        bulan = 'Oktober';
    }if (bulan === 10){
        bulan = 'November';
    }if (bulan === 11){
        bulan = 'Desember';
    }
    var tahun = sekarang.getFullYear();
    var detik = sekarang.getSeconds();
    if (detik < 10){
        detik = '0' + detik;
    }
    var menit = sekarang.getMinutes();
    if (menit < 10){
        menit = '0' + menit;
    }
    var jam = sekarang.getHours();
    if (jam < 10){
        jam = '0' + jam;
    }
	var showdate = '' + hari + ', ' + tanggal + ' ' + bulan + ' ' + tahun + ' - ' + jam + ':' + menit + ':' + detik + '';

    document.getElementById(id).innerHTML = showdate;
    setTimeout('set_datetimeclock(\'' + id + '\')', 1000);
}

function show_input(status, allowed, url){
	$("#load").html('&nbsp;Loading..');
	var boleh = allowed.search(status);
	//alert(boleh); return false;
	$(".newtrproses").remove();
	if(boleh>=0 && status!=""){
	//alert('1'); return false;
		$.get(url + status, function(hasil){
			if(hasil!=''){
				$("#trproses").after(hasil);
				$("#actproses").val(status);
				$("#load").html('');
				if($("#jml").val()>0 || $("#induk").val()=="NO"){
					if($("#jml").val()>0){
						alert("Data yang Diupload Belum Lengkap");
					}else{
						alert("Pengajuan Induk Belum Terbit");
					}
					$("#trcatatan").hide();
					$("#pros").hide();
				}else{
					$("#trcatatan").show();
					$("#pros").show();
				}
			}
		});
	}else{
	//alert('2'); return false;	
	if(boleh=="0"){
			$(".btp0").hide();
			$(".judulbtp").hide();
			$("#trcatatan").hide();
			$("#pros").hide();
			$("#load").html('');
		}else{
			$("#load").html('');
			$("#actproses").val("");
			$(".newtrproses").remove();
			if($("#jml").val()>0 || $("#induk").val()=="NO"){
				if($("#jml").val()>0){
						alert("Data yang Diupload Belum Lengkap");
					}else{
						alert("Pengajuan Induk Belum Terbit");
					}
				$("#trcatatan").hide();
				$("#pros").hide();
			}else{
				$("#trcatatan").show();
				$("#pros").show();
			}
		}
	}
	return false;
}


function is_int(value){ 
  if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
      return true;
  } else { 
      return false;
  } 
}