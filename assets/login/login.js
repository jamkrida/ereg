/* Delete Unnecessary Script! */
	function login(){
		$(".msgtitle_").hide();
		$(".msgtitle_").css('color', 'blue');
		$(".msgtitle_").fadeIn('slow');
		if($("#uid_").val()==""){
			$(".msgtitle_").css('color', 'red');
			$(".msgtitle_").html('Isi Username Terlebih Dahulu');
			$(".msgtitle_").fadeIn('slow');
			$("#uid_").focus();
		}else if($("#pwd_").val()==""){
			$(".msgtitle_").css('color', 'red');
			$(".msgtitle_").html('Isi Password Terlebih Dahulu');
			$(".msgtitle_").fadeIn('slow');
			$("#pwd_").focus();
		}else{
			$("#ok_").html('Loading...');
			$.ajax({
				type: 'POST',
				url: $('#flogin_').attr('action') + '/ajax',
				data: $('#flogin_').serialize(),
				success: function(data){
					if(data.search("YES")>=0){
						$(".msgtitle_").css('color', 'green');
						$(".msgtitle_").html('<span style="color:black;">Login Berhasil</span>');
						// $(".msgtitle_").fadeIn('slow');
						$(".msgtitle_").show(230);
						location.href = self.location;
					}else{
						if(data.search("NO")>=0){
							$(".msgtitle_").css('color', 'red');
							$(".msgtitle_").html('Username Atau Password Salah');
							$(".msgtitle_").fadeIn('slow');
							$("#ok_").html('Login');
						}
					}
				}
			});
		}
		return false;
	}
	
	function save_post(formid){
		$(".msgtitle_").hide();
		$(".msgtitle_").css('color', 'blue');
		$(".msgtitle_").html('Verifikasi Data..');
		$(".msgtitle_").fadeIn('slow');
		var notvalid = 0;
		$.each($("input:visible, select:visible, textarea:visible"), function(){
			if($(this).attr('wajib')){
				if($(this).attr('wajib')=="yes" && ($(this).val()=="" || $(this).val()==null)){
					$(this).addClass('wajib').css('border','1px solid red');
					notvalid++;
				}
			}
		});
		if(notvalid>0){
			$(".msgtitle_").css('color', 'red');
			$(".msgtitle_").html('Ada ' + notvalid + ' Kolom Yang Harus Diisi');
			$(".msgtitle_").fadeIn('slow');
			return false;
		}
		$.ajax({
			type: 'POST',
			url: $(formid).attr('action') + '/ajax',
			data: $(formid).serialize(),
			success: function(data){
				if(data.search("MSG")>=0){
					var arrdata = data.split('#');
					if(arrdata[1]=="OK"){
						$(".msgtitle_").css('color', 'green');
						$(".msgtitle_").html(arrdata[2]);
						if(formid == '#frm-reg3'){
							alert(arrdata[2]);
						}
					}else{
						$(".msgtitle_").css('color', 'red');
						$(".msgtitle_").html(arrdata[2]);
					}
					if(arrdata.length>3){
						setTimeout(function(){location.href = arrdata[3];}, 2000);
						return false;
					}
				}else{
					$(".msgtitle_").css('color', 'red');
					$(".msgtitle_").html('Proses Gagal.');
				}
			}
		});
		return false;
	}

	$(".num").keydown(function (e) {
		if(e.keyCode == 188){
			$(this).val($(this).val() + '.');
			e.preventDefault();
		}
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
		   (e.keyCode == 65 && e.ctrlKey === true) || 
		   (e.keyCode >= 35 && e.keyCode <= 39)) {
				return;
		}
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
	$(".telp").keydown(function (e) {
		if(e.keyCode == 188){
			$(this).val($(this).val() + '.');
			e.preventDefault();
		}
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
		   (e.keyCode == 65 && e.ctrlKey === true) || 
		   (e.keyCode == 189 ) || 
		   (event.shiftKey && e.keyCode == 57 ) || 
		   (event.shiftKey && e.keyCode == 48 ) || 
		   (e.keyCode >= 35 && e.keyCode <= 39)) {
				return;
		}
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});

	$(".day").focus(function(){
		$(this).mask("99");
	});
	$(".kodepos").focus(function(){
		$(this).mask("99999");
	});
	$("#npwp").focus(function(){
		$(this).mask("99.999.999.9-999.999");
	});
	$("#keycode").focus(function(){
		$(this).mask("999999"); 
	});
		$('.keyup-email').change(function() {
			$('span.error-keyup-7').remove();
			var inputVal = $(this).val();
			var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			if(!emailReg.test(inputVal)) {	
				$(this).after('<span style="color:red" class="error error-keyup-7">Format Email Salah</span>');
			}
		});


var detik = 1;  
function hitung(){
    var to = setTimeout(hitung,1000);
    detik --;
    if(detik < 0){
        clearTimeout(to);
        detik = 1;
        $(".alert").fadeOut("slow");
    }
}
